const path = require('path');

const IS_PRODUCTION = process.env.NODE_ENV === 'production';

module.exports = {
    entry: {
        'root': './src/main/webapp/app/root.jsx',
        'google-signin': './src/main/webapp/public/js/google.js',
        'student-home': './src/main/webapp/public/js/student-home.js',
        'admin-home': { import: './src/main/webapp/public/css/admin-home.scss', 'filename': '[name].bundle.1.0.3.js'},
        'admin-login': { import: ['./src/main/webapp/public/css/admin-login.scss', './src/main/webapp/public/js/admin-login.js'], 'filename': '[name].bundle.1.0.3.js'}
    },
    mode: IS_PRODUCTION ? 'production' : 'development',
    devtool: IS_PRODUCTION ? 'source-map' : 'inline-source-map',
    module: {
        rules: [
            {
                test: /\.js[x]?$/i,
                include: path.resolve(__dirname, 'src/main/webapp/app'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', { targets: "defaults" }],
                            ['@babel/preset-react', { targets: "defaults" }]
                        ],
                        plugins: [
                            '@babel/plugin-transform-runtime',
                            '@babel/plugin-proposal-class-properties',
                            '@babel/plugin-syntax-dynamic-import'
                        ]
                    }
                },
            },
            {
                test: /\.scss$/i,
                use: [
                  "style-loader",
                  "css-loader",
                  "sass-loader",
                ],
            },
        ],
    },
    output: {
        filename: '[name].bundle.1.0.3.js',
        path: path.resolve(__dirname, 'src/main/webapp/public/build'),
        clean: IS_PRODUCTION,
    },
};
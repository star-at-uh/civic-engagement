
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Calendar" %>

<%
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

    session.invalidate();
%>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>UServe</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://www.star.hawaii.edu/cdn/scripts/includes/version/cas/casloginV1.1.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
        <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.js"></script>
        <script src="public/build/admin-login.bundle.1.0.3.js"></script>
        <script type="text/javascript" src="public/build/google-signin.bundle.1.0.3.js"></script>
    </head>

    <body>
        <section id="top-nav-menu" class="ui top fixed borderless menu">
            <div class='left floated'>
                <a href="https://www.star.hawaii.edu/studentinterface" class="item">
                    <img src="https://www.star.hawaii.edu/cdn/images/star-logo/home-page/StarHome-black.svg" alt="star-logo" class="star-logo">
                </a>
            </div>

             <img src="https://star.hawaii.edu/cdn/images/civic-engagement/landing_page/1/USERVE-Logo-white.svg" alt="application-logo" class="application-logo">
        </section>

        <div class="login-subsection">
            <div class="ui container mobile header-container">
                <div class="login-options">
                    <h2 class="login-header">
                        Log in to USERVE
                    </h2>
                    <button class="ui fluid basic button uh-login" id="casEnterLogin">
                        <span>
                            <img class="uh-signin-logo" src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/uhlogo.svg"/>
                        </span>
                        <span class="text">
                            Continue with <span class="bold">UH Login</span>
                        </span>
                    </button>
                    <button class="ui fluid basic button google-login" id="googleEnterLogin">
                        <span>
                            <img class="google-signin-logo" src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/btn_google_light_normal_ios.svg"/>
                        </span>
                        <span class="text">
                            Continue with <span class="bold">Google</span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
            
        <div class="footer-container">
            <div class="mobile home-footer">
                <img class="footer-icon" src="https://www.star.hawaii.edu/cdn/images/star-logo/StarLogo-withtag.svg"/>
                <div class="footer-info">
                    <p>
                        For Help/Online Tutorial about STAR <a href="https://www.star.hawaii.edu/help/#/" target="_blank" rel="noopener noreferrer">click here</a>
                        <br/>
                        Searching thru 50,000 students to create an academic journey in an average of 30 seconds
                        &nbsp;
                        &nbsp;
                        <span class="word-group">
                            &copy; 
                            <%= Calendar.getInstance().get(Calendar.YEAR) %> 
                            STAR
                        </span>
                    </p>
                </div>
            </div>    
        </div>
    </body>
</html>




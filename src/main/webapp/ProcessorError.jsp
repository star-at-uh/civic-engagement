<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core"    %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"     %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"     %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isErrorPage="true" import="java.util.*, java.lang.StringBuilder, java.lang.Exception, java.io.PrintWriter, java.io.StringWriter, org.slf4j.Logger, org.slf4j.LoggerFactory, com.google.gson.*" %>
<%
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setHeader("X-UA-Compatible","IE=7"); //IE9 compatiblity
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<%
    exception = (Exception) request.getAttribute("javax.servlet.error.exception");

    if (exception != null) {
        exception.printStackTrace();

        try {
            Logger logger = LoggerFactory.getLogger(this.getClass());

            StringWriter writer = new StringWriter();
            PrintWriter printer = new PrintWriter(writer);
            printer.append("\n\n");
            exception.printStackTrace(printer); 
            String stacktrace = writer.toString();
            printer.close();
            writer.close();

            pageContext.setAttribute("stacktrace", stacktrace);

            // put all session variables into an ArrayList for sorting.
            Enumeration<String> names = session.getAttributeNames();
            List<String> list = new ArrayList<String>();
            while (names.hasMoreElements()) {
                list.add(names.nextElement());
            }
            Collections.sort(list);

            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            // concatinate all variables
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.append('\n');
            for (String attribute : list) {
                if ("debugStatements".equalsIgnoreCase(attribute) ||
                    "campuses".equalsIgnoreCase(attribute)) {
                    continue;
                }

                Object property = session.getAttribute(attribute);
                strBuilder.append(attribute);
                strBuilder.append("=>");
                strBuilder.append(gson.toJson(property));
                strBuilder.append('\n');
            }
            
            // set in session
            pageContext.setAttribute("longList", strBuilder.toString());
            pageContext.setAttribute("errorMessage", exception.getLocalizedMessage());
        }
        catch (Exception error) {
            StringWriter writer = new StringWriter();
            PrintWriter printer = new PrintWriter(writer);
            printer.append("\n\n");
            error.printStackTrace(printer); 
            String stacktrace = writer.toString();
            printer.close();

            System.out.println("Caught Error Within ProcessorError.jsp: " + stacktrace);
        }
    }
%>

<%-- Will only send email and return a json if there was actually a crash --%>
<c:if test="${ not empty pageScope.stacktrace }">
    <sql:query var="errorEmail">
        exec PerformanceCheckingDB.dbo.ErrorCaptureAndEmail ?,?,?,?,?, ?,?,?,?
        <sql:param value="${ sessionScope.user.username }"/>
        <sql:param value="${ pageContext.request.remoteAddr }"/>
        <sql:param value="${ pageScope.stacktrace }"/>
        <sql:param value="${ sessionScope.user.affiliation }"/>
        <sql:param value="${ initParam.version }"/>
        <sql:param value="${ initParam.CASApplicationType }"/>
        <sql:param value="${ sessionScope.user.id != -1 ? sessionScope.user.id : 'Not Applicable'}"/>
        <sql:param value="${ pageScope.longList }"/>
        <sql:param value="Error from ${ initParam.CASApplicationName } Site"/>
    </sql:query>

    <%-- Set the message to something more user friendly --%>
    <c:set scope="page" var="errorMessage" value="An unexpected error has happened, please try again at a later time. If this continues, please contact the STAR office at starhelp@hawaii.edu." />

    An unexpected error has happened, please try again at a later time. If this continues, please contact the STAR office at starhelp@hawaii.edu.
</c:if>
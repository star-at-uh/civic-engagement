export default function(session = {
    isLogged: false,
    siteAdmin: false,
    orgAdmin: false,
}, action){
        let newSession = {...session};
        switch(action.type){
            case "LOGOUT_USER":
                newSession = logoutUser();
                break;
            case "SET_PERMISSIONS":
                newSession = {...newSession, ...action.payload};
                break;
            case "SET_SESSION_INFO":
                newSession = {...newSession, ...action.payload};
                break;
            default:
                break;
        }

        return newSession;
}

const logoutUser = () => {
    return { isLogged: false };
}
export default function(messenger=
    // default session state
    {
        isOpen: false
    }
    , action){
        let newState = {};
        switch(action.type){
            case "OPEN_MESSENGER":
                newState = openMessenger();
                break;
            case "CLOSE_MESSENGER":
                newState = closeMessenger();
                break;
            default:
                newState = messenger;
        }

        return newState;
}

const openMessenger = (state) => {
    console.log('previous state: ');
    console.log(state);
    const ml = document.getElementById('message-list');
    if(ml){
        ml.classList.remove('expanded')
        ml.classList.add('contracted')
    }
    return { isOpen: true };
}

const closeMessenger = (state) => {
    console.log('previous state: ');
    console.log(state);
    const ml = document.getElementById('message-list');
    if(ml){
        ml.classList.add('expanded')
        ml.classList.remove('contracted')
    }
    return { isOpen: false };
}
export default function(state=null
    , action){
        switch(action.type){
            case "SET_NUM_OF_UNAUTH_EVENTS":
                return {...state, ...action.payload};
            default:
                return {...state};
        }
}
export default function (state = null
    , action) {
    switch (action.type) {
        case "SET_NUM_UNAUTH_ORGANIZATIONS":
            return {...state, ...action.payload};
        default:
            return state;
    }
}
import { configureStore } from '@reduxjs/toolkit';
import SessionReducer from './session-reducer.js';
import ComboboxReducer from './combobox-reducer.js';
import CreateEventReducer from './createevent-reducer.js';
import ModalReducer from './modal-reducer.js';
import EventsListReducer from './events-list-reducer.js';
import EventDetailsReducer from './event-details-reducer.js';
import OrganizationListReducer from './organization-reducer';
import { ISATSlice } from './isat.js';

const store = configureStore({
    reducer: {
        session: SessionReducer,
        combobox: ComboboxReducer,
        createevent: CreateEventReducer,
        modal: ModalReducer,
        eventsList: EventsListReducer,
        eventDetails: EventDetailsReducer,
        organizationList: OrganizationListReducer,
        isat: ISATSlice.reducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export default store;
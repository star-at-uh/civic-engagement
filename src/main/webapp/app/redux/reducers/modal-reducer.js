export default function(state=null
    , action){
        let newState = 
        {
            isOpen: false,
            message: null,
            forceOpen: false,
            callback: null,
            allowChoice: false,
            confirmText: null,
            header: null,
            // Custom modal params below
            // Duplicating Events Params
            isDuplicating: false,
            eventId: null,
        };
        switch(action.type){
            case "OPEN_MODAL":
                newState.isOpen = true;
                newState.message = action.payload;
                break;
            case "FORCE_CONFIRM":
                newState.forceOpen = true;
                newState.message = action.payload.message;
                newState.callback = action.payload.callback;
                newState.allowChoice = action.payload.allowChoice;
                newState.confirmText = action.payload.confirmText;
                newState.header = action.payload.header;
                break;
            case "OPEN_DUPLICATE_EVENT":
                newState.isDuplicating = true;
                newState.eventId = action.payload.eventId;
                break;
            case "CLOSE_MODAL":
                break;
            default:
                newState = state != null ? state : newState; // Take into account that any action call will be broadcasted to all reducers
                break;
        }
        return newState;
}
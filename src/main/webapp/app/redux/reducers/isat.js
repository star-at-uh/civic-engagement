import { createSlice } from '@reduxjs/toolkit';

export const ISATSlice = createSlice({
    name: 'isat',
    initialState: {
        ISATCountdownDays: 0,
        displayISATWarning: JSON.parse(sessionStorage.getItem('displayISATWarning'))
    },
    reducers: {
        /**
         * Changes the ISAT info of the user
         */
        storeISAT: (state, action) => {
            if (action.payload.countdown != null) {
                state.ISATCountdownDays = action.payload.countdown;
            }
            // ensure that this state can only be flipped to true once per session
            if (state.displayISATWarning == null || !action.payload.displayWarning) {
                state.displayISATWarning = action.payload.displayWarning;
                sessionStorage.setItem('displayISATWarning', action.payload.displayWarning);
            }
        }
    }
});

export const { storeISAT } = ISATSlice.actions;
import { getRequest, postRequest } from '../requests.js'; 

const likeEvent = (data) => {
    return { 
        type: 'LIKE_EVENT',
        then: (callback) => postRequest('../api/events/like', data).then(callback)
    };
};

const dislikeEvent = (data) => {
    return { 
        type: 'DISLIKE_EVENT',
        then: (callback) => postRequest('../api/events/dislike', data).then(callback)
    };
};

export { likeEvent, dislikeEvent };
const openMessenger = (state) => {
    return {
        type: 'OPEN_MESSENGER',
        payload: state
    }
};

const closeMessenger = (state) => {
    return {
        type: 'CLOSE_MESSENGER',
        payload: state
    }
};

export { openMessenger, closeMessenger };
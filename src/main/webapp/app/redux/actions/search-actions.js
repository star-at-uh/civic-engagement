import { getRequest, postRequest } from '../requests.js'; 

const search = (data) => {
    return { 
        type: 'SEARCH_EVENT',
        payload: data,
        then: (callback) => postRequest('../api/events/search', data).then(callback)
    };
};

export { search };
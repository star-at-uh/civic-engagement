import { getRequest, postRequest } from '../requests.js'; 

const getAttendanceTypes = () => {
    return { 
        type: 'GET_ATTENDANCE_TYPES',
        payload: getRequest('../api/combobox/attendanceTypes') 
    };
};

/**
 * Fetches all the organizations the user is allowed to see
 */
const getAllOrganizations = () => {
    return {
        type: 'GET_ALL_ORGANIZATIONS',
        payload: getRequest('../api/combobox/all-organizations') 
    };
};

/**
 * Fetches the organizations the user is an admin in
 */
const getOwnOrganizations = () => {
    return {
        type: 'GET_OWN_ORGANIZATIONS',
        payload: getRequest('../api/combobox/owned-organizations') 
    }
};

const getOrganizationLabels = () => {
    return {
        type: 'GET_ORGANIZATION_LABELS',
        payload: getRequest('../api/combobox/organization-labels') 
    }
};

const getSocialCohortTypes = () => {
    return {
        type: 'GET_SOCIAL_COHORT_TYPES',
        payload: getRequest('../api/combobox/socialCohort') 
    }
};

export { getAttendanceTypes, getAllOrganizations, getOwnOrganizations, getOrganizationLabels, getSocialCohortTypes };
import { getRequest, postRequest, formRequest } from '../requests.js'; 

/**
 * Calls the proc to create a new Userve event
 * @param {*} data 
 */
const createEvent = (data) => {
    return { 
        type: 'CREATE_EVENT',
        payload: formRequest('../api/events/createEvent', data) 
    };
};

/**
 * Calls the proc to update a specified event's information
 * @param {*} data 
 */
const editEvent = (data) => {
    return { 
        type: 'EDIT_EVENT',
        payload: formRequest('../api/events/editEvent', data) 
    };
};

/**
 * Calls a proc to delete a specified event
 * @param {*} data 
 */
const cancelEvent = (data) => {
    return { 
        type: 'CANCEL_EVENT',
        payload: postRequest('../api/events/cancelEvent', data) 
    };
};

/**
 * Calls a proc to duplicate a specified event with a specified date range
 * @param {*} data 
 */
const duplicateEvent = (data) => {
    return { 
        type: 'DUPLICATE_EVENT',
        payload: postRequest('../api/events/duplicateEvent', data) 
    };
};

/**
 * Calls a proc to delete a specified event
 * @param {Object} data an object containing the parameters you want to pass to the http call
 */
const deleteEvent = (data) => {
    return { 
        type: 'DUPLICATE_EVENT',
        payload: postRequest('../api/events/deleteEvent', data) 
    };
};

/**
 * Fetches event types that can be used to label events
 */
const getEventTypes = () => {
    return { 
        type: 'GET_EVENT_TYPES',
        then: (callback) => getRequest('../api/events/getEventTypes').then(callback),
    };
};

export { createEvent, editEvent, cancelEvent, duplicateEvent, deleteEvent, getEventTypes };
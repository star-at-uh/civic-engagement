import { getRequest, postRequest } from '../requests.js'; 

const viewRegisteredEvents = (data) => {
    return { 
        type: 'VIEW_REGISTERED_EVENTS',
        payload: postRequest('../api/events/viewRegisteredEvents', data) 
    };
};

const viewExploreEvents = (data) => {
    return { 
        type: 'VIEW_EXPLORE_EVENTS',
        payload: postRequest('../api/events/viewExploreEvents', data) 
    };
};

const viewClockInEvents = (data) => {
    return { 
        type: 'VIEW_CLOCK_EVENTS',
        payload: postRequest('../api/events/viewClockInEvents', data) 
    };
};

// admin view

/**
 * Grabs past events for a given org
 * @param {*} data an object containing, at minimum, a key called orgId (number)
 */
const viewPastEvents = (data) => {
    return { 
        type: 'VIEW_EVENTS',
        payload: getRequest('../api/events/viewPastEvents', data) 
    };
};

/**
 * Grabs upcoming events for a given org
 * @param {*} data an object containing, at minimum, a key called orgId (number)
 */
const viewUpcomingEvents = (data) => {
    return { 
        type: 'VIEW_EVENTS',
        payload: getRequest('../api/events/viewUpcomingEvents', data) 
    };
};

/**
 * Grabs a list of events marked as authorized
 * @param {*} data 
 */
const viewAuthorizedEvents = (data) => {
    return { 
        type: 'VIEW_AUTH_EVENTS',
        payload: getRequest('../api/events/viewAuthorizedEvents', data) 
    };
};

/**
 * Grabs a list of events marked as rejected
 * @param {*} data
 */
const viewRejectedEvents = (data) => {
    return { 
        type: 'VIEW_UNAUTH_EVENTS',
        payload: getRequest('../api/events/viewRejectedEvents', data) 
    };
};

/**
 * Grabs a list of events marked as unauthorized
 * @param {*} data
 */
const viewUnauthorizedEvents = (data) => {
    return { 
        type: 'VIEW_UNAUTH_EVENTS',
        payload: getRequest('../api/events/viewUnauthorizedEvents', data) 
    };
};

/**
 * Updates an event status as authorized or unauthorized
 * @param {{*}} data an object containing at minimum keys of eventIds (string), authorization status (boolean), authorization comment (string)  
 */
const updateEventAuthorization = (data) => {
    return {
        type: 'UPDATE_EVENT_AUTH',
        payload: postRequest('../api/events/authorizeEvent', data)
    }
};

/**
 * Grabs a list of the currently logged in user's liked events
 * @param {*} data an object containing data to pass to the java function (none required)
 */
const viewFavoriteEvents = (data) => {
    return {
        type: 'VIEW_FAVORITE_EVENTS',
        payload: getRequest('../api/events/viewFavoriteEvents', data)
    };
};

/**
 * Grabs the number of unauthorized events there are
 */
const viewNumOfUnauthorizedEvents = () => {
    return {
        type: 'VIEW_NUMBER_OF_UNAUTH_EVENTS',
        payload: getRequest('../api/events/viewNumUnauthorizedEvents')
    };
};

/**
 * Sets the number of unauthorized events into the container
 * @param {Number} numOfUnauthorizedEvents the number to set as the # of unauthorized events 
 */
const setNumOfUnauthorizedEvents = (numOfUnauthorizedEvents) => {
    return {
        type: 'SET_NUM_OF_UNAUTH_EVENTS',
        payload: {numOfUnauthorizedEvents},
    };
};

export { viewRegisteredEvents, viewExploreEvents, viewPastEvents, viewUpcomingEvents, 
         viewClockInEvents, viewAuthorizedEvents, viewUnauthorizedEvents, updateEventAuthorization, 
         viewRejectedEvents, viewFavoriteEvents, viewNumOfUnauthorizedEvents, setNumOfUnauthorizedEvents };
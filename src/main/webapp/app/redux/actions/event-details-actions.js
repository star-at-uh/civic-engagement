import { getRequest, postRequest } from '../requests.js'; 

const eventDetails = (data) => {
    return { 
        type: 'EVENT_DETAILS',
        payload: data,
        then: (callback) => postRequest('../api/events/eventDetails', data).then(callback)
    };
};

const eventAttendees = (data) => {
    return { 
        type: 'EVENT_ATTENDEES',
        payload: data,
        then: (callback) => postRequest('../api/events/eventAttendees', data).then(callback)
    };
};

const register = (data) => {
    return { 
        type: 'REGISTER_EVENT',
        payload: data,
        then: (callback) => postRequest('../api/events/register', data).then(callback)
    };
};

const cancelRegistration = (data) => {
    return { 
        type: 'CANCEL_EVENT_REGISTRATION',
        payload: data,
        then: (callback) => postRequest('../api/events/cancelRegistration', data).then(callback)
    };
};

const clockButton = (data) => {
    return { 
        type: 'CLOCK',
        payload: data,
        then: (callback) => postRequest('../api/events/clockButton', data).then(callback)
    };
};

const clockManual = (data) => {
    return { 
        type: 'CLOCK_MANUAL',
        payload: data,
        then: (callback) => postRequest('../api/events/clockManual', data).then(callback)
    };
};


export { eventDetails, eventAttendees, register, cancelRegistration, clockButton, clockManual };
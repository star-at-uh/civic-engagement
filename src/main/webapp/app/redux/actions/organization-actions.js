import { getRequest, postRequest, formRequest } from '../requests.js'; 

/**
 * Attempts to get the current logged in user's organizations they are a part of
 * @param {*} data 
 */
const getOwnOrganizations = (data) => (
    {
        type: 'OWN_ORGS',
        payload: data,
        then: (callback) => getRequest('../api/organizations/getOwnOrganizations', data).then(callback)
    }
);

/**
 * Calls the proc to grab an organization's details
 * @param {*} data 
 */
const organizationDetails = (data) => (
    { 
        type: 'ORG_DETAILS',
        payload: data,
        then: (callback) => postRequest('../api/organizations/viewOrganizationDetails', data).then(callback)
    }
);

/**
 * Calls a proc to attempt to create an organization
 * @param {*} data 
 */
const createOrganization = (data) => (
    { 
        type: 'CREATE_ORG',
        payload: data,
        then: (callback) => postRequest('../api/organizations/createOrganization', data).then(callback)
    }
);

/**
 * Calls a proc to update an organization with new information
 * @param {*} data 
 */
const editOrganization = (data) => (
    { 
        type: 'EDIT_ORG',
        payload: data,
        then: (callback) => formRequest('../api/organizations/editOrganization', data).then(callback)
    }
);

/**
 * Calls a proc to attempt to delete an organization
 * @param {*} data 
 */
const deleteOrganization = (data) => (
    {
        type: 'DELETE_ORG',
        payload: data,
        then: (callback) => postRequest('../api/organizations/deleteOrganization', data).then(callback)
    }
);

/**
 * Calls a proc to grab events that have already passed
 * @param {*} data 
 */
const pastEvents = (data) => (
    { 
        type: 'PAST_EVENTS',
        payload: data,
        then: (callback) => postRequest('../api/organizations/viewPastEvents', data).then(callback)
    }
);

/**
 * Calls a proc to grab events that have not passed the current date
 * @param {*} data 
 */
const upcomingEvents = (data) => (
    { 
        type: 'UPCOMING_EVENTS',
        payload: data,
        then: (callback) => postRequest('../api/organizations/viewUpcomingEvents', data).then(callback)
    }
);

/**
 * Calls a proc to grab a list of admins for an organization
 * @param {*} data 
 */
const organizationAdmins = (data) => (
    {
        type: 'ORGANIZATION_ADMINS',
        payload: data,
        then: (callback) => getRequest('../api/organizations/organizationAdmins', data).then(callback)
    }
);

/**
 * Calls a proc to fetch the data of a UH affiliated user
 * @param {*} data an object containing at least a field that contains the user's username
 */
const getUserInfo = (data) => (
    {
        type: 'GET_USER_INFO',
        payload: data,
        then: (callback) => getRequest('../api/organizations/userInfo', data).then(callback)
    }
);

/**
 * Calls a proc to insert the current logged in user as an admin for an organization
 * @param {*} data 
 */
const insertSelfAdmin = (data) => (
    { 
        type: 'ADD_SELF_ADMIN',
        payload: data,
        then: (callback) => postRequest('../api/organizations/newSelfAdmin', data).then(callback)
    }
);

/**
 * Calls a proc to insert a UH affiliated user as an admin for an organization
 * @param {*} data 
 */
const insertUHAdmin = (data) => (
    { 
        type: 'ADD_UH_ADMIN',
        payload: data,
        then: (callback) => postRequest('../api/organizations/newUHAdmin', data).then(callback)
    }
);

/**
 * Calls a proc to insert a non-UH affiliated user as an admin for an organization
 * @param {*} data 
 */
const insertNonUHAdmin = (data) => (
    { 
        type: 'ADD_NON_UH',
        payload: data,
        then: (callback) => postRequest('../api/organizations/newNonUHAdmin', data).then(callback)
    }
);

/**
 * Calls a proc to remove a given uh user from an organization's admin list
 * @param {*} data 
 */
const deleteUHAdmin = (data) => (
    { 
        type: 'DELETE_UH',
        payload: data,
        then: (callback) => postRequest('../api/organizations/removeUHAdmin', data).then(callback)
    }
);

/**
 * Calls a proc to remove a non-uh user from an organization's admin list
 * @param {*} data 
 */
const deleteNonUHAdmin = (data) => (
    { 
        type: 'DELETE_NON_UH',
        payload: data,
        then: (callback) => postRequest('../api/organizations/removeNonUHAdmin', data).then(callback)
    }
);

/**
 * Calls a proc to toggle an organizations state to active or inactive
 * @param {*} data 
 */
const toggleActiveState = (data) => (
    {
        type: 'TOGGLE_ACTIVE_STATE',
        payload: data,
        then: (callback) => postRequest('../api/organizations/toggleActiveState', data).then(callback)
    }
);

/**
 * Attempts to fetch the number of unauthorized organizations
 */
const getNumOfUnauthorizedOrganizations = () => (
    {
        type: 'NUM_UNAUTH_ORGANIZATIONS',
        then: (callback) => getRequest('../api/organizations/getNumOfUnauthorizedOrganizations').then(callback)
    }
);

/**
 * Sets the number of unauthorized organizations into the organization reducer
 * @param {*} numOfUnauthorizedOrganizations the number to set
 */
const setNumOfUnauthorizedOrganizations = (numOfUnauthorizedOrganizations) => {
    return {
        type: 'SET_NUM_UNAUTH_ORGANIZATIONS',
        payload: {numOfUnauthorizedOrganizations},
    };
};

/**
 * Grabs a list of organizations marked as authorized
 */
const viewAuthorizedOrganizations = () => {
    return { 
        type: 'VIEW_AUTH_ORGANIZATIONS',
        payload: getRequest('../api/organizations/viewAuthorizedOrganizations', ) 
    };
};

/**
 * Grabs a list of organizations marked as rejected
 */
const viewRejectedOrganizations = () => {
    return { 
        type: 'VIEW_UNAUTH_ORGANIZATIONS',
        payload: getRequest('../api/organizations/viewRejectedOrganizations', ) 
    };
};

/**
 * Grabs a list of organizations marked as unauthorized
 */
const viewUnauthorizedOrganizations = () => {
    return { 
        type: 'VIEW_UNAUTH_ORGANIZATIONS',
        payload: getRequest('../api/organizations/viewUnauthorizedOrganizations', ) 
    };
};

export { organizationDetails, createOrganization, editOrganization, 
         pastEvents, upcomingEvents, organizationAdmins, getUserInfo, 
         insertUHAdmin, insertNonUHAdmin, deleteNonUHAdmin, deleteUHAdmin,
         insertSelfAdmin, toggleActiveState, deleteOrganization, getOwnOrganizations,
         getNumOfUnauthorizedOrganizations, setNumOfUnauthorizedOrganizations, viewAuthorizedOrganizations,
         viewRejectedOrganizations, viewUnauthorizedOrganizations };
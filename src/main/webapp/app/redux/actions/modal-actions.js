/**
 * Opens up a modal displaying a message to the user
 * @param {string} message the message to be displayed in the modal 
 */
const openModal = (message) => {
    return {
        type: 'OPEN_MODAL',
        payload: message
    }
};

/**
 * Closes the modal
 */
const closeModal = () => {
    return {
        type: 'CLOSE_MODAL',
        payload: null
    }
};

/**
 * Opens up a modal that forces the user to click on the modal's button(s) in order to close it
 * @param {string} message the message to be displayed in the modal
 * @param {function} callback the function to be called after the user selects an option
 * @param {boolean} allowChoice controls whether or not a "cancel" button will show up which will not fire the callback
 * @param {string} confirmText custom text to display as the confirmation button text
 */
const forceConfirm = (message, callback, allowChoice, confirmText, header) => {
    return {
        type: 'FORCE_CONFIRM',
        payload: { message, callback, allowChoice, confirmText, header }
    }
};

/**
 * Opens the modal for duplicating events
 * @param {Number} eventId the id of the event to be duplicated 
 */
const openDuplicationModal = (eventId) =>  {
    return {
        type: 'OPEN_DUPLICATE_EVENT',
        payload: {eventId},
    }
}

export { openModal, closeModal, forceConfirm, openDuplicationModal };
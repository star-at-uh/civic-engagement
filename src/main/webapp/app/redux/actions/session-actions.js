import { getRequest, postRequest } from '../requests.js'; 

/**
 * Used to login the user
 * @param {*} event native event object passed from triggers like onClick 
 */
const loginUser = (event) => {
    sessionStorage.setItem('returnHash', window.location.hash);
    triggerCAS(event);

    return {
        type: 'LOGIN_USER',
        payload: null
    }
};

/**
 * Logs the user out of the current session
 * @param {*} session 
 */
const logoutUser = (session) => {
    return {
        type: 'LOGOUT_USER',
        payload: session
    }
};

/**
 * Gets the logged in user's session info
 */
const getSessionInfo = () => {
    return {
        type: 'GET_SESSION_INFO',
        payload: getRequest('../api/general/addUserSessionInfo')
    }
};

/**
 * Returns the currrent logged in user's email
 */
const getUserEmail = () => {
    return {
        type: 'GET_SESSION_EMAIL',
        payload: getRequest('../api/general/email')
    }
};

/**
 * Sets the user's current site permissions
 * @param {boolean} orgAdmin the user's organization admin permissions
 * @param {boolean} siteAdmin the user's site admin permissions 
 */
const setPermissions = (orgAdmin, siteAdmin) => {
    return {
        type: 'SET_PERMISSIONS',
        payload: {orgAdmin, siteAdmin}
    }
};

/**
 * Sets the given session info into the session reducer
 * @param {Object} sessionInfo contains at least orgAdmin (boolean), siteAdmin (boolean), name (String), id (String) fields 
 */
const setSessionInfo = (sessionInfo) => {
    return {
        type: 'SET_SESSION_INFO',
        payload: {
            orgAdmin: sessionInfo.organizationAdmin,
            siteAdmin: sessionInfo.siteAdmin,
            name: sessionInfo.fullname,
            id: sessionInfo.pidm,
            isLogged: sessionInfo.isLogged
        }
    };
};

export { loginUser, logoutUser, getSessionInfo, getUserEmail, setPermissions, setSessionInfo };
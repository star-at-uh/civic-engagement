import { getRequest, postRequest } from '../requests.js'; 


/**
 * STUDENT SIDE METHODS
 */

/**
 * student my progress view
 * @param {Object} data 
 */
const myProgress = (data) => {
    return { 
        type: 'MY_PROGRESS',
        payload: data,
        then: (callback) => postRequest('../api/serviceHours/myProgress', data).then(callback)
    };
};

/**
 * student goals combobox
 * @param {Object} data 
 */
const comboboxUserGoals = (data) => {
    return { 
        type: 'USER_GOALS',
        payload: data,
        then: (callback) => postRequest('../api/serviceHours/comboboxUserGoals', data).then(callback)
    };
};

/**
 * student set new goal
 * @param {Object} data 
 */
const setNewGoal = (data) => {
    return { 
        type: 'SET_GOAL',
        payload: data,
        then: (callback) => postRequest('../api/serviceHours/setNewGoal', data).then(callback)
    };
};
  
/**
 * student edit goal
 * @param {Object} data 
 */
const editGoal = (data) => {
    return { 
        type: 'EDIT_GOAL',
        payload: data,
        then: (callback) => postRequest('../api/serviceHours/editGoal', data).then(callback)
    };
};
  

/**
 * ADMIN SIDE METHODS
 */
  

/**
* admin: service-hours roster
* @param {Object} data 
*/
const viewServiceHoursRoster = (data) => {
    return { 
        type: 'VIEW_STUDENT_SERVICE_ROSTER',
        payload: data,
        then: (callback) => getRequest('../api/serviceHours/viewServiceHoursRoster', data).then(callback)
    };
};

/**
* admin: single-student progress view
* @param {Object} data 
*/
const viewStudentServiceRecord = (data) => {
   return { 
       type: 'VIEW_STUDENT_SERVICE_RECORD',
       payload: data,
       then: (callback) => postRequest('../api/serviceHours/viewStudentServiceRecord', data).then(callback)
   };
};

/**
* admin: single-student progress view
* @param {Object} data 
*/
const viewStudentGoals = (data) => {
    return { 
        type: 'VIEW_STUDENT_SERVICE_GOALS',
        payload: data,
        then: (callback) => postRequest('../api/serviceHours/studentGoals', data).then(callback)
    };
 };

export { myProgress, comboboxUserGoals, setNewGoal, editGoal, viewServiceHoursRoster, viewStudentServiceRecord, viewStudentGoals };
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { myProgress, comboboxUserGoals, setNewGoal, editGoal, viewServiceHoursRoster, viewStudentServiceRecord, viewStudentGoals } from '../actions/service-hours-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        myProgress, comboboxUserGoals, setNewGoal, editGoal, viewServiceHoursRoster, viewStudentServiceRecord, viewStudentGoals
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
import { connect } from 'react-redux';

function mapStateToProps(state) {
    return {
        session: state.session
    }
}

export default (component) => connect(mapStateToProps)(component)
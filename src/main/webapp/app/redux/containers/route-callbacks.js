import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { closeMessenger, openMessenger } from '../actions/messenger-actions.js';

function mapStateToProps(state) {
    return {
        messenger: state.messenger
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        closeMessenger: closeMessenger,
        openMessenger: openMessenger
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createEvent, editEvent, cancelEvent, duplicateEvent, deleteEvent, getEventTypes } from '../actions/create-event-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        createEvent, editEvent, cancelEvent, duplicateEvent, deleteEvent, getEventTypes
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
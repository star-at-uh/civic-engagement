import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getAttendanceTypes, getAllOrganizations, getOwnOrganizations, getOrganizationLabels, getSocialCohortTypes } from '../actions/combobox-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        getAttendanceTypes,
        getAllOrganizations,
        getOwnOrganizations,
        getOrganizationLabels,
        getSocialCohortTypes
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
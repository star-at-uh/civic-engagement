import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { viewExploreEvents, viewRegisteredEvents, viewPastEvents, viewUpcomingEvents, 
         viewClockInEvents, viewAuthorizedEvents, viewRejectedEvents, viewUnauthorizedEvents, 
         updateEventAuthorization, viewFavoriteEvents, viewNumOfUnauthorizedEvents, setNumOfUnauthorizedEvents } from '../actions/events-list-actions.js';

function mapStateToProps(state) {
    return {
        eventList: state.eventsList
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        viewExploreEvents,
        viewRegisteredEvents,
        viewPastEvents, viewUpcomingEvents,
        viewClockInEvents,
        viewAuthorizedEvents,
        viewRejectedEvents,
        viewUnauthorizedEvents,
        updateEventAuthorization,
        viewFavoriteEvents,
        viewNumOfUnauthorizedEvents,
        setNumOfUnauthorizedEvents
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { openModal, closeModal, forceConfirm, openDuplicationModal } from '../actions/modal-actions.js';

function mapStateToProps(state) {
    return {
        modal: state.modal
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        openModal,
        closeModal,
        forceConfirm,
        openDuplicationModal
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
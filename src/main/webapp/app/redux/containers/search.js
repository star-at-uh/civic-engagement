import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {search} from '../actions/search-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        search
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
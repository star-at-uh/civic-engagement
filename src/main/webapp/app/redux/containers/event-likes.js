import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { likeEvent, dislikeEvent } from '../actions/event-likes-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        likeEvent, dislikeEvent
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
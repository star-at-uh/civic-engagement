import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { organizationDetails, createOrganization, editOrganization, 
         pastEvents, upcomingEvents, organizationAdmins, getUserInfo, 
         insertNonUHAdmin, insertUHAdmin, deleteNonUHAdmin, deleteUHAdmin, 
         insertSelfAdmin, toggleActiveState, deleteOrganization, getOwnOrganizations,
         getNumOfUnauthorizedOrganizations, setNumOfUnauthorizedOrganizations, viewAuthorizedOrganizations,
         viewRejectedOrganizations, viewUnauthorizedOrganizations } from '../actions/organization-actions.js';

function mapStateToProps(state) {
    return {
        organizationList: state.organizationList,
    }
};

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        organizationDetails, createOrganization, editOrganization, 
        pastEvents, upcomingEvents, organizationAdmins, 
        getUserInfo, insertNonUHAdmin, insertUHAdmin, 
        deleteNonUHAdmin, deleteUHAdmin, insertSelfAdmin, 
        toggleActiveState, deleteOrganization, getOwnOrganizations, 
        getNumOfUnauthorizedOrganizations, setNumOfUnauthorizedOrganizations,
        viewAuthorizedOrganizations, viewRejectedOrganizations, viewUnauthorizedOrganizations
    }, dispatch)
};

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component);
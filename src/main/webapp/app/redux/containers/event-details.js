import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { eventDetails, eventAttendees, register, cancelRegistration, clockButton, clockManual } from '../actions/event-details-actions.js';

function mapStateToProps(state) {
    return {
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({
        eventDetails, eventAttendees, register, cancelRegistration, clockButton, clockManual
    }, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loginUser } from '../actions/session-actions.js';

function mapStateToProps(state) {
    return {
        session: state.session
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({loginUser: loginUser}, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
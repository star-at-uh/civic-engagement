import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {loginUser, logoutUser, getSessionInfo, getUserEmail, setPermissions, setSessionInfo} from '../actions/session-actions.js';

function mapStateToProps(state) {
    return {
        session: state.session,
    }
}

function matchDispatchToProps(dispatch){
    return bindActionCreators({loginUser, logoutUser, getSessionInfo, getUserEmail, setPermissions, setSessionInfo}, dispatch)
}

export default (component) => connect(mapStateToProps, matchDispatchToProps)(component)
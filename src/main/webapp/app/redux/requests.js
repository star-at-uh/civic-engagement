import Axios from 'axios';
import { logoutUServe } from '../util/cas-helper';
import IntervalHelper from '../util/interval-helper';

/**
 *  Error handler for http request made by axios
 *
 */
const handleAxiosError = (error) => {
    let message;
    if (error.response && error.response.status == 401) {
        message = 'Your session has expired, please log in again.';

        IntervalHelper.clearAllIntervals();
    }
    else {
        message = 'An unexpected error has occurred. Please try again later.';
    }
    //save error
    let locationUrl = window.location.href;
    if(locationUrl.indexOf('localhost') > -1){
        // only print to console if on dev
        console.log(error.response);
    }
    else {
        // send to db
        let jsonResponse = JSON.stringify(error, null, 2);
        Axios.post('../api/error/log', {
            jsonResponse,
            locationUrl,
        }); // no handling response since we're in error mode already   
    }

    window.reactForceConfirm(message, error?.response.status == 401 ? logoutUServe : null);
}

/**
 * Wrappers for axios to log out user if 401 sent back by JWT auth filter
 * however will log user out on any error
 */

const getRequest = (url, data) => {
    return new Promise((resolve, reject) => {
        Axios.get(url, data).then((response) => {
            // default pass through
            resolve(response);
        }).catch(handleAxiosError);
    });
}

const postRequest = (url, data) => {
    return new Promise((resolve, reject) => {
        Axios.post(url, data).then((response) => {
            // default pass through
            resolve(response);
        }).catch(handleAxiosError);
    });
}

/**
 * Send a post request as an multipart/form-data type
 */
const formRequest = (url, form) => {
    return Axios.post(url, form, {
                    headers: {
                        'Content-Type': 'multipart/form-data'  
                    }
                })
                .then((response) => {
                    // default pass through
                   return response;
                })
                .catch(handleAxiosError);
}

export { getRequest, postRequest, formRequest };

import React from 'react';
import { Grid, Loader } from 'semantic-ui-react';
import _ from 'lodash';
import SearchEventContainer from '../redux/containers/search.js';
import EventPreview from '../components/events-list/EventPreview.jsx';
import EventsListContainer from '../redux/containers/events-list.js';

import '../css/search.scss';
import { sortEvents, markOngoingEvents } from '../util/event-helper.js';
import { getServerTime } from '../util/TimeOptions.js';

class SearchPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { 
            filterOpen: false, 
            query: null, 
            events: [],
            isLoading: true,
         };
        this.handleSearchChange = this.handleSearchChange.bind(this);
    }

    /**
     * Fires when component is loaded initially
     * Fetches a list of upcoming events 
     */
    componentDidMount() {
        if (this.state.events.length < 1) {
            this.handleSearchChange();
        }
    }

    /**
     * Updates related search state and fetches a list of events
     * based on the query
     */
    handleSearchChange(e) {
        const QUERY = e?.target?.value;
        this.setState({isLoading: true}, () => {
            const TIME_PROMISE = getServerTime();
            const SEARCH_PROMISE = this.props.search({ searchKeyword: QUERY ?? "" });

            Promise.all([SEARCH_PROMISE, TIME_PROMISE])
                    .then(values => {
                        sortEvents(values[0].data, 'EventStartDateTime', false, 'date');
                        markOngoingEvents(values[0].data, 'EventStartDateTime', 'EventEndDateTime', values[1].data);
                        
                        this.setState({ events: values[0].data, isLoading: false });
                    });
        });
    }

    /**
     * Main render function
     */
    render() {
        return (
            <div>
                <Grid textAlign='center' className={'search-and-explore'}>
                    <img className="info-image right desktop-home" src="https://star.hawaii.edu/cdn/images/civic-engagement/USERVEPhone-image.png"/>
                    <Grid.Row columns='1'>
                        <Grid.Column>
                            <center>
                                <div id='event-search'>
                                    <div className="ui search">
                                        <div className="ui icon input">
                                            <input 
                                                autoComplete="off" type="text" tabIndex="0" className="prompt"
                                                onChange={_.debounce(this.handleSearchChange, 500, {leading: true})}
                                                placeholder={'Search for an event by name, keyword or category.'}/>
                                            <i aria-hidden="true" className="search icon"></i>
                                        </div>
                                    </div>
                                </div>
                            </center>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
                <div className='eds-layout__body'>
                    {
                        !this.state.isLoading &&
                            (this.state.events.length > 0 ? 
                                this.state.events.map((event) => {
                                                    const EVENT_DATE = new Date(event.EventStartDateTime)
                                                    return (
                                                        <EventPreview data={event}
                                                            id={event.EventID}
                                                            imageUrl={event.EventImageURL}
                                                            dateDay={EVENT_DATE.getDate()}
                                                            dateMo={event.EventStartDateTime.split(' ')[0]}
                                                            name={event.EventName}
                                                            locationName={event.EventLocationName}
                                                            orgname={event.OrganizationName}
                                                            description={event.EventDescription}
                                                            isPast={event.IsPastEvent}
                                                            key={event.EventID}
                                                        />);
                                                    }) 
                                :
                                <div id='search-placeholder'>
                                    <p>No events found.</p>
                                </div>)
                    }
                    {
                        this.state.isLoading &&
                            <Loader className='search-loader' active={true} inline='centered'>Loading Events</Loader>
                    }
                </div>
            </div>
        );
    }
}

export default EventsListContainer(SearchEventContainer(SearchPage));
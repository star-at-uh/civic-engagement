import React from 'react';
import { Button, Container, Icon, Label, Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import ComboBoxContainer from '../redux/containers/combobox.js';
import _ from 'lodash';
import simpleSearch from '@matthewlam.js/json-tf-idf';
import ORG_CATEGORIES from '../util/OrganizationCategories.js';
import sw from 'stopword';

import '../css/organization-list.scss';

class OrganizationsList extends React.Component {
    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            organizations: [],
            orgCategories: [],
            isLoading: false,
            results: [],
            searchText: '',
            showAll: false,
            showRandom: false
        };
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount() {
        const grabOrganizations = this.props.viewOwnOrgs ? this.props.getOwnOrganizations : this.props.getAllOrganizations;

        grabOrganizations().payload
                            .then((response) => {
                                let searchText = sessionStorage.getItem('searchText');
                                let callback = () => this.handleSearchChange({target: {value: searchText}}, true);
                                let filteredResults = response.data;
                                const LAST_LOCATION_ARRAY = window.lastLocation;
                                
                                /**
                                 * The browser pushes the current page onto the top of the history stack, but refreshing clears the stack, so the only item left is the current page
                                 * so if they refresh we want to remember what they are filtering, also since the current page is always on top of the stack, we check the entry behind it
                                 * to see if they came from an organization page, as we also want to remember the user filter when they go back from viewing an org's details
                                 */
                                if (!searchText ||
                                    LAST_LOCATION_ARRAY.length == 0 || 
                                    ((LAST_LOCATION_ARRAY.length === 1 && LAST_LOCATION_ARRAY[LAST_LOCATION_ARRAY.length - 1].pathname !== '/organizations') || 
                                    (LAST_LOCATION_ARRAY.length > 1 && LAST_LOCATION_ARRAY[LAST_LOCATION_ARRAY.length - 2].pathname !== '/organization'))) {
                                        searchText = '';
                                        callback = null;
                                }

                                if (this.props.viewFavorites) {
                                    filteredResults = filteredResults.filter(organization => organization.DateTimeLiked);
                                }

                                this.setState({ organizations: filteredResults, orgCategories: ORG_CATEGORIES, searchText }, callback);
                            });
    }

    /**
     * uses js-search td-idf library
     * @param {*} searchElement
     */
    handleSearchChange(searchElement) {
        const VALUE = searchElement.target.value;

        // Memorize what the user is filtering for
        sessionStorage.setItem('searchText', VALUE);

        this.setState({ isLoading: true, searchText: VALUE }, () => {
                if (this.state.searchText.length < 1) {
                    return this.setState({
                                    isLoading: false, results: [], searchText: '',
                                    showRandom: false, showAll: false
                                });
                } 
                

                const RESULTS = simpleSearch(
                        VALUE.replace(/[^0-9a-z\s]/gi, '').replace(/\s\s+/g, ' '),
                        this.state.organizations
                    );

                this.setState({
                        isLoading: false,
                        results: RESULTS,
                        showRandom: false,
                        showAll: false
                    });
            });
    }


    /**
     * ListItem UI function
     */
    listItem(name, id, description, labels, search, numberOfUpcomingEvents, isOrgAdmin) {
        const IS_ADVISOR = window.appConfig.getAttribute('data') === 'advisor';

        if(search && description && description.length > 0){       
            sw.removeStopwords(search.replace(/[^0-9a-z\s]/gi, '').replace(/\s\s+/g, ' ').split(' '))
                .forEach((word) => {
                    const REG = new RegExp(' '+word, 'ig');
                    description = description.replace(REG, (match, p1, p2, p3, offset, string) => `<strong><mark>${match}</mark></strong>`);
                });
        }
        if ((this.props.viewOwnOrgs && isOrgAdmin) || !this.props.viewOwnOrgs) {
            return (
                <div className='list-item' key={id}>
                    <div className='main-section'>
                        <h3><Link exact='true' to={IS_ADVISOR && isOrgAdmin  ?`/edit-organization?id=${id}` : `/organization?id=${id}`}>{name}</Link></h3>
                        <div className='box'>
                            {/* INLINE STYLE TO CIRCUMVENT REACT BUG REMOVING "-webkit-box-orient" FROM CSS */}
                            <p style={{WebkitBoxOrient: 'vertical'}} dangerouslySetInnerHTML={{__html: description}}></p>
                        </div>
                        <div className='li-label-group'>
                            {/* Duplicate labels can exist for now, so use a combination of org id, label name and index */}
                            {labels && labels.split('|').map((label, index) => <Label key={`${id}${label}${index}`} onClick={()=>this.handleSearchChange({target: {value: label}})} className='org-label'>{label}</Label>)}
                        </div>
                    </div>
                    {
                        (this.props.viewOwnOrgs && isOrgAdmin) &&
                        <div className='admin-options'>
                            <Link exact='true' to={`/create-event?orgId=${id}`} >
                                <Button exact='true' to='/'>
                                    <Icon name='plus'/>
                                    New Event
                                </Button>
                            </Link>
                            {/* For the sake of getting a demo ready URL params will still be used */}
                            <Link exact='true' to={`/events-list?orgId=${id}&name=${name}`} >
                                <Button exact='true' to='/'>
                                    <img src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/calendaricon.svg"/>
                                    View Events
                                </Button>
                            </Link>
                            {
                                numberOfUpcomingEvents != 0 &&
                                <div className='number-of-events-notification'>{numberOfUpcomingEvents}</div>
                            }
                        </div>
                    }
                </div>
            )
        }
        return '';
    }

    /**
     * function to toggle views
     */
    toggleView(random){
        this.setState({showAll: this.state.showRandom != random ? true : !this.state.showAll, showRandom: random});
    }

    /**
     * main render function
     */
    render() {

        // store state
        const { searchText, results } = this.state;

        // decide what to show, if no search show all, otherwise show search result
        let show = searchText.length > 0 ? results : this.state.organizations;

        // if on random
        if(this.state.showRandom) show = _.shuffle(show);

        // if on A-Z
        if(this.state.showAll && !this.state.showRandom) show = _.sortBy(show, (e)=>e.OrganizationName);

        return (
            <div id='my-organizations'>
                <Container>
                    {
                        ((window.appConfig.getAttribute('data') === 'student' || !this.props.viewOwnOrgs) && !this.props.viewFavorites) &&
                        <div className='category-filter-section'>
                            <center>
                                <div className={'bottom-mobile-spaced'}>
                                    <h1 className={'main-title'}>USERVE ORGANIZATIONS</h1>
                                </div>
                                <div id='org-search'>
                                    <div className="ui search">
                                        <div className="ui icon input">
                                            <input autoComplete="off" type="text" tabIndex="0" className="prompt"
                                                value={this.state.searchText}
                                                onChange={_.debounce(this.handleSearchChange.bind(this), 600, {
                                                    leading: true,
                                                })}
                                                placeholder={'search organizations by keyword...'} />
                                            {
                                                this.state.searchText.length > 0 ?
                                                    <Icon name='delete' link onClick={()=>this.handleSearchChange({target: {value: ''}})}/> 
                                                    :
                                                    <i aria-hidden="true" className="search icon"></i>
                                            }
                                        </div>
                                    </div>
                                </div>
                                <div className='org-search-buttons'>
                                    <Button
                                        className='inline'
                                        active={this.state.showAll && !this.state.showRandom}
                                        onClick={this.toggleView.bind(this, false)}>
                                        View Organizations (A-Z)
                                    </Button>
                                    <Button
                                        className='inline'
                                        active={this.state.showAll && this.state.showRandom}
                                        onClick={this.toggleView.bind(this, true)}>
                                        View Organizations (Random)
                                    </Button>
                                </div>
                            </center>
                        </div>
                    }
                    {
                        (this.state.searchText.length > 0 || this.state.showAll || this.props.viewOwnOrgs || this.props.viewFavorites) ?
                            <div className={window.mobileCheck() ? '' : 'ui container'}>
                                {
                                    show.length > 0 ?
                                        <div id='org-list'>
                                            {
                                                !this.state.searchText ? 
                                                    show.map((org) => {
                                                        return this.listItem(org.OrganizationName, org.OrganizationId, org.OrganizationDescription, org.OrganizationLabels, null, org.NumUpcomingEvents, org.IsOrganizationAdmin);
                                                    })
                                                    : 
                                                    show.map((org) => {
                                                        return this.listItem(org.OrganizationName, org.OrganizationId, org.OrganizationDescription, org.OrganizationLabels, this.state.value, org.NumUpcomingEvents, org.IsOrganizationAdmin);
                                                    })
                                            }
                                        </div>
                                        : 
                                        <center>No organizations found</center>
                                }
                            </div>
                            :
                            // categories
                            <center>
                                <div id='org-categories'>
                                    <div className='title'>VIEW ORGANIZATIONS BY CATEGORY</div>
                                    <Grid>
                                        <Grid.Row columns={4}>
                                            {
                                                this.state.orgCategories.map((category) => {
                                                                            return <Grid.Column key={category.name} className='org-category'>
                                                                                <center onClick={()=>this.handleSearchChange({target: {value: category.name}}, true)}>
                                                                                    <img src={category.icon}/>
                                                                                    <p>{category.name}</p>
                                                                                </center>
                                                                            </Grid.Column>
                                                                        })
                                            }
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            </center>
                    }
                </Container>
            </div>
        )
    }
}

export default ComboBoxContainer(OrganizationsList);
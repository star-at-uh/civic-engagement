import React from 'react';
import _ from 'lodash';
import { Modal, Button } from 'semantic-ui-react';
import { getRequest, postRequest } from '../../redux/requests.js'; 

import '../../css/clock-in-out-log.scss';
import '../../css/time-table.scss';

/**
 * Rendering the student progress list/roster page
 */
class ClockInOutLog extends React.Component {

    /**
     * constructor describes initial state
     * and init variables/bind functions
     */
    constructor(props) {
        super(props);

        this.state = {
            roster: [],
            clockStampMap: {},
            displayModel: {}, // tracks multiple models
            triggerDeleteCheck: {} // tracks multiple clock-in/out time
        }

        this.changeModalVisibility = this.changeModalVisibility.bind(this);
        this.triggerDeleteConfirmation = this.triggerDeleteConfirmation.bind(this);
        this.getHoursTimeTable = this.getHoursTimeTable.bind(this);
    }

    /**
     * fires on component load
     */
    componentDidMount() {
        this.getHoursTimeTable();
    }

    /**
     * Recieves a list of students' clock-in/put times + total hours served
     */
    getHoursTimeTable() {
        const { eventId, viewServiceHoursRoster } = this.props;

        // get hours
        viewServiceHoursRoster({
            params: {
                eventId
            }
        })
        .then((response) => {
            if((response.data[0]?.unSortedsRowMap[0]?.Status ?? response.data[1]?.unSortedsRowMap[0]?.Status) == 0){
                this.props.alertUser(response.data[0]?.ErrorMsg ?? response.data[1]?.ErrorMsg);
            }
            else if (response.data[0]?.unSortedsRowMap[0]?.Status == null) {
                this.setState({roster: response.data[0]?.unSortedsRowMap ?? []});
            }
        });

        getRequest('../api/events/getClockedTimes', {
            params: {
                eventId,
                studentId: null
            }
        })
        .then((response) => {
            this.setState({ clockStampMap: _.groupBy(response.data, 'AttendeeUserID') });
        });
    }

    /**
     * Show or hide the clock in/out log of an event attendee.
     * Reset the state
     */
    changeModalVisibility(attendeeUserId, isVisible) {
        if (!isVisible) {
            this.getHoursTimeTable();
        }

        this.setState({
            displayModel: {
                ...this.state.displayModel, 
                [attendeeUserId]: isVisible 
            },
            triggerDeleteCheck: {}
        });
    }

    /**
     * Display or hide the yes/no comfirmation for 
     * deletion of clock-in/out time
     */
     triggerDeleteConfirmation(index, doCheck) {
        this.setState({
            triggerDeleteCheck: {
                ...this.state.triggerDeleteCheck, 
                [index]: doCheck 
            }
        });
    }

    /**
     * Delete the clock-in/out time of the student
     */
    deleteTime(attendeeId, index) {
        const { clockStampMap } = this.state;
        const TIMESTAMP_INFO = clockStampMap[attendeeId][index];
        
        this.setState({triggerDeleteCheck: false});

        postRequest('../api/events/discardClockedTimes',{
            eventID: TIMESTAMP_INFO.EventID ?? this.props.eventId,
            clockedTimeID: TIMESTAMP_INFO.DateTimeID,
            studentId: TIMESTAMP_INFO.AttendeeUserID
        })
        .then((response) => {
            const DATA = response.data[0];

            if (DATA.Status) {
                clockStampMap[attendeeId].splice(index, 1);
                this.setState({clockStampMap});
            }
        });
    }

    /**
     * main render method
     */
    render() {
        const { roster, displayModel, clockStampMap, triggerDeleteCheck } = this.state;

        return (
            <table cellSpacing={0}>
                <thead>
                    <tr>
                        <td>Student Name</td>
                        <td className='hours-column'>Hours Served</td>
                    </tr>
                </thead>
                <tbody>
                    {
                        roster?.length ?
                            roster.sort((studentOne, studentTwo) => studentOne.LastName.localeCompare(studentTwo.LastName))
                                    .map((student) => {
                                        const TABLE_ROW = (
                                            <tr className='pointer' key={`total-hours-${student.UserID}`} onClick={() => this.setState({redirectURL: `/student?id=${student.UserID}&displayN=${student.FirstName}+${student.LastName}`})}>
                                                <td>
                                                    {student.LastName}, {student.FirstName}
                                                </td>
                                                <td className='hours-column'>
                                                    {student.UserServiceHoursTotal}
                                                </td>
                                            </tr>
                                        );

                                        return (
                                            <Modal
                                                trigger={TABLE_ROW}
                                                open={displayModel[student.UserID]}
                                                onOpen={() => this.changeModalVisibility(student.UserID, true)}
                                                key={`modal-clock-${student.UserID}`}
                                                id='clock-in-out-model'
                                            >
                                                <Modal.Header>
                                                    {student.FirstName} {student.LastName}'s Clock-In & Clock-Out Log
                                                </Modal.Header>
                                                <Modal.Content className='time-table'>
                                                    <table cellSpacing={0}>
                                                        <thead>
                                                            <tr>
                                                                <td>Clock In</td>
                                                                <td>Clock Out</td>
                                                                <td>Created by Admin</td>
                                                                <td>Uploaded By</td>
                                                                <td>{/* filler column */}</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                clockStampMap[student.UserID] != null &&
                                                                    clockStampMap[student.UserID].map((entry, index) => (
                                                                        <tr key={ entry.DateTimeClockIn }>
                                                                            <td>{ entry.DateTimeClockIn }</td>
                                                                            <td>{ entry.DateTimeClockOut }</td>
                                                                            <td>{ entry.IsUploadedByAdmin ? 'Yes' : 'No' }</td>
                                                                            <td>{ entry.UploadedBy }</td>
                                                                            {
                                                                                !triggerDeleteCheck[index] ?
                                                                                    <td className='delete-check'>
                                                                                        <i className="trash alternate icon" onClick={() => this.triggerDeleteConfirmation(index, true)}></i>
                                                                                    </td>
                                                                                    :
                                                                                    <td className='delete-check'>
                                                                                        <div>
                                                                                            <b>Are you sure?</b>
                                                                                        </div>
                                                                                        <div>
                                                                                            <Button basic color='red' onClick={() => this.triggerDeleteConfirmation(index, false)}>
                                                                                                No
                                                                                            </Button>
                                                                                            <Button basic color='green' onClick={() => this.deleteTime(student.UserID, index)}>
                                                                                                Yes
                                                                                            </Button>
                                                                                        </div>
                                                                                    </td>
                                                                            }
                                                                        </tr>
                                                                    ))
                                                            }
                                                        </tbody>
                                                    </table>
                                                </Modal.Content>
                                                <Modal.Actions>
                                                    <Button className='close' basic color='black' onClick={() => this.changeModalVisibility(student.UserID, false)}>
                                                        Close
                                                    </Button>
                                                </Modal.Actions>
                                            </Modal>
                                        );
                                    })
                            :
                            <tr>
                                <td>No records exist</td>
                                <td></td>
                            </tr>
                    }
                </tbody>
            </table>
        );
    }
}

export default ClockInOutLog;
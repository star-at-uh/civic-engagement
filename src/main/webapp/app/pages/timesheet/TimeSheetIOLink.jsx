import React from 'react';
import _ from 'lodash';
import { Modal, Button } from 'semantic-ui-react';

import Axios from 'axios';

import '../../css/timesheet-io-link.scss';

/**
 * Provides a pair of links that the user can click on to
 * upload or download the timesheets
 */
class TimeSheetIOLink extends React.Component {
    /**
     * initialize the passed procs and create initial state 
     */
    constructor(props) {
        super(props);

        this.inputFileReference = React.createRef();
        this.state ={
            displayModel: false,
            error: '',
            uploadStatus: -1
        };

        this.tiggerfileUpload = this.tiggerfileUpload.bind(this);
        this.processUpload = this.processUpload.bind(this);
    }

    /**
     * Toggle the visibility of the popup based on the 
     * boolean passed
     */
    toggleUploadModel(isVisible) {
        this.setState({ 
            displayModel: isVisible,
            uploadStatus: -1  
        });
    }

    /**
     * trigger the file input's behavior via React's referance object
     */
    tiggerfileUpload() {
        this.setState({ uploadStatus: -1 });
        this.inputFileReference.current.click();
    }

    /**
     * Upload the PDF to the backend as an base64 string
     */
     processUpload({ target }) {
         const FILE = target.files[0];

         if (FILE) {
            const FORM = new FormData();
    
            FORM.append('file', FILE, FILE.name);
            FORM.append('eventId', this.props.eventId);

            Axios.post('../api/media/uploadTimesheet', FORM, {
                    headers: {
                        // multipart/form-data cannot/should not be set manually so it's being done this way
                        // undefined is used as it would cause the XHR API to set the type to multipart/form-data
                        // and subsequently set the appropriate multi-part boundary
                        'Content-Type': undefined  
                    }
                })
                .then((response) => {
                    const DATA = response.data;

                    if (DATA.status) {
                        this.props.uploadCallback(true);
                    }

                    this.setState({
                        error: DATA.message,
                        uploadStatus: DATA.status
                    });
                })
                .catch(() => {
                    this.setState({
                        error: 'Unexpected Error, please try again at a later time',
                        uploadStatus: 0
                    });
                });
        }
     }

    /**
     * Create the timesheet table 
     */
     render() {
        const { displayModel, uploadStatus, error } = this.state;
        const UPLOAD_TIGGER = (
            <div className='link'>
                <i className='upload icon'></i> 
                Upload the completed PDF Timesheet to USERVE
            </div>
        );

        /**
         * @returns the class name to change the background color of the dialog inner content.
         * this to to serve as a visual aid for the success or failure of the pdf upload
         */
        const getStatusClass = () => {
            if (uploadStatus == 1) {
                return 'upload-ok';
            }
            else if (uploadStatus == 0) {
                return 'upload-error';
            }
        }

        /**
         * @returns either the success or failed message for the pdf upload
         */
        const getStatusMessage = () => {
            if (uploadStatus == 1) {
                return (
                    <div className='upload-message'>
                        Upload Completed
                    </div>
                );
            }
            else if (uploadStatus == 0) {
                return (
                    <div className='upload-message'>
                        {error}
                    </div>
                );
            }
        };

        return (
            <div id='timesheet-io'>
                <div className='link'>
                    <a href='https://www.star.hawaii.edu/cdn/PDFs/userve/MSA-TimeLog-2023-2024.pdf' download target='_blank' rel="noopener noreferrer">
                        <i className='download icon'></i> 
                        Download Fillable PDF Timesheet
                    </a>
                </div>
                <Modal
                    id='timesheet-io'
                    onClose={() => this.toggleUploadModel(false)}
                    onOpen={() => this.toggleUploadModel(true)}
                    open={displayModel}
                    trigger={UPLOAD_TIGGER}
                    size='tiny'
                >
                    <Modal.Header>
                        <i className="large file alternate icon"></i>
                        Upload Completed PDF Timesheet
                    </Modal.Header>
                    <Modal.Content>
                        <Modal.Description>
                            {
                                !this.props.eventId ?
                                    <div>
                                        Please select an event before attempting to upload a PDF
                                    </div>
                                    :
                                    <div className={`io-interface ${getStatusClass()}`}>
                                        <div className='upload-icon'>
                                            <i className='big upload icon'></i>
                                        </div>
                                        <div>
                                            <input type="file" hidden ref={this.inputFileReference} onChange={this.processUpload} />
                                            <Button className='upload-button' color='teal' type='file' onClick={this.tiggerfileUpload}>
                                                Upload PDF
                                            </Button>
                                        </div>
                                        {
                                            getStatusMessage()
                                        }
                                    </div>
                            }
                        </Modal.Description>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button className='close' basic color='black' onClick={() => this.toggleUploadModel(false)}>
                            Close
                        </Button>
                    </Modal.Actions>
                </Modal>
            </div>
        );
     }
}

export default TimeSheetIOLink;
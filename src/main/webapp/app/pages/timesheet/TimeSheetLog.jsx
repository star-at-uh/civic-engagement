import React, { useEffect, useReducer } from 'react';
import _ from 'lodash';
import { Loader, Button, Modal, Form, Icon, Popup, TextArea, Dropdown } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';

import '../../css/timesheet-log.scss';
import { getRequest, postRequest } from '../../redux/requests.js';

/**
 * Creates a new state 
 */
function reducer(state, newState) {
    return {
        ...state,
        ...newState,
    };
}

/**
 * A table that lists the timesheets that the user has uploaded
 */
const TimeSheetLog = (props) => {
    const [state, dispatch] = useReducer(reducer, {
        log: [],
        isLoadingLogs: true,
        isAddingComment: false,
        selectedEntry: null,
        editCommentInput: '',
        sortBy: 'Date (Descending)',
    });

    /**
     * Change/update the state
     */
    const setState = (newState) => dispatch(newState);

    useEffect(() => {
        getTimesheet();
    }, []);

    useEffect(() => {
        if (state.isLoadingLogs) {
            getRequest('../api/media/getAllTimesheet',{
                params: {
                    eventId: props.eventId
                }
            })
            .then((response) => {
                const RECORDS = response.data.filter((timesheet) => !props.eventId || (timesheet.eventId == props.eventId))
                setState({ log: RECORDS, isLoadingLogs: false });
            })
            .catch((error) => {}); // just ignore the error for now
        }
    }, [state.isLoadingLogs]);

    useEffect(() => {
        updateLog();
    }, [props.eventId]);

    useEffect(() => {
        if (props.newTimeSheetUploaded) {
            updateLog();
        }
    }, [props.newTimeSheetUploaded]);

    /**
     * Get the latest list if the timesheets that
     * has been uploaded
     */
    const updateLog = () => {
        getTimesheet();
        if (props.handleNewTimeSheetUploaded) {
            props.handleNewTimeSheetUploaded();
        }
    }

    /**
     * Get the list of timesheet pdfs that the user has uploaded
     */
    const getTimesheet = () => {
        setState({isLoadingLogs: true});
    }

    /**
     * Open the pdf file in a separate tab
     * @param {int} timeSheetKey the id of the timesheet to be downloaded/opened
     */
    const openPDF = (timeSheetKey) => {
        getRequest('../api/media/getPDFData',{
                params: {
                    timeSheetKey
                },
            })
            .then((response) => {
                if (response.data[0].errorMsg) {
                    props.openModal(response.data[0].errorMsg);
                }
                else {
                    const DECODED_STRING = atob(response.data[0].pdfBase64);
                    const BYTES = new Array(DECODED_STRING.length);
            
                    for (let x = 0; x < DECODED_STRING.length; x++) {
                        BYTES[x] = DECODED_STRING.charCodeAt(x);
                    }
            
                    const LINK = document.createElement('a');
                    if (LINK.download !== undefined) {
                        const REF = URL.createObjectURL(new Blob([new Uint8Array(BYTES)], { type: 'application/pdf;base64' }));
                        LINK.setAttribute('href', REF);
                        LINK.setAttribute('download', response.data[0].fileName);
                        document.body.appendChild(LINK);
                        LINK.click();
                    }
                    else {
                        window.open(LINK, '_blank', 'noopener, noreferrer');
                    }
                }
            })
            .catch((error) => {}); // just ignore the error for now
    }

    /**
     * Removes the stored PDF data for the given timesheet key
     * @param {*} timeSheetKey the id for the timesheet
     */
    const deletePDF = (timeSheetKey) => {
        postRequest('../api/media/deletePDFData',{
                timeSheetKey
            })
            .then((response) => {
                if (response.data[0].ErrorMsg) {
                    props.openModal(response.data[0].ErrorMsg);
                }
                else {
                    props.forceConfirm('Timesheet successfully deleted', null, null, null, 'Success!');
                    getTimesheet();
                }
            });
    }

    /**
     * Prompts the user with the choice if they really want to delete the timesheet
     * @param {*} timeSheetKey the id of the timesheet to delete
     */
    const confirmDelete = (timeSheetKey) => {
        const MESSAGE = `Are you sure you want to do this?<br/><br/>Deleted timesheets will not be available for download; however, their records will remain visible.`;
        props.forceConfirm(MESSAGE, () => deletePDF(timeSheetKey), true, null, 'Warning!');
    }

     /**
      * Opens/Closes the edit comment popup while setting information to track the timesheet being edited
      * @param {*} isAddingComment true to show, falsy to hide
      * @param {*} selectedEntry object containing timeSheetKey and timeSheetComment fields
      */
     const toggleEditComment = (isAddingComment, selectedEntry) => {
        setState({isAddingComment, selectedEntry, editCommentInput: selectedEntry?.timeSheetComment ?? ''});
     }

    /**
     * Updates the currently selected timesheet's comment
     */
    const submitComment = () => {
        postRequest('../api/media/updateTimesheetComment',{
                timeSheetKey: state.selectedEntry.timeSheetKey,
                timeSheetComment: state.editCommentInput,
            })
            .then(response => {
                if (!response.data) {
                    props.openModal('An unexpected error has occurred please try again later');
                }
                else if (response.data[0].Status == 1) {
                    toggleEditComment(false, null);
                    props.forceConfirm("Comment successfully updated");
                    getTimesheet();
                }
                else if (response.data[0].Status != 1) {
                    props.openModal(response.data[0].ErrorMessage);
                }
            });
    }

    /**
     * Sorts the uploaded timesheets based on a given field
     * @param {*} type keyword for the field to sort on (Name, Date)
     * @param {*} descending true to sort by descending order, false to sort by ascending order
     */
    const sortEntries = (type, descending) => {
        const ENTRIES = [...state.log];
        let sorter = null;
        switch (type) {
            case 'Name':
                sorter = (entryOne, entryTwo) => (entryOne.firstName > entryTwo.firstName ? 1 : -1) * (descending ? 1 : -1);
                break;
            case 'Date':
                sorter = (entryOne, entryTwo) => dateFns[descending ? 'compareDesc' : 'compareAsc'](entryOne.uploadDate, entryTwo.uploadDate);
            default:
                break;
        }
        ENTRIES.sort(sorter);
        setState({log: ENTRIES, sortBy: `${type} ${descending ? '(Descending)' : '(Ascending)'}`});
    }

    /**
     * Create a timestamp string of the passed-in timesheet's upload date
     */
    const timestamp = (entry) =>  {
        const DATE = dateFns.format(new Date(entry.uploadDate), 'MM/DD/YYYY (h:mm A)');
        const NAME = props.adminView ? `${entry.firstName} ${entry.lastName}` : entry.eventName;
        const FILE_NAME = !entry.timeSheetDeleted ? <a onClick={() => openPDF(entry.timeSheetKey)}>{entry.fileName ? entry.fileName : `${NAME}_${DATE}_${entry.eventName}.pdf`}</a> : <div className='deleted-text'>Uploaded Timesheet was Deleted</div>;

        return props.adminView ? <>{NAME} - {DATE} - {entry.eventName}{FILE_NAME}</> : <>{DATE} - {NAME}{FILE_NAME}</>;
    }

    const IS_STUDENT = window.appConfig.getAttribute('data') == 'student';

    return (
        !state.isLoadingLogs ?
            <div id="timesheet-log">
                {
                    !IS_STUDENT &&
                    <Dropdown item text={`Sort by ${state.sortBy}`}>
                        <Dropdown.Menu>
                            <Dropdown.Item onClick={() => sortEntries("Name", true)}>Name (Descending)</Dropdown.Item>
                            <Dropdown.Item onClick={() => sortEntries("Name", false)}>Name (Ascending)</Dropdown.Item>
                            <Dropdown.Item onClick={() => sortEntries("Date", true)}>Date Uploaded (Descending)</Dropdown.Item>
                            <Dropdown.Item onClick={() => sortEntries("Date", false)}>Date Uploaded (Ascending)</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                }
                {(props.eventId && !props.adminView) && 'Your Timesheets for this Event:'}
                <table cellSpacing={0}>
                    <thead>
                        <tr>
                            <td>PDF Timesheet</td>
                            {
                                !IS_STUDENT &&
                                    <td>Admin Comment</td>
                            }
                            <td className="download-column" align="right">
                                <div>
                                    Download
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            state.log.map((entry) => (
                                <tr key={`${entry.uploadDate}-${entry.eventName}`}>
                                    <td>{timestamp(entry)}</td>
                                    {
                                        !IS_STUDENT &&
                                            <td className='timesheet-comment'>
                                                <div>{entry.timeSheetComment?.length > 0 ? entry.timeSheetComment : 'No Comment Entered'}</div>
                                                <div>
                                                    <Popup 
                                                        trigger={<Icon name='edit' onClick={() => toggleEditComment(true, entry)}/>}
                                                        content='Edit admin timesheet comment' basic/>
                                                </div>
                                            </td>
                                    }
                                    <td className={`download-column ${!IS_STUDENT ? 'timesheet-advisor' : ''} context`} align="right">
                                        <div>
                                            {
                                                entry.timeSheetDeleted ?
                                                    <div className='deleted-text'>Uploaded Timesheet was Deleted</div>
                                                    :
                                                    <>
                                                        {
                                                            !IS_STUDENT &&
                                                                <>
                                                                    <div className='deletion-text' onClick={() => confirmDelete(entry.timeSheetKey)}>Delete Timesheet</div>
                                                                    <div className='download-divider'></div>
                                                                </>
                                                        }
                                                        <i onClick={() => openPDF(entry.timeSheetKey)} className="large download icon"></i>
                                                    </>
                                            }
                                        </div>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
                <Modal
                    className='timesheet-comment-modal'
                    onClose={() => toggleEditComment(false, null)}
                    open={state.isAddingComment}>
                    <Modal.Header>Update Timesheet Comment</Modal.Header>
                    <Modal.Content>
                        <Form>
                            <TextArea
                                value={state.editCommentInput}
                                maxLength={500}
                                onChange={(event, data) => setState({editCommentInput: data.value})}/>
                                <div align='right'>{state.editCommentInput.length}/500</div>
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button positive onClick={submitComment.bind(this)}>Update</Button>
                        <Button onClick={() => toggleEditComment(false, null)}>Cancel</Button>
                    </Modal.Actions>
                </Modal>
            </div>
            :
            <div>
                <Loader active inline='centered'>Fetching Uploaded Timesheets</Loader>
            </div>
    );
}

export default ModalContainer(TimeSheetLog);
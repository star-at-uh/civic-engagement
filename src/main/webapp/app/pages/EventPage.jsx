import React from 'react';
import queryString from 'query-string';
import { Form, Container, Grid, Button, Icon, Confirm, Message, Dropdown, Popup } from 'semantic-ui-react';
import PeopleGoing from '../components/event-page/PeopleGoing.jsx';
import EventDetailsContainer from '../redux/containers/event-details.js';
import CreateEventContainer from '../redux/containers/create-event.js';
import RootContainer from '../redux/containers/app.js';
import ModalContainer from '../redux/containers/modal.js';
import { Redirect, Link } from 'react-router-dom';
import BackLink from '../components/nav/BackLink.jsx';
import LikesContainer from '../redux/containers/event-likes.js';

import '../css/event-page.scss';

/**
 * Component for rendering an event's page
 */
class EventPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      openCancelModal: false,
      optionalComment: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.loadEventDetails = this.loadEventDetails.bind(this);
  }

  /**
   * Init function for EventPage.jsx
   * currently checks if the id param exists in the URL upon this page's load
   */
  componentDidMount() {
    // get event details
    this.loadEventDetails();
  }

  /**
   * Called whenever something related to the component updates
   * @param {*} prevProps 
   */
  componentDidUpdate(prevProps) {
    // Currently used to check if a duplicate event was made and then navigated to
    if (prevProps.location.search !== this.props.location.search) {
      this.props.closeModal();
      this.loadEventDetails();
    }
  }

  loadEventDetails() {
    const PARAMS = queryString.parse(this.props.location.search);
    if (PARAMS.id) {
      this.props.eventDetails({ eventId: PARAMS.id }).then((response) => {
        if (response.data.length > 0) {
          this.setState({ eventDetails: response.data[0] });
        } else {
          this.props.openModal('Invalid event ID.');
          this.setState({ redirect: true });
        }
      });
    } else {
      this.setState({ redirect: true });
    }
  }

  /**
     * Updates the state that manages inputs
     * @param {*} event 
     * @param {*} data 
     */
    handleInputChange(event, data) {
      // Dropdowns and textareas have different ways they format the event param (textarea -> event.target.value | dropdowns -> event.value)
      // For any old value not coming from either a dropdown or a text area you can just pass the value
      this.setState({[data.name]: data.target || data.value ? (data.target ? data.target.value : data.value) : data.checked});
  }

  /**
   * function to register user to event
   */
  register() {
    this.props.register({
      eventId: this.state.eventDetails.EventID,
      description: this.state.optionalComment
    }).then((response) => {
      if (response.data.length) {
        if (response.data[0].Status === 1) {
          this.componentDidMount();
        } else {
          this.props.openModal(response.data[0].ErrorMsg);
        }
      } else {
        this.props.openModal('Oops! Something went wrong. Please try again in a minute.');
      }
    })
  }

  /**
   * function to cancel user event with confirm dialog
   */
  cancelEventAttendance(e) {
    e.preventDefault();
    this.props.cancelRegistration({ eventId: this.state.eventDetails.EventID }).then((response) => {
      if (response.data.length) {
        if (response.data[0].Status === 1) {
          this.componentDidMount();
        } else {
          this.props.openModal(response.data[0].ErrorMsg);
        }
      } else {
        this.props.openModal('Oops! Something went wrong. Please try again in a minute.');
      }
    })
    this.setState({ openCancelModal: false });
  }

  /**
   * handle like button
   * props methods: likeEvent, dislikeEvent
   */
  handleLike(e){
    e.stopPropagation();
    const DATA = this.state.eventDetails;
    let { likeEvent, dislikeEvent, openModal } = this.props;
    const sendFunction = DATA.DateTimeEventLiked == null ? likeEvent : dislikeEvent;
    sendFunction({eventId: DATA.EventID}).then((response) => {
      if(!response.data.length || !response.data[0].Status){
        openModal('An unexpected error has occurred, event not liked!');
      }
      else if(response.data[0].Status){
        DATA.DateTimeEventLiked = DATA.DateTimeEventLiked == null ? new Date() : null;
        this.forceUpdate();
      }
    });
  }

  /**
   * function to render event controls
   */
  controls() {
    const { eventDetails } = this.state;
    let button = '';

    if (!this.props.session.isLogged) {
      button = <Button id='login' className='normal' onClick={this.props.loginUser}>Login to Register</Button>
    }
    else if (!eventDetails.IsAttending || (eventDetails.IsAttending && !eventDetails.EventUserIsAcceptedForSelectAdmissions && eventDetails.EventIsSelectiveAdmissions)) {
      button = <Button id='register' className='normal' disabled={eventDetails.IsPastEvent} onClick={this.register.bind(this)}>{eventDetails.EventIsSelectiveAdmissions ? 'Apply' : 'Register'}</Button>;
    }
    else if (eventDetails.EventIsSelectiveAdmissions && eventDetails.IsAttending && !eventDetails.EventUserIsAcceptedForSelectAdmissions) {
      button = <Button id='pending' className='normal' disabled={eventDetails.IsPastEvent} onClick={() => this.setState({ openCancelModal: true })}>Pending Approval</Button>;
    }
    else {
      button =  <Button id='clock-change' className='normal' onClick={() => this.setState({redirectToClock: true})}>
                  <Icon name='clock' />{eventDetails.DateTimeClockIn ? 'Clock-out' : 'Clock-in'}
                </Button>;
    }

    return (
      <div id='student-eventpage-controls' className='ep-button-group'>
        {button}
      </div>
    )
  }

  /**
   * function to render date/time and loction
   */
  dateTimeLocation() {
    const START_DATE_TIME = new Date(this.state.eventDetails.EventStartDateTime);
    const END_DATE_TIME = new Date(this.state.eventDetails.EventEndDateTime);

    return (
      <div>
        <h1 className={'sub-section-title'}>Date and Time</h1>
          <Grid className='section-content' stackable={true}>
              <Grid.Row columns={"equal"}>
                {/* If datefns version is updated to >= v2.0 change MMM to LLL */}
                <Grid.Column>
                  Start Date: {dateFns.format(START_DATE_TIME, 'MMM D, YYYY')}
                </Grid.Column>
                {/* No conversion needed, as the date time string used has no timezone, so user timezone will not affect the result */}
                {/* Also if datefns is >= v2.0 change A to aa */}
                <Grid.Column>
                  Start Time: {dateFns.format(START_DATE_TIME, 'h:mm A')} HST
                </Grid.Column>
              </Grid.Row>
              <Grid.Row columns={"equal"}>
                <Grid.Column>
                  {`End Date:  ${dateFns.format(END_DATE_TIME, 'MMM D, YYYY')}`}
                </Grid.Column>
                <Grid.Column>
                  {`End Time:  ${dateFns.format(END_DATE_TIME, 'h:mm A')} HST`}
                </Grid.Column>
              </Grid.Row>
          </Grid>

          <h1 className={'sub-section-title'}>Location</h1>
          {this.locationFormatter()}
          
          <table className='section-content'>
            <tbody>
              <tr>
                <td className='bold-text'>
                  Attendance Type:
                </td>
                <td>{this.state.eventDetails.AttendanceTypeName}</td>
              </tr>
              {
                this.state.eventDetails.EventTypeName?.length > 0 &&
                <tr>
                  <td className='bold-text'>
                    Event Type:
                  </td>
                  <td>{this.state.eventDetails.EventTypeName}</td>
                </tr>
              }
              {
                this.state.eventDetails.SocialCohortTypeName?.length > 0 &&
                <tr>
                  <td>
                    <strong>Social Cohort Type:</strong>
                  </td>
                  <td>{this.state.eventDetails.SocialCohortTypeName}</td>
                </tr>
              }
            </tbody>
          </table>
      </div>
    );
  }

  /**
   * method to render location / address / zoom attendance data
   */
  locationFormatter(){

    // grab state
    const EVENT_DETAILS = this.state.eventDetails;
    let searchQuery = '';

    // decide what to search

    // if link given choose link
    if(EVENT_DETAILS.EventVirtualMeetingLink){
      searchQuery = EVENT_DETAILS.EventVirtualMeetingLink;
      searchQuery = searchQuery.indexOf('http') < 0 ? `http://${searchQuery}` : searchQuery;
    }
    // physical address given
    else if(this.state.eventDetails.EventLocationAddressL1){
      searchQuery += this.state.eventDetails.EventLocationAddressL2 ? '+'+this.state.eventDetails.EventLocationAddressL2.split(' ').join('+') : '';
      searchQuery += this.state.eventDetails.EventLocationCity ? '+'+this.state.eventDetails.EventLocationCity.split(' ').join('+') : '';
      searchQuery += this.state.eventDetails.EventLocationState ? '+'+this.state.eventDetails.EventLocationState.split(' ').join('+') : '';
      searchQuery += this.state.eventDetails.EventLocationZip ? '+'+this.state.eventDetails.EventLocationZip.split(' ').join('+') : '';
      searchQuery = `http://www.google.com/maps?q=${searchQuery}`;
    }
    // just try searching name if all else fails
    else{
      searchQuery = `http://www.google.com/maps?q=${this.state.eventDetails.EventLocationName.split(' ').join('+')}`;
    }

    return (
      <p className='section-content'>
        <a id='location-name'
          href={searchQuery}
          target='_blank' rel='noopener noreferrer'>
            {this.state.eventDetails.EventLocationName}
          </a><br />
        {this.state.eventDetails.EventLocationAddressL1} {this.state.eventDetails.EventLocationAddressL1 && <br />}
        {this.state.eventDetails.EventLocationAddressL2} {this.state.eventDetails.EventLocationAddressL2 && <br />}
        {this.state.eventDetails.EventLocationCity}{this.state.eventDetails.EventLocationCity && ','} {this.state.eventDetails.EventLocationState} {this.state.eventDetails.EventLocationZip}
      </p>
    )
  }

  /**
   * Checks a given link to see if it starts with https:// and formats if
   * it does not start with that
   * @param {String} url the url to be checked
   */
  checkLink(url) {
    // No http allowed, only https, if no https add https so redirect works
    if (url.substring(0, 7) === 'http://') {
      return `https://${url.substring(7)}`;
    }
    else if (url.substring(0, 8) !== 'https://') {
      return `https://${url}`;
    }
    return url;
  }

  /**
   * Calls the deleteEvent action to attempt a deletion of the
   * currently viewed event
   */
  deleteEvent() {
    this.props.deleteEvent({eventId: this.state.eventDetails.EventID})
                    .payload
                    .then(response => {
                        if (response.data[0].ErrorMsg.length > 0) {
                            this.props.openModal(response.data[0].ErrorMsg);
                        }
                        else {
                            this.props.forceConfirm('Event successfully deleted');
                            this.setState({redirect: true});
                        }
                    });
  }

  /**
   * Main HTML Render
   */
  render() {
    return (
      <div id={'event-page'}>

        {this.state.redirect && <Redirect push exact='true' to='/' />}
        {this.state.redirectToClock && <Redirect push exact='true' to={`/clock-in-out?id=${this.state.eventDetails.EventID}`} />}
        {window.appConfig.getAttribute('data') == 'advisor' && this.state.redirectToEdit && <Redirect push exact='true' to={`/edit-event?id=${this.state.eventDetails.EventID}`} />}

        <Confirm
          open={this.state.openCancelModal}
          onCancel={() => this.setState({ openCancelModal: false })}
          onConfirm={this.cancelEventAttendance.bind(this)}
          content='Are you sure you want to cancel your registration for this event?'
        />
        <div id='top-spacer'/>

        {this.state.eventDetails && <Container id='event-page-container'>
          <div className='scrollable'>
            <div id='top-section'>
              <div id={'event-image'} style={{ backgroundImage: `url(${this.state.eventDetails.EventImageURL})`, backgroundPositionY: `${this.state.eventDetails.EventImageCenterPosition*100}%` }} />
              <div id={'card-info'}>
                {
                  (window.appConfig.getAttribute('data') === 'advisor' && this.state.eventDetails.IsPartOfOrganization) ?
                    <Dropdown className='event-options-menu' icon='ellipsis vertical' direction='left'>
                      <Dropdown.Menu direction='right'>
                        <Dropdown.Item onClick={() => this.props.openDuplicationModal(this.state.eventDetails.EventID)}>
                          Duplicate Event
                        </Dropdown.Item>
                        <Dropdown.Item onClick={() => this.props.forceConfirm('This action will delete this event.', this.deleteEvent.bind(this), true)}>
                          Delete Event
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                    :
                    ''
                }
                <div id={'date'}>
                  {dateFns.format(new Date(this.state.eventDetails.EventStartDateTime), 'ddd, MMM D, YYYY | h:mm A')}
                </div>
                <h1 id={'event-page-title'}>{this.state.eventDetails.EventName}</h1>
                <Link exact='true' to={`/organization?id=${this.state.eventDetails.EventOrganizationID}`}>by {this.state.eventDetails.OrganizationName}</Link>
                <br/>
                <br/>
                <div className='desktop' >
                  <BackLink />
                </div>
              </div>
              
            </div>
            

            <div id='event-likes'>
              <div id='like-container'>
                {
                  this.props.session.isLogged &&
                    <div id='like-button'
                      onClick={this.handleLike.bind(this)}>
                      <img 
                        className='like-icon'
                        src={`https://www.star.hawaii.edu/cdn/images/civic-engagement/${this.state.eventDetails.DateTimeEventLiked != null ? 'LikeIcon-liked' : 'LikeIcon-notliked'}.svg`} />
                    </div>
                }
              </div>
              {
                // Prevent 0 from being displayed on screen
                (window.appConfig.getAttribute('data') == 'advisor' && this.state.eventDetails.IsPartOfOrganization) == true &&
                
                <div id={'student-eventpage-controls'} className='desktop admin-controls'>
                  <Button className='normal' id='register'
                    onClick={() => this.setState({redirectToEdit: true})}><Icon name='edit outline'/>Edit Event</Button>
                </div>
              }
              {window.appConfig.getAttribute('data') == 'student' && (
                <div className='desktop' id='desktop-student-controls-container'>{this.controls()}</div>
              )}
            </div>

            {this.state.eventDetails.EventOutsideAttendeesInstructionMessage &&
            <div id='outside-attendees-message-container'>
              <Message>{this.state.eventDetails.EventOutsideAttendeesInstructionMessage}</Message>
            </div>}

            <div id='event-content'>

              <div id='first-column'>
                <div className='mobile'>              
                  {this.dateTimeLocation()}  
                  <br/>
                </div>

                {
                  this.state.eventDetails.EventIsSelectiveAdmissions &&
                  <>
                    <h1 className={'section-title'} id='about-this'>Selective Admissions</h1>
                    <p id='selective' className='section-content'>
                      Thank you for your interest in this event/opportunity. Please note this is a Special Admissions event that requires confirmation from the event coordinator to be accepted.
                    </p>
                  </>
                }

                <h1 className={'section-title'} id='about-this'>About this Opportunity</h1>
                <p id='description' className='section-content'>
                  {
                    this.state.eventDetails.EventDescription.length &&
                    // get rid of non-8bit single and double quote formatting as not compatible with certain machines
                    this.state.eventDetails.EventDescription.normalize('NFD')
                                                            .replace(/[\u0300-\u036f]/g, '')
                                                            .replace(/[\u02bb]/g,'\'')
                                                            .replace(/[\u2018\u2019]/g, "'")
                                                            .replace(/[\u201C\u201D]/g, '"') 
                    ||
                    'N/A'
                  }
                </p>
                {
                  this.state.eventDetails.ExternalSignupLink && this.state.eventDetails.ExternalSignupLink.length > 0 &&
                  <div>
                    <h1 className={'section-title'} id='about-this'>External Organization Signup Link</h1>
                    <a href={this.checkLink(this.state.eventDetails.ExternalSignupLink)} target='_blank' rel="noopener noreferrer">
                      {this.state.eventDetails.ExternalSignupLink}   
                    </a>
                  </div>
                }
                <br/>
                <h1 className={'section-title'}>Important things to know</h1>
                <table className='section-content'>
                  <tbody>
                    <tr>
                      <td>
                        <strong>What to Bring:</strong>
                      </td>
                      <td className='logistics-answer'>
                        {this.state.eventDetails.EventItemsToBring.length > 0 &&
                          this.state.eventDetails.EventItemsToBring
                          || 'N/A'}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Access:</strong>
                      </td>
                      <td className='logistics-answer'>
                        {this.state.eventDetails.EventAccess.length > 0 &&
                          this.state.eventDetails.EventAccess
                          || 'N/A'}
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <strong>Food Provided:</strong>
                      </td>
                      <td className='logistics-answer'>
                        {this.state.eventDetails.EventFoodProvided.length > 0 &&
                          this.state.eventDetails.EventFoodProvided
                          || 'N/A'}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>

                <div id='second-column'>
                <div className='desktop'>
                  {this.dateTimeLocation()}
                </div>

                <br/>

                <PeopleGoing eventId={this.state.eventDetails.EventID} isAdminForOrg={this.state.eventDetails.IsPartOfOrganization}/>

                <br />

                { 
                  // Not sure where to send them exactly rn for the service hours page
                  window.appConfig.getAttribute('data') == 'advisor' &&
                  <div className='to-student-service'>
                    <Link exact='true' to={`/student-progress?id=${this.state.eventDetails.EventID}&name=${this.state.eventDetails.EventName}`}>
                          Student Service Hours <img src={`https://www.star.hawaii.edu/cdn/images/civic-engagement/icons/userclock-icon.svg`} />
                    </Link>
                  </div>
                }

                {
                  window.appConfig.getAttribute('data') == 'student' &&
                  [
                    <h1 key='section-title-comments' className={'section-title'}><img id='my-comment-icon' src='https://star.hawaii.edu/cdn/images/civic-engagement/icons/Icon_material-comment.svg'/> My Comment / Question</h1>    
                    ,
                    this.state.eventDetails.EventUserDateTimeRegistered ?
                      <div key='user-comment'>

                        <p>
                          {this.state.eventDetails.EventUserComment || 'N/A'}
                        </p>
                        <p>Please contact the event organizer should you have any additional questions.</p>
                      </div>
                      :
                      <div key='add-comment'>
                        <Form>
                          <Form.TextArea
                            disabled={this.state.eventDetails.IsPastEvent || !this.props.session.isLogged}
                            value={this.state.optionalComment}
                            onChange={this.handleInputChange}
                            name='optionalComment'
                            label='Optional Event Comment / Question (starts email thread to organizer)'
                            placeholder='write your question or comment to the organizer here and click "REGISTER" to send with your registration'
                            id='event-registration-comment' />
                        </Form>
                        <Popup
                          trigger={
                            // Wrapping the button in a div b/c the popup doesn't render properly on disabled components
                            <div id='send-question'> 
                              <Button 
                                disabled={this.state.eventDetails.IsPastEvent || !this.props.session.isLogged} 
                                onClick={this.register.bind(this)}
                              >
                                Send
                              </Button>
                            </div>
                          }
                          content='Please login to send a comment or question'
                        />
                      </div>
                    ]
                }
              </div>
            </div>
            <div id='organizer-container'>
              <h1 className={'section-title'}>Organizer</h1>
              <table id='organizer-info' className='section-content'>
                <tbody>
                  <tr>
                    <td>
                      <strong>Name:</strong>
                    </td>
                    <td>
                      {this.state.eventDetails.EventContactPersonName}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Email:</strong>
                    </td>
                    <td>
                      {this.state.eventDetails.EventContactPersonEmailAddress.length &&
                      this.state.eventDetails.EventContactPersonEmailAddress || 'N/A'}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Phone:</strong>
                    </td>
                    <td>
                      {this.state.eventDetails.EventContactPersonPhoneNumber.length &&
                      this.state.eventDetails.EventContactPersonPhoneNumber || 'N/A'}
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div id='mobile-registration' className='mobile'>
            {
                window.appConfig.getAttribute('data') == 'advisor' &&
                
                <div id={'student-eventpage-controls'}>
                  <Button className='normal' id='register'
                    onClick={() => this.setState({redirectToEdit: true})}><Icon name='edit outline'/>Edit Event</Button>
                </div>
            }
            {window.appConfig.getAttribute('data') == 'student' && (
              this.controls()
            )}
          </div>
          {
            !this.state.eventDetails.IsPastEvent && this.state.eventDetails.IsAttending == true &&
            <div id='cancel-section'>
              <Button
                onClick={() => this.setState({ openCancelModal: true })}
                className='red'>
                <Icon name='times' />Cancel my Registration
              </Button>
            </div>
          }
        </Container>}
        <br />
        <br />
      </div>
    );
  }
}

export default RootContainer(CreateEventContainer(LikesContainer(ModalContainer(EventDetailsContainer(EventPage)))));
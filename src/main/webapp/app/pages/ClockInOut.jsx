import React from 'react';
import queryString from 'query-string';
import { Redirect, Link } from 'react-router-dom';
import { Form, Button, Icon, Grid, Container, Loader } from 'semantic-ui-react';
import DatePicker from '../components/global/DatePicker.jsx';
import EventDetailsContainer from '../redux/containers/event-details.js';
import ModalContainer from '../redux/containers/modal.js';
import EventsListContainer from '../redux/containers/events-list.js';
import TIME_OPTIONS from '../util/TimeOptions.js';
import BackLink from '../components/nav/BackLink.jsx';
import TimeSheetIOLink from './timesheet/TimeSheetIOLink.jsx';
import TimeSheetLog from './timesheet/TimeSheetLog.jsx';
import { postRequest } from '../redux/requests.js'; 

import '../css/clock-in-out.scss';

/**
 * Component for rendering the "Clock In/Out" form
 */
class ClockInOut extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            eventId: null,
            clockIn: null,
            clockOut: null,
            attendees: [],
            registeredEvents: [],
            selectedStudent: null,
            newDate: '',
            newStartTime: '',
            newEndTime: '',
            manualTimes: [],
            isLoadingTimeEntry: true,
            eventDateTimes: {},
            filteredTimes: TIME_OPTIONS,
            filteredStartTimes: TIME_OPTIONS,
            filteredEndTimes: TIME_OPTIONS,
            isFirstLoad: true,
            newTimeSheetUploaded: false,
        };

        this.handleManualTimeChange = this.handleManualTimeChange.bind(this);
        this.removeEntry = this.removeEntry.bind(this);
        this.grabEventDetails = this.grabEventDetails.bind(this);
        this.filterAvailableTimes = this.filterAvailableTimes.bind(this);
        this.handleNewTimeSheetUploaded = this.handleNewTimeSheetUploaded.bind(this);
    }

    /**
     * Init function for the component
     */
    componentDidMount() {
        // get event details
        const URL = this.props.location.search;
        const PARAMS = queryString.parse(URL);
        
        this.props.viewClockInEvents()
                    .payload
                    .then((response) => {
                        const SELECTED_OPTIONS = [];
                        const EVENTS = {};
                        response.data.forEach(event => {
                                        if ((event.EventIsSelectiveAdmissions && event.IsAcceptedForSelectAdmissions) || !event.EventIsSelectiveAdmissions) {
                                            EVENTS[event.EventID] = this.eventDateTimeParserHelper(event.EventStartDateTime, event.EventEndDateTime);
                                            SELECTED_OPTIONS.push({ 
                                                                key: event.EventID, 
                                                                text: <div>
                                                                        {event.EventName}{'\u00A0\u00A0'}
                                                                        <span className='right-date'>
                                                                            {dateFns.format(new Date(event.EventStartDateTime), 'ddd, MMM D, YYYY')}
                                                                        </span>
                                                                    </div>, 
                                                                value: event.EventID 
                                                            });
                                        }
                                    });

                        const NEW_STATE = {
                            registeredEvents: SELECTED_OPTIONS,
                            eventDateTimes: EVENTS,
                        };
                        
                        if (PARAMS.id) {
                            NEW_STATE['eventId'] = PARAMS.id;
                            if (dateFns.differenceInCalendarDays(EVENTS[PARAMS.id].startDateTime, EVENTS[PARAMS.id].startDateTime) == 0) {
                                NEW_STATE.newDate = dateFns.format(EVENTS[PARAMS.id].startDateTime, 'YYYY-MM-DD');
                            } 
                        }
                        this.setState(NEW_STATE, () => {
                                                    this.populate();
                                                    this.filterAvailableTimes();
                                                });
                    });
    }


    /**
     * function to populate clock in/out data based on event
     */
    populate(){
        if (window.appConfig.getAttribute('data') === 'advisor') {
            this.props.eventAttendees({ 
                            eventId: this.state.eventId 
                        })
                        .then((response) => {
                            const ATTENDEES = [];
    
                            response.data.members.forEach((student) => {
                                                        ATTENDEES.push({ 
                                                                    key: student.EventUserID, 
                                                                    text: `${student.EventUserFirstName} ${student.EventUserLastName}`, 
                                                                    value: student.EventUserID 
                                                                });
                                                    });
                            this.setState({ attendees: ATTENDEES });
                        });
        }

        this.grabEventDetails();
    }

    /**
     * Fetches the details for the currently selected event
     */
    grabEventDetails() {
        if ((window.appConfig.getAttribute('data') == 'student' || this.state.selectedStudent) && this.state.eventId) {
            this.setState({isLoadingTimeEntry: true}, () => {
                    this.props.eventDetails({ 
                                    eventId: this.state.eventId,
                                    userId: this.state.selectedStudent,
                                })
                                .then((response) => {
                                    if (response.data.length > 0) {
                                        // populate manual clock in out form if applicable
                                        const MANUAL_TIME_ENTRY = response.data.flatMap(entry => {
                                                                                    if (entry.DateTimeClockIn && entry.DateTimeClockOut) {
                                                                                        const FORMATTED_ENTRY = {...entry, ...this.eventDateTimeParserHelper(entry.DateTimeClockIn, entry.DateTimeClockOut)};
                                                                                        
                                                                                        FORMATTED_ENTRY['hours'] = entry.EventHoursCompleted;
                                                                                        FORMATTED_ENTRY['minutes'] = entry.EventMinuteCompleted;
                                                                                        
                                                                                        return [FORMATTED_ENTRY];                                    
                                                                                    }

                                                                                    return [];
                                                                                });
                                        const NEW_STATE = {
                                            manualTimes: MANUAL_TIME_ENTRY,
                                            isLoadingTimeEntry: false,
                                            newStartTime: '',
                                            newEndTime: '',
                                        };

                                        if (dateFns.differenceInCalendarDays(new Date(response.data[0].EventStartDateTime), new Date(response.data[0].EventEndDateTime)) == 0) {
                                            NEW_STATE.newDate = dateFns.format(response.data[0].EventStartDateTime, 'YYYY-MM-DD');
                                        }                     

                                        this.setState(NEW_STATE);
                                    } 
                                    else {
                                        // Students have the possibility of arriving at this page w/o an event id
                                        if (this.state.isFirstLoad && window.appConfig.getAttribute('data') == 'student' && !this.state.eventId) {
                                            this.setState({isFirstLoad: false});
                                        }
                                        else {
                                            this.props.openModal('Invalid event ID.');
                                        }
                                    }
                                });
                });
        }
    }

    /**
     * Helps parse the datetimes from the database into strings used for
     * display
     * @param {*} startDateTime the start date time from the db 
     * @param {*} endDateTime the end date time from the db 
     */
    eventDateTimeParserHelper(startDateTime, endDateTime) {
        const FORMATTED_ENTRY = {};

        if (startDateTime) {
            const START_DATE = new Date(startDateTime);
            FORMATTED_ENTRY['startDateTime'] = START_DATE;
            FORMATTED_ENTRY['startDate'] =  dateFns.format(START_DATE, 'MM/DD/YYYY');
            FORMATTED_ENTRY['startTime'] = dateFns.format(START_DATE, 'h:mm A');
        }

        if (endDateTime) {
            const END_DATE = new Date(endDateTime);
            FORMATTED_ENTRY['endDateTime'] = END_DATE;
            FORMATTED_ENTRY['endDate'] = dateFns.format(END_DATE, 'MM/DD/YYYY');
            FORMATTED_ENTRY['endTime'] = dateFns.format(END_DATE, 'h:mm A');
        }

        return FORMATTED_ENTRY;
    }

    /**
     * Filters the times available for choosing as start and end times using TIME_OPTIONS and the start and end date times
     * of the currently selected event
     */
    filterAvailableTimes() {
        const SELECTED_EVENT = this.state.eventDateTimes[this.state.eventId];
        let filteredTimes = TIME_OPTIONS;
        let filteredStartTimes = TIME_OPTIONS;
        let filteredEndTimes = TIME_OPTIONS;
        // Make sure that there is at least always a selectable time period if for some reason the filtering process hard fails
        try {
            if (SELECTED_EVENT) {
                filteredTimes = TIME_OPTIONS.filter(timeSlot => {
                                                if (SELECTED_EVENT) {
                                                    return dateFns.isBefore(dateFns.setMinutes(dateFns.setHours(SELECTED_EVENT.endDateTime, timeSlot.hours), timeSlot.minutes), dateFns.addMinutes(SELECTED_EVENT.endDateTime, 1)) && 
                                                           dateFns.isAfter(dateFns.setMinutes(dateFns.setHours(SELECTED_EVENT.startDateTime, timeSlot.hours), timeSlot.minutes), dateFns.subMinutes(SELECTED_EVENT.startDateTime, 1));
                                                }
                                                return true;
                                            });
            }
        }
        finally {
            /* There should not be a case where no time options exist (otherwise the event shouldn't exist) so in this case just show all time options
             * and leave it to the user to properly select time ranges
             */ 
            if (filteredTimes.length < 1) {
                filteredTimes = TIME_OPTIONS;
            }
            else {
                filteredStartTimes = filteredTimes.slice(0, -1);
                filteredEndTimes = filteredTimes.slice(1);
            }
            this.setState({filteredTimes, filteredStartTimes, filteredEndTimes});
        }
    }

    /**
     * function to clock out of event
     */
    submitManual() {
        const { eventId, newDate, newStartTime, newEndTime } = this.state;

        if (!newDate || !newStartTime || !newEndTime) {
            this.props.openModal('All fields must be filled out');
        }
        else {
            // For the backend to digest
            const DATE_TIME_LOG = {
                "DateTimeClockIn": `${newDate}T${newStartTime}`,
                "DateTimeClockOut": `${newDate}T${newEndTime}`
            };
    
            this.props.clockManual({
                            eventId: eventId,
                            clockInClockOutStamp: JSON.stringify(DATE_TIME_LOG),
                        })
                        .then((response) => {
                            if (response.data[0].Status == 0) {
                                this.props.openModal(response.data[0].ErrorMsg);
                            } 
                            else {
                                this.setState({newDate: '', newStartTime: '', newEndTime: ''}, () => {
                                        this.props.forceConfirm('Time successfully submitted.', this.grabEventDetails);
                                    });
                            }
                        });
        }
    }

    /**
     * Updates the manualTimes state to reflect user input on a specific time/date input field
     * The data param will have to be custom made, or come from semantic ui, if using default html input only event will be populated
     * @param {*} event a more detailed object compared to data, but holds the entirety of the event that fired from the user's action
     * @param {*} data object containing some information about what the user did
     */
    handleManualTimeChange(event, data) {
        const NEW_STATE = {};
        const USE_EVENT_PARAM = data === undefined; // The data param is semantic ui specific (or custom made)
        
        NEW_STATE[USE_EVENT_PARAM ? event.target.name : data.name] = USE_EVENT_PARAM ? event.target.value : data.value;

        this.setState(NEW_STATE);
    }

    /**
     * Updates the newTimesheetUploaded variable to see if we need to reload the timesheet log
     * after a user uploads a timesheet.
     * @param {boolean} wasUploaded  true if a timesheet was just uploaded, false otherwise
     */
    handleNewTimeSheetUploaded(wasUploaded) {
        this.setState({newTimeSheetUploaded: wasUploaded});
    }

    /**
     * Removes the given entry based on the entry's EventID, DateTimeID, and the associated student
     * @param {Object} entry an object containing various details about the current selected event
     */
    removeEntry(entry) {
        postRequest('../api/events/discardClockedTimes',{
                eventID: entry.EventID,
                clockedTimeID: entry.DateTimeID,
                studentId: this.state.selectedStudent,
            })
            .then((response) => {
                const DATA = response.data[0];

                if (DATA.ErrorMsg) {
                    this.props.openModal(response.data[0].ErrorMsg);
                }
                else {
                    this.props.forceConfirm('Entry successfully removed', this.grabEventDetails);
                }
            });
    }

    /**
     * main render function
     */
    render() {
        const IS_STUDENT = window.appConfig.getAttribute('data') == 'student';
        const EVENT_DATE_TIME = this.state.eventDateTimes[this.state.eventId];

        return (
            <div id={'clock-in-out'}>
                <Container>
                    {this.state.eventId && this.state.redirect && <Redirect push exact='true' to={`/event?id=${this.state.eventId}`}/>}

                    <BackLink />

                    <Grid>
                        <Grid.Row columns={3}>
                            <Grid.Column width={9}>
                                <h1 className={'main-title'}>CLOCK-IN & OUT</h1>
                            </Grid.Column>
                            {
                                IS_STUDENT &&
                                [
                                    <Grid.Column width={1} key='or-seperator'>
                                        <div className='or-seperator'>
                                            OR
                                        </div>
                                    </Grid.Column>,
                                    <Grid.Column width={5} key='timesheet-link'>
                                        <TimeSheetIOLink eventId={this.state.eventId} uploadCallback={this.handleNewTimeSheetUploaded}/>
                                    </Grid.Column>
                                ]
                            }
                        </Grid.Row>
                    </Grid>

                    {
                        !IS_STUDENT &&
                        [
                            <h1 key='select-student-header' className={'sub-title'}>STUDENT ID:</h1>,
                            <Form.Select key='student-select' search fluid options={this.state.attendees}
                                value={this.state.selectedStudent} placeholder={'Select Student'}
                                onChange={(event, value)=> this.setState({ selectedStudent: value.value }, this.grabEventDetails)}
                            />
                        ]
                    }

                    <h1 className={'sub-title'}>{this.state.eventId ? 'EVENT:' : 'SELECT THE EVENT YOU\'RE ATTENDING:'}</h1>

                    <Form.Select search fluid
                        options={this.state.registeredEvents}
                        value={typeof (this.state.eventId) === 'string' ? parseInt(this.state.eventId) : this.state.eventId}
                        placeholder={'Select event'}
                        onChange={(event,value)=>{
                            this.setState({eventId: value.value}, () => {
                                this.populate();
                                this.filterAvailableTimes();
                            });
                        }}
                    />
                    {
                        this.state.eventId &&
                        [
                            <Button key='event-link' id='event-link' as={Link} exact='true' to={`/event?id=${this.state.eventId}`}>
                                Go to event page <Icon name='external alternate'/>
                            </Button>,
                            <div key='section-divider' className='section-divider'></div>,
                            (IS_STUDENT || this.state.selectedStudent) &&
                                <Form key='time-entry-headers' className={'new-clock-in-section'}>
                                    <Form.Group>
                                        <Form.Field>
                                            <label>DATE</label>
                                            <DatePicker
                                                name={'newDate'}
                                                min={dateFns.format(EVENT_DATE_TIME?.startDate, 'YYYY-MM-DD')}
                                                max={dateFns.format(EVENT_DATE_TIME?.endDate, 'YYYY-MM-DD')}
                                                value={this.state.newDate}
                                                callback={this.handleManualTimeChange} 
                                            />
                                        </Form.Field>
                                        <Form.Select
                                            name={'newStartTime'}
                                            fluid
                                            search
                                            label='START TIME'
                                            options={this.state.filteredStartTimes}
                                            value={this.state.newStartTime}
                                            placeholder='Select time'
                                            onChange={this.handleManualTimeChange}
                                        />
                                        <Form.Select
                                            name={'newEndTime'}
                                            fluid
                                            search
                                            label='END TIME'
                                            options={this.state.filteredEndTimes}
                                            value={this.state.newEndTime}
                                            placeholder='Select time'
                                            onChange={this.handleManualTimeChange}
                                        />
                                        <Button basic color='teal' onClick={this.submitManual.bind(this)}>
                                            Add Time
                                        </Button>
                                    </Form.Group>
                                </Form>,
                            <div key='time-entry-section'>
                                {
                                    !IS_STUDENT && !this.state.selectedStudent ? 
                                        <div>Select a student to view their time entries and timesheets</div>
                                        :
                                        this.state.isLoadingTimeEntry ? 
                                            <Loader inline='centered' active>Loading time entries</Loader>
                                            :
                                            <Grid>
                                                <Grid.Row>
                                                    <Grid.Column>
                                                        Your Hours for the Event:
                                                    </Grid.Column>
                                                </Grid.Row>
                                                <Grid.Row className='clock-in-entry initial-entry' columns={4}>
                                                    <Grid.Column width={3}>
                                                        Date
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        Start Time
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        End Time
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        Hours
                                                    </Grid.Column>
                                                    <Grid.Column width={2}>
                                                        Minutes
                                                    </Grid.Column>
                                                    <Grid.Column width={2}>{/* Remove entry column */}</Grid.Column>
                                                </Grid.Row>
                                                {
                                                    this.state.manualTimes.map((entry) => {
                                                                                return (
                                                                                        <Grid.Row className='clock-in-entry' columns={4} key={entry.DateTimeID}>
                                                                                            <Grid.Column width={3}>
                                                                                                {entry.startDate}
                                                                                            </Grid.Column>
                                                                                            <Grid.Column width={3}>
                                                                                                {entry.startTime}
                                                                                            </Grid.Column>
                                                                                            <Grid.Column width={3}>
                                                                                                {entry.endTime}
                                                                                            </Grid.Column>
                                                                                            <Grid.Column width={3}>
                                                                                                {entry.hours}
                                                                                            </Grid.Column>
                                                                                            <Grid.Column width={2}>
                                                                                                {entry.minutes}
                                                                                            </Grid.Column>
                                                                                            <Grid.Column width={2} textAlign='right'>
                                                                                                <Icon name="trash alternate" onClick={() => this.props.forceConfirm('This action will delete this time entry from the event.', () => this.removeEntry(entry), true)}/>
                                                                                            </Grid.Column>
                                                                                        </Grid.Row>
                                                                                );
                                                                            })
                                                }
                                            </Grid>
                                }
                                {
                                    (IS_STUDENT || this.state.selectedStudent) &&
                                        <TimeSheetLog eventId={this.state.eventId} newTimeSheetUploaded={this.state.newTimeSheetUploaded} handleNewTimeSheetUploaded={this.handleNewTimeSheetUploaded}/>
                                }
                            </div>
                        ]
                    }
                </Container>
            </div>
        );
    }
}

export default EventsListContainer(ModalContainer(EventDetailsContainer(ClockInOut)));
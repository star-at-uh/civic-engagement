import React from 'react';
import _ from 'lodash';
import queryString from 'query-string';
import ServiceHoursContainer from '../redux/containers/service-hours.js';
import ModalContainer from '../redux/containers/modal.js';
import RootContainer from '../redux/containers/app.js';
import BackLink from '../components/nav/BackLink.jsx';
import { Modal, Button, Form } from 'semantic-ui-react';
import DatePicker from '../components/global/DatePicker.jsx';
import TimeSheetLog from './timesheet/TimeSheetLog.jsx';

import '../css/my-progress.scss';

/**
 * Component for rendering the my progress page
 */
class MyProgress extends React.Component {

  /**
   * constructor describes initial state
   */
  constructor(props) {
    super(props);
    this.state = {
      currentGoalIndex: null,
      goals: [],
      newGoalName: null,
      newGoalHours: 0,
      newStartDate: null,
      newEndDate: null,
      serviceHourLog: [],
      openGoalEditor: false,
      name: '',
    }
  }

  /**
   * fires on component load
   * 
   * props avail methods:
   * myProgress, comboboxUserGoals, setNewGoal, editGoal, viewServiceHoursRoster, viewStudentServiceRecord
   */
  componentDidMount() {
    // decide admin vs student view
    let url = this.props.location.search;
    let params = queryString.parse(url);
    let hoursFunction = this.props.myProgress;
    let goalsFunction = this.props.comboboxUserGoals;
    if(this.props.admin && params.id){
      hoursFunction = this.props.viewStudentServiceRecord;
      goalsFunction = this.props.viewStudentGoals;
      if(params.displayN){
        this.setState({studentName: params.displayN});
      }
    }
    // get hours
    hoursFunction({
      userId: params.id
    }).then((response) => {
      if (response.data.length && Object.keys(response.data[0]).includes('Status') && response.data[0].Status == 0) {
        this.props.openModal('Service record could not be loaded, please try reloading the page.')
      }
      else {
        this.setState({ serviceHourLog: response.data });
      }
    });
    // get goals
    goalsFunction({
      userId: params.id
    }).then((response) => {
      if (response.data.length && Object.keys(response.data[0]).includes('Status') && response.data[0].Status == 0) {
        this.props.openModal('User goals could not be loaded, please try reloading the page.')
      }
      else if (response.data.length && !Object.keys(response.data[0]).includes('Status')) {
        const GOALS = response.data
        const CURRENT_GOAL = GOALS[0];
        this.setState({
          goals: GOALS,
          newGoalName: CURRENT_GOAL.GoalName,
          newGoalHours: CURRENT_GOAL.GoalHoursTotal,
          newStartDate: dateFns.format(CURRENT_GOAL.GoalStartDate, 'YYYY-MM-DD'),
          newEndDate: dateFns.format(CURRENT_GOAL.GoalEndDate, 'YYYY-MM-DD'),
          currentGoalIndex: 0
        });
      }
    });

    this.props.getSessionInfo().payload
                                .then(response => {
                                  this.setState({name: response.data.fullname});
                                });
  }

  // open editor to create new goal
  openEditorNew() { this.setState({ newGoalName: null, newGoalHours: 0, openGoalEditor: true }) }

  // open editor to edit existing goal
  openEditorExisting(goalName, goalHours, selectedGoalID) { this.setState({ newGoalName: goalName, newGoalHours: goalHours, openGoalEditor: true, selectedGoalID }) }

  /**
   * sets goal
   */
  setGoal(root) {
    let { newGoalName, newGoalHours, newStartDate, newEndDate, currentGoalIndex, goals } = root.state;
    let sendFunction = currentGoalIndex != null ? root.props.editGoal : root.props.setNewGoal;
    sendFunction({
        goalName: newGoalName,
        startDate: newStartDate,
        endDate: newEndDate,
        goalHours: newGoalHours,
        goalId: currentGoalIndex != null ? goals[currentGoalIndex].GoalID : null
    }).then((response) => {
      root.setState({openGoalEditor: false}, ()=> {
        this.componentDidMount();
        if(response.data && response.data.length && !response.data[0].Status){
          root.props.openModal(response.data[0].ErrorMsg);
        }
      });
    });
  }

  /**
   * Displays a prompt for the user to notify them that landscape mode may be needed when they try print the page
   */
  promptPrint() {
    // Delay the call to print so it doesn't capture the dialog box instead
    const printTimer = () => {
          setTimeout(() => {
            window.print();
          }, 250);
        };
    this.props.forceConfirm('If landscape layout is necessary, please change the print orientation to landscape.', printTimer);
  }

  /**
   * main render method
   */
  render() {

    const root = this;
    const today = new Date();

    let { serviceHourLog, goals, currentGoalIndex, newGoalHours, newGoalName, newStartDate, newEndDate } = this.state,
      hoursComplete = _.sum(serviceHourLog.map(entry => entry.EventHoursCompleted)),
      totalHours = goals.length && currentGoalIndex != null ? goals[currentGoalIndex].GoalHoursTotal : 'N/A',
      goalName = goals.length && currentGoalIndex != null ? goals[currentGoalIndex].GoalName : 'No goal set',
      goalBarWidth = (hoursComplete * 100) / totalHours;
    //cap width at 100%
    goalBarWidth = goalBarWidth > 100 ? 100 : goalBarWidth;

    return (
      <div id={'my-progress'} className='eds-layout__body'>

        <Modal
          id='goal-modal'
          size='small'
          open={this.state.openGoalEditor}
          onClose={() => this.setState({ openGoalEditor: false })}>
          <Modal.Header>{this.state.currentGoalIndex !=null ? 'Edit Goal' : 'Set a Goal'}</Modal.Header>
          <Modal.Content>
            <Modal.Description className='ui form'>
              <Form.Input
                label='Goal Name'
                type='text'
                placeholder={`${today.getMonth() < 6 ? 'Spring' : 'Fall'} ${today.getFullYear()}`}
                value={newGoalName}
                onChange={(event, target) => root.setState({newGoalName: target.value})}
                />
              <Form.Input
                label='Set Hours to Complete'
                type='number'
                placeholder={40}
                value={newGoalHours}
                onChange={(event, target) => root.setState({newGoalHours: parseInt(`${target.value}`)})}
                />
                <Form.Group widths='equal'>
                    <Form.Field>
                        <label>START DATE</label>
                        <DatePicker
                          id='new-goal-start-date'
                          value={newStartDate}
                          callback={(e) => this.setState({newStartDate: e.target.value})}/>
                    </Form.Field>
                    <Form.Field>
                        <label>END DATE</label>
                        <DatePicker
                          id='new-goal-end-date'
                          value={newEndDate}
                          callback={(e) => this.setState({newEndDate: e.target.value})}/>
                    </Form.Field>
                </Form.Group>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button
              content='Save'
              onClick={root.setGoal.bind(root, root)}
              positive />
            <Button
              content='Cancel'
              onClick={() => root.setState({openGoalEditor: false})}
               />
          </Modal.Actions>
        </Modal>

        <h1 className='title'>{this.props.admin ? this.state.studentName : 'MY PROGRESS'}</h1>
        <BackLink />

        <h3 className='print-only'>Name: {this.state.name}</h3>
        <Button className='print-button' onClick={this.promptPrint.bind(this)}>Print</Button>

        <div id='goal-container'>
          <div id='goal'>
            <span id='goal-name'><strong>Goal:</strong> {goalName}</span>
            {!this.props.admin &&
              <a id='goal-edit' onClick={() => this.setState({ openGoalEditor: true })}>{currentGoalIndex != null ? 'Edit' : 'Set Goal'}</a>}
            {
              currentGoalIndex != null &&
              <div id='goal-date-range'>
                {'effective dates '}
                {dateFns.format(goals[currentGoalIndex].GoalStartDate, 'MMM D, YYYY')}
                {' to '} 
                {dateFns.format(goals[currentGoalIndex].GoalEndDate, 'MMM D, YYYY')}
              </div>
            }
          </div>
          <div id='goal-hours'><strong>{hoursComplete}</strong> of <strong>{totalHours}</strong> hours complete</div>
          <div id='progress-bar-container'>
            <div id='progress-bar-complete' style={{ width: `${goalBarWidth}%` }} />
          </div>
        </div>

        <div id='my-service-hours'>
          <h1>{this.props.admin ? 'Service Hours:' : 'My Service Hours:'}</h1>

          <table>
            <thead>
              <tr>
                <td>Opportunity Name</td>
                <td>Organization</td>
                <td>Date</td>
                <td className='center-text'>Hours Served</td>
                <td className='center-text'>Minutes Served</td>
              </tr>
            </thead>
            <tbody>
              {
                this.state.serviceHourLog.map((attendance, index) => {
                  return (
                    // Due to duplicate entries being allowed in the past, there is no field available from the proc right now that allows unique distinction, so use index
                    <tr key={`${attendance.EventName}-${attendance.DateTimeClockIn}-${index}`}>
                      <td>{attendance.EventName}</td>
                      <td>{attendance.OrganizationName}</td>
                      <td>{dateFns.format(new Date(attendance.DateTimeClockIn), 'MM/DD/YYYY')}</td>
                      <td className='center-text'>{attendance.EventHoursCompleted}</td>
                      <td className='center-text'>{attendance.EventMinuteCompleted}</td>
                    </tr>
                  );
                })
              }
            </tbody>
          </table>

          <div className='or-header'>
              - OR -
          </div>

          <TimeSheetLog/>

        </div>
      </div>
    );
  }
}

export default RootContainer(ModalContainer(ServiceHoursContainer(MyProgress)));
import React, { useState, useEffect } from 'react';
import { Tab, Loader } from 'semantic-ui-react';
import EventPreview from '../components/events-list/EventPreview.jsx';
import EventsListContainer from '../redux/containers/events-list.js';
import OrganizationsList from './OrganizationsList.jsx';

import '../css/favorites.scss';

/**
 * Component for rendering the my events page
 */
function Favorites(props) {
    const [likedEvents, setLikedEvents] = useState([]);
    const [activeTab, setActiveTab] = useState(0);
    const [isLoading, setIsLoading] = useState(true);
    
    /**
     * fires on component load
     */
    useEffect(() => {
        const MEMORIZED_TAB_DATA = sessionStorage.getItem('favorite-selectedTab');
        let activeTabIndex = 0;
        if (MEMORIZED_TAB_DATA) {
            activeTabIndex = parseInt(MEMORIZED_TAB_DATA);
        }
    
        setActiveTab(activeTabIndex);
        
        // load events
        props.viewFavoriteEvents().payload
                                    .then((response) => {
                                        setLikedEvents(sortEvents(false, response.data));
                                        setIsLoading(false);
                                    });
    }, []);

    /**
     * Store in local session the user's current tab and switch to it. 
     * @param {*} event native react event
     * @param {*} data custom data object (must have activeIndex key)
     */
    function memorizeTab(event, data) {
        sessionStorage.setItem('favorites-selectedTab', `${data.activeIndex}`);
        setActiveTab(data.activeIndex);
    }

    /**
     * Sort the given events or liked events in ascending or descending order
     * @param {boolean} ascendingSort true for ascending, false for descending
     * @param {Array} events the events to sort, leave null if you want to use current likedEvents
     * @returns {Array} the list of events sorted (if events was provided)
     */
    function sortEvents(ascendingSort, events) {
        let eventList = events ?? likedEvents;
        const COMPARE_FUNCTION = ascendingSort ? dateFns.compareAsc : dateFns.compareDesc;
        eventList = eventList.sort((eventOne, eventTwo) => COMPARE_FUNCTION(eventOne.EventStartDateTime, eventTwo.EventStartDateTime));
        
        if (eventList) {
            return eventList;
        }
        else {
            setLikedEvents(eventList);
        }
    }

    /**
     * Generates the contents of the tabs on the favorites page
     */
    function generateTabs() {
        return [
            {
                menuItem: 'Events',
                render: () => {
                    return (
                        <Tab.Pane key='favorite-events'>
                        {
                            likedEvents && likedEvents.length > 0 ?
                                likedEvents.map((event) => <EventPreview key={event.EventID} data={event} />)
                                :
                                <div className='display-message'>
                                    {
                                        isLoading ?
                                            <Loader active={true} inline='centered'>Loading Events</Loader>
                                            :
                                            <h4>No liked events</h4>
                                    }
                                </div>
                        }
                        </Tab.Pane>
                    );
                },
            },
            {
                menuItem: 'Organizations',
                render: () => {
                    return (
                        <Tab.Pane key='favorite-organizations'>
                            <OrganizationsList viewFavorites={true}/>
                        </Tab.Pane>
                    );
                },
            },
        ];
    }


    return (
        <div className={'eds-layout__body'} id={'favorites-list'}>
            <h1>My Liked {activeTab == 0 ? 'Events' : 'Organizations'}</h1>
            <Tab className={'favorite-tabs'} menu={{ secondary: true, pointing: true }} panes={generateTabs()} activeIndex={activeTab} onTabChange={memorizeTab}/>
        </div>
    );
}

export default EventsListContainer(Favorites);
import React, { useState, useEffect } from 'react';
import { Grid, Label, Dropdown } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import OrganizationContainer from '../redux/containers/organization.js';
import RootContainer from '../redux/containers/app.js';
import ModalContainer from '../redux/containers/modal.js';
import queryString from 'query-string';
import OrganizationEvents from '../components/organization/OrganizationEvents.jsx';
import BackLink from '../components/nav/BackLink.jsx';
import { Redirect } from 'react-router-dom';

import '../css/organization.scss';

/**
 * Component for rendering the organization page
 */
const OrganizationPage = (props) => {
    const [organizationInfo, setOrganizationInfo] = useState({});
    const [redirect, setRedirect] = useState(false);

    /**
     * fires when component mounts, initializes data
     */
    useEffect(() => {
        let url = props.location.search;
        let params = queryString.parse(url);
        if (params.id) {
            // populate organization page 
            props.organizationDetails({ organizationId: params.id }).then((response) => {
                if (response.data[0].OrganizationName) {
                    setOrganizationInfo(response.data[0]);
                } else {
                    props.openModal(response.data[0].ErrorMsg);
                }
            });
        }
    }, []);

    /**
     * Attemps to delete the current organization
     */
    const deleteOrganization = () => {
        props.deleteOrganization({organizationId: organizationInfo.OrganizationID})
             .then(response => {
                 if (response.data[0].ErrorMsg.length > 0) {
                     props.openModal(response.data[0].ErrorMsg);
                 }
                 else {
                     props.forceConfirm('Organization successfully deleted');
                     setRedirect(true);
                 }
             });
    };

    return (
        <div id={'org-page'}>
            {redirect && <Redirect push exact='true' to={`/`}/>}
            <div className={'ui container'}>
                {organizationInfo.OrganizationImageURL ?
                    <div id={'org-image'} style={{ backgroundImage: `url(${organizationInfo.OrganizationImageURL})`, backgroundPositionY: `${organizationInfo.OrganizationImageCenterPosition}%` }} />
                    :
                    <div id={'no-org-image'}/>
                }
                <BackLink />
                <Grid>
                    <Grid.Row columns={window.mobileCheck() ? 1 : 2}>
                        <Grid.Column>
                            <div>
                                <div className='inline'>
                                    <h1 className={'main-title'}>
                                        {organizationInfo.OrganizationName}
                                    </h1>
                                </div>
                                {   // Explicit check to avoid generating a 0 when false
                                    (window.appConfig.getAttribute('data') == 'advisor' && organizationInfo.IsPartOfOrganization) == true &&
                                    <div className='inline left-spaced'>
                                        (<Link exact='true' to={`/edit-organization?id=${organizationInfo.OrganizationID}`}>
                                            EDIT
                                        </Link>)
                                    </div>
                                }
                            </div>
                            {organizationInfo.OrganizationLabelIDs &&
                            organizationInfo.OrganizationLabelIDs.split('//').map((idGroup) => {
                                const ID_GROUP = idGroup.split('||'); // [0] is name, [1] is id
                                return <Label key={ID_GROUP[1]}>{ID_GROUP[0]}</Label> 
                            })}
                        </Grid.Column>
                        <Grid.Column className='flex-element'>
                        {
                            (window.appConfig.getAttribute('data') === 'advisor' && props.session.siteAdmin) ?
                                <Dropdown className='organization-options-menu' icon='ellipsis vertical' direction='left'>
                                    <Dropdown.Menu direction='right'>
                                        <Dropdown.Item onClick={() => props.forceConfirm('This action will delete this organization.', deleteOrganization, true)}>
                                            Delete Organization
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                                :
                                ''
                        }
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={window.mobileCheck() ? 1 : 2}>
                        <Grid.Column width={window.mobileCheck() ? 16 : 6}>

                            <h1 className={'sub-title'}>ABOUT</h1>
                            <p id='org-desc'>{organizationInfo.OrganizationDescription ? organizationInfo.OrganizationDescription : 'N/A'}</p>


                            <h1 className={'sub-title'}>TYPES OF EVENTS</h1>
                            <p>{organizationInfo.AttendanceTypesPrintable ? organizationInfo.AttendanceTypesPrintable : 'N/A'}</p>

                            <h1 className={'sub-title'}>CONTACT INFO</h1>
                            <table>
                                <tbody>
                                    <tr>
                                        <td className='bold-text'>
                                            Name:
                                        </td>
                                        <td>
                                            {organizationInfo.OrganizationContactPersonFullName || 'N/A'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className='bold-text'>
                                            Email:
                                        </td>
                                        <td>
                                            {organizationInfo.OrganizationEmailAddress || 'N/A'}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className='bold-text'>
                                            Phone:
                                        </td>
                                        <td>
                                            {organizationInfo.OrganizationPhoneNumber || 'N/A'}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>


                            <h1 className={'sub-title'}>WEBSITE</h1>
                            <p>{organizationInfo.OrganizationWebpageURL ? <a href={organizationInfo.OrganizationWebpageURL} target='_blank' rel='noopener noreferrer'>{organizationInfo.OrganizationWebpageURL}</a> : 'N/A'}</p>


                            <h1 className={'sub-title'}>ADDRESS</h1>
                            {
                                organizationInfo.OrganizationAddressLine1 ||
                                organizationInfo.OrganizationAddressLine2 ||
                                organizationInfo.OrganizationAddressCity ||
                                organizationInfo.OrganizationAddressState ||
                                organizationInfo.OrganizationAddressZipCode ?
                                    <>
                                        <div>{organizationInfo.OrganizationAddressLine1}</div>
                                        <div>{organizationInfo.OrganizationAddressLine2}</div>
                                        {organizationInfo.OrganizationAddressCity}{organizationInfo.OrganizationAddressCity && ','} {organizationInfo.OrganizationAddressState} {organizationInfo.OrganizationAddressZipCode}
                                    </> 
                                    : 
                                    <p>N/A</p>
                            }

                            {   // Explicit to avoid rendering a 0 when false
                                (window.appConfig.getAttribute('data') === 'advisor' && organizationInfo.IsPartOfOrganization) == true &&
                                <>
                                    <h1 className='sub-title'>ORGANIZATION STATUS</h1>
                                    Organization is {organizationInfo.OrganizationIsInActive ? 'Inactive' : 'Active'}
                                </>
                            }
                        </Grid.Column>
                        <Grid.Column width={window.mobileCheck() ? 16 : 9}>

                            <h1 className={'sub-title'}>EVENTS</h1>
                            <OrganizationEvents organizationId={organizationInfo.OrganizationID} />
                        </Grid.Column>
                    </Grid.Row>
                </Grid>

            </div>
        </div>
    );
}

export default RootContainer(ModalContainer(OrganizationContainer(OrganizationPage)));
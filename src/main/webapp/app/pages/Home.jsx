import React from 'react';
import queryString from 'query-string';

import '../css/home.scss';
import SearchPage from './SearchPage.jsx';

/**
 * Component for rendering the homepage
 */
class Home extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      showRegistered: false
    }
  }

  componentDidMount(){
    // from main menu?
    let url = this.props.location.search;
    let params = queryString.parse(url);
    if(params.showRegistered === 'true'){
        this.setState({showRegistered: true});
    }
  }

  /**
   * main render function
   */
  render() {
    return (
      <div className={'student-home'}>
        <SearchPage/>
      </div>
    );
  }
}

export default Home;
import React from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Segment, Grid, Container } from 'semantic-ui-react';
import queryString from 'query-string';
import DatePicker from '../../components/global/DatePicker.jsx';
import SBox from '../../components/global/SBox.jsx';
import ImageUpload from '../../components/global/ImageUpload.jsx';
import ChooseAttendanceType from '../../components/create-event/ChooseAttendanceType.jsx';
import ChooseOrganization from '../../components/create-event/ChooseOrganization.jsx';
import CreateEventContainer from '../../redux/containers/create-event.js';
import ModalContainer from '../../redux/containers/modal.js';
import EventDetailsContainer from '../../redux/containers/event-details.js';
import TIME_OPTIONS from '../../util/TimeOptions.js';
import BackLink from '../../components/nav/BackLink.jsx';
import PhoneInput from '../../components/inputs/PhoneInput.jsx';
import LeaveConfirm from '../../components/global/LeaveConfirm.jsx';

import '../../css/settings.scss';
import '../../css/create-event.scss';

/**
 * Component for rendering the "Create an Event" form
 */
class CreateEvent extends React.Component {

    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = { 
            eventName: '',
            eventTypes: null,
            organizationId: '',
            allowWaitlist: false,
            startDate: '',
            endDate: '',
            startTime: '',
            endTime: '',
            attendanceTypeId: '',
            locationName: '',
            locationAddress1: '',
            locationAddress2: '',
            locationCity: '',
            locationState: '',
            locationZip: '',
            virtualMeetingLink: '',
            limitNumAttendees: false,
            attendeeLimit: '0',
            eventAccessInstructions: '',
            imageName: '',
            imageBlob: null,
            imageUrl: '',
            imageCenter: null,
            imageChanged: false,
            description: '',
            externalSignupLink: '',
            nonInstitutionalMessage: '',
            contactPersonName: '',
            contactPersonEmail: '',
            contactPersonPhone: '',
            itemsToBring: '',
            foodProvided: '',
            receiveEmails: true,
            eventId: null,
            redirect: false,
            missingFields: '',
            copyOfInitialState: {},
            modifiedFields: [],
        };

        this.handleInputChange = this.handleInputChange.bind(this);
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount(){
        let url = this.props.location.search;
        let params = queryString.parse(url);
        if(params.id && window.location.href.indexOf('edit-event') > -1){
            this.setState({editing: true, eventId: params.id}, this.populate.bind(this));
        }
        if (params.orgId) {
            // Semantic UI does type checks for their dropdowns, and we grab organization ids as ints
            this.setState({organizationId: parseInt(params.orgId), copyOfInitialState: this.state});
        }
        this.getEventTypes.bind(this)();
    }

    /**
     * Grabs event types that an event can be
     */
    getEventTypes() {
        this.props.getEventTypes()
                    .then(response => {
                        if (response.data.length > 0) {
                            const EVENT_TYPES = response.data.map(eventType => (
                                                                    {
                                                                        key: eventType.EventTypeID, 
                                                                        text: eventType.EventTypeName, 
                                                                        value: eventType.EventTypeID,
                                                                    }
                                                                ));

                            this.setState({eventTypeOptions: EVENT_TYPES});
                        }
                    });
    }

    /**
     * Helper function that extracts the date from datetime
     * @param {*} dateTime the datetime 
     * @return {String} the date fields from the datetime
     */
    dateFormatHelper(dateTime) {
        return dateFns.format(dateTime, 'YYYY-MM-DD'); // Does not factor timezone, but for now assumed to be HST
    }

    /**
     * Helper function that extracts the time from datetime
     * @param {*} dateTime the datetime 
     * @return {String} the time fields from the datetime
     */
    timeFormatHelper(dateTime) {
        return dateFns.format(dateTime, 'HH:mm:ss.SSS'); // Does not factor timezone, but for now assumed to be HST
    }

    /**
     * Updates the state that manages inputs
     * @param {*} event raw web event data regarding the action triggering this function
     * @param {*} data formatted data containing at least a .value field.
     * @param {*} callback a function to be run after the state is updated
     */
    handleInputChange(event, data, callback) {
        // Dropdowns and textareas have different ways they format the event param (textarea -> event.target.value | dropdowns -> event.value)
        // For any old value not coming from either a dropdown or a text area you can just pass the value
        let value = null;
        const NEW_STATE = {};

        if (data.type === 'checkbox') {
            value = data.checked;
        }
        else if (data.target) {
            value = data.target.value
        }
        else {
            value = data.value
        }

        NEW_STATE[data.name] = value;

        const MODIFIED_FIELDS = this.state.modifiedFields;

        if (!MODIFIED_FIELDS.includes(data.name)) {
            MODIFIED_FIELDS.push(data.name);
            NEW_STATE.modifiedFields = MODIFIED_FIELDS;
        }

        if (data.name === 'limitNumAttendees' && !data.value) {
            NEW_STATE['allowWaitlist'] = false;
            NEW_STATE['attendeeLimit'] = '0';
        }

        if (data.name === 'startDate' && dateFns.isPast(value)) {
            this.props.openModal('Please note the start date you have selected is in the past');
        }

        this.setState(NEW_STATE, callback);
    }

    /**
     * Checks if a field has been modified and not saved
     * @return {boolean} true if something has been modified or there is a pending admin change
     */
    doChangesExist() {
        let changesExist = false;

        this.state.modifiedFields.forEach(fieldName => {
            if (this.state.copyOfInitialState[fieldName] !== this.state[fieldName]) {
                changesExist = true;
            }
        });

        return changesExist;
    }

    // populate event page for edit
    populate(){
        this.props.eventDetails({eventId: this.state.eventId}).then((response) => {
            if(response.data.length > 0){
                const EVENT = response.data[0];
                const START_DATE_TIME = new Date(EVENT.EventStartDateTime);
                const END_DATE_TIME = new Date(EVENT.EventEndDateTime);
                const NEW_STATE = {
                    eventName: EVENT.EventName,
                    startDate: this.dateFormatHelper(START_DATE_TIME),
                    endDate: this.dateFormatHelper(END_DATE_TIME),
                    startTime: this.timeFormatHelper(START_DATE_TIME),
                    endTime: this.timeFormatHelper(END_DATE_TIME),
                    organizationId: EVENT.EventOrganizationID,
                    attendanceTypeId: EVENT.EventAttendanceTypeID,
                    allowWaitlist: EVENT.EventAllowWaitlist,
                    locationName: EVENT.EventLocationName,
                    locationAddress1: EVENT.EventLocationAddressL1,
                    locationAddress2: EVENT.EventLocationAddressL2,
                    locationCity: EVENT.EventLocationCity,
                    locationState: EVENT.EventLocationState,
                    locationZip: EVENT.EventLocationZip,
                    virtualMeetingLink: EVENT.EventVirtualMeetingLink,
                    limitNumAttendees: EVENT.EventAttendeeLimit ? true : false,
                    attendeeLimit: EVENT.EventAttendeeLimit ?? 0,
                    eventAccessInstructions: EVENT.EventAccess,
                    imageUrl: EVENT.EventImageURL,
                    imageCenter: EVENT.EventImageCenterPosition,
                    description: EVENT.EventDescription,
                    externalSignupLink: EVENT.ExternalSignupLink === null ? '' : EVENT.ExternalSignupLink,
                    nonInstitutionalMessage: EVENT.EventOutsideAttendeesInstructionMessage === null ? '' : EVENT.EventOutsideAttendeesInstructionMessage,
                    contactPersonName: EVENT.EventContactPersonName,
                    contactPersonEmail: EVENT.EventContactPersonEmailAddress,
                    contactPersonPhone: EVENT.EventContactPersonPhoneNumber,
                    itemsToBring: EVENT.EventItemsToBring,
                    foodProvided: EVENT.EventFoodProvided,
                    receiveEmails: EVENT.EventContactReceiveCopyOfAtendeeEmailNotifications,
                    eventType: EVENT.EventTypeID,
                };
                NEW_STATE.copyOfInitialState = NEW_STATE;

                this.setState(NEW_STATE);
            } else {
                this.setState({
                    limitNumAttendees: false,
                    eventId: null,
                    redirect: false,
                    copyOfInitialState: this.state
                });
            }
        })
    }

    /**
     * function to create / edit event
     */
    postevent(){
        const { eventName, eventType, organizationId, allowWaitlist, startDate, endDate, startTime, endTime, attendanceTypeId, locationName, locationAddress1,
                locationAddress2, locationCity, locationState, locationZip, virtualMeetingLink, limitNumAttendees, attendeeLimit, eventAccessInstructions, 
                imageCenter, externalSignupLink, contactPersonName, contactPersonEmail, contactPersonPhone, itemsToBring, foodProvided, receiveEmails, eventId } = this.state;

        let { description, nonInstitutionalMessage } = this.state; // these variables are changed

        if(description.length > 0){
            // remove unsupported diacratics
            description = description.normalize('NFD')
                                     .replace(/[\u0300-\u036f]/g, '')
                                     .replace(/[\u02bb]/g,'\'')
                                     .replace(/[\u2018\u2019]/g, "'")
                                     .replace(/[\u201C\u201D]/g, '"');
        }

        if(nonInstitutionalMessage.length){
            // remove unsupported diacratics
            nonInstitutionalMessage = nonInstitutionalMessage.normalize('NFD')
                                                             .replace(/[\u0300-\u036f]/g, '')
                                                             .replace(/[\u02bb]/g,'\'')
                                                             .replace(/[\u2018\u2019]/g, "'")
                                                             .replace(/[\u201C\u201D]/g, '"');
        }

        const PHONE_REGEX = /^\(\d{3}\)-\d{3}-\d{4}$/;
        const NUMBER_ONLY_REGEX = /^[0-9]*$/; 
        const ZIP_CODE_REGEX = /^([0-9]{5})$|^([0-9]{5}-[0-9]{4})$|^([0-9]{9})$/;
        const IMAGE_NAME_REGEX = /^[a-zA-Z_\d\-]+$/;
        // missing data handle
        let missingFields = '';
        missingFields += !organizationId ? `- Organization\n` : '';
        missingFields += !eventName ? `- Event Name\n` : '';
        missingFields += !eventType ? `- Event Type\n` : '';
        missingFields += !startDate ? `- Valid Start Date\n` : '';
        missingFields += !endDate ? `- Valid End Date\n` : '';
        missingFields += !startTime ? `- Start Time\n` : '';
        missingFields += !endTime ? `- End Time\n` : '';
        missingFields += !locationName ? `- Location Name\n` : '';
        missingFields += !attendanceTypeId ? `- Attendance Type\n` : '';
        missingFields += !contactPersonName ? `- Contact Person Name\n` : '';
        missingFields += !contactPersonEmail ? `- Contact Person Email\n` : '';
        missingFields += (contactPersonPhone && !PHONE_REGEX.test(contactPersonPhone)) ? '- Phone number must be formatted as (###)-###-####.\n' : '';
        missingFields += (limitNumAttendees && attendeeLimit && !NUMBER_ONLY_REGEX.test(attendeeLimit)) ? '- Attendee Limit must be a whole number with no decimals.' : '';
        missingFields += (attendanceTypeId === 3 && locationZip && !ZIP_CODE_REGEX.test(locationZip)) ? '- ZIP code must contain 5 or 9 digits and be formatted as 12345, 12345-6789, or 123456789.' : '';
        missingFields += (this.state.imageName && this.state.imageName.split('.').length > 2) ? '- Image file name should not contain a period outside of the extension\n' : '';
        missingFields += (this.state.imageName && !IMAGE_NAME_REGEX.test(this.state.imageName.split('.')[0])) ? '- Image file name can only contain dashes, hyphens, or alphanumeric characters\n' : '';
        
        this.setState({missingFields});

        // send it
        if (missingFields.length < 1) {
            let sendFunction;
            // choose f(x) to call
            if (this.state.editing && this.state.eventId){
                sendFunction = this.props.editEvent;
            } 
            else {
                sendFunction = this.props.createEvent;
            }

            const FORM = new FormData();
            if (this.state.imageBlob != null) {
                FORM.append('file', this.state.imageBlob, this.state.imageName);
            }
            FORM.append('params', JSON.stringify({
                eventId,
                organizationId,
                eventName,
                startDate: `${startDate} ${startTime}`,
                endDate: `${endDate} ${endTime}`,
                locationName,
                locationAddress1,
                locationAddress2,
                locationCity,
                locationState,
                locationZip,
                virtualMeetingLink,
                allowWaitlist,
                attendeeLimit: limitNumAttendees ? attendeeLimit : 0,
                attendanceTypeId,
                eventAccess: eventAccessInstructions,
                imageCenter,
                imageChanged: this.state.imageChanged,
                description,
                externalSignupLink,
                outsideAttendeesInstruction: nonInstitutionalMessage,
                contactPersonName,
                contactPersonEmail,
                contactPersonPhone,
                itemsToBring,
                foodProvided,
                receiveEmails,
                eventType
            }));

            sendFunction(FORM)
                .payload
                .then((response) => {
                    if(response.data.length > 0){
                        if(response.data[0]['Status'] == 1){
                            if (this.state.editing) {
                                this.props.openModal('Attention: The event will be set as inactive until the updates have been reviewed and approved.');
                            }
                            else {
                                this.props.openModal('Congratulations! Your new event has been created. Please note that it will need to be approved by UH before it can be published.');
                            }
                            this.setState({redirect: true, eventId: this.state.editing ? this.state.eventId : response.data[0].EventID, modifiedFields: [] });
                        } 
                        else {
                            this.props.openModal(response.data[0].ErrorMsg);
                        }
                    } 
                    else {                        
                        this.props.openModal('Oops! Something went wrong. Please try again in a minute.');
                    }
                });
        } 
        else {
            // missing data
            this.props.openModal('Please fill in all required fields.\n\nMissing the following:\n\n' + missingFields);
        }
    }

    /**
     * main render function
     */
    render() {

        return (
            <div id={'create-event'}>

                {this.state.eventId && this.state.redirect && <Redirect push exact to={`/event?id=${this.state.eventId}`}/>}

                <Container>
                        
                    <h1 className={'main-title'}>{this.state.editing && this.state.eventId ? 'EDIT EVENT DETAILS' : 'CREATE AN EVENT'}</h1>

                    <BackLink />

                    <div id={'create-event-form'} className={'ui form'}>    
                        <Grid>
                            <Grid.Row columns={window.mobileCheck() ? 1 : 2}>
                                <Grid.Column>
                                    <Form.Input 
                                        value={this.state.eventName}
                                        className={this.state.missingFields.length > 0 && !this.state.eventName ? 'missing-field' : ''}
                                        onChange={this.handleInputChange}
                                        limit={40}
                                        name='eventName'
                                        label='EVENT NAME *' 
                                        placeholder={'What\'s your event name? (40 characters or less)'}/>
                                    <Form.Select
                                        name='eventType'
                                        fluid
                                        search
                                        label='EVENT TYPE *'
                                        options={this.state.eventTypeOptions}
                                        value={this.state.eventType}
                                        placeholder='Select an event type'
                                        onChange={this.handleInputChange}
                                        className={(this.state.missingFields.length > 0 && !this.state.startTime) ? 'missing-field' : ''}/>
                                    <ChooseOrganization
                                        value={this.state.organizationId}
                                        callback={this.handleInputChange}
                                        name='organizationId'
                                        className={this.state.missingFields.length > 0 && !this.state.organizationId ? 'missing-field' : ''}/>
                                    <ChooseAttendanceType 
                                        name='attendanceTypeId'
                                        callback={this.handleInputChange}
                                        value={this.state.attendanceTypeId} 
                                        className={this.state.missingFields.length > 0 && !this.state.attendanceTypeId ? 'missing-field' : ''}/>
                                    <label><strong>LOCATION DETAILS:</strong></label>
                                    <Segment>
                                        <Form.Input
                                            value={this.state.locationName}
                                            onChange={this.handleInputChange}
                                            name='locationName' 
                                            label='NAME *' 
                                            className={this.state.missingFields.length > 0 && !this.state.locationName ? 'missing-field' : ''}
                                            placeholder={'Enter the name of location'}/> 
                                        {
                                            this.state.attendanceTypeId === 3 ?
                                                <div>
                                                    <Form.Input 
                                                        value={this.state.locationAddress1} 
                                                        onChange={this.handleInputChange} 
                                                        name='locationAddress1' 
                                                        label='ADDRESS 1' 
                                                        placeholder={'i.e. 123 Sesame Street.'}/> 
                                                    <Form.Input 
                                                        value={this.state.locationAddress2} 
                                                        onChange={this.handleInputChange} 
                                                        name='locationAddress2' 
                                                        label='ADDRESS 2' 
                                                        placeholder={'i.e. Suite 106'}/> 
                                                    <Form.Input 
                                                        value={this.state.locationCity} 
                                                        onChange={this.handleInputChange} 
                                                        name='locationCity' 
                                                        label='CITY' 
                                                        placeholder={'Enter city'}/> 
                                                    <Form.Input 
                                                        value={this.state.locationState} 
                                                        onChange={this.handleInputChange} 
                                                        name='locationState' 
                                                        label='STATE' 
                                                        placeholder={'Enter state'}/> 
                                                    <Form.Input 
                                                        value={this.state.locationZip} 
                                                        onChange={this.handleInputChange} 
                                                        name='locationZip' 
                                                        label='ZIPCODE' 
                                                        placeholder={'Enter zipcode'}/> <br/>
                                                </div> 
                                                : 
                                                <div>
                                                    <Form.Input 
                                                        value={this.state.virtualMeetingLink} 
                                                        onChange={this.handleInputChange} 
                                                        name='virtualMeetingLink' 
                                                        label='VIRTUAL MEETING URL' 
                                                        placeholder={'i.e. https://us02web.zoom.us/j/...'}/><br/>
                                                </div>
                                        }
                                        <Form.TextArea
                                            value={this.state.eventAccessInstructions} 
                                            onChange={this.handleInputChange}
                                            name='eventAccessInstructions' 
                                            label='ACCESS' 
                                            placeholder='Help people to access the location. Will there be parking? Is there a Zoom meeting access code?' />
                                    </Segment>
                                    <Form.Group widths='equal'>
                                        <Form.Field className={this.state.missingFields.length > 0 && !this.state.attendanceTypeId ? 'missing-field' : ''}>
                                            <label>START DATE *</label>
                                            <DatePicker 
                                                name='startDate'
                                                value={this.state.startDate}
                                                callback={this.handleInputChange}/>
                                        </Form.Field>
                                        <Form.Field className={this.state.missingFields.length > 0 && !this.state.attendanceTypeId ? 'missing-field' : ''}>
                                            <label>END DATE * </label>
                                            <DatePicker 
                                                name='endDate'
                                                value={this.state.endDate}
                                                callback={this.handleInputChange}/>
                                        </Form.Field>
                                    </Form.Group>
                                    <Form.Group widths='equal'>
                                        <Form.Select
                                            name='startTime'
                                            fluid
                                            search
                                            label='START TIME *'
                                            options={TIME_OPTIONS}
                                            value={this.state.startTime}
                                            placeholder='Select time'
                                            onChange={this.handleInputChange}
                                            className={(this.state.missingFields.length > 0 && !this.state.startTime) ? 'missing-field' : ''}
                                        />
                                        <Form.Select
                                            name='endTime'
                                            fluid
                                            search
                                            label='END TIME *'
                                            options={TIME_OPTIONS}
                                            value={this.state.endTime}
                                            placeholder='Select time'
                                            onChange={this.handleInputChange}
                                            className={this.state.missingFields.length && !this.state.endTime > 0 ? 'missing-field' : ''}
                                        />
                                    </Form.Group>
                                    <label><strong>CONTACT PERSON DETAILS:</strong></label>
                                    <Segment>
                                        <Form.Input 
                                            value={this.state.contactPersonName}
                                            onChange={this.handleInputChange}
                                            name='contactPersonName' 
                                            label='NAME *' 
                                            placeholder={'Enter contact person name'}
                                            className={this.state.missingFields.length > 0 && !this.state.contactPersonName ? 'missing-field' : ''}/> 
                                        <Form.Input 
                                            value={this.state.contactPersonEmail}
                                            onChange={this.handleInputChange}
                                            name='contactPersonEmail' 
                                            label='EMAIL *'
                                            placeholder={'Enter contact person email'}
                                            className={this.state.missingFields.length > 0 && !this.state.contactPersonEmail ? 'missing-field' : ''}/> 
                                        <PhoneInput 
                                            value={this.state.contactPersonPhone}
                                            onChange={this.handleInputChange}
                                            name='contactPersonPhone' 
                                            label='PHONE (XXX)-XXX-XXXX' 
                                            placeholder={'Enter contact person phone'}/>
                                            
                                        <Grid className={'toggle-settings-table'}>
                                            <Grid.Row className={'setting-row'}>
                                                <Grid.Column width={12}>
                                                    Receive emails about volunteer registrations
                                                </Grid.Column>
                                                <Grid.Column width={4} textAlign={'right'}>
                                                    <SBox name='receiveEmails' value={this.state.receiveEmails} callback={this.handleInputChange}/>
                                                </Grid.Column>
                                            </Grid.Row>
                                        </Grid>
                                    </Segment>
                                </Grid.Column>

                                <Grid.Column>
                                
                                    <div className='mobile'/>
                                        <ImageUpload
                                            imagetype='EVENT' 
                                            image={this.state.imageUrl} 
                                            center={this.state.imageCenter}
                                            handleInputChange={this.handleInputChange} />
                                    <div/>

                                    <Form.TextArea
                                        value={this.state.description}
                                        onChange={this.handleInputChange}
                                        name='description' 
                                        label='DESCRIPTION' 
                                        placeholder='Describe this event' />

                                    <Form.Input
                                        value={this.state.externalSignupLink}
                                        onChange={this.handleInputChange}
                                        name='externalSignupLink' 
                                        label='EXTERNAL SIGNUP LINK' 
                                        placeholder={'Enter a link if signup is also required on the organization\'s site'}/>

                                    <Form.TextArea
                                        value={this.state.nonInstitutionalMessage}
                                        onChange={this.handleInputChange}
                                        name='nonInstitutionalMessage' 
                                        label='INSTRUCTIONS FOR NON-INSTITUTION ATTENDEES' 
                                        placeholder='Write special instructions here for registration of non-UH attendees.' />
                                    
                                    <Form.Input
                                        value={this.state.itemsToBring}
                                        onChange={this.handleInputChange}
                                        name='itemsToBring' 
                                        label='WHAT TO BRING' 
                                        placeholder={'Let people know what they should bring'}/>
                                    <Form.Input
                                        value={this.state.foodProvided}
                                        onChange={this.handleInputChange}
                                        name='foodProvided' 
                                        label='FOOD PROVIDED' 
                                        placeholder={'Will there be food?'}/>
                                    <Grid>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                <h1 className='field-header'>ATTENDEE LIMIT?</h1>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <Form.Select
                                                    options={[{ key: 'yes', text: 'yes', value: true },{ key: 'no', text: 'no', value: false }]}
                                                    value={this.state.limitNumAttendees}
                                                    name='limitNumAttendees'
                                                    onChange={this.handleInputChange}
                                                    fluid />
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                {
                                                    this.state.limitNumAttendees ? 
                                                        <h1 className='field-header'>SET LIMIT</h1> 
                                                        :
                                                        <Form.Field disabled><h1 className='field-header'>SET LIMIT</h1></Form.Field> 
                                                }
                                            </Grid.Column>
                                            <Grid.Column>
                                                {
                                                    this.state.limitNumAttendees ? 
                                                        <Form.Input 
                                                            value={this.state.attendeeLimit}
                                                            onChange={this.handleInputChange}
                                                            name='attendeeLimit' 
                                                            className={
                                                                        (this.state.missingFields.length > 0 && 
                                                                        this.state.limitNumAttendees && 
                                                                        this.state.attendeeLimit && 
                                                                        (isNaN(this.state.attendeeLimit) || this.state.attendeeLimit.indexOf(".") != -1)) ? 
                                                                            'missing-field' 
                                                                            : 
                                                                            ''
                                                                      }
                                                            fluid/> 
                                                        :
                                                        <Form.Input value={this.state.attendeeLimit} fluid disabled/>  
                                                }
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>

                                    <Grid className={'toggle-settings-table'}>
                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12} className={!this.state.limitNumAttendees && 'disabled-field'}>
                                                Allow Student to Waitlist if event is full
                                            </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowWaitlist' value={this.state.allowWaitlist} callback={this.handleInputChange} disabled={!this.state.limitNumAttendees}/>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>

                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row columns={2}>
                                <Grid.Column/>
                                <Grid.Column className='controls'>
                                    <Button size={'massive'} className={'submit-button'} onClick={this.postevent.bind(this)}>POST EVENT</Button>
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                        
                    </div>
                </Container>
                <LeaveConfirm shouldBlockNavigation={this.doChangesExist()}/>
            </div>
        );
    }
}

export default EventDetailsContainer(ModalContainer(CreateEventContainer(CreateEvent)));
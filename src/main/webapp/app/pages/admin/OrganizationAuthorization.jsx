import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon, Container, Dropdown, Loader } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';
import OrganizationListContainer from '../../redux/containers/organization.js';
import EventListContainer from '../../redux/containers/events-list.js';
import comboboxContainer from '../../redux/containers/combobox.js';
import AuthorizationToolbar from '../../components/authorization/AuthorizationToolbar.jsx';
import AuthorizationOrganization from '../../components/authorization/AuthorizationOrganization.jsx';

import '../../css/authorization.scss';
import IntervalHelper from '../../util/interval-helper.js';

/**
 * Component for rendering the page to authorize/deauthorize organization
 */
const OrganizationAuthorization = (props) => {
    const [organizationList, setOrganizationList] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [filterStatus, setFilterStatus] = useState(-1);
    let refreshOrganizationsInterval = null;

    /**
     * When organization list is not null anymore and repopulated, finish the loader
     */
    useEffect(() => {
        if (organizationList) {
            setIsLoading(false);
        }
    }, [organizationList]);

    /**
     * Refresh the organization list when the filterStatus is updated
     */
    useEffect(() => {
        refreshOrganizationsInterval = setInterval(() => grabOrganizationsHelper(true), 300000);
        IntervalHelper.addInterval(refreshOrganizationsInterval);
        grabOrganizationsHelper();

        return () => IntervalHelper.removeInterval(refreshOrganizationsInterval);
    }, [filterStatus]);
    
    /**
     * Grabs the list of organizations that are currently marked as unauthorized, denoted by null being the indicator
     */
    const grabUnauthorizedOrganizationsList = () => {
        props.viewUnauthorizedOrganizations({}).payload
                                                .then((response) => setOrganizationList(response.data))
                                                .catch(() => IntervalHelper.removeInterval(refreshOrganizationsInterval));
    };

    /**
     * Grabs the list of organizations that are currently marked as authorized, denoted by 1 (or true) being the indicator
     */
    const grabAuthorizedOrganizationsList = () => {
        props.viewAuthorizedOrganizations({}).payload
                                                .then((response) => setOrganizationList(response.data))
                                                .catch(() => IntervalHelper.removeInterval(refreshOrganizationsInterval));
    };

    /**
     * Grabs the list of organizations that are currently marked as rejected, denoted by 0 (or false) being the indicator
     */
    const grabRejectedOrganizationsList = () => {
        props.viewRejectedOrganizations({}).payload
                                            .then((response) => setOrganizationList(response.data))
                                            .catch(() => IntervalHelper.removeInterval(refreshOrganizationsInterval));
    };

    /**
     * Updates the state to properly store the selected filter option
     * @param {*} event raw data of the event triggered 
     * @param {*} data filtered event trigger data (property used data.value)
     */
    const handleFilterChange = (event, data) => {
        setFilterStatus(data.value);
    };

    /**
     * This function calls a specific organization grabber function based on the filterStatus state
     * @param {boolean} initNumbers true to call the procs to fetch event/org unauthorized numbers, false otherwise
     */
    const grabOrganizationsHelper = (ignoreInitNumbers) => {
        setIsLoading(true);

        if (!ignoreInitNumbers) {
            checkUnauthorizedNumbers();
        }

        switch (filterStatus) {
            case 0:
                grabAuthorizedOrganizationsList();
                break;
            case 1:
                grabRejectedOrganizationsList();
                break;
            default:
                grabUnauthorizedOrganizationsList();
                break;
        }
    };

    /**
     * Checks the number of unauthorized events/organizations to update the displayed number
     */
    const checkUnauthorizedNumbers = () => {
        const EVENTS_PROMISE = props.viewNumOfUnauthorizedEvents().payload;
        const ORGANIZATIONS_PROMISE = props.getNumOfUnauthorizedOrganizations().then();

        Promise.all([EVENTS_PROMISE, ORGANIZATIONS_PROMISE])
                .then(responses => {
                    props.setNumOfUnauthorizedEvents(responses[0].data[0].NumUnauthorizedEvents);
                    props.setNumOfUnauthorizedOrganizations(responses[1].data[0].NumInactiveOrganizations);
                })
                .catch(() => IntervalHelper.removeInterval(refreshOrganizationsInterval));
    };

    const FILTER_OPTIONS = [
        {
            key: 'unprocessed',
            value: -1,
            text: 'Unprocessed',
        },
        {
            key: 'accepted',
            value: 0,
            text: 'Accepted',
        },
        {
            key: 'rejected',
            value: 1,
            text: 'Rejected',
        },
    ];
    
    /**
     * main HTML section
     */
    return (
        <div id={'authorization-details'}>
            <AuthorizationToolbar/>
            <div className={'admin-button-container'}>
                <Link className={'big-home-button top right'} exact to='/create-organization'>
                    <Button exact to='/' icon labelPosition='left'>
                        New Organization
                        <Icon name='plus'/>
                    </Button>
                </Link>
            </div>
            <div>
                <Container className='admin-homepage-header'>
                    <div className={'admin-main-title'}>Authorize Organizations</div>
                    <Link exact to='/'>
                        <div>My USERVE Organizations</div>
                    </Link>
                </Container>
            </div>
            <div className='actions-toolbar'>
                <Container>
                    <div>
                        Filter:
                        <Dropdown
                            placeholder='Select status'
                            selection
                            defaultValue={-1}
                            options={FILTER_OPTIONS}
                            onChange={handleFilterChange}
                        />
                    </div>
                </Container>
            </div>
            {
                isLoading ?
                    <Loader active/>
                    :
                    <div className='unauthorized-event-list'>
                        <Container>
                            {
                                organizationList?.length > 0 ?
                                    organizationList.map((organization, index) => 
                                                            <AuthorizationOrganization 
                                                                key={organization.organizationID}
                                                                organization={organization} 
                                                                index={index} 
                                                                grabOrganizations={grabOrganizationsHelper}
                                                                filterStatus={filterStatus}/>)
                                    :
                                    <div className='no-events'>No organizations found</div>
                            }
                        </Container>
                    </div>
            }
        </div>
    );
};

export default EventListContainer(ModalContainer(OrganizationListContainer(comboboxContainer(OrganizationAuthorization))));
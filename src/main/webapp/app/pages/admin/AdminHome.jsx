import React from 'react';
import { Container, Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import OrganizationsList from '../OrganizationsList.jsx';
import RootContainer from '../../redux/containers/app.js';
import AuthorizationToolbar from '../../components/authorization/AuthorizationToolbar.jsx';

import '../../css/authorization.scss';
import '../../css/home.scss';

/**
 * Component for rendering the admin homepage
 */
const AdminHome = (props) => {

  /**
   * Main Render
   */
  return (
      <div id={'homepage'}>
        {
          props.session.siteAdmin && 
            <AuthorizationToolbar/>
        }
        <div className={'admin-button-container'}>
          <Link className={'big-home-button top right'} exact='true' to='/create-organization'>
            <Button exact='true' to='/' icon labelPosition='left'>
              New Organization
              <Icon name='plus'/>
            </Button>
          </Link>
        </div>
        <div>
          <Container className='admin-homepage-header'>
            <h3 className={'admin-main-title'}>MY USERVE ORGANIZATIONS</h3>
          </Container>
          {
            (props.session.orgAdmin || props.session.siteAdmin) ? 
              <OrganizationsList viewOwnOrgs={true}/>
              :
              <h3 className='orgless-admin-header'>Please click the New Organization button to create a Volunteer Organization, your submission will be reviewed by an admin</h3>
          } 
        </div>
      </div>
  );
};

export default RootContainer(AdminHome);
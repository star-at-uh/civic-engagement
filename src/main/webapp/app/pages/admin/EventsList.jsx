import React from 'react';
import { Tab, Button, Icon, Loader } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import EventPreview from '../../components/events-list/EventPreview.jsx';
import EventsListContainer from '../../redux/containers/events-list.js';

import '../../css/events-list.scss';
import { sortEvents, markOngoingEvents } from '../../util/event-helper.js';
import { getServerTime } from '../../util/TimeOptions.js';

/**
 * Component for rendering the my events page
 */
class EventsList extends React.Component {

  /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
    super(props);
    this.state = {
      upcomingEvents: [],
      pastEvents: [],
      maxUpcomingEventDisplayInit: 4,
      maxPastEventDisplayInit: 4,
      organizationName: '',
      orgId: null,
      activeTabIndex: 0,
      isLoading: true,
    }

    this.memorizeTab = this.memorizeTab.bind(this);
  }

  /**
   * fires on component load
   */
  componentDidMount() {
    const URL = this.props.location.search;
    const URL_PARAMS = queryString.parse(URL); 

    const PARAMS = {
      params: {
        orgId: URL_PARAMS.orgId
      }
    };

    const MEMORIZED_TAB_DATA = sessionStorage.getItem('eventlist-selectedTab');
    let activeTabIndex = 0;
    if (MEMORIZED_TAB_DATA) {
      const PARSED_TAB_DATA = MEMORIZED_TAB_DATA.split('!');

      // if no org id (accessed from top right menu) it will be undefined, but also check that the entire thing actually exists
      if (PARSED_TAB_DATA[0] === URL_PARAMS.orgId || (PARSED_TAB_DATA[0] === 'undefined' && PARSED_TAB_DATA[1])) {
        activeTabIndex = parseInt(PARSED_TAB_DATA[1]);
      }
    }

    // Load events
    const TIME_PROMISE = getServerTime();
    const UPCOMING_EVENTS_PROMISE = this.props.viewUpcomingEvents(PARAMS).payload;
    const PAST_EVENTS_PROMISE = this.props.viewPastEvents(PARAMS).payload;

    Promise.all([UPCOMING_EVENTS_PROMISE, PAST_EVENTS_PROMISE, TIME_PROMISE])
            .then(values => {
              sortEvents(values[0].data, 'EventStartDateTime', false, 'date');
              sortEvents(values[1].data, 'EventStartDateTime', false, 'date');
              markOngoingEvents(values[0].data, 'EventStartDateTime', 'EventEndDateTime', values[2].data);

              this.setState({ upcomingEvents: values[0].data, pastEvents: values[1].data, isLoading: false });
            });

    this.setState({organizationName: URL_PARAMS.name, orgId: URL_PARAMS.orgId, activeTabIndex});
  }

  memorizeTab(event, data) {
    sessionStorage.setItem('eventlist-selectedTab', `${this.state.orgId}!${data.activeIndex}`);
    this.setState({activeTabIndex: data.activeIndex});
  }

  /**
   * main render function
   */
  render() {

    // init upcoming events lists
    const { upcomingEvents, pastEvents } = this.state;
    const PANES = [
      {
        menuItem: 'Upcoming',
        render: () => {
          return (
            <Tab.Pane key='upcoming-tab'>
              {
                upcomingEvents && upcomingEvents.length > 0 ?
                  upcomingEvents.map((event) => <EventPreview key={event.EventID} data={event} />)
                  :
                  <div className='events-display-message'>
                    {
                      this.state.isLoading ?
                        <Loader active={true} inline='centered'>Loading Events</Loader>
                        :
                        <h4>No upcoming events</h4>
                    }
                  </div>
              }
            </Tab.Pane>
          );
        },
      },
      {
        menuItem: 'Past',
        render: () => {
          return (
            <Tab.Pane key='past-tab'>
              {
                pastEvents && pastEvents.length > 0 ?
                  pastEvents.map((event) => <EventPreview key={event.EventID} data={event} />)
                  :
                  <div className='events-display-message'>
                    {
                      this.state.isLoading ?
                        <Loader active={true} inline='centered'>Loading Events</Loader>
                        :
                        <h4>No past events</h4>
                    }
                  </div>
              }
            </Tab.Pane>
          );
        },
      },
    ];

    return (
      <div className={'eds-layout__body'} id={'events-list'}>
        <div className={'new-event-button'}>
          <Link exact='true' to={`/create-event?orgId=${this.state.orgId}`} >
              <Button exact='true' to='/'>
                  <Icon name='plus'/>
                  New Event
              </Button>
          </Link>
        </div>
        <h1>{!this.state.organizationName ? 'My Events' : this.state.organizationName}</h1>
        <Tab className={'eventlist-tabs'} menu={{ secondary: true, pointing: true }} panes={PANES} activeIndex={this.state.activeTabIndex} onTabChange={this.memorizeTab}/>
      </div>
    );
  }
}

export default EventsListContainer(EventsList);
import React from 'react';
import _ from 'lodash';
import queryString from 'query-string';
import ServiceHoursContainer from '../../redux/containers/service-hours.js';
import ModalContainer from '../../redux/containers/modal.js';
import { Redirect, Link } from 'react-router-dom';
import TimeSheetLog from '../timesheet/TimeSheetLog.jsx';
import CloakInOutLog from '../timesheet/ClockInOutLog.jsx';

import '../../css/time-table.scss';
import '../../css/service-hours.scss';
import BackLink from '../../components/nav/BackLink.jsx';

/**
 * Rendering a log the the event's/student's service hours
 */
class ServiceHours extends React.Component {

  /**
   * create the component's initial state
   */
  constructor(props) {
    super(props);
    const URL = this.props.location.search;
    const PARAMS = queryString.parse(URL);

    this.state = {
      redirectURL: null,
      eventId: PARAMS.id,
      eventName: PARAMS.name,
    }
  }

  /**
   * Main renderer
   */
  render() {
    const { eventId, eventName } = this.state;
    const { viewServiceHoursRoster } = this.props;
    const IS_ADVISOR = window.appConfig.getAttribute('data') === 'advisor';

    return (
      <div id={'service-hours'} className='eds-layout__body'>

        {/* student progress individual redirect */
          this.state.redirectURL && <Redirect push exact to={this.state.redirectURL} />
        }
        <div className='upper-container'>
          <BackLink/>
          <span className='title'>
              Service Hours:
          </span>
          <span className='hour-adjust-link'>
              <i className="plus icon"></i>
              <Link to={`/clock-in-out?id=${ eventId }`}> Add Service hours for a Student</Link>
          </span>
          {
            eventName &&
              <h3>
                Event Name: <span className='event-name'>{eventName}</span> 
              </h3>
          }
        </div>

        <div id='student-progress-hours' className='time-table'>
            <CloakInOutLog eventId={eventId} viewServiceHoursRoster={viewServiceHoursRoster}/>

            <div className='or-header'>
              - OR -
            </div>

            <TimeSheetLog eventId={eventId} adminView={IS_ADVISOR}/>
        </div>
      </div>
    );
  }
}

export default ModalContainer(ServiceHoursContainer(ServiceHours));
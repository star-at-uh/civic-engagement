import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Form, Button, Segment, Grid, Container, Popup } from 'semantic-ui-react';
import queryString from 'query-string';
import SBox from '../../components/global/SBox.jsx';
import OrganizationContainer from '../../redux/containers/organization.js';
import RootContainer from '../../redux/containers/app.js';
import ModalContainer from '../../redux/containers/modal.js';
import BackLink from '../../components/nav/BackLink.jsx';
import ImageUpload from '../../components/global/ImageUpload.jsx';
import ChooseOrgLabels from '../../components/create-organization/ChooseOrgLabels.jsx';
import AdminList from '../../components/create-organization/AdminList.jsx';
import PhoneInput from '../../components/inputs/PhoneInput.jsx';
import LeaveConfirm from '../../components/global/LeaveConfirm.jsx';

import '../../css/settings.scss';
import '../../css/create-organization.scss';

/**
 * Component for rendering the "Create an Organization" form
 */
class CreateOrganization extends React.Component {

    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            organizationId: null,
            organizationName: '',
            organizationLabels: [],
            locationAddress1: '',
            locationAddress2: '',
            locationCity: '',
            locationState: '',
            locationZip: '',
            contactPersonName: '',
            contactPersonEmail: '',
            contactPersonPhone: '',
            imageName: '',
            imageBlob: null,
            imageUrl: '',
            imageChanged: false,
            description: '',
            webPageURL: '',
            allowInPerson: false,
            allowOnline: false,
            allowHybrid: false,
            allowIndependent: false,
            allowVolunteers: false,
            isOrgActive: false,
            redirect: false,
            // Adding admin fields
            createUHAdmin: 'uh-admin',
            isAddingAdmin: true,
            nonUHAdmins: [],
            UHAdmins: [],
            adminFirstName: '',
            adminLastName: '',
            adminUserId: '',
            createNonUHUserCounter: 0,
            copyOfInitialState: {},
            modifiedFields: [],
            fieldsToIgnore: {
                adminFirstName: true,
                adminLastName: true,
                adminUserId: true,
                createNonUHUserCounter: true,
                createUHAdmin: true,
                isAddingAdmin: true,
                nonUHAdmins: true,
                UHAdmins: true,
            }
        };

        this.handleInputChange = this.handleInputChange.bind(this);
        this.getAdminList = this.getAdminList.bind(this);
        this.clearInputs = this.clearInputs.bind(this);
        this.handleDeleteAdmin = this.handleDeleteAdmin.bind(this);
        this.doChangesExist = this.doChangesExist.bind(this);
        this.checkIfCanDeleteAdmins = this.checkIfCanDeleteAdmins.bind(this);
        this.didTriggerFieldsChange = this.didTriggerFieldsChange.bind(this);
        this.createOrganization = this.createOrganization.bind(this);
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount() {
        let url = this.props.location.search;
        let params = queryString.parse(url);
        if (params.id && window.location.href.indexOf('edit-organization') > -1) {
            this.setState({ editing: true, organizationId: params.id }, this.populate.bind(this));
        }
        else {
            this.props.getUserEmail().payload.then(response => {
                this.setState({contactPersonEmail: response.data.email, copyOfInitialState: this.state});
            });
        }
    }

    /**
     * Updates the state that manages inputs
     * @param {*} event 
     * @param {*} data 
     */
    handleInputChange(event, data) {
        // Dropdowns and textareas have different ways they format the event param (textarea -> event.target.value | dropdowns -> event.value)
        // For any old value not coming from either a dropdown or a text area you can just pass the value
        const NEW_STATE = {};
        
        if (data.type === 'checkbox') {
            NEW_STATE[data.name] = data.checked;
        }
        else {
            NEW_STATE[data.name] = data.target?.value ?? data.value;
        }

        const MODIFIED_FIELDS = this.state.modifiedFields;

        if (!this.state.fieldsToIgnore[data.name] && !MODIFIED_FIELDS.includes(data.name)) {
            MODIFIED_FIELDS.push(data.name);
            NEW_STATE.modifiedFields = MODIFIED_FIELDS;
        }

        this.setState(NEW_STATE);
    }

    // populate organization page for edit
    populate() {
        this.props.organizationDetails({ organizationId: this.state.organizationId }).then((response) => {
            if (response.data[0].OrganizationName) {
                const ORGANIZATION_DETAILS = response.data[0];
                const NEW_STATE = {
                    organizationName: ORGANIZATION_DETAILS.OrganizationName,
                    locationAddress1: ORGANIZATION_DETAILS.OrganizationAddressLine1,
                    locationAddress2: ORGANIZATION_DETAILS.OrganizationAddressLine2,
                    locationCity: ORGANIZATION_DETAILS.OrganizationAddressCity,
                    locationState: ORGANIZATION_DETAILS.OrganizationAddressState,
                    locationZip: ORGANIZATION_DETAILS.OrganizationAddressZipCode,
                    description: ORGANIZATION_DETAILS.OrganizationDescription,
                    contactPersonName: ORGANIZATION_DETAILS.OrganizationContactPersonFullName,
                    contactPersonEmail: ORGANIZATION_DETAILS.OrganizationEmailAddress,
                    contactPersonPhone: ORGANIZATION_DETAILS.OrganizationPhoneNumber,
                    webPageURL: ORGANIZATION_DETAILS.OrganizationWebpageURL,
                    imageUrl: ORGANIZATION_DETAILS.OrganizationImageURL,
                    imageCenter: ORGANIZATION_DETAILS.OrganizationImageCenterPosition,
                    allowInPerson: ORGANIZATION_DETAILS.AllowInPerson,
                    allowOnline: ORGANIZATION_DETAILS.AllowOnline,
                    allowHybrid: ORGANIZATION_DETAILS.AllowHybrid,
                    allowIndependent: ORGANIZATION_DETAILS.AllowIndependent,
                    allowVolunteers: ORGANIZATION_DETAILS.OrganizationNotAcceptingVolunteers,
                    organizationLabels: ORGANIZATION_DETAILS.OrganizationLabelIDs?.split('//').map((idGroup) => {
                        return parseInt(idGroup.split('||')[1])
                    }),
                    isOrgActive: !ORGANIZATION_DETAILS.OrganizationIsInActive,
                };

                NEW_STATE.copyOfInitialState = NEW_STATE;

                if (!ORGANIZATION_DETAILS.IsPartOfOrganization) {
                    this.props.forceConfirm('You do not have permission to edit this organization');
                    window.location.href = `${window.location.href.split('/app/#')[0]}/app/#`;
                }
                else {
                    this.setState(NEW_STATE);
                    this.getAdminList();
                }
            } else {
                this.props.openModal(response.data[0].ErrorMsg);
            }
        });

    }

    /**
     * Grabs the list of admins for the current organization
     */
    getAdminList() {
        this.props.organizationAdmins({
                      params: {
                          orgId: this.state.organizationId 
                      } 
                  })
                  .then(response => {
                      const UH_ADMIN_LIST = [];
                      const NON_UH_LIST = [];
                      
                      response.data.forEach(user => {
                          if (user.is_Affiliated) {
                              UH_ADMIN_LIST.push(user);
                          }
                          else {
                              NON_UH_LIST.push(user);
                          }
                      });

                      this.setState({UHAdmins: UH_ADMIN_LIST, nonUHAdmins: NON_UH_LIST, isAddingAdmin: UH_ADMIN_LIST.length < 1 && NON_UH_LIST < 1});
                  });
    }

    /**
     * Adds an admin to the organization
     */
    addAdmin() {
        const EMAIL_DOMAIN = this.state.adminUserId.split('@');

        if (this.state.adminUserId.length < 1 || (this.state.createUHAdmin === 'non-uh-admin' && (this.state.adminFirstName.length < 1 || this.state.adminLastName.length < 1))) {
            this.props.openModal('Please fill out all the admin fields');
        }
        else if (this.state.createUHAdmin === 'non-uh-admin' && !(EMAIL_DOMAIN[EMAIL_DOMAIN.length - 1] === 'hawaii.edu' || EMAIL_DOMAIN[EMAIL_DOMAIN.length - 1] === 'gmail.com')) {
            this.props.openModal('Coordinator email must be a UH email or Gmail');
        }
        else {
            const IS_CREATE_UH = this.state.createUHAdmin === 'uh-admin';
            // Retrieve List to add to
            const NEW_ADMINS_LIST = IS_CREATE_UH ? this.state.UHAdmins : this.state.nonUHAdmins;

            if (IS_CREATE_UH) {
                // UH CASE
                this.props.getUserInfo({
                              params: {
                                  userId: this.state.adminUserId
                              }
                          })
                          .then((response) => {
                              const DATA = response.data;
                              if (DATA.status) {
                                  NEW_ADMINS_LIST.push({
                                      FirstName: DATA.results.firstName,
                                      Username: DATA.results.username,
                                      LastName: DATA.results.lastName,
                                      UserID: DATA.results.pidm,
                                      isNew: true, // Used to help prevent redundant calls later
                                      isDeleting: false, // To ensure this value is false by default
                                      is_Affiliated: true, // Name is to match DB output name
                                  });

                                  this.setState({UHAdmins: NEW_ADMINS_LIST}, () => this.clearInputs());
                              }
                              else {
                                  this.props.openModal(DATA.errorMsg);
                              }
                          });
            }
            else {
                // Non UH Case
                NEW_ADMINS_LIST.push({
                    EmailAddress: this.state.adminUserId,
                    FirstName: this.state.adminFirstName,
                    LastName: this.state.adminLastName,
                    UserID: this.state.createNonUHUserCounter,
                    isNew: true, // Used to help prevent redundant calls later, not foolproof but it'll help
                    isDeleting: false, // To ensure this value is false by default
                    is_Affiliated: false,
                });

                this.setState({
                    nonUHAdmins: NEW_ADMINS_LIST,
                    createNonUHUserCounter: this.state.createNonUHUserCounter + 1,
                }, () => this.clearInputs());
            }
        }
    }

    /**
     * Clears the inputs for the adding admins section
     */
    clearInputs() {
        this.setState({
            adminUserId: '',
            adminFirstName: '',
            adminLastName: ''
        });
    }

    /**
     * Marks preexisting admins for deletion and removes new admins
     * @param {Object} user the user to be deleted
     */
    handleDeleteAdmin(user) {
        const IS_AFFILIATED = user.is_Affiliated;
        const USER_INFO_TYPE = IS_AFFILIATED ? 'UserID' : 'EmailAddress';
        const ADMIN_LIST = IS_AFFILIATED ? this.state.UHAdmins : this.state.nonUHAdmins;
        for (let i = 0; i < ADMIN_LIST.length; i++) {
            if (ADMIN_LIST[i][USER_INFO_TYPE] == user[USER_INFO_TYPE]) {
                ADMIN_LIST[i].isDeleting = !ADMIN_LIST[i].isDeleting;
            }
        }

        this.setState({[IS_AFFILIATED ? 'UHAdmins' : 'nonUHAdmins']: ADMIN_LIST});
    }

    /**
     * Checks both admins lists to see if there is only 1 non-deleted admin left to stop
     * organizations with no admins from happening
     * @return {boolean} true if there is still an admin that would exist, false otherwise
     */
    checkIfCanDeleteAdmins() {
        // Adding and deleting admins
        const ADMINS_MAX_LENGTH = Math.max(this.state.UHAdmins.length, this.state.nonUHAdmins.length);
        let numAdminRemaining = 0;

        if (!this.state.editing) {
            numAdminRemaining = 2; // Creating an organization
        }
        else if (this.state.UHAdmins.length + this.state.nonUHAdmins.length > 1) {
            for (let i = 0; i < ADMINS_MAX_LENGTH; i++) {
                if (i < this.state.UHAdmins.length && !this.state.UHAdmins[i].isDeleting) {
                    numAdminRemaining++;
                }
                if (i < this.state.nonUHAdmins.length && !this.state.nonUHAdmins[i].isDeleting) {
                    numAdminRemaining++;
                }
            }
        }

        return numAdminRemaining > 1;
    }

    /**
     * Checks if a field has been modified or there is a tentative admin change
     * @return {boolean} true if something has been modified or there is a pending admin change
     */
    doChangesExist() {
        const ADMINS_MAX_LENGTH = Math.max(this.state.UHAdmins.length, this.state.nonUHAdmins.length);
        let changesExist = false;

        this.state.modifiedFields.forEach(fieldName => {
            if (this.state.copyOfInitialState[fieldName] !== this.state[fieldName]) {
                changesExist = true;
            }
        });

        for (let i = 0; i < ADMINS_MAX_LENGTH; i++) {
            if (this.state.UHAdmins[i]?.isNew || this.state.UHAdmins[i]?.isDeleting || this.state.nonUHAdmins[i]?.isNew || this.state.nonUHAdmins[i]?.isDeleting) {
                changesExist = true;
            }
        }

        return changesExist;
    }

    /**
     * creates / edits organization
     */
    createOrganization(editConfirmed) {
        const STATUS_WILL_CHANGE = this.didTriggerFieldsChange(); 
        if (this.state.isOrgActive && this.state.editing && !editConfirmed && STATUS_WILL_CHANGE) {
            this.props.forceConfirm('This update will cause the organization to be set inactive and undergo review again.', () => this.createOrganization(true), true);
            return;
        }

        const ADDRESS_1 = this.state.locationAddress1 ?? null;
        const ADDRESS_2 = this.state.locationAddress2 ?? null;
        const CITY = this.state.locationCity ?? null;
        const STATE = this.state.locationState ?? null;
        const ZIP = this.state.locationZip ?? null;
        let attendanceTypeIds = [];
        if (this.state.allowInPerson) {
            attendanceTypeIds.push(3); // id=3
        } 
        if (this.state.allowOnline) {
            attendanceTypeIds.push(1); // id=1
        }
        if (this.state.allowHybrid) {
            attendanceTypeIds.push(2); // id=2
        } 
        if (this.state.allowIndependent) {
            attendanceTypeIds.push(4); // id=4
        } 
        attendanceTypeIds = attendanceTypeIds.join(',');

        let missingFields = '';
        const PHONE_REGEX = /^\(\d{3}\)-\d{3}-\d{4}$/;
        const ZIP_CODE_REGEX = /^([0-9]{5})$|^([0-9]{5}-[0-9]{4})$|^([0-9]{9})$/;
        const IMAGE_NAME_REGEX = /^[a-zA-Z_\d\-]+$/;

        missingFields += !this.state.organizationName ? `- Organization Name\n` : '';
        missingFields += (this.state.contactPersonPhone && !PHONE_REGEX.test(this.state.contactPersonPhone)) ? '- Phone number must be formatted as (###)-###-####.\n' : '';
        missingFields += (this.state.locationZip && !ZIP_CODE_REGEX.test(this.state.locationZip)) ? '- ZIP code must contain 5 or 9 digits and be formatted as 12345, 12345-6789, or 123456789.' : '';
        missingFields += (this.state.imageName && this.state.imageName.split('.').length > 2) ? '- Image file name should not contain a period outside of the extension\n' : '';
        missingFields += (this.state.imageName && !IMAGE_NAME_REGEX.test(this.state.imageName.split('.')[0])) ? '- Image file name can only contain dashes, hyphens, or alphanumeric characters\n' : '';

        // send it
        if (missingFields.length < 1) {
            let sendFunction;
            // choose f(x) to call
            if (this.state.editing && this.state.organizationId) {
                sendFunction = this.props.editOrganization;
            } 
            else {
                sendFunction = this.props.createOrganization;
            }

            const FORM = new FormData();
            if (this.state.imageBlob != null) {
                FORM.append('file', this.state.imageBlob, this.state.imageName);
            }
            FORM.append('params', JSON.stringify({
                organizationId: this.state.organizationId ? this.state.organizationId : null,
                organizationName: this.state.organizationName,
                locationAddress1: ADDRESS_1,
                locationAddress2: ADDRESS_2,
                locationCity: CITY,
                locationState: STATE,
                locationZip: ZIP,
                description: this.state.description,
                contactPersonName: this.state.contactPersonName,
                contactPersonEmail: this.state.contactPersonEmail,
                contactPersonPhone: this.state.contactPersonPhone,
                attendanceTypeIds,
                webPageURL: this.state.webPageURL,
                notAcceptingVolunteers: this.state.allowVolunteers,
                imageCenter: this.state.imageUrl ? parseFloat(this.state.imageCenter) : null,
                imageChanged: this.state.imageChanged,
                organizationLabels: this.state.organizationLabels?.join(',')
            }));

            sendFunction(FORM).then((response) => {
                if (response.data.length > 0) {
                    if (response.data[0]['Status'] == 1) {
                        const ORG_ID = response.data[0].OrganizationID ?? this.state.organizationId;

                        // Adding and deleting admins
                        const ADMINS_MAX_LENGTH = Math.max(this.state.UHAdmins.length, this.state.nonUHAdmins.length);

                        for (let i = 0; i < ADMINS_MAX_LENGTH; i++) {
                            if (i < this.state.UHAdmins.length && (this.state.UHAdmins[i].isNew || this.state.UHAdmins[i].isDeleting)) {
                                const PARAMS = {
                                    adminUser: this.state.UHAdmins[i].isDeleting ? this.state.UHAdmins[i].UserID : this.state.UHAdmins[i].Username,
                                    organizationId: ORG_ID,
                                    adminFirstName: this.state.UHAdmins[i].FirstName,
                                    adminLastName: this.state.UHAdmins[i].LastName,
                                };

                                if (this.state.UHAdmins[i].isDeleting && !this.state.UHAdmins[i].isNew) {
                                    this.props.deleteUHAdmin(PARAMS).then((response) => {
                                                                        if (response.data[0].ErrorMsg?.length > 0) {
                                                                            this.props.forceConfirm(response.data[0].ErrorMsg);
                                                                        }
                                                                        else if (i == ADMINS_MAX_LENGTH.length - 1) {
                                                                            this.getAdminList();
                                                                        }
                                                                    });
                                }
                                else if (!this.state.UHAdmins[i].isDeleting) {
                                    this.props.insertUHAdmin(PARAMS).then((response) => {
                                                                        if (response.data[0].ErrorMsg?.length > 0) {
                                                                            this.props.forceConfirm(response.data[0].ErrorMsg);
                                                                        }
                                                                        else if (i == ADMINS_MAX_LENGTH.length - 1) {
                                                                            this.getAdminList();
                                                                        }
                                                                    });
                                }
                            }
                            if (i < this.state.nonUHAdmins.length && (this.state.nonUHAdmins[i].isNew || this.state.nonUHAdmins[i].isDeleting)) {
                                const PARAMS = {
                                    adminUser: this.state.nonUHAdmins[i].EmailAddress,
                                    organizationId: ORG_ID,
                                    adminFirstName: this.state.nonUHAdmins[i].FirstName,
                                    adminLastName: this.state.nonUHAdmins[i].LastName,
                                };

                                if (this.state.nonUHAdmins[i].isDeleting && !this.state.nonUHAdmins[i].isNew) {
                                    this.props.deleteNonUHAdmin(PARAMS).then((response) => {
                                                                            if (response.data[0].ErrorMsg?.length > 0) {
                                                                                this.props.forceConfirm(response.data[0].ErrorMsg);
                                                                            }
                                                                            else if (i == ADMINS_MAX_LENGTH.length - 1) {
                                                                                this.getAdminList();
                                                                            }
                                                                        });
                                }
                                else if (!this.state.nonUHAdmins[i].isDeleting) { // It could be a new admin that they marked to delete, so no point calling proc
                                    this.props.insertNonUHAdmin(PARAMS).then((response) => {
                                                                            if (response.data[0].ErrorMsg?.length > 0) {
                                                                                this.props.forceConfirm(response.data[0].ErrorMsg);
                                                                            }
                                                                            else if (i == ADMINS_MAX_LENGTH.length - 1) {
                                                                                this.getAdminList();
                                                                            }
                                                                        });
                                }
                            }
                        }

                        let callback = null;

                        if (this.state.editing) {
                            callback = this.populate;
                            let confirmationWording = 'Organization successfully updated';

                            if (this.state.isOrgActive && STATUS_WILL_CHANGE) {
                                confirmationWording = 'Attention: Your organization will be set as inactive until the updates have been reviewed and approved.';
                            }
                            
                            this.props.forceConfirm(confirmationWording);
                        }
                        else {
                            if (!this.props.session.orgAdmin) {
                                // This callback is to recheck an orgless admin's permissions after they create an organization since they will be this org's admin
                                callback = () => {
                                    this.props.getSessionInfo()
                                                .payload
                                                .then((response) => {
                                                    this.props.setPermissions(response.data.organizationAdmin, response.data.siteAdmin);
                                                });
                                };
                            }
                            this.props.forceConfirm('Congratulations! You have successfully created an organization. However, please note that it is not yet active and requires approval from UH before it can become active.');
                        } 


                        this.setState({ 
                            modifiedFields: [],
                            redirect: this.state.editing ? false : true, 
                            organizationId: this.state.editing ? this.state.organizationId : response.data[0].OrganizationID,
                            displayUpdateMsg: this.state.editing, 
                        }, callback);
                    } else {
                        this.props.openModal(response.data[0].ErrorMsg);
                    }
                } else {
                    this.props.openModal('Oops! Something went wrong. Please try again in a minute.');
                }
            });

        } else {
            // missing data
            this.props.openModal('Please fill in all required fields.\n\nMissing the following:\n\n' + missingFields);
        }
    }

    /**
     * Checks if fields that would set an organization inactive were changed
     * and returns true if they were
     */
    didTriggerFieldsChange() {
        // These fields are key fields that will trigger an organization inactive again
        const TRIGGER_FIELDS = ['organizationName', 'contactPersonEmail', 'locationAddress1', 'locationAddress2', 
                                'locationCity', 'locationState', 'locationZip', 'contactPersonPhone', 'description',
                                'webPageURL', 'imageBlob'];
        
        return TRIGGER_FIELDS.some(field =>  this.state.copyOfInitialState[field] != this.state[field]);
    }

    /**
     * Calls the deleteOrganization action to delete the current
     * organization being looked at
     */
    deleteOrganization() {
        this.props.deleteOrganization({organizationId: this.state.organizationId})
                    .then(response => {
                        if (response.data[0].ErrorMsg.length > 0) {
                            this.props.openModal(response.data[0].ErrorMsg);
                        }
                        else {
                            this.props.forceConfirm('Organization successfully deleted');
                            this.setState({redirect: true});
                        }
                    });
    }

    /**
     * main render function
     */
    render() {

        return (
            <div id={'create-organization'}>

                {this.state.organizationId && this.state.redirect && <Redirect exact='true' push to={`/`} />}

                <Container>

                    <h1 className={'main-title'}>{this.state.editing && this.state.organizationId ? 'EDIT ORGANIZATION DETAILS' : 'CREATE AN ORGANIZATION'}</h1>
                    
                    <BackLink/>

                    <div className={'ui form'}>

                        <br />
                        <Grid>
                            <Grid.Row columns={window.mobileCheck() ? 1 : 2}>
                                <Grid.Column>
                                    <Form.Input 
                                        name='organizationName'
                                        label='ORGANIZATION NAME *'
                                        value={this.state.organizationName} 
                                        onChange={this.handleInputChange} 
                                        placeholder={'What\'s your organization name?'}/>
                                    <label><strong>TYPE OF ORGANIZATION:</strong></label>
                                    <ChooseOrgLabels key={this.state.organizationLabels} value={this.state.organizationLabels} name={'organizationLabels'} handleChange={this.handleInputChange}/>
                                    <br/>
                                    <Form.Input 
                                        name='locationAddress1' 
                                        label='ADDRESS 1' 
                                        value={this.state.locationAddress1}
                                        onChange={this.handleInputChange}
                                        placeholder={'i.e. 123 Sesame Street.'} />
                                    <Form.Input 
                                        name='locationAddress2' 
                                        label='ADDRESS 2' 
                                        value={this.state.locationAddress2}
                                        onChange={this.handleInputChange}
                                        placeholder={'i.e. Suite 106'} />
                                    <Form.Group widths='equal'>
                                        <Form.Input 
                                            name='locationCity'
                                            label='CITY' 
                                            value={this.state.locationCity} 
                                            onChange={this.handleInputChange} 
                                            placeholder={'Enter city'} />
                                        <Form.Input name='locationState' onChange={this.handleInputChange} value={this.state.locationState} label='STATE' placeholder={'Enter state'} />
                                    </Form.Group>
                                    <Form.Input 
                                        name='locationZip' 
                                        label='ZIPCODE' 
                                        value={this.state.locationZip} 
                                        onChange={this.handleInputChange} 
                                        placeholder={'Enter zipcode'} />
                                    <label><strong>CONTACT PERSON DETAILS:</strong></label>
                                    <Segment>
                                        <Form.Group widths='equal'>
                                            <Form.Input 
                                                name='contactPersonName' 
                                                value={this.state.contactPersonName} 
                                                onChange={this.handleInputChange}
                                                label='NAME' 
                                                placeholder={'Enter contact person name'} />
                                            <PhoneInput 
                                                name='contactPersonPhone' 
                                                value={this.state.contactPersonPhone} 
                                                onChange={this.handleInputChange} 
                                                label='PHONE (###)-###-####' 
                                                placeholder={'Enter contact person phone'} />
                                        </Form.Group>
                                        <Form.Input 
                                            name='contactPersonEmail' 
                                            value={this.state.contactPersonEmail} 
                                            onChange={this.handleInputChange} 
                                            label='EMAIL' 
                                            placeholder={'Enter contact person email'} />
                                    </Segment>
                                </Grid.Column>

                                <Grid.Column>
                                    
                                    <br className='mobile'/>
                                    <ImageUpload 
                                        imagetype='ORGANIZATION' 
                                        image={this.state.imageUrl} 
                                        center={this.state.imageCenter} 
                                        handleInputChange={this.handleInputChange}/>
                                    <br/>
                                    <Form.TextArea
                                        name='description'
                                        value={this.state.description}
                                        onChange={this.handleInputChange} 
                                        label='DESCRIPTION' 
                                        placeholder='Describe this organization' />
                                    <Form.Input 
                                        name='webPageURL'
                                        value={this.state.webPageURL} 
                                        onChange={this.handleInputChange} 
                                        label='WEBSITE' 
                                        placeholder={'Enter website URL'} />

                                    <br />

                                    <label><strong>TYPES OF EVENTS OFFERED</strong></label>
                                    <Grid className={'toggle-settings-table'}>
                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12}>
                                                In-Person
                                        </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowInPerson' value={this.state.allowInPerson} callback={this.handleInputChange} />
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12}>
                                                Online
                                        </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowOnline' value={this.state.allowOnline} callback={this.handleInputChange} />
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12}>
                                                Hybrid
                                        </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowHybrid' value={this.state.allowHybrid} callback={this.handleInputChange}/>
                                            </Grid.Column>
                                        </Grid.Row>

                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12}>
                                                Other
                                        </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowIndependent' value={this.state.allowIndependent} callback={this.handleInputChange}/>
                                            </Grid.Column>
                                        </Grid.Row>

                                    </Grid>

                                    <br />

                                    <Grid className={'toggle-settings-table'}>
                                        <label><strong>STATUS</strong></label>
                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12}>
                                                Not Accepting Volunteers
                                            </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowVolunteers' value={this.state.allowVolunteers} callback={this.handleInputChange}/>
                                            </Grid.Column>
                                        </Grid.Row>

                                        {
                                            this.state.editing &&
                                                <Popup 
                                                    content={
                                                        this.props.session.siteAdmin ? 
                                                            'Organization status can be changed in the Authorize Organizations page'
                                                            :
                                                            'This is the current approval status of the organization, only approved organizations are visible to users.'
                                                    }
                                                    position='top right'
                                                    trigger={
                                                        <Grid.Row className={'setting-row'}>
                                                            <Grid.Column width={12}>
                                                                Organization Status
                                                            </Grid.Column>
                                                            <Grid.Column width={4} textAlign={'right'}>
                                                                {this.state.isOrgActive ? 'Active' : 'Inactive'}
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                    }/>
                                        }
                                    </Grid>
                                    <div className='organization-admins'>
                                        <label>PLEASE INPUT YOUR ORGANIZATION COORDINATORS:</label>
                                        <Segment>
                                            {
                                                this.state.UHAdmins.length > 0 &&
                                                <AdminList adminList={this.state.UHAdmins} isUH={true} deleteCallback={(user) => this.handleDeleteAdmin(user)} canDelete={this.checkIfCanDeleteAdmins()}/>
                                            }
                                            {
                                                this.state.nonUHAdmins.length > 0 &&
                                                <AdminList adminList={this.state.nonUHAdmins} isUH={false} deleteCallback={(user) => this.handleDeleteAdmin(user)} canDelete={this.checkIfCanDeleteAdmins()}/>
                                            }
                                            {
                                                this.state.isAddingAdmin ?
                                                    <div className='add-admin-section'>
                                                        <label>Admin Type</label>
                                                        <Form.Group>
                                                            <Form.Radio 
                                                                name='createUHAdmin' 
                                                                label='UH Faculty/Staff Coordinator'
                                                                value='uh-admin'
                                                                onChange={this.handleInputChange} 
                                                                checked={this.state.createUHAdmin === 'uh-admin'}/>
                                                            <Form.Radio 
                                                                name='createUHAdmin'
                                                                label='Non-UH Coordinator'
                                                                value='non-uh-admin'
                                                                onChange={this.handleInputChange} 
                                                                checked={this.state.createUHAdmin === 'non-uh-admin'}/>
                                                        </Form.Group>
                                                        {
                                                            this.state.createUHAdmin === 'non-uh-admin' &&
                                                            <Form.Group widths='equal'>
                                                                <Form.Input 
                                                                    name='adminFirstName' 
                                                                    label='First Name' 
                                                                    value={this.state.adminFirstName}
                                                                    placeholder={'Enter first name'} 
                                                                    onChange={this.handleInputChange} />
                                                                <Form.Input 
                                                                    name='adminLastName' 
                                                                    label='Last Name' 
                                                                    value={this.state.adminLastName}
                                                                    placeholder={'Enter last name'} 
                                                                    onChange={this.handleInputChange}/>
                                                            </Form.Group>
                                                        }
                                                        <Form.Group widths='equal'>
                                                            <Form.Input 
                                                                name='adminUserId' 
                                                                label={this.state.createUHAdmin === 'uh-admin' ? 'Username' : 'Coordinator Email (must be UH email or Gmail)'} 
                                                                value={this.state.adminUserId}
                                                                placeholder={`Enter ${this.state.createUHAdmin === 'uh-admin' ? 'username' : 'email'}`} 
                                                                onChange={this.handleInputChange} />
                                                            <Form.Button basic color='teal' onClick={this.addAdmin.bind(this)}>Add</Form.Button>
                                                        </Form.Group>
                                                    </div>
                                                    :
                                                    <div className='new-admin-button'>
                                                        <a onClick={() => this.handleInputChange(null, {name: 'isAddingAdmin', value: true})}>+ Add another admin</a>
                                                    </div>
                                            }
                                        </Segment>
                                    </div>                                 
                                </Grid.Column>
                            </Grid.Row>

                            <Grid.Row columns={window.mobileCheck() ? 1 : 2}>
                                <Grid.Column />
                                <Grid.Column className='controls'>
                                    {
                                        this.state.displayUpdateMsg &&
                                            <div id="org-update-msg">Update successful.</div>
                                    }
                                    <Button size={'massive'} className='submit-button' onClick={() => this.createOrganization()}>{this.state.editing ? 'UPDATE ORGANIZATION' : 'CREATE ORGANIZATION'}</Button>
                                    {
                                        (this.props.session.siteAdmin && this.state.editing) &&
                                            <Button size={'massive'} color='red' onClick={() => this.props.forceConfirm('This action will delete this organization.', () => this.deleteOrganization(), true)}>DELETE ORGANIZATION</Button>
                                    }
                                    {
                                        !this.state.editing &&
                                            <Link exact='true' to='/'>
                                                <Button size={'massive'} color='red' >CANCEL</Button>
                                            </Link>
                                    }
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>

                    </div>
                </Container>
                <LeaveConfirm shouldBlockNavigation={this.doChangesExist()}/>
            </div>
        );
    }
}

export default (RootContainer(ModalContainer(OrganizationContainer(CreateOrganization))));
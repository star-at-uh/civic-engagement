import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Icon, Container, Dropdown, Loader } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';
import EventListContainer from '../../redux/containers/events-list.js';
import OrganizationListContainer from '../../redux/containers/organization.js';
import comboboxContainer from '../../redux/containers/combobox.js';
import AuthorizationEvent from '../../components/authorization/AuthorizationEvent.jsx';
import AuthorizationToolbar from '../../components/authorization/AuthorizationToolbar.jsx';

import '../../css/authorization.scss';
import IntervalHelper from '../../util/interval-helper.js';

/**
 * Component for rendering the page to authorize/deauthorize unauthorized events
 */
class EventAuthorization extends React.Component {
    
    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            eventList: [],
            authorizationComment: '',
            modifiedIndexes: {}, // To help avoid duplicates
            organizationOptions: [],
            isLoadingEvents: false,
            filterStatus: 'unprocessed',
        };
        this.processEventsList = this.processEventsList.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.grabEventsHelper = this.grabEventsHelper.bind(this);
        this.refreshEventsInterval = null;
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount(){
        this.grabUnauthorizedEventList();
        this.refreshEventsInterval = setInterval(() => this.grabEventsHelper(true), 300000);
        IntervalHelper.addInterval(this.refreshEventsInterval);

        // To help reduce the # of calls made to this proc when filtering due to the nature of ChooseOrganization.jsx
        this.props.getAllOrganizations().payload
                                     .then((response) => {
                                         const OPTIONS = [];
                                         response.data.forEach((option) => {
                                             OPTIONS.push({ key: option.OrganizationId, text: option.OrganizationName, value: option.OrganizationId });
                                         });
                                         this.setState({organizationOptions: OPTIONS});
                                     });
    }

    /**
     * Fires when component is unloaded, used to stop the interval
     */
    componentWillUnmount() {
        IntervalHelper.removeInterval(this.refreshEventsInterval);
    }
    
    /**
     * Grabs the list of events that are currently marked as unauthorized, denoted by null being the indicator
     */
    grabUnauthorizedEventList() {
        this.props.viewUnauthorizedEvents({}).payload
                                             .then(this.processEventsList);
    }

    /**
     * Grabs the list of events that are currently marked as authorized, denoted by 1 (or true) being the indicator
     */
    grabAuthorizedEventList() {
        this.props.viewAuthorizedEvents({}).payload
                                             .then(this.processEventsList);
    }

    /**
     * Grabs the list of events that are currently marked as rejected, denoted by 0 (or false) being the indicator
     */
    grabRejectedEventList() {
        this.props.viewRejectedEvents({}).payload
                                            .then(this.processEventsList);
    }

    /**
     * A helper function made to process the data from response.data to format data within the event list returned by the server
     * to the apporpriate format for the front
     * @param {*} eventList a list of events 
     */
    processEventsList(rawEventList) {
        const EVENT_LIST = rawEventList.data.map(event => {
                                                const START_DATE_TIME = new Date(event.EventStartDateTime);
                                                const END_DATE_TIME = new Date(event.EventEndDateTime);

                                                event.startTime = this.timeFormatHelper(START_DATE_TIME);
                                                event.endTime = this.timeFormatHelper(END_DATE_TIME);
                                                event.startDate = this.dateFormatHelper(START_DATE_TIME);
                                                event.endDate = this.dateFormatHelper(END_DATE_TIME);

                                                return event;
                                            });

        this.setState({eventList: EVENT_LIST, isLoadingEvents: false});
    }

    /**
     * Helper function that extracts the date from datetime
     * @param {*} dateTime the datetime 
     * @return {String} the date fields from the datetime
     */
    dateFormatHelper(dateTime) {
        return dateFns.format(dateTime, 'YYYY-MM-DD'); // Does not factor timezone, but for now assumed to be HST
    }

    /**
     * Helper function that extracts the time from datetime
     * @param {*} dateTime the datetime 
     * @return {String} the time fields from the datetime
     */
    timeFormatHelper(dateTime) {
        return dateFns.format(dateTime, 'HH:mm:ss.SSS'); // Does not factor timezone, but for now assumed to be HST
    }

    /**
     * Updates the state to properly store the selected filter option
     * @param {*} event raw data of the event triggered 
     * @param {*} data filtered event trigger data (property used data.value)
     */
    handleFilterChange(event, data) {
        this.setState({filterStatus: data.value}, this.grabEventsHelper);
    }

    /**
     * This function calls a specific event grabber function based on the filterStatus state
     */
    grabEventsHelper(ignoreInitNumbers) {
        this.setState({isLoadingEvents: true}, () => {
            if (!ignoreInitNumbers) {
                this.checkUnauthorizedNumbers();
            }

            switch (this.state.filterStatus) {
                case 'accepted':
                    this.grabAuthorizedEventList();
                    break;
                case 'rejected':
                    this.grabRejectedEventList();
                    break;
                default:
                    this.grabUnauthorizedEventList();
                    break;
            }
        });
    }

    /**
     * Checks the number of unauthorized events/organizations to update the displayed number
     */
    checkUnauthorizedNumbers() {
        const EVENTS_PROMISE = this.props.viewNumOfUnauthorizedEvents().payload;
        const ORGANIZATIONS_PROMISE = this.props.getNumOfUnauthorizedOrganizations().then();

        Promise.all([EVENTS_PROMISE, ORGANIZATIONS_PROMISE])
                .then(responses => {
                    this.props.setNumOfUnauthorizedEvents(responses[0].data[0].NumUnauthorizedEvents);
                    this.props.setNumOfUnauthorizedOrganizations(responses[1].data[0].NumInactiveOrganizations);
                })
                .catch(() => IntervalHelper.removeInterval(this.refreshEventsInterval));
    }

    /**
     * main render function
     */
    render() {
        const FILTER_OPTIONS = [
            {
                key: 'unprocessed',
                value: 'unprocessed',
                text: 'Unprocessed',
            },
            {
                key: 'accepted',
                value: 'accepted',
                text: 'Accepted',
            },
            {
                key: 'rejected',
                value: 'rejected',
                text: 'Rejected',
            },
        ];

        return (
            <div id={'authorization-details'}>
                <AuthorizationToolbar/>
                <div className={'admin-button-container'}>
                    <Link className={'big-home-button top right'} exact to='/create-organization'>
                        <Button exact to='/' icon labelPosition='left'>
                            New Organization
                            <Icon name='plus'/>
                        </Button>
                    </Link>
                </div>
                <div>
                    <Container className='admin-homepage-header'>
                        <h3 className={'admin-main-title'}>Authorize Events</h3>
                        <Link exact to='/'>
                            <div>My USERVE Organizations</div>
                        </Link>
                    </Container>
                </div>
                <div className='actions-toolbar'>
                    <Container>
                        <div>
                            Filter:
                            <Dropdown
                                placeholder='Select status'
                                selection
                                defaultValue={'unprocessed'}
                                options={FILTER_OPTIONS}
                                onChange={this.handleFilterChange}
                            />
                        </div>
                    </Container>
                </div>
                {
                    this.state.isLoadingEvents ?
                        <Loader active/>
                        :
                        <div className='unauthorized-event-list'>
                            <Container>
                                {
                                    this.state.eventList.length > 0 ?
                                        this.state.eventList.map((event, index) => 
                                                                <AuthorizationEvent 
                                                                    key={event.EventID}
                                                                    event={event} 
                                                                    index={index} 
                                                                    grabEvents={this.grabEventsHelper}
                                                                    organizationOptions={this.state.organizationOptions}/>)
                                        :
                                        <div className='no-events'>No events found</div>
                                }
                            </Container>
                        </div>
                }
            </div>
        );
    }
}

export default OrganizationListContainer(ModalContainer(EventListContainer(comboboxContainer(EventAuthorization))));
import React, { useState, useEffect } from 'react';
import { createRoot } from 'react-dom/client';
import { HashRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { Provider, useDispatch } from 'react-redux';
import store from './redux/reducers';
import RootContainer from './redux/containers/app.js';
import Home from './pages/Home.jsx';
import Nav from './components/nav/Nav.jsx';
import MainMenu from './components/nav/MainMenu.jsx';
import SearchPage from './pages/SearchPage.jsx';
import RouteCallBacks from './components/nav/RouteCallBacks.jsx';
import ClockInOut from './pages/ClockInOut.jsx';
import EventPage from './pages/EventPage.jsx';
import Alerts from './components/global/Alerts.jsx';
import { Dimmer, Loader } from 'semantic-ui-react';
import OrganizationPage from './pages/OrganizationPage.jsx';
import ModalContainer from './redux/containers/modal.js';
import ForceConfirm from './components/global/ForceConfirm.jsx';
import OrganizationsList from './pages/OrganizationsList.jsx';
import MyProgress from './pages/MyProgress.jsx';
import Favorites from './pages/Favorites.jsx';
import UpcomingEvents from './components/home/UpcomingEvents.jsx';

/**
 * Admin imports
 */
import CreateOrganization from './pages/admin/CreateOrganization.jsx';
import CreateEvent from './pages/admin/CreateEvent.jsx';
import AdminHome from './pages/admin/AdminHome.jsx';
import EventsList from './pages/admin/EventsList.jsx';
import ServiceHours from './pages/admin/ServiceHours.jsx';
import EventAuthorization from './pages/admin/EventAuthorization.jsx';
import OrganizationAuthorization from './pages/admin/OrganizationAuthorization.jsx';
import DuplicateEvent from './components/create-event/DuplicateEvent.jsx';
import ISATNotify from './components/global/ISAT/ISATNotify.jsx';

import { storeISAT } from './redux/reducers/isat.js';

/**
 * global utils
 */
import closeMenu from './util/CloseMenu.js';

import './css/global.scss';

/**
 * Root Component is the main class of the application
 * Contains React Router which handles all URL routes and permissions
 * Attaches Redux Global State (similar to $RootScope in Angular)
 */
const Root = (props) => {
    const [sessionLoaded, flagLoadedSession] = useState(false);
    const dispatch = useDispatch();
    const IS_ADMIN_SITE = window.appConfig.getAttribute('data') == 'advisor';

    useEffect(() => {
        // make redux dispatch functions available outside of react
        window.reactForceConfirm = props.forceConfirm;
        window.reactModal = props.openModal;

        // if from button make sure to push state
        /**
         * FOR FUTURE DEV!
         * 
         * temporary hack for gary's meeting 12/16/2020
         * we push history twice because we have to double back each back button
         * if we dont push twice here, going back from home will go to CAS
         * 
         * originally, the method tried was to push once here, and also only go back
         * once. We now achieve desired results with two replace states on back
         * and one replace state on forward. to comepensate we push twice first
         * 
         * btw push = pushState(), replace = replaceState()
         * 
         * When removing this in future don't forget to check RouteCallbacks.jsx
         */

        // session info
        props.getSessionInfo()
                .payload
                .then((response) => {
                    const DATA = response.data;
                    flagLoadedSession(true);
                    const RETURN_PAGE = sessionStorage.getItem('returnHash');
                    if(!DATA['valid']){
                        props.forceConfirm('You are not authorized to use this page', () => window.location.href = window.location.href.split('/app/#')[0]);
                    }
                    else {
                        props.setSessionInfo(DATA);
                        if (IS_ADMIN_SITE) {
                            dispatch(storeISAT({
                                countdown: DATA.ISATDaysLeft,
                                displayWarning: DATA.displayISATWarning ?? false // default to false when not given back to prevent page from crashing on refresh
                            }));
                        }
                    }

                    if (RETURN_PAGE) {
                        sessionStorage.removeItem('returnHash');
                        window.location.href = window.location.href.split('/#')[0] + RETURN_PAGE;
                    }
                });
    }, []);

    return (
        <Router>
            <MainMenu/>
            <RouteCallBacks />
            <Alerts />
            <ForceConfirm />
            {
                IS_ADMIN_SITE && 
                <ISATNotify/>
            }
            <DuplicateEvent/>
            {
                sessionLoaded ? 
                    <div id='main-view'>
                        <Nav/>
                        <div id={'scroll-body'} onClick={closeMenu}>
                            <div id='app-body'>
                                <Switch>
                                    { 
                                        // admin-only pages
                                        IS_ADMIN_SITE &&
                                        [
                                            <ProtectedRoute key='admin-home' isLogged={props.session.isLogged} exact path="/" component={AdminHome}/>,
                                            <ProtectedRoute key='admin-create-organization' isLogged={props.session.isLogged} exact path="/create-organization" component={CreateOrganization}/>,
                                            <ProtectedRoute key='admin-edit-organization' isLogged={props.session.isLogged} exact path="/edit-organization" component={CreateOrganization}/>,
                                            <ProtectedRoute key='admin-create-event' isLogged={props.session.isLogged} exact path="/create-event" component={CreateEvent}/>,
                                            <ProtectedRoute key='admin-edit-event' isLogged={props.session.isLogged} exact path="/edit-event" component={CreateEvent}/>,
                                            <ProtectedRoute key='admin-events-list' isLogged={props.session.isLogged} exact path="/events-list" component={EventsList}/>,
                                            <ProtectedRoute key='admin-student-progress' isLogged={props.session.isLogged} exact path="/student-progress" component={ServiceHours}/>,
                                            <ProtectedRoute key='admin-progress-check' isLogged={props.session.isLogged} exact path="/student" component={MyProgress} admin={true}/>,
                                            <ProtectedRoute key='admin-authorize-event' isLogged={props.session.isLogged} exact path="/authorize-events" component={EventAuthorization} admin={true}/>,
                                            <ProtectedRoute key='admin-authorize-org' isLogged={props.session.isLogged} exact path="/authorize-orgs" component={OrganizationAuthorization} admin={true}/>
                                        ]
                                    }
                                    { // student-only pages
                                        window.appConfig.getAttribute('data') == 'student' &&
                                        [
                                            <ProtectedRoute key='home' isLogged={true} exact path="/" component={Home}/>,
                                            <ProtectedRoute key='my-event' isLogged={props.session.isLogged} exact path="/my-events" component={UpcomingEvents}/>,
                                            <ProtectedRoute key='my-progress' isLogged={props.session.isLogged} exact path="/my-progress" component={MyProgress}/>,
                                            <ProtectedRoute key='my-favorites' isLogged={props.session.isLogged} exact path='/my-favorites' component={Favorites}/>
                                        ]
                                    }
                                    <ProtectedRoute key='clock-in-out' isLogged={props.session.isLogged} exact path="/clock-in-out" component={ClockInOut}/>,
                                    <ProtectedRoute key='search' isLogged={true} exact path="/search" component={SearchPage}/>
                                    <ProtectedRoute key='event' isLogged={true} exact path="/event" component={EventPage}/>
                                    <ProtectedRoute key='organization' isLogged={true} exact path="/organization" component={OrganizationPage}/>
                                    <ProtectedRoute key='organizations' isLogged={true} exact path="/organizations" component={OrganizationsList}/>
                                </Switch>
                            </div>
                        </div>
                    </div> 
                    : 
                    <Dimmer>
                        <Loader active/>
                    </Dimmer>
            }
      </Router>
    );
};

/**
 * ProtectedRoute (see React Router v4 sample)
 * Checks for session login before routing to the requested page, otherwise goes to signin page.
 * @param {any} { component: Component, ...rest }
 */
const ProtectedRoute = ({ component: Component, isLogged: isLogged, ...rest }) => (
  <Route
    {...rest}
    render={(props) => {
      return isLogged ?
          (<Component {...props} />) :
          (<Redirect to={{ pathname: '/', state: { from: props.location } }}/>
      );
    }}
  />
);

const App = ModalContainer(RootContainer(Root));

$(() => {
    createRoot(document.getElementById('root')).render(
        <React.StrictMode>
            <Provider store={store}>
                <App/>
            </Provider>
        </React.StrictMode>
    );
});

import React, {useState, useEffect} from 'react';
import { Button, Input, Form, Icon, Segment, Grid, Container, Radio, Label } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';
import EventListContainer from '../../redux/containers/events-list.js';
import OrganizationContainer from '../../redux/containers/organization.js';
import SBox from '../../components/global/SBox.jsx';
import AdminList from '../../components/create-organization/AdminList.jsx';

import '../../css/settings.scss';

/**
 * Component for displaying the events that are being reviewed for their authorization status
 */
const AuthorizationEvent = (props) => {
    const [authorizationStatus, setAuthorizationStatus] = useState(props.filterStatus);
    const [isSeeMoreToggled, setIsSeeMoreToggled] = useState(false);
    const [authorizationComment, setAuthorizationComment] = useState(props.organization.OrganizationIsInActiveComments);
    const [additionalDetails, setAdditionDetails] = useState({});
    const [UHAdmins, setUHAdmins] = useState([]);
    const [nonUHAdmins, setNonUHAdmins] = useState([]);

    /**
     * Grabs the all of the details of the current org when see more is changed to true 
     */
    useEffect(() => {
        if (!additionalDetails.OrganizationName && isSeeMoreToggled) {
            grabOrgDetails();
        }
    }, [isSeeMoreToggled]);

    /**
     * Attempts to fetch all the details of the organization
     */
    const grabOrgDetails = () => {
        props.organizationDetails({ organizationId: props.organization.OrganizationID })
                .then((response) => {
                    if (response.data[0].OrganizationName) {
                        setAdditionDetails(response.data[0]);
                        getAdminList();
                    } 
                    else {
                        this.props.openModal(response.data[0].ErrorMsg);
                    }
                });
    };

    /**
     * Grabs the list of admins for the current organization
     */
    const getAdminList = () => {
        props.organizationAdmins({
                      params: {
                          orgId: props.organization.OrganizationID 
                      } 
                  })
                  .then(response => {
                      const UH_ADMIN_LIST = [];
                      const NON_UH_LIST = [];
                      
                      response.data.forEach(user => {
                          if (user.is_Affiliated) {
                              UH_ADMIN_LIST.push(user);
                          }
                          else {
                              NON_UH_LIST.push(user);
                          }
                      });

                      setUHAdmins(UH_ADMIN_LIST);
                      setNonUHAdmins(NON_UH_LIST);
                  });
    };

    /**
     * This function updates the local state controlling the radio group display for the particular event
     * @param {*} event object containing data of what triggered this function to run
     * @param {*} data custom object containing at least a value key
     */
    const handleAuthStatusUpdate = (event, data) => {
        setAuthorizationStatus(data.value);
    };

    /**
     * Handles the opening and closing of the see more section via updating the boolean isSeeMoreToggled
     */
    const handleSeeMoreToggle = () => {
        setIsSeeMoreToggled(!isSeeMoreToggled);
    };

    /**
     * Updates the status of events that have been marked as "modified" via changing the auth status in any way
     */
    const updateOrgAuthStatus = () => {
        const PARAMS = {
            organizationId: props.organization.OrganizationID,
            authorizationStatus: authorizationStatus,
            authorizationComment: authorizationComment,
        };
        
        props.toggleActiveState(PARAMS)
                .then(response => {
                    if (!response.data) {
                        props.openModal('An unexpected error has occurred');
                    }
                    else if (response.data[0].ErrorMessage) {
                        props.openModal(response.data[0].ErrorMessage);
                    }
                    else {
                        props.forceConfirm('Organization status updated successfully', props.grabOrganizations);
                    }
                });
    };

    /**
     * Handles input change for the authorization comment
     * @param {object} event object containing details of the trigger that called this function
     * @param {object} data contains at least a value key field
     */
    const handleAuthorizationCommentUpdate = (event, data) => {
        setAuthorizationComment(data.value);
    };

    /**
     * Returns the text representation of the status key
     * @param {int} key the id of the status
     */
    const statusTextFromKey = (key) => {
        switch (key) {
            case 0:
                return 'Accepted';
            case 1:
                return 'Rejected';
            default:
                return 'Unprocessed';
        };
    };

    /**
     * Main HTML section
     */
    return (
        <Container>
            <div className={'ui form'}>
                <div className='authorization-status'>
                    <span>Set Status To:</span>
                    <Radio
                        label='Unprocessed'
                        name={`IsAuthorizedEventIndicator${props.index}`} // Unique name needed so semantic ui can style appropriately
                        value={-1}
                        checked={authorizationStatus === -1 || authorizationStatus === undefined}
                        onChange={handleAuthStatusUpdate}/>
                    <Radio
                        label='Accepted'
                        name={`IsAuthorizedEventIndicator${props.index}`}
                        value={0}
                        checked={authorizationStatus == 0} // Technically === 1, but initial return is as true and the other statuses are falsy
                        onChange={handleAuthStatusUpdate}/>
                    <Radio
                        label='Rejected'
                        name={`IsAuthorizedEventIndicator${props.index}`}
                        value={1}
                        checked={authorizationStatus == 1}
                        onChange={handleAuthStatusUpdate}/>
                    <div className='authorization-comment'>
                        <img src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/Icon_material-insert-comment.svg"/>
                        <Input
                            value={authorizationComment}
                            placeholder='Enter a comment for these events and select an action to the right...'
                            onChange={handleAuthorizationCommentUpdate}/>                    
                    </div>
                    <div>
                        <Button basic color='teal' onClick={updateOrgAuthStatus}>Submit</Button>
                    </div>
                </div>
                <div>
                    <Container className='organization-auth'>
                        <div className={'ui form'}>
                            <Grid>
                                <Grid.Row>
                                    <Grid.Column className='authorize-column'>
                                        <div className='label-types'>
                                            <label>ORGANIZATION STATUS:</label> 
                                            <div>{statusTextFromKey(props.organization.OrganizationIsInActive)}</div>
                                        </div>
                                        <Form.Input 
                                            name='organizationName'
                                            label='ORGANIZATION NAME'
                                            value={props.organization.OrganizationName}
                                            disabled/>
                                        <div className='label-types'>
                                            <label>TYPE OF ORGANIZATION:</label>
                                            <div>
                                                {
                                                    props.organization.OrganizationLabels?.split('//')
                                                                                            .map((idGroup) => {
                                                                                                const ID_GROUP = idGroup.split('||'); // [0] is name, [1] is id
                                                                                                return <Label key={ID_GROUP[1]}>{ID_GROUP[0]}</Label> 
                                                                                            })
                                                }
                                            </div>
                                        </div>
                                        {
                                            isSeeMoreToggled &&
                                            <>
                                                <Form.Input 
                                                    name='locationAddress1' 
                                                    label='ADDRESS 1' 
                                                    value={additionalDetails.OrganizationAddressLine1}
                                                    disabled />
                                                <Form.Input 
                                                    name='locationAddress2' 
                                                    label='ADDRESS 2' 
                                                    value={additionalDetails.OrganizationAddressLine2}
                                                    disabled />
                                                <Form.Group widths='equal'>
                                                    <Form.Input 
                                                        name='locationCity'
                                                        label='CITY' 
                                                        value={additionalDetails.OrganizationAddressCity} 
                                                        disabled />
                                                    <Form.Input 
                                                        name='locationState' 
                                                        value={additionalDetails.OrganizationAddressState} 
                                                        label='STATE' 
                                                        disabled/>
                                                </Form.Group>
                                                <Form.Input 
                                                    name='locationZip' 
                                                    label='ZIPCODE' 
                                                    value={additionalDetails.OrganizationAddressZipCode} 
                                                    disabled />
                                                <label>CONTACT PERSON DETAILS:</label>
                                                <Segment>
                                                    <Form.Group widths='equal'>
                                                        <Form.Input 
                                                            name='contactPersonName' 
                                                            value={additionalDetails.OrganizationContactPersonFullName} 
                                                            label='NAME' 
                                                            disabled />
                                                        <Form.Input 
                                                            name='contactPersonPhone' 
                                                            value={additionalDetails.OrganizationPhoneNumber} 
                                                            label='PHONE (###)-###-####' 
                                                            disabled />
                                                    </Form.Group>
                                                    <Form.Input 
                                                        name='contactPersonEmail' 
                                                        value={additionalDetails.OrganizationEmailAddress} 
                                                        label='EMAIL' 
                                                        disabled />
                                                </Segment>
                                            </>
                                        }
                                    </Grid.Column>
                                    <Grid.Column className='authorize-column'>
                                        {
                                            isSeeMoreToggled &&
                                                <div className='authorize-event-image'>
                                                    <label>ORGANIZATION IMAGE:</label>
                                                    <img id='image-upload-preview'
                                                        src={additionalDetails.OrganizationImageURL} 
                                                        center={additionalDetails.OrganizationImageCenterPosition} />
                                                </div>
                                        }
                                        <Form.TextArea
                                            name='description'
                                            value={props.organization.OrganizationDescription}
                                            label='DESCRIPTION' 
                                            disabled />
                                        <Form.Input 
                                            name='webPageURL'
                                            value={props.organization.OrganizationWebpageURL} 
                                            label='WEBSITE' 
                                            disabled />
                                        {
                                            isSeeMoreToggled &&
                                            <>
                                                <label>TYPES OF EVENTS OFFERED</label>
                                                <Grid className={'toggle-settings-table'}>
                                                    <Grid.Row className={'setting-row'}>
                                                        <Grid.Column width={12}>
                                                            In-Person
                                                        </Grid.Column>
                                                        <Grid.Column width={4} textAlign={'right'}>
                                                            <SBox name='allowInPerson' value={additionalDetails.AllowInPerson} disabled />
                                                        </Grid.Column>
                                                    </Grid.Row>

                                                    <Grid.Row className={'setting-row'}>
                                                        <Grid.Column width={12}>
                                                            Online
                                                        </Grid.Column>
                                                        <Grid.Column width={4} textAlign={'right'}>
                                                            <SBox name='allowOnline' value={additionalDetails.AllowOnline} disabled />
                                                        </Grid.Column>
                                                    </Grid.Row>

                                                    <Grid.Row className={'setting-row'}>
                                                        <Grid.Column width={12}>
                                                            Hybrid
                                                        </Grid.Column>
                                                        <Grid.Column width={4} textAlign={'right'}>
                                                            <SBox name='allowHybrid' value={additionalDetails.AllowHybrid} disabled />
                                                        </Grid.Column>
                                                    </Grid.Row>

                                                    <Grid.Row className={'setting-row'}>
                                                        <Grid.Column width={12}>
                                                            Other
                                                        </Grid.Column>
                                                        <Grid.Column width={4} textAlign={'right'}>
                                                            <SBox name='allowIndependent' value={additionalDetails.AllowIndependent} disabled/>
                                                        </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>

                                                <Grid className={'toggle-settings-table'}>
                                                    <label>STATUS</label>
                                                    <Grid.Row className={'setting-row'}>
                                                        <Grid.Column width={12}>
                                                            Not Accepting Volunteers
                                                        </Grid.Column>
                                                        <Grid.Column width={4} textAlign={'right'}>
                                                            <SBox name='allowVolunteers' value={additionalDetails.OrganizationNotAcceptingVolunteers} disabled/>
                                                        </Grid.Column>
                                                    </Grid.Row>
                                                </Grid>
                                                <div className='organization-admins'>
                                                    <label>ORGANIZATION ADMIN(S):</label>
                                                    <Segment>
                                                        {
                                                            UHAdmins.length > 0 &&
                                                            <AdminList adminList={UHAdmins} isUH={true}/>
                                                        }
                                                        {
                                                            nonUHAdmins.length > 0 &&
                                                            <AdminList adminList={nonUHAdmins} isUH={false}/>
                                                        }
                                                    </Segment>
                                                </div>                                 
                                            </>
                                        }
                                    </Grid.Column>
                                </Grid.Row>

                            </Grid>

                        </div>
                    </Container>
                    <div className='see-more-button' onClick={handleSeeMoreToggle}>
                        See {isSeeMoreToggled ? 'less' : 'more'}
                        <Icon name={`angle ${isSeeMoreToggled ? 'up' : 'down'}`}/>
                    </div>
                </div>
            </div>
        </Container>
    );
};

export default OrganizationContainer(ModalContainer(EventListContainer(AuthorizationEvent)));
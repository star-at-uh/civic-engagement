import React from 'react';
import { Button, Input, Form, Icon, Segment, Grid, Container, Radio } from 'semantic-ui-react';
import DatePicker from '../../components/global/DatePicker.jsx';
import SBox from '../../components/global/SBox.jsx';
import ChooseOrganization from '../../components/create-event/ChooseOrganization.jsx';
import TIME_OPTIONS from '../../util/TimeOptions.js';
import ChooseAttendanceType from '../../components/create-event/ChooseAttendanceType.jsx';
import ModalContainer from '../../redux/containers/modal.js';
import EventListContainer from '../../redux/containers/events-list.js';

import '../../css/settings.scss';

/**
 * Component for displaying the events that are being reviewed for their authorization status
 * Props:
 *  - event {*} [REQUIRED] contains the details of the event
 *  - index {Number} [REQUIRED] corresponds to the event's position in an array
 */
class AuthorizationEvent extends React.Component {

    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            authorizationStatus: null,
            isSeeMoreToggled: false,
            authorizationComment: '',
        };

        this.handleAuthStatusUpdate = this.handleAuthStatusUpdate.bind(this);
        this.handleSeeMoreToggle = this.handleSeeMoreToggle.bind(this);
        this.updateEventAuthStatus = this.updateEventAuthStatus.bind(this);
    }

    /**
     * Init for this component
     */
    componentDidMount() {
        this.setState({authorizationStatus: this.props.event.IsAuthorizedEventIndicator, authorizationComment: this.props.event.IsAuthorizedEventComments});
    }

    /**
     * This function updates the local state controlling the radio group display for the particular event
     * @param {*} event 
     * @param {*} data 
     * @param {Number} index the index number that the currently modfied event lies in within the eventList state
     */
    handleAuthStatusUpdate(event, data) {
        this.setState({authorizationStatus: data.value});
    }

    /**
     * Handles the opening and closing of the see more section via updating the boolean isSeeMoreToggled
     */
    handleSeeMoreToggle() {
        this.setState({isSeeMoreToggled: !this.state.isSeeMoreToggled});
    }

    /**
     * Updates the status of events that have been marked as "modified" via changing the auth status in any way
     */
    updateEventAuthStatus() {
        const MODIFIED_EVENT = {
            'EventID': this.props.event.EventID,
            'IsAuthorizedEventIndicator': this.state.authorizationStatus,
            'IsAuthorizedEventComments': this.state.authorizationComment,
        };
        
    
        const PARAMS = {
            eventList: JSON.stringify(MODIFIED_EVENT),
        };

        this.props.updateEventAuthorization(PARAMS).payload
                                                    .then(response => {
                                                        if (!response.data) {
                                                            this.props.openModal('An unexpected error has occurred');
                                                        }
                                                        else if (response.data[0].ErrorMessage) {
                                                            this.props.openModal(response.data[0].ErrorMessage);
                                                        }
                                                        else {
                                                            this.props.forceConfirm('Event updated successfully', this.props.grabEvents);
                                                        }
                                                    });
    }

    /**
     * Handles input change for the authorization comment
     */
    handleAuthorizationCommentUpdate(event, data) {
        this.setState({authorizationComment: data.value});
    }

    /**
     * Returns the text representation of the status key
     * @param {int} key the id of the status
     */
    statusTextFromKey = (key) => {
        switch (key) {
            case 1:
                return 'Accepted';
            case 0:
                return 'Rejected';
            default:
                return 'Unprocessed';
        };
    };

    /**
     * main render function
     */
    render() {
        return (
            <Container>
                <div className={'ui form'}>
                    <div className='authorization-status'>
                        <span>Set Status To:</span>
                        <Radio
                            label='Unprocessed'
                            name={`IsAuthorizedEventIndicator${this.props.index}`} // Unique name needed so semantic ui can style appropriately
                            value={-1}
                            checked={this.state.authorizationStatus === -1 || this.state.authorizationStatus === undefined}
                            onChange={(event, data) => this.handleAuthStatusUpdate(event, data, this.props.index)}/>
                        <Radio
                            label='Accepted'
                            name={`IsAuthorizedEventIndicator${this.props.index}`}
                            value={1}
                            checked={this.state.authorizationStatus == true} // Technically === 1, but initial return is as true and the other statuses are falsy
                            onChange={(event, data) => this.handleAuthStatusUpdate(event, data, this.props.index)}/>
                        <Radio
                            label='Rejected'
                            name={`IsAuthorizedEventIndicator${this.props.index}`}
                            value={0}
                            checked={this.state.authorizationStatus == 0 && (this.state.authorizationStatus !== -1 && this.state.authorizationStatus !== undefined)}
                            onChange={(event, data) => this.handleAuthStatusUpdate(event, data, this.props.index)}/>
                        <div className='authorization-comment'>
                            <img src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/Icon_material-insert-comment.svg"/>
                            <Input
                                value={this.state.authorizationComment}
                                placeholder='Enter a comment for these events and select an action to the right...'
                                onChange={this.handleAuthorizationCommentUpdate.bind(this)}/>                    
                        </div>
                        <div>
                            <Button basic color='teal' onClick={this.updateEventAuthStatus}>Submit</Button>
                        </div>
                    </div>
                    <div className='event-details'>
                        <div className='authorize-spacer'></div>
                        <div className='authorize-column first-column'>
                            <div className='label-types'>
                                <label>EVENT STATUS:</label> 
                                <div>{this.statusTextFromKey(this.props.event.IsAuthorizedEventIndicator)}</div>
                            </div>
                            <Form.Input
                                disabled
                                name='EventName' 
                                limit={40} label='EVENT NAME *' 
                                placeholder={'What\'s your event name? (40 characters or less)'}
                                value={this.props.event.EventName}/>
                            <Form.Input
                                disabled
                                name='EventType' 
                                limit={40} label='EVENT TYPE *' 
                                placeholder={'What\'s your event name? (40 characters or less)'}
                                value={this.props.event.EventTypeName}/>
                            <div className='field'>
                                <ChooseOrganization
                                    disabled
                                    options={this.props.organizationOptions}
                                    value={this.props.event.EventOrganizationID}  />
                            </div>
                            {
                                this.state.isSeeMoreToggled &&
                                <div className='field'>
                                    <ChooseAttendanceType
                                        disabled
                                        value={this.props.event.EventAttendanceTypeID} />
                                    <label><strong>LOCATION DETAILS:</strong></label>
                                    <Segment>
                                        <Form.Input disabled label='NAME *' value={this.props.event.EventLocationName}/> 
                                        {
                                            this.props.event.EventAttendanceTypeID == 3 ?
                                                <div>
                                                    <Form.Input disabled label='ADDRESS 1' value={this.props.event.EventLocationAddressL1}/> 
                                                    <Form.Input disabled label='ADDRESS 2' value={this.props.event.EventLocationAddressL2}/> 
                                                    <Form.Input disabled label='CITY' value={this.props.event.EventLocationCity}/> 
                                                    <Form.Input disabled label='STATE' value={this.props.event.EventLocationState}/> 
                                                    <Form.Input disabled label='ZIPCODE' value={this.props.event.EventLocationZip}/>
                                                </div> 
                                                : 
                                                <div>
                                                    <Form.Input disabled id='eventform-locationMeetingURL' label='VIRTUAL MEETING URL' value={this.props.event.EventVirtualMeetingLink}/><br/>
                                                </div>
                                        }
                                        <Form.TextArea
                                            label='ACCESS' 
                                            value={this.props.event.EventAccess} 
                                            disabled />
                                    </Segment>
                                </div>
                            } 
                            <Form.Group widths='equal'>
                                <Form.Field>
                                    <label>START DATE</label>
                                    <DatePicker 
                                        disabled
                                        value={this.props.event.startDate}/>
                                </Form.Field>
                                <Form.Field>
                                    <label>END DATE</label>
                                    <DatePicker 
                                        disabled
                                        value={this.props.event.endDate}/>
                                </Form.Field>
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Select
                                    fluid
                                    disabled
                                    search
                                    label='START TIME'
                                    options={TIME_OPTIONS}
                                    value={this.props.event.startTime}
                                    placeholder='Select time'
                                />
                                <Form.Select
                                    fluid
                                    disabled
                                    search
                                    label='END TIME'
                                    options={TIME_OPTIONS}
                                    value={this.props.event.endTime}
                                    placeholder='Select time'
                                />
                            </Form.Group>
                            {
                                this.state.isSeeMoreToggled &&
                                <div className='field'>
                                    <label><strong>CONTACT PERSON DETAILS:</strong></label>
                                    <Segment>
                                        <Form.Input disabled label='NAME *' value={this.props.event.EventContactPersonName}/> 
                                        <Form.Input disabled label='EMAIL' value={this.props.event.EventContactPersonEmailAddress}/> 
                                        <Form.Input disabled label='PHONE' value={this.props.event.EventContactPersonPhoneNumber}/>
                                    </Segment>
                                </div>
                            }
                        </div>
                        <div className='authorize-column'>
                            {
                                this.state.isSeeMoreToggled &&
                                <div className='authorize-event-image'>
                                    <label>EVENT IMAGE:</label>
                                    <img id='image-upload-preview'
                                        src={this.props.event.EventImageURL} 
                                        center={this.props.event.EventImageCenterPosition} />
                                </div>
                            }
                            <Form.TextArea
                                disabled 
                                label='DESCRIPTION' 
                                placeholder='Describe this event' 
                                value={this.props.event.EventDescription}/>

                            {
                                this.state.isSeeMoreToggled &&
                                <div className='field'>
                                    <Form.TextArea
                                        disabled 
                                        label='INSTRUCTIONS FOR NON-INSTITUTION ATTENDEES' 
                                        value={this.props.event.EventOutsideAttendeesInstructionMessage} />
                                    
                                    <Form.Input
                                        disabled 
                                        label='WHAT TO BRING' 
                                        value={this.props.event.EventItemsToBring}/>

                                    <Form.Input
                                        disabled 
                                        label='FOOD PROVIDED' 
                                        value={this.props.event.EventFoodProvided}/>

                                    <Grid>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                <h1 className='field-header'>ATTENDEE LIMIT?</h1>
                                            </Grid.Column>
                                            <Grid.Column>
                                                <Form.Select
                                                    disabled
                                                    options={[{ key: 'yes', text: 'yes', value: true },{ key: 'no', text: 'no', value: false }]}
                                                    value={this.props.event.EventIsAttendeeLimit}
                                                    fluid />
                                            </Grid.Column>
                                        </Grid.Row>
                                        <Grid.Row columns={2}>
                                            <Grid.Column>
                                                {
                                                    this.props.event.EventAttendeeLimit ? 
                                                        <h1 className='field-header'>SET LIMIT</h1> 
                                                        :
                                                        <Form.Field disabled><h1 className='field-header'>SET LIMIT</h1></Form.Field> 
                                                }
                                            </Grid.Column>
                                            <Grid.Column>
                                                {
                                                    this.props.event.EventAttendeeLimit ? 
                                                        <Form.Input 
                                                            disabled type={'number'}
                                                            value={this.props.event.EventAttendeeLimit}
                                                            fluid/> 
                                                        :
                                                        <Form.Input
                                                            fluid disabled/>  
                                                }
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>

                                    <Grid className={'toggle-settings-table'}>
                                        <Grid.Row className={'setting-row'}>
                                            <Grid.Column width={12} className={!this.state.limitNumAttendees && 'disabled-field'}>
                                                Allow Student to Waitlist if event is full
                                            </Grid.Column>
                                            <Grid.Column width={4} textAlign={'right'}>
                                                <SBox name='allowWaitlist' value={this.state.allowWaitlist} disabled={!this.state.limitNumAttendees}/>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                </div>
                            }

                            <div className='see-more-button' onClick={this.handleSeeMoreToggle}>
                                See {this.state.isSeeMoreToggled ? 'less' : 'more'}
                                <Icon name={`angle ${this.state.isSeeMoreToggled ? 'up' : 'down'}`}/>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        );
    }
}

export default ModalContainer(EventListContainer(AuthorizationEvent));
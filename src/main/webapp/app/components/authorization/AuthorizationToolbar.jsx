import React, { useEffect } from 'react';
import { Container, Button, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import RootContainer from '../../redux/containers/app.js';
import EventListContainer from '../../redux/containers/events-list.js';
import OrganizationListContainer from '../../redux/containers/organization.js';
import IntervalHelper from '../../util/interval-helper.js';

/**
 * Component for rendering the site admin toolbar
 */
const AuthorizationToolbar = (props) => {
    let unauthorizedInterval = null;

    /**
     * Onload callback, fetch the # of unauthorized orgs and events
     */
    useEffect(() => {
        unauthorizedInterval = setInterval(checkUnauthorizedNumbers, 10000);

        IntervalHelper.addInterval(unauthorizedInterval);

        checkUnauthorizedNumbers();

        return () => {
            IntervalHelper.removeInterval(unauthorizedInterval);  
        };
    }, []);

    /**
     * Checks the number of unauthorized events/organizations to update the displayed number
     */
    const checkUnauthorizedNumbers = () => {
        const EVENTS_PROMISE = props.viewNumOfUnauthorizedEvents().payload;
        const ORGANIZATIONS_PROMISE = props.getNumOfUnauthorizedOrganizations().then();

        Promise.all([EVENTS_PROMISE, ORGANIZATIONS_PROMISE])
                .then(responses => {
                    props.setNumOfUnauthorizedEvents(responses[0].data[0].NumUnauthorizedEvents);
                    props.setNumOfUnauthorizedOrganizations(responses[1].data[0].NumInactiveOrganizations);
                })
                .catch(() => IntervalHelper.removeInterval(unauthorizedInterval));
    };

    /**
     * Main Render
     */
    return (
        <div className='site-admin-toolbar'>
            <Container>
                <div className='toolbar-header'>Outstanding Site Admin Tasks:</div>
                <Link exact='true' to='/authorize-orgs'>
                    <Button basic className={`${props.organizationList?.numOfUnauthorizedOrganizations > 0 ? 'something-to-authorize' : 'nothing-to-authorize'}`}>
                        <Icon name='group'/>
                        Organization Approvals
                    </Button> 
                    {
                        props.organizationList?.numOfUnauthorizedOrganizations > 0 &&
                        <div className='number-of-events-notification'>{props.organizationList.numOfUnauthorizedOrganizations}</div>
                    }
                </Link>
                <Link exact='true' to='/authorize-events'>
                    <Button basic className={`${props.eventList?.numOfUnauthorizedEvents > 0 ? 'something-to-authorize' : 'nothing-to-authorize'}`}>
                        <Icon name='calendar alternate outline'/> 
                        Event Approvals
                    </Button> 
                    {
                        props.eventList?.numOfUnauthorizedEvents > 0 &&
                        <div className='number-of-events-notification'>{props.eventList.numOfUnauthorizedEvents}</div>
                    }
                </Link>
            </Container>
        </div>
    );
}

export default OrganizationListContainer(EventListContainer(RootContainer(AuthorizationToolbar)));
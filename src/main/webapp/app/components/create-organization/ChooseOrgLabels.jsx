import React from 'react';
import { Dropdown } from 'semantic-ui-react';
import ComboboxContainer from '../../redux/containers/combobox.js';

/**
 * class for a multiple select dropdown for org labels
 */
class ChooseOrgLabels extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            value: [],
            options: [],
            changed: false
        }
    }

    /**
     * fires when component mounts
     */
    componentDidMount(){
        this.props.getOrganizationLabels().payload.then((response) => {
            let options = [];
            response.data.forEach((option) => {
                options.push({ key: option.LabelID, text: option.LabelName, value: option.LabelID });
            })
            this.setState({options});
        });
        if(this.props.value && this.state.changed == false){
            this.setState({value: this.props.value});
        }
    }

    /**
     * handles mutliple selection change
     * @param {*} event 
     * @param {*} target 
     */
    handleChange(event, target){
        this.setState({changed: true, value: target.value});
        this.props.handleChange(event, target);
    }

    /**
     * main render function
     */
    render(){
        return (
            <div>
                <Dropdown
                    id='choose-org-labels'
                    name={this.props.name}
                    placeholder='Organization Labels'
                    multiple
                    selection
                    search
                    onChange={this.handleChange.bind(this)}
                    options={this.state.options}
                    value={this.state.value}/>
            </div>
        );
    }
}

export default ComboboxContainer(ChooseOrgLabels);
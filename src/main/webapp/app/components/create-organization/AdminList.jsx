import React from 'react';
import { Grid, Popup, Icon } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';
import RootContainer from '../../redux/containers/app.js';

import '../../css/admin-list.scss';

/**
 * Component for listing out admins of an org
 * @param {Array} adminList a list of objects containing the admins
 * @param {boolean} isUH if true, display username, else display email
 * @param {function} deleteCallback function called when the delete icon is clicked on 
 */
const AdminList = (props) => {
    /**
     * Handles the deletion status of the user
     * @param {*} user the user to have their deletion status changed
     */
    const handleDelete = (user) => {
        if (!user.isDeleting && props.canDelete) {
            const MESSAGE = `This action will mark this admin for deletion from the organization. ${props.session.id == user.UserID ? 'Please note that if you remove yourself from this organization you will no longer have any access to this organization.': ''} Please note that changes are not final until submission.`;
            props.forceConfirm(MESSAGE, () => props.deleteCallback(user), true);
        }
        else if (user.isDeleting) { // The undo delete, so check that the given user is being deleted
            props.deleteCallback(user);
        }
    };

    /**
     * Main HTML Component
     */
    return (
        <div className='org-admin-list'>
            <label>{props.isUH ? 'UH Admin:' : 'Non-Institutional Admin:'}</label>
            {
                props.adminList.map(user =>
                    <Grid column={3} key={user.UserID} className={user.isDeleting && 'error-message'}>
                        <Grid.Column width={4} className='admin-name-column'>
                            {`${user.FirstName} ${user.LastName}`}
                            {
                                (user.isNew || user.isDeleting) && 
                                    <Popup content={`Please be sure to submit your changes to ${user.isDeleting ? 'remove' : 'add'} the admin ${user.isDeleting ? 'from' : 'to'} the organization`} 
                                           trigger={<div className='new-admin-notice'>*</div>}/>
                            }
                        </Grid.Column>
                        <Grid.Column width={8}>{props.isUH ? user.Username : user.EmailAddress}</Grid.Column>
                        <Grid.Column width={4}> 
                            {
                                props.deleteCallback &&
                                <div onClick={() => handleDelete(user)}>
                                    {
                                        !user.isDeleting ?
                                            <Popup trigger={<div className={!props.canDelete ? 'disabled-field' : ''}><img src="https://star.hawaii.edu/cdn/images/civic-engagement/icons/trashcan.svg"/> Remove</div>}
                                                   content='Organizations must have at least 1 admin' disabled={props.canDelete}/>
                                            :
                                            <>
                                                <Icon name='undo'/>
                                                Undo Removal
                                            </>
                                    }
                                </div>
                            }
                        </Grid.Column>                                                        
                    </Grid>
                )
            }
        </div>
    );
};

export default RootContainer(ModalContainer(AdminList));
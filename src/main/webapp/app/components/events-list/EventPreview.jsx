import React from 'react';
import { Redirect } from 'react-router-dom';
import { Icon, Button } from 'semantic-ui-react';
import LikesContainer from '../../redux/containers/event-likes.js';
import ModalContainer from '../../redux/containers/modal.js';
import RootContainer from '../../redux/containers/app.js';

import '../../css/event-preview.scss';

/**
 * Event Preview module for displaying events in upcoming events list
 */
class EventPreview extends React.Component {

    /**
     * constructor inits state
     * @param {*} props 
     */
    constructor(props){
      super(props);
      this.state = { redirect: false, liked: false };
    }

    /**
     * handle like button
     * props methods: likeEvent, dislikeEvent
     */
    handleLike(e){
      e.stopPropagation();
      this.setState({redirect: false});
      let { likeEvent, dislikeEvent, data, openModal } = this.props;
      const sendFunction = data.DateTimeEventLiked == null ? likeEvent : dislikeEvent;
      sendFunction({eventId: data.EventID}).then((response) => {
        if(!response.data.length || !response.data[0].Status){
          openModal('An unexpected error has occurred, event not liked!');
        }
        else if(response.data[0].Status){
          data.DateTimeEventLiked = data.DateTimeEventLiked == null ? new Date() : null;
          this.forceUpdate();
        }
      });
    }

    /**
     * Renders the small status text that appears at the bottom of the card
     * @param {*} data the specific event in question
     */
    renderAttendanceStatus(data) {
      let attendanceStatus = '';
      if (window.appConfig.getAttribute('data') == 'student') {
        if (data.IsAttending) {
          if (data.EventIsSelectiveAdmissions && !data.EventUserIsAcceptedForSelectAdmissions) {
            attendanceStatus = <Button className='preview-status preview-pending'><Icon name='checkmark'/>PENDING APPROVAL</Button>;
          }
          else {
            attendanceStatus = <Button className={data.IsPastEvent ? 'preview-status preview-attended' : 'preview-status preview-attending'}><Icon name='checkmark'/>{data.IsPastEvent ? 'ATTENDED' : 'ATTENDING'}</Button>;
          }
        }
        else if (data.IsPastEvent) {
          attendanceStatus = <Button className='preview-status preview-attended'><Icon name='close'/>ENDED</Button>;
        }
        else if (data.EventIsSelectiveAdmissions) {
          attendanceStatus = <Button className='preview-status preview-selective'><Icon name='checkmark'/>SELECTIVE ADMISSIONS</Button>;
        }
      }

      return attendanceStatus;
    }

    /**
     * main render function
     */
    render() {

      const DATA = this.props.data;
      
      // id={event.EventID}
      // imageUrl={event.EventImageURL}
      // dateDay={eventDate.getDate()}
      // dateMo={event.EventStartDateTime.split(' ')[0]}
      // name={event.EventName}
      // orgname={event.OrganizationName}
      // orgid={event.EventOrganizationID}
      // locationName={event.EventLocationName}
      // description={event.EventDescription}
      // attending={event.IsAttending}
      // isPast={event.IsPastEvent}

      const CARD = (
        <div key={DATA.EventID} onClick={() => this.setState({redirect: true})} className={'event-preview'+(this.props.first ? ' first' : '')}>
          {(this.state.redirect && !DATA.IsExternalEventIndicator) && <Redirect push exact to={`/event?id=${DATA.EventID}`}/>}
          <div className={'ep-picture'} style={{backgroundImage: `url(${DATA.EventImageURL})`, backgroundPositionY: `${DATA.EventImageCenterPosition*100}%`}}>
            <div className='ep-event-type'>
              {!DATA.IsExternalEventIndicator ? 'University of Hawai\'i' : 'External'} {DATA.EventTypeInternshipIndicator ? 'Internship' : 'Local Opportunity'}
            </div>
          </div>
          <div className={'ep-info'}>
            <div className={'ep-date'}>
              {DATA.isOngoing && <>Ongoing<br/></>}
              <span className={DATA.isOngoing ? 'ongoing-preview' : ''}>{dateFns.format(new Date(DATA.EventStartDateTime), 'ddd, MMM D, YYYY | h:mm A')}</span>
            </div>
            <div className='et-name-container'>
              <p style={{WebkitBoxOrient: 'vertical'}} onClick={() => this.setState({redirect: true})} className={'et-eventname'}>{DATA.EventName}</p>
              <p>By: {DATA.OrganizationName}</p>
            </div>
            <div className='button-container'>
              {/* if in student side show reg controls and buttons */}
              {this.renderAttendanceStatus(DATA)} 
            </div>
            {
              this.props.session.isLogged &&
                <div className='like-container'>
                  <div onClick={this.handleLike.bind(this)}>
                    <img className='like-icon' src={`https://www.star.hawaii.edu/cdn/images/civic-engagement/${DATA.DateTimeEventLiked != null ? 'LikeIcon-liked' : 'LikeIcon-notliked'}.svg`} />
                  </div>
                </div>
            }
          </div>
        </div>
      );

      return (
        DATA.IsExternalEventIndicator ?
          <a href={DATA.ExternalEventURL} target="_blank" rel="noopener noreferrer">
            {CARD}
          </a>
          :
          CARD
      );
    }
}

export default RootContainer(ModalContainer(LikesContainer(EventPreview)));
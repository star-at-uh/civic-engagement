import React from 'react';
import { List } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

/**
 * Component to render a Messages list message preview
 */
class MessagesListedItem extends React.Component {
    
  /**
   * main render function
   */
  render() {
    
    const months = ["January", "February", "March",
                    "April", "May", "June", "July",
                    "August", "September", "October",
                    "November", "December"];
    return (
        <List.Item className={'message-listed-item'} as={Link} exact to={`/messages/thread?id=${1}`}>
            <List.Icon verticalAlign='middle'>
              <div className={'msg-list-avatar'} style={{backgroundImage: `url(${this.props.profileImage})`}}/>
            </List.Icon>
            <List.Content verticalAlign={'middle'} className={'msg-list-item-content'}>
                <List.Header>
                  {this.props.senderName} <span className={'msg-date'}>{months[this.props.date.getMonth()]} {this.props.date.getDate()}</span>
                </List.Header>
                <List.Description>{this.props.messagePreview}</List.Description>
            </List.Content>
            {
              this.props.unread &&
              <List.Icon color={'green'} verticalAlign='middle' size={'tiny'} name={'circle'}/>
            }
        </List.Item>
    );
  }
}

export default MessagesListedItem;
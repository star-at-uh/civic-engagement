import React from 'react';
import MessagesContainer from '../../redux/containers/messenger.js';
import { Grid, Icon, Form } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import MessageBubble from './MessageBubble.jsx';

import '../../css/message-thread.scss';

/**
 * Component to render a Message Thread / Chat
 */
class MessageThread extends React.Component {

  /**
   * constructor for thread, initializes state
   */
  constructor(props){
    super(props);
    this.state = {
      messageBoxText: null,
      messages: []
    }
  }

  /**
   * message send handler
   */
  submit(e){
    // stop form reload
    e.preventDefault();

    if(this.state.messageBoxText){
      // send message
      console.log('sending message: ' + this.state.messageBoxText)
      const outBound = {incoming: false, text: this.state.messageBoxText};
      this.setState({messages: this.state.messages.concat(outBound)}, ()=> {
        // scroll to latest message
        const convo = document.getElementById('conversation');
        convo.scrollTop = convo.scrollHeight;

        // fake reply (for now)
        setTimeout(() => {
          const inBound = {incoming: true, text: 'If I may...'}
          this.setState({messages: this.state.messages.concat(inBound)}, ()=> {
            // scroll to latest message
            const convo = document.getElementById('conversation');
            convo.scrollTop = convo.scrollHeight;
          });
        }, 500);
      });

      // reset box
      document.getElementById('msg-input').reset();
      this.setState({messageBoxText: null});
    }
    return false;
  }
    
  /**
   * main render function
   */
  render() {
    return (
        <div id={'message-thread'}>
          <div id={'msg-thread-header'}>
            <Grid>
              <Grid.Row>
                <Grid.Column width={3} textAlign={'left'} verticalAlign={'middle'}>
                  <Link exact to='/messages'>
                    <Icon name={'arrow left'} size={'large'} />
                  </Link>
                </Grid.Column>
                <Grid.Column width={10} className={'avatar-column'}>
                  <div className={'avatar-container'} verticalAlign={'middle'}>
                    <div className={'msg-thread-avatar'} style={{backgroundImage: `url(${'https://manoa.hawaii.edu/news/attachments/img3893_1061l.jpg'})`}}/>
                    <div className={'short-name'}><h1>Gary R</h1></div>
                  </div>
                </Grid.Column>
                <Grid.Column width={3} />
              </Grid.Row>
            </Grid>
          </div>

          <div id={'conversation'}>
            {this.state.messages.map((message) => {
              return (<MessageBubble incoming={message.incoming} text={message.text} />);
            })}
          </div>

          <Form id={'msg-input'} onSubmit={this.submit.bind(this)}>
            <Grid>
              <Grid.Row>
                <Grid.Column width={12} id={'text-input-column'}>
                  <Form.Input onChange={
                    // relays change to state
                    (e, data) => 
                      this.setState({messageBoxText: data.value == '' ? null : data.value})
                    }
                    className='' placeholder='type your message' />
                </Grid.Column>
                <Grid.Column width={4}>
                  <Form.Button type='submit'>send</Form.Button>
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form>
            
        </div>
    );
  }
}

export default MessagesContainer(MessageThread);
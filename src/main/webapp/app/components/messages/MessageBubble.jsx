import React from 'react';

import '../../css/message-bubble.scss';

/**
 * Component to render a Message Bubble in a message thread
 */
class MessageBubble extends React.Component {
    
  /**
   * main render function
   */
  render() {
    
    return (
        <div className={'message-bubble-container'}>
            <div className={'message-bubble ' + (this.props.incoming ? 'left' : 'right')}>
              <p className={'message-content'}>{this.props.text}</p>
            </div>
        </div>
    );
  }
}

export default MessageBubble;
import React from 'react';
import { Tab } from 'semantic-ui-react';
import OrganizationEventsList from './OrganizationEventsList.jsx';

/**
 * Component for rendering the event tabs for an organization 
 */
const OrganizationEvents = (props) => {
    const PANES = [
        {
            menuItem: 'Upcoming',
            render: () => <OrganizationEventsList key={`upcoming-${props.organizationId}`} organizationId={props.organizationId} viewPast={false} />,
        },
        {
            menuItem: 'Past',
            render: () => <OrganizationEventsList key={`past-${props.organizationId}`} organizationId={props.organizationId} viewPast={true} />,
        }
    ];

    return (
        <Tab menu={{ secondary: true, pointing: true }} panes={PANES} />
    );
}

export default OrganizationEvents;
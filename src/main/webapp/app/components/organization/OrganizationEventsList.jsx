import React, { useState, useEffect } from 'react';
import OrganizationContainer from '../../redux/containers/organization.js';
import EventPreview from '../events-list/EventPreview.jsx';
import { sortEvents, markOngoingEvents } from '../../util/event-helper.js';
import { Loader } from 'semantic-ui-react';
import { getServerTime } from '../../util/TimeOptions.js';

/**
 * Component for rendering an organization's past or upcoming events
 */
const OrganizationEventsList = (props) => {
    const [events, setEvents] = useState([]);
    const [loading, setLoading] = useState(true);

    /**
     * Fetches the new list of events past or upcoming on viewPast change
     */
    useEffect(() => {
        setLoading(true);
        fetchEvents();
    }, [props.viewPast]);

    /**
     * Attempts to fetch a list of past/upcoming events and then sorts them by descending order on start date
     */
    const fetchEvents = () => {
        const EVENT_FETCHER = props.viewPast ? props.pastEvents : props.upcomingEvents;
        const TIME_PROMISE = getServerTime();

        Promise.all([EVENT_FETCHER({organizationId: props.organizationId}), TIME_PROMISE])
                .then(values => {
                    sortEvents(values[0].data, 'EventStartDateTime', false, 'date');
                    markOngoingEvents(values[0].data, 'EventStartDateTime', 'EventEndDateTime', values[1].data);
                    
                    setEvents(values[0].data);
                    setLoading(false);
                });
    };

    return (
        <div>
            {
                !loading &&
                    events.map((event) => {
                        const EVENT_DATE = new Date(event.EventStartDateTime);
                        return (
                            <EventPreview 
                                key={event.EventID}
                                data={event}
                                id={event.EventID}
                                imageUrl={event.EventImageURL}
                                dateDay={EVENT_DATE.getDate()}
                                dateMo={event.EventStartDateTime.split(' ')[0]}
                                name={event.EventName}
                                locationName={event.EventLocationName}
                                description={event.EventDescription}
                                orgname={event.OrganizationName}
                                isPast={event.IsPastEvent}
                            />
                        );
                    })
            }
    
            {(!loading && events.length == 0) && <center>No events to display</center>}

            {loading && <Loader className='search-loader' active={true} inline='centered'>Loading Events</Loader>}
        </div>
    );
}

export default OrganizationContainer(OrganizationEventsList);
import React from 'react';
import { List } from 'semantic-ui-react';

import '../../css/recent-experiences.scss';

/**
 * Component for rendering recent experiences in the profile page display
 */
class RecentExperiences extends React.Component{

    /**
     * UI Function to render each recent experience
     */
    recentExperience(name, group, date){
        return (
            <List.Item className={'mg-sgp'}>
                <List.Icon verticalAlign='middle'>
                    <h2 className={'view-exp'}>View Experience</h2>
                </List.Icon>
                <List.Content>
                    <List.Header as='a'>{name}</List.Header>
                    <List.Description as='a'>{group}</List.Description>
                    <List.Description as='a'>{date}</List.Description>
                </List.Content>
            </List.Item>
        )
    }

    /**
     * main render function
     */
    render(){
        return (
            <List divided relaxed size={'large'}>
                {this.recentExperience('HELP THE ELDERLY', 'Hawaii Kupunas', 'May 28, 2020')}
                {this.recentExperience('BEACH CLEAN-UP', 'Hawaii Kupunas', 'May 30, 2020')}
                {this.recentExperience('FOOD DRIVE', 'Hawaii Kupunas', 'Apr 1, 2020')}
            </List>
        );
    }
}

export default RecentExperiences;
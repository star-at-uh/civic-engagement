import React from 'react';

import '../../css/contact-info.scss';

/**
 * Component for rendering contant info in the profile page display
 */
class ContactInfo extends React.Component{


    /**
     * main render function
     */
    render(){
        return (
            <table className={'contact-info-table'}>
                <tr>
                    <td><strong>Phone:</strong></td>
                    <td>(808) 956-4036‬</td>
                </tr>

                
                <tr>
                    <td><strong>Email:</strong></td>
                    <td>starhelp@hawaii.edu</td>
                </tr>

                
                <tr>
                    <td><strong>Year:</strong></td>
                    <td>Senior</td>
                </tr>

                
                <tr>
                    <td><strong>Campus:</strong></td>
                    <td>Manoa</td>
                </tr>

                
                <tr>
                    <td><strong>Location:</strong></td>
                    <td>Honolulu, 96822</td>
                </tr>
            </table>
        );
    }
}

export default ContactInfo;
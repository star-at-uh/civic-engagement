import React from 'react';
import { Dimmer, Grid, Icon, Loader, Segment } from 'semantic-ui-react';
import EventPreview from '../events-list/EventPreview.jsx';
import EventsListContainer from '../../redux/containers/events-list.js';
import RootContainer from '../../redux/containers/app.js';
import ReactSwitch from 'react-switch';
import { Link } from 'react-router-dom';

import '../../css/upcoming-events.scss';
import { sortEvents, markOngoingEvents } from '../../util/event-helper.js';
import { getServerTime } from '../../util/TimeOptions.js';

/**
 * Component for rendering the upcoming events list on the homepage
 */
class UpcomingEvents extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      exploreEvents: [],
      registeredEvents: [],
      showRegistered: false,
      loading: true
    };
  }

  /**
   * Fires on initial load to fetch events
   */
  componentDidMount() {
    const PROMISE_ARRAY = [];
    const TIME_PROMISE = getServerTime();
    const EXPLORE_EVENTS_PROMISE = this.props.viewExploreEvents().payload;

    PROMISE_ARRAY.push(TIME_PROMISE, EXPLORE_EVENTS_PROMISE);

    if (!this.props.admin && this.props.session.isLogged) {
      PROMISE_ARRAY.push(this.props.viewClockInEvents().payload);
    }

    Promise.all(PROMISE_ARRAY)
            .then(values => {
              sortEvents(values[1].data, 'EventStartDateTime', false, 'date');
              markOngoingEvents(values[1].data, 'EventStartDateTime', 'EventEndDateTime', values[0].data);

              const NEW_STATE = {
                exploreEvents: values[1].data, 
                loading: false
              };

              // Registered Events
              if (values[2]) {
                sortEvents(values[2].data, 'EventStartDateTime', false, 'date');
                markOngoingEvents(values[2].data, 'EventStartDateTime', 'EventEndDateTime', values[0].data);

                NEW_STATE.registeredEvents = values[2].data;
                NEW_STATE.showRegistered = this.props.showRegisteredFirst;
              }
              
              this.setState(NEW_STATE);
            });
  }

  /**
   * Main render function
   */
  render() {

    const events = this.state.showRegistered && !this.props.admin ? this.state.registeredEvents : this.state.exploreEvents
    
    return (
      <div key={this.state.events} className='eds-layout__body'>
        <Grid>
          <Grid.Row columns='equal'>
            <Grid.Column floated='left'>
              <div className="home-banner info-group">
                <img className="info-image right desktop-home" src="https://star.hawaii.edu/cdn/images/civic-engagement/USERVEPhone-image.png"/>
                <div className="info-text">
                  <h1 className="info-heading">DISCOVER<br/>SERVICE OPPORTUNITIES</h1>
                </div>
              </div>
            </Grid.Column>
            <Grid.Column floated='right' className='mobile-home'>
              <div className='mobile-org-link'>
                <Link className='link' exact='true' to='/organizations'>
                  BROWSE<br/>ORGANIZATIONS <Icon name='arrow right'/>
                </Link>
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
        {
          !this.props.admin &&
          <Grid>
            <Grid.Row columns='equal'>
              <Grid.Column floated='left'>
                <div className={'ue-top-toggle'}>
                  <div className={'ue-slider'}>ALL OPPORTUNITIES</div>
                  <ReactSwitch
                    className={'ue-slider'}
                    checked={this.state.showRegistered}
                    onChange={() => this.setState({ showRegistered: !this.state.showRegistered })}
                    onColor="#008B9D"
                    handleDiameter={20}
                    uncheckedIcon={false}
                    checkedIcon={false}
                    boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
                    activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
                    height={15}
                    width={37}
                  />
                  <div className={'ue-slider'}>MY REGISTERED OPPORTUNITIES</div>
                </div>
              </Grid.Column>
              <Grid.Column floated='right' className='desktop-home'>
                <div className='loadmore'><Link exact='true' to='/organizations'>BROWSE ORGANIZATIONS <Icon name='arrow right'/></Link></div>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        }

        <div id='upcoming-events-list'>
          {
            this.state.loading ? 
              <Segment key='event-loader' id='event-loader'><Dimmer inverted active><Loader content='Loading'/></Dimmer></Segment>
              :
              events.map((event, index) => {
                  return <EventPreview key={event.EventID} first={index == 0} data={event} />;
              })
          }
        </div>

        {!this.state.loading && events.length == 0 && this.state.showRegistered && <div id='sorry'><center>No events to display</center></div>}

        {
          !this.state.loading && events.length == 0 && !this.state.showRegistered &&
          <div id='sorry'>
            We're sorry, there are no community engagement events at this time. Please feel free to browse our partner{'\u00A0'}
            <Link exact to='/organizations'>organizations</Link>.
            </div>
        }

        <br />
      </div>
    );
  }
}

export default RootContainer(EventsListContainer(UpcomingEvents));
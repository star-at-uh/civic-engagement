import React from 'react';
import { Form, Grid, Icon, Button, Radio } from 'semantic-ui-react';
import _ from 'lodash';

/**
 * Component for rendering a 3-option privacy radio group in settings
 */
class PrivacyCombo extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            choice: this.props.seedValue ? 0 : this.props.seedValue,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(value){
        this.setState({choice: value});
    }

    render(){
        return (
            <div value={this.state.choice}>
                
            <Grid.Row className={'setting-row'}>
                <Grid.Column>
                    <Form.Field>
                    <Radio
                        label='Everyone'
                        name={this.props.id ? this.props.id : 'radioGroup'}
                        value='this'
                        checked={this.state.choice == 0}
                        onChange={() => this.handleChange(0)}
                    />
                    </Form.Field>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row className={'setting-row'}>
                <Grid.Column>
                    <Form.Field>
                    <Radio
                        label='People in my groups'
                        name={this.props.id ? this.props.id : 'radioGroup'}
                        value='this'
                        checked={this.state.choice == 1}
                        onChange={() => this.handleChange(1)}
                    />
                    </Form.Field>
                </Grid.Column>
            </Grid.Row>

            <Grid.Row className={'setting-row'}>
                <Grid.Column>
                    <Form.Field>
                    <Radio
                        label='Only advisors'
                        name={this.props.id ? this.props.id : 'radioGroup'}
                        value='this'
                        checked={this.state.choice == 2}
                        onChange={() => this.handleChange(2)}
                    />
                    </Form.Field>
                </Grid.Column>
            </Grid.Row>

            </div>
        );
    }
}

export default PrivacyCombo;
import React from 'react';
import { Table } from 'semantic-ui-react';

class ListView extends React.Component{
    constructor(props){
        super(props);
    }

    /**
     * ListItem UI function
     */
    listItem(name, year, campus){
        return (
            <tr>
                <td>{name}</td>
                <td>{year}</td>
                <td>{campus}</td>
            </tr>
        )
    }

    render(){
        return (
            <table id='member-list'>
                <thead>
                    <tr>
                        <td>Name</td>
                        <td>Academic Year</td>
                        <td>Campus</td>
                    </tr>
                </thead>
                <tbody>
                    {this.props.members.map((member) => {
                        return this.listItem(member.name, member.year, member.campus);
                    })}
                </tbody>
            </table>
        )
    }
}  

export default ListView;
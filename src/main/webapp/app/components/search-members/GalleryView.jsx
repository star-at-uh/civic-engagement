import React from 'react';
import { Grid } from 'semantic-ui-react';
import UserPreview from '../global/UserPreview.jsx';

class GalleryView extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return (
            <Grid id='member-gallery'>
                <Grid.Row className={'gallery-view'}>
                    {this.props.members.map((member) => {
                        return (
                            <UserPreview id={member.EventUserID} firstName={member.EventUserFirstName} lastName={member.EventUserLastName} img={member.ProfilePicture} admin={false} />
                        )
                    })}
                </Grid.Row>
            </Grid>
        )
    }
}  

export default GalleryView;
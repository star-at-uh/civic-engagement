import React from 'react';
import { Menu, Grid } from 'semantic-ui-react';
import { Link, Redirect } from 'react-router-dom';
import MainMenuContainer from '../../redux/containers/main-menu.js';
import { logoutUServe } from '../../util/cas-helper.js';

import '../../css/footer.scss';

/**
 * Footer component 
 */
class Foot extends React.Component {

  constructor(props){
    super(props);
    this.state = { redirect: false }
  }

  /**
   * function to log out user
   */
  gohome(){
    this.props.logoutUser(this.props.session);
    this.setState({redirect: true}, ()=> logoutUServe());
  }
  
  
  /**
   * main render function
   */
  render() {
    return (
        <div id='foot-container'>
        <div id={'footer'}>
          {this.state.redirect ? 
          <Redirect to={{ pathname: '/'}}/> : ''}
            <Grid>
                <Grid.Row columns={3}>
                    <Grid.Column>  
                        <img id='footer-logo' src='https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/USERVE-Logo-Blue.svg'></img>
                    </Grid.Column>

                    <Grid.Column>
                        <Menu.Item
                            as={Link}
                            exact to='/'
                            className={'foot-link'}>
                            <div>
                            HOME
                            </div>
                        </Menu.Item>
                    </Grid.Column>
                
                    <Grid.Column className={'lastcolumn'}>

                        <Menu.Item
                            onClick={this.gohome.bind(this)}
                            className={'foot-link'}>
                            <div>
                            LOGOUT
                            </div>
                        </Menu.Item>
                        
                    </Grid.Column>
                </Grid.Row>
            </Grid>

        </div>
            <div id={'bottom-foot'}>
                University of Hawaii at Manoa | Powered by STAR
            </div>
        </div>
    );
  }
}

// wrap in redux container
export default MainMenuContainer(Foot);
import React, { Component } from "react";
import RSwitch from "react-switch";
 
/**
 * component for custom md-style toggle slider with custom background color
 */
class SBox extends Component {
  constructor(props) {
    super(props);
    this.state = { checked: false, changed: false };
    this.handleChange = this.handleChange.bind(this);
  }

  /**
   * Sets the checked state to the prop if present upon component mount
   */
  componentDidMount(){
    // set initial value
    if(this.props.value) {
      this.setState({checked: this.props.value});
    }
  }

  /**
   * Called when a value changes
   * Currently used to check if the checked value was modified externally, and updates internal state to match
   * @param {*} prevProps 
   */
  componentDidUpdate(prevProps) {
    if (this.props.value != prevProps.value) {
      this.setState({checked: this.props.value});
    }
  }
 
  /**
   * function to set checkbox state
   * @param {boolean} checked the value the input was set to
   * @param {Object} event the event that triggered the callback, may or may not contain the value
   * @param {String} name the id given to the Switchbox being triggered
   */
  handleChange(checked, event, name) {
    if(this.props.callback) {
      this.props.callback(event, {name, checked, type: 'checkbox'});
    }

    this.setState({ checked, changed: true });
  }
 
  // main render function
  render() {
    return (
      <div id={this.props.id ? this.props.id : null} value={this.state.checked} key={this.props.name}>
        <RSwitch
            disabled={this.props.disabled}
            className={this.props.className}
            id={this.props.name}
            checked={this.props.value && !this.state.changed ? this.props.value : this.state.checked}
            onChange={this.handleChange}
            onColor="#008B9D"
            handleDiameter={20}
            uncheckedIcon={false}
            checkedIcon={false}
            boxShadow="0px 1px 5px rgba(0, 0, 0, 0.6)"
            activeBoxShadow="0px 0px 1px 10px rgba(0, 0, 0, 0.2)"
            height={15}
            width={37}
        />
      </div>
    );
  }
}

export default SBox;
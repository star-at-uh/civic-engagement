import React from 'react';
import { Form, Segment, Header, Icon, Button } from 'semantic-ui-react';
import _ from 'lodash';

import '../../css/image-upload.scss';

/**
 * UI function to render a image uploader
 */
const MIN_PERCENTAGE = 0.00001; // workaround for 0, null, and undefined all eval to false
class ImageUpload extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            image: '',
            onceRemoved: false,
            centerReceived: false,
            center: this.props.center,
            aspectX: 676.2,
            aspectY: 405,
            width: 0,
            height: 0,
            minPercent: MIN_PERCENTAGE,
            maxPercent: 1,
            maxPercentFactor: 1
        }
        this.uploadFile = this.uploadFile.bind(this);
        this.propagateImageUpdate = this.propagateImageUpdate.bind(this);

        this.imageRef = React.createRef();
    }

    /**
     * fires when component loads
     */
    componentDidUpdate(){
        if(this.props.image && this.props.image != this.state.image && !this.state.centerReceived){

            let { aspectX, aspectY, centerReceived, center } = this.state;

            let i = new Image(); 
            const PARENT_CLASS = this;

            i.onload = function(){
                let maxPercentFactor = 1 - (((aspectY/aspectX)*i.width)/(i.height));
                PARENT_CLASS.setState({
                    width: i.width,
                    height: i.height,
                    maxPercentFactor,
                    center: PARENT_CLASS.props.center ? PARENT_CLASS.props.center : !centerReceived ? (0.5-(((aspectY/aspectX)*i.width)/(2*i.height))) : center
                });
            };

            i.src = this.props.image; 
        }
        if(this.props.center && !this.state.centerReceived){
            this.setState({center: this.props.center, centerReceived: true});
            this.forceUpdate();
        }
    }

    /**
     * function to handle file input change and display base64 image
     */
    uploadFile(event) {
        const FILE = event.target.files[0];

        if (!FILE) {
            return;
        }

        const FILE_READER = new FileReader();
        FILE_READER.readAsDataURL(FILE);

        FILE_READER.onloadend = () => {
            const IMAGE = new Image();
            IMAGE.src = FILE_READER.result;
            IMAGE.onload = () => {
                // Resize the image
                const CANVAS = document.createElement('canvas');
                const MAX_SIZE = 500;
                let width = IMAGE.width;
                let height = IMAGE.height;
                
                if (width > height) {
                    if (width > MAX_SIZE) {
                        height *= MAX_SIZE / width;
                        width = MAX_SIZE;
                    }
                } 
                else {
                    if (height > MAX_SIZE) {
                        width *= MAX_SIZE / height;
                        height = MAX_SIZE;
                    }
                }

                CANVAS.width = width;
                CANVAS.height = height;
                CANVAS.getContext('2d').drawImage(IMAGE, 0, 0, width, height);
                const IMAGE_BASE_64 = CANVAS.toDataURL(FILE.type);

                const { aspectX, aspectY } = this.state;
                const MAX_PERCENT_FACTOR = 1 - (((aspectY/aspectX)*width)/(height));
                const CENTER = 0.5;
                CANVAS.toBlob((blob) => {
                    this.setState({ 
                            image: blob, 
                            width, 
                            height, 
                            maxPercentFactor: MAX_PERCENT_FACTOR, 
                            center: CENTER 
                        }, () => this.propagateImageUpdate(FILE.name, this.state.image, IMAGE_BASE_64, CENTER)
                    );
                }, FILE.type);
            }
        };
    }

    /**
     * handles image centering
     */
    handleResize(event) {
        if(this.state.mousedown) {
            let { center, startPos, maxPercent, minPercent, maxPercentFactor } = this.state;
            const HEIGHT = this.imageRef.current.height;
            const PRE_CENTER = center + ((startPos - event.screenY) / (maxPercentFactor * HEIGHT));
            const FINAL_CENTER = PRE_CENTER >= maxPercent ? maxPercent : PRE_CENTER <= minPercent ? minPercent : PRE_CENTER;

            this.setState({
                startPos: event.screenY, 
                center: FINAL_CENTER, 
                centerReceived: true
            }, () => {
                if (this.props.handleInputChange) {
                    // Set the center state for the parent 
                    this.props.handleInputChange(null, {
                        value: FINAL_CENTER,
                        name: "imageCenter"
                    });
                }
            });
        }
    }

    /**
     * UI function to display upload form
     */
    uploadForm() {
        return (
            <Form.Field>
                <label>{this.props.imagetype} IMAGE</label>
                <Segment placeholder className={'photo-upload'}>
                    <Header icon>
                        <Icon name='upload' />
                    </Header>
                    <input
                        type='file'
                        style={{ display: 'none' }}
                        id='image-upload'
                        name='image-upload'
                        accept='image/png, image/jpeg'
                        onChange={this.uploadFile} />
                    <Button className='upload-button' size='small' onClick={() => document.getElementById('image-upload').click()}>Upload Image</Button>
                </Segment>
            </Form.Field>
        );
    }

    /**
     * Removes the currently uploaded image
     */
    removeImage() {
        this.setState({
                image: null, 
                onceRemoved: true, 
                center: 0
            }, () => this.propagateImageUpdate('', null, '', 0)
        );
    }

    /**
     * Update/Bubble up the various image states to this component's parent
     */
    propagateImageUpdate(imageName, imageBlob, imageBase64, imageCenter) {
        if (this.props.handleInputChange) {
            this.props.handleInputChange(null, {
                value: imageBlob,
                name: "imageBlob"
            });
            this.props.handleInputChange(null, {
                value: true,
                name: "imageChanged"
            });
            this.props.handleInputChange(null, {
                value: imageName,
                name: "imageName"
            });
            this.props.handleInputChange(null, {
                value: imageBase64,
                name: "imageUrl"
            });
            this.props.handleInputChange(null, {
                value: imageCenter,
                name: "imageCenter"
            });
        }
    }

    /**
     * UI function to display current uploaded image
     */
    imageDisplay() {
        return (
            <Form.Field>
                <label>{this.props.imagetype} IMAGE</label>
                <div id='image-upload-container'>
                    <div id='top-block' className='shaded-block'>
                        <p>click and drag the section below to choose center position</p>
                    </div>
                    <div className={`crop-container ${this.props.imagetype == 'EVENT' ? 'event' : 'organization'}`} 
                            onMouseDown={(event) => this.setState({mousedown: true, startPos: event.screenY})}
                            onMouseUp={() => this.setState({mousedown: false})}
                            onMouseLeave={() => this.setState({mousedown: false})}
                            onMouseMove={_.debounce(this.handleResize.bind(this), 10, {
                                leading: true,
                            })}>
                        <img id='image-upload-preview'
                            ref={this.imageRef}
                            style={{
                                backgroundImage: `url(${this.props.image})`,
                                backgroundPositionY: `${this.state.center * 100}%`
                            }}
                        />
                    </div>
                    <div id='bottom-block' className='shaded-block'>
                        <p>click and drag the section above to choose center position</p>
                    </div>
                    <Button className='remove-button' onClick={this.removeImage.bind(this)}>
                        Remove
                    </Button>
                </div>
            </Form.Field>
        )
    }

    render() {
        if (this.state.image || (!this.state.onceRemoved && this.props.image)) {
            return this.imageDisplay(this.state.center);
        }
        else {
            return this.uploadForm();
        }
    }
}

export default ImageUpload;
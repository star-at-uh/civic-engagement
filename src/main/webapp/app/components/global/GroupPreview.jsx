import React from 'react';
import { Grid, GridColumn } from 'semantic-ui-react';

import '../../css/group-preview.scss';

/**
 * Component for rendering a group preview in the homepage
 */
class GroupPreview extends React.Component{

    /**
     * main render function
     */
    render(){
        return (
            <div className={'group-preview'}>
                <Grid className={'gp-grid'}>
                    <Grid.Row>
                        <GridColumn textAlign={'center'} className={'gp-img'} width={5} style={{backgroundImage: `url(${this.props.imageUrl})`}}>
                        </GridColumn>
                        <GridColumn width={11}>
                            <h1>{this.props.name}</h1>
                            <p>{this.props.tagline}</p>
                        </GridColumn>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}

export default GroupPreview;
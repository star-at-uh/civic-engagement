import React, { useState } from 'react';
import { Prompt, Redirect } from 'react-router-dom';
import ModalContainer from '../../redux/containers/modal.js'

/**
 * Leave confirmation modal
 */
const LeaveConfirm = (props) => {
    const [confirmedNavigation, setConfirmedNavigation] = useState(false);
    const [redirect, setRedirect] = useState(null);

    /**
     * Called when user attempts to navigate to a new page with modified values on the page
     * @param {*} nextLocation an object containing details about the user's next destination
     */
    const handleBlockedNavigation = (nextLocation) => {
        if (!confirmedNavigation && props.shouldBlockNavigation) {
            props.forceConfirm('You have unsaved changes. Are you sure you want to leave?', () => handleConfirmNavigationClick(nextLocation), true, 'Yes');
            return false;
        }
        
        return true;
    };
    
    /**
     * Handles navigation on confirmation
     * @param {*} nextLocation an object containing details about where the user was going to navigate to
     */
    const handleConfirmNavigationClick = (nextLocation) => { 
        if (nextLocation) {
            setConfirmedNavigation(true);
            setRedirect(nextLocation);
        }
    };

    /**
     * The main component to render
     */
    return [
        <Prompt
            key='browser-prompt'
            when={props.shouldBlockNavigation}
            message={handleBlockedNavigation}/>,
        redirect && <Redirect key='page-redirect' exact to={`${redirect.pathname}${redirect.search}`} />
    ];
};

export default ModalContainer(LeaveConfirm);
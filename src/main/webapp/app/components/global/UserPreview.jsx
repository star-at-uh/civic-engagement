import React from 'react';
import { Grid } from 'semantic-ui-react';

import '../../css/user-preview.scss';

/**
 * UI function to render a user-preview
 */
export default ({id, firstName, lastName, img, admin}) => {
    return (
        <Grid.Column width={1} key={id}>
            <div className={'user-container'}>
                <center>
                    <div className={admin ? 'admin-avatar' : 'user-avatar'} style={{backgroundImage: `url(${img ? img : 'https://star.hawaii.edu/cdn/images/civic-engagement/avatar-placeholder.gif'})`}}/>
                    <div className={admin ? 'admin-name' : 'user-name'}><h1>{firstName} {lastName}</h1></div>
                </center>
            </div>
        </Grid.Column>
      )
}
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Modal, Button } from 'semantic-ui-react';
import { storeISAT } from '../../../redux/reducers/isat.js';

/**
 * Displays a message about the user's pending ISAT renewal 
 */
const ISATNotify = (props) => {
    const dispatch = useDispatch();
    const ISAT = useSelector((state) => state.isat);

    /**
     * Close the dialog
     */
    const close = () => {
        dispatch(storeISAT({
            displayWarning: false
        }));
    };

    return (               
        <Modal open={ISAT.displayISATWarning} onClose={close} size='small'>
            <Modal.Header>Warning</Modal.Header>
            <Modal.Content>
                <Modal.Description>
                    Your Information Security Awareness Training (ISAT) is due to expire in {ISAT.ISATCountdownDays} days.
                    Please ensure you update your security training prior to the expiry date to prevent any restrictions on accessing STAR.
                    Click here for the <a href='https://www.hawaii.edu/its/acer/' target='_blank' rel='noopener noreferrer'>UH Security Awareness training</a>.
                </Modal.Description>
            </Modal.Content>
            <Modal.Actions>
                <Button
                    content="OK"
                    labelPosition='right'
                    icon='checkmark'
                    onClick={close}
                    positive />
            </Modal.Actions>
        </Modal>
    );
}

export default ISATNotify;
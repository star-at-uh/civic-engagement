import React from 'react';

/**
 * Wrapper class for <input type=date> to default to JQuery datepicker if not HTML5 supported
 */
class DatePicker extends React.Component {

    /**
     * Initialize props and variables
     */
    constructor(props) {
        super(props);
        this.callback = this.callback.bind(this);
    }

  /**
   * onload handler that chooses correct datepicker
   */
  componentDidMount() {
    if ($('[type="date"]').prop('type') != 'date') {
      $('[type="date"]').datepicker();
    }
  }

  /**
   * generic event callback wrapper for choosing date
   * 
   * @param {*} event 
   * @param {*} data 
   */
  callback(event, data) {
    if (this.props.callback) {
      this.props.callback(event, {value: event.target.value, name: event.target.name});
    }
  }

  /**
   * main render function
   */
  render() {
    return (
      this.props.value != null ?
        <input type='date'
          disabled={this.props.disabled}
          value={this.props.value}
          id={this.props.id}
          name={this.props.name}
          min={this.props.min}
          max={this.props.max}
          className={`ui form field datepicker ${this.props.className ? this.props.className : ''}`}
          placeholder='mm/dd/yyyy'
          onChange={this.callback} />
        :
        <input type='date'
          id={this.props.id}
          disabled={this.props.disabled}
          min={this.props.min}
          max={this.props.max}
          className={`ui form field datepicker ${this.props.className ? this.props.className : ''}`}
          placeholder='mm/dd/yyyy'
          onChange={this.callback} />
    );
  }
}

export default DatePicker;


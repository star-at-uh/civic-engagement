import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';

/**
 * Component for forcing a confirm callback (such as on session expire)
 */
class ForceConfirm extends React.Component{
    constructor(props){
        super(props);
        
        this.onCloseHandler = this.onCloseHandler.bind(this);
    }

    /**
     * fires callback on modal close
     * @param {boolean} fireCallback signals whether or not to fire the passed in callback or not when closing the modal
     */
    onCloseHandler(fireCallback){
        this.props.closeModal();
        if (fireCallback && this.props.modal.callback) {
            this.props.modal.callback();
        }
    }

    render(){
        return(
            <Modal
                open={this.props.modal.forceOpen}
                onClose={this.onCloseHandler}
            >
                {this.props.modal.header && <Modal.Header>{this.props.modal.header}</Modal.Header>}
                <Modal.Content>
                    <Modal.Description>
                        <div dangerouslySetInnerHTML={{__html: this.props.modal.message}}></div>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button
                        content={this.props.modal.confirmText ?? "OK"}
                        labelPosition='right'
                        icon='checkmark'
                        onClick={() => this.onCloseHandler(true)}
                        positive/>
                    {
                        this.props.modal.allowChoice &&
                        <Button
                            content="Cancel"
                            labelPosition='right'
                            icon='times'
                            onClick={() => this.onCloseHandler(false)}
                            negative/>
                    }
                </Modal.Actions>
            </Modal>
        );
    }

}

export default ModalContainer(ForceConfirm);
import React from 'react';
import { Modal, Button } from 'semantic-ui-react';
import ModalContainer from '../../redux/containers/modal.js';

/**
 * Component for modals
 */
class Alerts extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <Modal
                open={this.props.modal.isOpen}
            >
                <Modal.Header>Warning!</Modal.Header>
                <Modal.Content>
                    <Modal.Description id='alert-modal-content'>
                        {this.props.modal.message}
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button
                    content="OK"
                    labelPosition='right'
                    icon='checkmark'
                    onClick={this.props.closeModal}
                    positive/>
                </Modal.Actions>
            </Modal>
        );
    }

}

export default ModalContainer(Alerts);
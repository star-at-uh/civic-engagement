import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { Menu, Icon } from 'semantic-ui-react';
import NavContainer from '../../redux/containers/nav.js';
import RootContainer from '../../redux/containers/app.js';

import '../../css/nav.scss';

/**
 * Component for rendering top-page main navigation bar
 */
const Nav = (props) => {
    /**
     * UI function: opens main menu
     */
    const openMenu = () => {
        // Needed to open the nav (will look into later if time permits)
        document.getElementById('main-menu').style.display = 'block';
    };

    /**
     * Main HTML Section
     */
    return (
        <Menu className={'main-nav'} attached='top' borderless>
            <Menu.Item as={NavLink} activeClassName='' exact to='/'>
                <img id='nav-logo' src='https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/USERVE-Logo-Blue.svg'></img>
            </Menu.Item>
            <Menu.Item position='right'>
                {
                    props.session.isLogged &&
                        <>Logged in as: {props.session.name}</>
                }
                
                { 
                    (window.appConfig.getAttribute('data') != 'advisor' || props.session.siteAdmin || props.session.orgAdmin) && 
                        <Link className={'nav-icon'} exact='true' to='/search' ><Icon name='search'></Icon></Link>
                }
                <Icon name={'bars'} onClick={openMenu} className={'nav-icon'}/>
            </Menu.Item>
        </Menu>
    );
};

// wrap in redux container
export default RootContainer(NavContainer(Nav));
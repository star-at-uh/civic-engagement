import React from 'react';

import '../../css/notifications.jsx';

/**
 * Component for rendering the notifications popup from the menu bar
 */
class Notifications extends React.Component {
    
  render() {
    return (
        <div>
            <div className={'notification'}>
                <p>You went to <strong>BEACH CLEANUP</strong></p>
            </div>
            <div className={'notification'}>
                <p><strong>YMCA HAWAII</strong> posted a new event.</p>
            </div>
        </div>
    );
  }
}

export default Notifications;
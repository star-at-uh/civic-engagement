import React, { useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import RouteCallbacksContainer from '../../redux/containers/route-callbacks.js';

/**
 * Top level component used to trigger callbacks when switching pages
 */
const RouteCallBacks = ({ history, messenger, openMessenger, closeMessenger }) => {
    useEffect(() => history.listen((location, action) => {
        
        // close menu and scrolltop when switching pages
        const mainMenu = document.getElementById('main-menu');
        const scrollBody = document.getElementById('scroll-body');
        if(mainMenu){
            document.getElementById('main-view').style = '';
            document.getElementById('body').style = '';
            mainMenu.style.display = 'none';
        }
        scrollBody?.scroll({top: 0, behavior: 'smooth'});

        // If it's 1, they either just logged in or they just refreshed the page
        if (window.lastLocation.length > 1) {
            sessionStorage.setItem('previousPage', JSON.stringify(window.lastLocation[window.lastLocation.length - 1]));
        }

        // saving state, reset or init if going back home
        if(!window.lastLocation || location.pathname == '/'){
            window.lastLocation = [{pathname: '/', search: '', hash: '', state: undefined}];
        }
        window.lastLocation.push(location);

        // catch non-pushed states
        window.history.replaceState({}, null, window.location.href)

        // hide footer callback
        if(location.pathname == '/messages/thread'){
            openMessenger();
        } else {
            closeMessenger();
        }
    }), []);

    return <></>;
}

export default withRouter(RouteCallbacksContainer(RouteCallBacks));
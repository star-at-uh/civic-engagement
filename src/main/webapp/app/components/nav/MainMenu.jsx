import React, { useState, useEffect } from 'react';
import { Icon, Menu } from 'semantic-ui-react';
import { Redirect, NavLink } from 'react-router-dom';
import MainMenuContainer from '../../redux/containers/main-menu.js';
import RootContainer from '../../redux/containers/app.js';
import closeMenu from '../../util/CloseMenu.js';
import { logoutUServe } from '../../util/cas-helper.js';

import '../../css/main-menu.scss';

/**
 * Main menu component accessed via nav bar
 */
const MainMenu = (props) => {
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    if (redirect) {
      logoutUServe();
    }
  }, [redirect]);

  /**
   * function to log out user
   */
  const goHome = (event) => {
    if (!props.session.isLogged) {
      props.loginUser(event);
    }
    else {
      props.logoutUser(props.session);
      setRedirect(true);
    }
  };

  /**
   * UI function for a menu divider element
   */
  const divider = (numberLabel) => {
    return (
      <div className='menu-divider' key={`menu-divider#${numberLabel}`}></div>
    );
  };

  /**
   * main render function
   */
    return (
      <div id={'main-menu'} onClick={(e) => {e.stopPropagation(); /** stop closemenu function from clicking screen **/}}>
        {redirect ? <Redirect push to={{ pathname: '/'}}/> : ''}
        <div className={'exit'}>
          <Icon name={'close'} id='closemenu' onClick={closeMenu}/>
        </div>
        <div className={'mm-options'}>
          { // student menu 
            window.appConfig.getAttribute('data') == 'student' &&
            [
              <div className={'mm-option'} key='home'>
                <Menu.Item
                  as={NavLink}
                  exact to='/'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  HOME
                </Menu.Item>
              </div>,
              divider(0),
              props.session.isLogged && 
                <div className={'mm-option'} key='events'>
                  <Menu.Item
                    as={NavLink}
                    exact to='/my-events'
                    activeClassName={'mm-selected'}
                    className={'mm-link'}>
                    EVENTS
                  </Menu.Item>
                </div>,
              <div className={'mm-option'} key='organization'>
                <Menu.Item
                  as={NavLink}
                  exact to='/organizations'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  ORGANIZATIONS
                </Menu.Item>
              </div>,
              props.session.isLogged && divider(1), // This is to avoid having a thick line due to 2 dividers overlapping
              props.session.isLogged &&
                <div className={'mm-option'} key='clock-in-out'>
                  <Menu.Item
                    as={NavLink}
                    exact to='/clock-in-out'
                    activeClassName={'mm-selected'}
                    className={'mm-link'}>
                    CLOCK IN/OUT
                  </Menu.Item>
                </div>,
              props.session.isLogged &&
                <div className={'mm-option'} key='progress'>
                  <Menu.Item
                    as={NavLink}
                    exact to='/my-progress'
                    activeClassName={'mm-selected'}
                    className={'mm-link'}>
                    TRACK MY HOURS
                  </Menu.Item>
                </div>,
              props.session.isLogged &&
                <div className={'mm-option'} key='favorites'>
                  <Menu.Item
                    as={NavLink}
                    exact to='/my-favorites'
                    activeClassName={'mm-selected'}
                    className={'mm-link'}>
                    FAVORITES
                  </Menu.Item>
                </div>
            ]
          }
          { // admin menu
            // These options are available to only those who have any amount of permission 
            (window.appConfig.getAttribute('data') == 'advisor' && (props.session.orgAdmin || props.session.siteAdmin)) &&
            [
              <div className={'mm-option'} key='admin-home'>
                <Menu.Item
                  as={NavLink}
                  exact to='/'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  HOME
                </Menu.Item>
              </div>,
              divider(2),
              <div className={'mm-option'} key='admin-event-list'>
                <Menu.Item
                  as={NavLink}
                  exact to='/events-list'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  EVENTS
                </Menu.Item>
              </div>,
              <div className={'mm-option'} key='organization'>
                <Menu.Item
                  as={NavLink}
                  exact to='/organizations'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  ORGANIZATIONS
                </Menu.Item>
              </div>,
              divider(3),
              <div className={'mm-option'} key='admin-create-event'>
                <Menu.Item
                  as={NavLink}
                  exact to='/create-event'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  CREATE AN EVENT
                </Menu.Item>
              </div>
            ]
          }
          {/* This option is available to all admins */}
          {
            window.appConfig.getAttribute('data') == 'advisor' &&
              <div className={'mm-option'} key='admin-create-organization'>
                <Menu.Item
                  as={NavLink}
                  exact to='/create-organization'
                  activeClassName={'mm-selected'}
                  className={'mm-link'}>
                  CREATE AN ORGANIZATION
                </Menu.Item>
              </div>
          }
          {divider(4)}
          
          <h1 className={'mm-link'} onClick={goHome}>{props.session.isLogged ? 'LOGOUT' : 'LOGIN'}</h1>
        </div>
      </div>
  );
};

// wrap in redux container
export default RootContainer(MainMenuContainer(MainMenu));
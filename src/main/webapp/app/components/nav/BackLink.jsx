
import React from 'react';
import { Icon } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

/**
 * class to create in-app back button that works with react router
 */
const BackLink = (props) => {
    /**
     * Function to handle going back, removes history from stack
     */
    const goBack = () => {
        if (props.history.length > 1) {
            props.history.goBack();
        }
        else {
            // A fallback in case the history object supposedly reports there is no history
            props.history.replaceState('/');
        }
    };

    /**
     * Main component to render
     */
    return (
        <div>
            <div className='ui-link' onClick={goBack}><Icon name='arrow left' /> go back</div>
        </div>
    );
};

export default withRouter(BackLink);
import React from 'react';
import { Form, Input } from 'semantic-ui-react';

/**
 * This component serves to act as a phone input for this app, as this app
 * will have all phone numbers entered onto the app be formatted the same way
 * @param {*} value a variable that serves as the value of the input
 * @param {boolean} required true displays a * next to the label, false displays nothing
 * @param {function} onChange a callback function that is called upon the input changing, passes (event, data)
 * @param {String} name name of the input 
 * @param {String} label the label to display above the input
 * @param {String} placeholder the text to display when the input has no value
 * @param {String} className class to append to the input
 */
class PhoneInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            phoneNumber: '',
        };

        this.handlePhoneInput = this.handlePhoneInput.bind(this);
        this.phoneInputFocusHandler = this.phoneInputFocusHandler.bind(this);
        this.setSelectionPosition = this.setSelectionPosition.bind(this);

        this.phoneInputRef = React.createRef();
    }

    /**
     * Updates the value prop given and formats the inputted numbers if possible
     * @param {*} event 
     * @param {*} data 
     */
    handlePhoneInput(event, data) {
        let phoneInput = '';
        const PHONE_REGEX = /[\(\)\-\s]/g;
        let filteredString = data.value.replace(PHONE_REGEX , '');
        let selectionStartIndex = event.target.selectionStart;

        if (!isNaN(event.nativeEvent.data) && filteredString.length < 11) {
            // Handle backspace input at specific indicies
            if (event.nativeEvent.inputType === 'deleteContentBackward') {
                if (selectionStartIndex == 4 || selectionStartIndex == 5) {
                    filteredString = `${filteredString.slice(0, 2)}${filteredString.slice(3)}`;
                    selectionStartIndex = 3;
                }
                else if (selectionStartIndex == 6) {
                    selectionStartIndex = 4;
                }
                else if (selectionStartIndex == 9) {
                    filteredString = `${filteredString.slice(0, 5)}${filteredString.slice(6)}`;
                    selectionStartIndex = 8;
                }
                else if (selectionStartIndex == 10) {
                    selectionStartIndex = 9;
                }
            }
            else { // Handles input positioning at specific indicies
                if (selectionStartIndex == 1 || selectionStartIndex == 10 || selectionStartIndex == 6 || selectionStartIndex == 0) {
                    selectionStartIndex++;
                }
                else if (selectionStartIndex == 5) {
                    selectionStartIndex += 2;
                }
            }
            
            // Constructing the phone # string
            if (filteredString.length <= 3) {
                phoneInput = `(${filteredString}`;
            }
            else if (filteredString.length <= 6) {
                phoneInput = `(${filteredString.substring(0,3)})-${filteredString.substring(3)}`;
            }
            else {
                phoneInput = `(${filteredString.substring(0,3)})-${filteredString.substring(3,6)}-${filteredString.substring(6, 10)}`;
            }

            data.value = phoneInput;
            this.setState({phoneNumber: phoneInput}, this.setSelectionPosition.bind(this, selectionStartIndex));
            this.props.onChange(event, data);
        }
        else {
            this.setSelectionPosition(this.phoneInputRef.current.inputRef.selectionStart);
        }
    }

    /**
     * Checks if a parenthesis needs to be added or removed from the beginning of the phone input when
     * the input is focused, in order to try reduce confusion of what the user needs to type.
     * @param {*} event the native event object from the input being focused/unfocused 
     */
    phoneInputFocusHandler(event) {
        const DATA = {
            name: event.target.name,
        };

        if (event.nativeEvent.type === 'focus' && this.props.value.length < 1) {
            DATA.value = '(';
            this.props.onChange(event, DATA);
        }
        else if (event.nativeEvent.type === 'blur' && this.props.value.length == 1) {
            DATA.value = '';
            this.props.onChange(event, DATA);
        }
    }

    /**
     * Updates the user's caret position in the input to the given index
     * @param {*} selectionStartIndex the index position in the input to move the user to
     */
    setSelectionPosition(selectionStartIndex) {
        if (selectionStartIndex) {
            this.phoneInputRef.current.inputRef.selectionStart = selectionStartIndex;
            this.phoneInputRef.current.inputRef.selectionEnd = selectionStartIndex;
        }
    }

    /**
     * Renders the phone input
     */
    render() {
        return (
            <Form.Field>
                <label>{this.props.label} {this.props.required && '*'}</label>
                <Input 
                    value={this.props.value}
                    onChange={this.handlePhoneInput}
                    onFocus={this.phoneInputFocusHandler}
                    onBlur={this.phoneInputFocusHandler}
                    name={this.props.name}
                    placeholder={this.props.placeholder}
                    className={this.props.className}
                    ref={this.phoneInputRef}/>
            </Form.Field>
        );
    }
}

export default PhoneInput;
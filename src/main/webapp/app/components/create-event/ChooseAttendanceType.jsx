import comboboxContainer from '../../redux/containers/combobox.js';
import React from 'react'
import { Dropdown, Form } from 'semantic-ui-react';

/**
 * Combobox component for attendance type
 */
class ChooseAttendanceType extends React.Component{
    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            options: [],
        }
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount(){
        // get types
        this.props.getAttendanceTypes().payload.then((response) => {
            const OPTIONS = [];
            response.data.forEach((option) => {
                OPTIONS.push({ key: option.AttendanceTypeID, text: option.AttendanceTypeName, value: option.AttendanceTypeID });
            })
            this.setState({options: OPTIONS});
        });
    }

    render(){
        return(
            <div key={this.props.value} className={this.props.className ? this.props.className : ''}>
                <Form.Select
                    fluid
                    disabled={this.props.disabled}
                    search
                    label='ATTENDANCE TYPE *'
                    name={this.props.name}
                    options={this.state.options}
                    value={this.props.value}
                    onChange={this.props.callback}
                    placeholder='Select attendance type'
                />
            </div>
        );
    }
}

export default comboboxContainer(ChooseAttendanceType);
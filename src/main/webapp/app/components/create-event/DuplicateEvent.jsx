import React from 'react';
import { Modal, Button, Form } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import CreateEventContainer from '../../redux/containers/create-event.js';
import ModalContainer from '../../redux/containers/modal.js';
import DatePicker from '../global/DatePicker.jsx';
import TIME_OPTIONS from '../../util/TimeOptions.js';

/**
 * A modal for duplicating an event
 * @param {function} handleClose function that controls the variable given as the decider of open or close, will pass false as 1st param
 * @param {boolean} showDupeEventModal variable that controls if the modal should be shown or not
 * @param {string/number} eventId the id of the event being duplicated
 */
class DuplicateEvent extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            endDate: '',
            startDate: '',
            endTime: '',
            startTime: '',
            errorMessage: '',
            newEventId: null,
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.submitDuplicateEvent = this.submitDuplicateEvent.bind(this);
    }

    /**
     * Called upon component being updated (either via props or state)
     * @param {*} prevProps 
     */
    componentDidUpdate(prevProps) {
        // Check to see if we are trying to duplicate a different event, if so clear the fields for the new event
        if (prevProps.modal.eventId != this.props.modal.eventId) {
            this.setState({
                    endDate: '',
                    startDate: '',
                    endTime: '',
                    startTime: '',
                    errorMessage: '',
                    newEventId: null,
                });
        }
    }

    /**
     * Updates the state that manages inputs
     * @param {*} event raw web event data regarding the action triggering this function
     * @param {*} data formatted data containing at least a .value field.
     */
    handleInputChange(event, data) {
        // Dropdowns and textareas have different ways they format the event param (textarea -> event.target.value | dropdowns -> event.value)
        // For any old value not coming from either a dropdown or a text area you can just pass the value
        let value = null;

        if (data.type === 'checkbox') {
            value = data.checked;
        }
        else if (data.taget) {
            value = data.target.value
        }
        else {
            value = data.value
        }

        this.setState({[data.name]: value});
    }

    /**
     * Creates a duplicate event with the currently set datetime given by the user
     */
    submitDuplicateEvent() {
        const { startDate, startTime, endDate, endTime } = this.state;

        if (!startDate || !startTime || !endDate || !endTime) {
            this.setState({errorMessage: 'Please enter valid dates and times.'});
        }
        else {
            this.props.duplicateEvent({
                        eventId: this.props.modal.eventId,
                        startDate: `${startDate} ${startTime}`,
                        endDate: `${endDate} ${endTime}`,
                      })
                      .payload
                      .then(response =>  {
                        if(response.data.length > 0) {
                            if(response.data[0]['Status'] == 1) {
                                this.setState({newEventId: response.data[0].EventID, errorMessage: ''});
                            } 
                            else {
                                this.setState({errorMessage: response.data[0].ErrorMessage});
                            }
                        } 
                        else {                        
                            this.setState({errorMessage: 'Oops! Something went wrong. Please try again in a minute.'});
                        }
                      });
        }
    }

    /**
     * Returns the modal to be rendered for DuplicateEvent.jsx
     */
    render(){
        return(
            <Modal open={this.props.modal.isDuplicating}>
                <Modal.Content>
                    {
                        !this.state.newEventId ?
                            <div>
                                <h3>Enter the new date and time period for the new event</h3>
                                <Form>
                                    <Form.Group widths='equal'>
                                        <Form.Field className={(this.state.errorMessage.length > 0 && !this.state.startTime) ? 'missing-field' : ''}>
                                            <label>START DATE *</label>
                                            <DatePicker 
                                                name='startDate'
                                                value={this.state.startDate}
                                                callback={this.handleInputChange}/>
                                        </Form.Field>
                                        <Form.Field className={(this.state.errorMessage.length > 0 && !this.state.startTime) ? 'missing-field' : ''}>
                                            <label>END DATE * </label>
                                            <DatePicker 
                                                name='endDate'
                                                value={this.state.endDate}
                                                callback={this.handleInputChange}/>
                                        </Form.Field>
                                    </Form.Group>
                                    <Form.Group widths='equal'>
                                        <Form.Select
                                            name='startTime'
                                            fluid
                                            search
                                            label='START TIME *'
                                            options={TIME_OPTIONS}
                                            value={this.state.startTime}
                                            placeholder='Select time'
                                            onChange={this.handleInputChange}
                                            className={(this.state.errorMessage.length > 0 && !this.state.startTime) ? 'missing-field' : ''}
                                        />
                                        <Form.Select
                                            name='endTime'
                                            fluid
                                            search
                                            label='END TIME *'
                                            options={TIME_OPTIONS}
                                            value={this.state.endTime}
                                            placeholder='Select time'
                                            onChange={this.handleInputChange}
                                            className={this.state.errorMessage.length && !this.state.endTime > 0 ? 'missing-field' : ''}
                                        />
                                    </Form.Group>
                                </Form>
                            </div>
                            :
                            <div>
                                Would you like to view the new event?
                            </div>
                    }
                </Modal.Content>
                <Modal.Actions>
                    <span className='error-message'>{this.state.errorMessage}</span>
                    {
                        !this.state.newEventId ?
                            <Button onClick={this.submitDuplicateEvent}>Duplicate</Button>
                            :
                            <Link exact='true' to={`/event?id=${this.state.newEventId}`} onClick={this.props.closeModal}>
                                <Button>Yes</Button>
                            </Link>
                    }
                    <Button onClick={this.props.closeModal}>{!this.state.newEventId ? 'Cancel' : 'No'}</Button>
                </Modal.Actions>
            </Modal>
        );
    }

}

export default CreateEventContainer(ModalContainer(DuplicateEvent));
import comboboxContainer from '../../redux/containers/combobox.js';
import React from 'react'
import { Dropdown, Form } from 'semantic-ui-react';


/**
 * Combobox component for social cohort
 */
class ChooseSocialCohortType extends React.Component{
    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            value: null,
            options: [],
            changed: false
        }
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount(){
        this.props.getSocialCohortTypes().payload.then((response) => {
            let options = [];
            response.data.forEach((option) => {
                options.push({ key: option.SocialCohortTypeID, text: option.SocialCohortTypeName, value: option.SocialCohortTypeID });
            })
            this.setState({options: options});
        });
    }

    render(){
        return(
            <div key={this.props.seedValue}
                id='eventform-socialCohortType'
                thing={1}
                value={this.state.value}>
                <Form.Select
                    fluid
                    search
                    label='SOCIAL COHORT TYPE *'
                    options={this.state.options}
                    value={!this.state.changed && this.props.seedValue ? this.props.seedValue : this.state.value}
                    onChange={(event, value) => {
                        this.setState({value: value.value, changed: true}, () => {
                            document.getElementById('eventform-socialCohortType').value = value.value;
                        })
                    }}
                    placeholder='Select social cohort type'
                />
            </div>
        );
    }
}

export default comboboxContainer(ChooseSocialCohortType);
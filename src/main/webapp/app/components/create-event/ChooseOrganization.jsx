import organizationContainer from '../../redux/containers/organization.js';
import React from 'react'
import { Dropdown, Form } from 'semantic-ui-react';


/**
 * Combobox component for org
 */
class ChooseOrganization extends React.Component{
    /**
     * Constructor initializes state variables
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            options: [],
        }
    }

    /**
     * fires when component mounts, initializes data
     */
    componentDidMount(){
        if (!this.props.options) {
            this.props.getOwnOrganizations()
                        .then((response) => {
                            const OPTIONS = [];
                            response.data.forEach((option) => {
                                            OPTIONS.push({ key: option.OrganizationID, text: option.OrganizationName, value: option.OrganizationID });
                                        });

                            this.setState({options: OPTIONS});
                        });
        }
        else {
            this.setState({options: this.props.options});
        }
    }

    render(){
        return(
            <div key={this.props.seedValue}
                id='eventform-organization'
                className={this.props.className ? this.props.className : ''}
                thing={1}
                value={this.state.value}>
                <Form.Select
                    disabled={this.props.disabled}
                    fluid
                    name={this.props.name}
                    search
                    label='ORGANIZATION *'
                    options={this.state.options}
                    value={this.props.value}
                    onChange={this.props.callback}
                    placeholder='Select organization'
                />
            </div>
        );
    }
}

export default organizationContainer(ChooseOrganization);
import React from 'react';
import { Grid, Button, Modal, Icon } from 'semantic-ui-react';
import UserPreview from '../global/UserPreview.jsx';
import EventDetailsContainer from '../../redux/containers/event-details.js';
import GalleryView from '../search-members/GalleryView.jsx';

import '../../css/people-going.scss';

/**
 * component to display event attendees
 */
class PeopleGoing extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            members: [],
            rowSize: window.mobileCheck() ? 3 : 6,
            numRows: 2,
            isOpen: false
        }
    }

    /**
     * Init function for PeopleGoing, populate the list if an event id is given
     */
    componentDidMount() {
        // get users
        if (this.props.eventId) {
            this.props.eventAttendees({ eventId: this.props.eventId }).then((response) => {
                let { count, members } = response.data;
                if(!members){
                    members = [];
                }
                this.setState({ count, members });
            });
        } 
    }

    /**
     * Creates a csv containing the users who are attending the event and attempts to allow the user to
     * download the file
     */
    exportCSVFile(){
        // , is used to define the cell
        // " is used to define the content inside the cell. If a user want to 
        // use " as part of thier input, it will need to be escaped by another " (" -> "")
        // to not cause the format to get messed up

        /* 
        This escapes double quotes by adding another double quote to it
        " -> "", so "cat" will become ""cat"". 
        
        csv file uses double quote
        as a delimiter for the cells, so this needs to be done to avoid
        a misformatted file 
        */
        const IS_ADVISOR = window.appConfig.getAttribute('data') == 'advisor';

        let csvString = `"Student First","Student Last",${IS_ADVISOR ? '"Student Email"' : '""'},${IS_ADVISOR ? '"Registration Date Time"' : '""'}`;

        this.state.members.forEach((entry) => {
            csvString += `\n"${entry.EventUserFirstName}","${entry.EventUserLastName}","${IS_ADVISOR ? entry.EmailAddress : ''}","${IS_ADVISOR ? entry.EventUserDateTimeRegistered : ''}"`;
        });

        const BLOB = new Blob([csvString], { type: 'application/json;utf - 8' });
        const USER_LINK = document.createElement('a');
        USER_LINK.setAttribute('download', `report_eventId${this.props.eventId}.csv`);
        USER_LINK.setAttribute('href', window.URL.createObjectURL(BLOB));
        USER_LINK.click();
    }

    /**
     * Main render method for PeopleGoing.jsx
     */
    render() {
        const IS_ADVISOR = window.appConfig.getAttribute('data') === 'advisor';

        return (
            <div className='people-going'>
                <h1 className={'section-title'}><Icon id='people-going-icon' name='user'/>{this.state.count} {this.state.count == 1 ? 'Person' : 'People'} Going</h1>
                {/* The explicit check is to prevent a 0 from appearing */}
                { (IS_ADVISOR && this.props.isAdminForOrg) == true && <a className='download-link' onClick={this.exportCSVFile.bind(this)}><u>(Download List<Icon name='download'/>)</u></a> }
                {
                   (IS_ADVISOR && this.props.isAdminForOrg) == true &&
                    <div id='admin-list'>
                        <Grid>
                            <Grid.Row columns={this.state.rowSize}>
                                {
                                    this.state.members.slice(0,(this.state.rowSize * this.state.numRows)).map((member) => {
                                        return (
                                            <UserPreview key={member.EventUserID} id={member.EventUserID} firstName={member.EventUserFirstName} lastName={member.EventUserLastName} img={member.ProfilePicture} admin={false} />
                                        )
                                    })
                                }
                            </Grid.Row>
                        </Grid>
                        
                        {this.state.count == 0 && 'No attendees yet, sign up to be the first!'}

                        {this.state.count > 0 &&   
                        <div className={'loadmore'}>
                            {
                                (this.state.count > (this.state.rowSize * this.state.numRows)) &&
                                [
                                    <span key='view-all' onClick={() => this.setState({isOpen: true})}>View all</span>
                                ]
                            }
                        </div>}

                        {/* Full members list modal */}
                        <Modal open={this.state.isOpen}>
                            <Modal.Header>Event Attendees</Modal.Header>
                            <Modal.Content scrolling>
                                <GalleryView members={this.state.members} />
                            </Modal.Content>
                            <Modal.Actions>
                                <Button
                                    disabled={this.state.count==0}
                                    onClick={this.exportCSVFile.bind(this)}>
                                    Download <Icon name='download'/>
                                </Button>
                                <Button
                                    content="Close"
                                    onClick={() => this.setState({isOpen: false})}
                                    />
                            </Modal.Actions>
                        </Modal>
                    </div>
                }
            </div>
        )
    }
}
export default EventDetailsContainer(PeopleGoing);
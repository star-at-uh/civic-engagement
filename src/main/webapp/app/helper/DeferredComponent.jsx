import React, { Suspense } from 'react';

/**
 * Handler for dynamic import of componenets
 */
export const createDeferredComponent = (component) => (
    <Suspense fallback={<div>Page is Loading...</div>}>
        {component}
    </Suspense>
);
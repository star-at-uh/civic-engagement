/**
 * Extends the regular cas logout, clears the user's 
 * temporary cookies
 */
export const logoutUServe = (event) => {
    sessionStorage.clear();
    logout(event);
};

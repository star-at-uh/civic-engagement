/**
 * Serves to help store and cancel created intervals 
 */
class IntervalHelper {
    static intervals = new Set();

    /**
     * Adds the given id to the interval list
     * @param {int} id the numeric id number of the interval to add
     */
    static addInterval = (id) => {
        this.intervals.add(id)
    };

    /**
     * Creates an interval
     * @param {function} callback the function to fire in the interval 
     * @param {int} time how often the interval should fire in milliseconds
     * @returns the id of the created interval
     */
    static createInterval = (callback, time) => {
        const INTERVAL_ID = setInterval(callback, time);
        this.intervals.add(INTERVAL_ID);
        return INTERVAL_ID;
    };

    /**
     * Cancels the interval and removes it from the list
     * @param {int} id numeric id of the interval to cancel
     */
    static removeInterval = (id) => {
        clearInterval(id);
        this.intervals.delete(id);
    };

    /**
     * Cancels all intervals currently stored in the list
     */
    static clearAllIntervals = () => {
        this.intervals.forEach(id => {
            clearInterval(id);
        });
        this.intervals.clear();
    };
}

export default IntervalHelper;
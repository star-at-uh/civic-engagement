/**
 * Sorts a list of events in place using the given fieldName, data type, and order
 * @param {Array} eventsList the list of events to sort
 * @param {String} fieldName the key to sort on 
 * @param {Boolean} ascending true to do ascending sort, descending if false 
 * @param {String} type what data type the field to sort on should be treated as (ie 'date')
 */
const sortEvents = (eventsList, fieldName, ascending, type) => {
    let sorter = null;
    switch (type) {
        case 'date':
            const DATE_COMPARATOR = ascending ? dateFns.compareAsc : dateFns.compareDesc;
            sorter = (eventOne, eventTwo) => {
                return DATE_COMPARATOR(eventOne[fieldName], eventTwo[fieldName]);
            };
            break;
        default:
            sorter = (eventOne, eventTwo) => {
                let returnValue = 0;
                
                if (eventOne[fieldName] > eventTwo[fieldName]) {
                    return 1;
                }
                else if (eventOne[fieldName] < eventTwo[fieldName]) {
                    return -1;
                }
                
                return returnValue * (ascending ? -1 : 1);
            };
            break;
    }
    eventsList.sort(sorter);
};

/**
 * Flags an event if it is currently ongoing
 * @param {*} eventsList the list of events to go through
 * @param {*} startDateFieldName name of the start date time field
 * @param {*} endDateFieldName name of the end date time field
 * @param {*} currentDateTime the current time to look at
 */
const markOngoingEvents = (eventsList, startDateFieldName, endDateFieldName, currentDateTime) => {
    eventsList.forEach(event => {
        event.isOngoing = dateFns.isWithinRange(currentDateTime, event[startDateFieldName], event[endDateFieldName]);
    });
};

export { sortEvents, markOngoingEvents };
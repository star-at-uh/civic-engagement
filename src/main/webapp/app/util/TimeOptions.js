import { getRequest } from "../redux/requests";

// init TIME_OPTIONS for time picker
const TIME_OPTIONS = [];

for(let hour=0; hour<24; hour++){
    for(let minute=0; minute<60; minute += 15){
        // determine 12-hour time
        let textHour = hour == 0 ? 12 : hour;
        if(textHour > 12){
            textHour -= 12;
        }
        // add AM/PM
        const HALF = hour < 12 ? 'AM' : 'PM';
        //pad numbers
        const HOUR = hour < 10 ? '0'+hour : ''+hour;
        const MINUTE = minute < 10 ? '0'+minute : ''+minute;
        // create timestamp UTC
        const TIME_OPT_VALUE = `${HOUR}:${MINUTE}:00.000`;
        // create human-readable time string
        const timeOptString = `${textHour}:${MINUTE} ${HALF}`;
        TIME_OPTIONS.push({ key: TIME_OPT_VALUE, text: timeOptString, value: TIME_OPT_VALUE, hours: hour, minutes: minute });
    }
}

export default TIME_OPTIONS;

/**
 * Calls the server to retrieve server time (HST)
 * @returns a http promise whose response should contain the current time 
 */
export const getServerTime = () => {
    return getRequest('../api/general/current/time');
};
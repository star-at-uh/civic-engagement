// init ORG_CATEGORIES for org serach
let ORG_CATEGORIES = [
    {name: 'Government', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/Government-icon.svg'},
    {name: 'Bridging Generations', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/BridgingGenerations-icon.svg'},
    {name: 'Education', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/Education-icon.svg'},
    {name: 'Equality & Diversity', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/Equality-icon.svg'},
    {name: 'Health & Safety', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/Health-icon.svg'},
    {name: 'Arts, History & Culture', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/Arts-icon.svg'},
    {name: 'Sustainability', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/sustainability-icon.svg'},
    {name: 'Environment', icon: 'https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/CategoryIcons/environment-icon.svg'}
];

export default ORG_CATEGORIES;
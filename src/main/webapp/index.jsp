
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.Calendar, java.util.Optional" %>

<%
    response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
    response.setHeader("Pragma","no-cache"); //HTTP 1.0
    response.setDateHeader ("Expires", 0); //prevents caching at the proxy server

    if (session != null) {
        String errorMessage = Optional.ofNullable((String) session.getAttribute("errorMessage")).orElse("");

        // Check for the message from the RedirectFilter or other sources only if an errorMessage doesn't exist
        if (errorMessage.length() < 1) {
            errorMessage = Optional.ofNullable((String) session.getAttribute("message")).orElse("");
        }
        
        pageContext.setAttribute("showError", errorMessage.length() > 0);
        pageContext.setAttribute("errorMessage", errorMessage);
        session.invalidate();
    }
%>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>UServe</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.css">
        <script src="https://cdn.jsdelivr.net/npm/fomantic-ui@2.8.7/dist/semantic.min.js"></script>
        <c:if test="${ initParam.applicationType == 'student' }">
            <script type="text/javascript" src="https://www.star.hawaii.edu/cdn/scripts/includes/version/cas/casloginV1.1.js"></script>
            <script src="public/build/student-home.bundle.1.0.3.js"></script>
        </c:if>
        <c:if test="${ initParam.applicationType == 'advisor' }">
            <script src="public/build/admin-home.bundle.1.0.3.js"></script>
        </c:if>
    </head>

    <body>
        <section id="top-nav-menu" class="ui top fixed borderless menu">
            <div class='left floated'>
                <a href="https://www.star.hawaii.edu/studentinterface" class="item">
                    <img src="https://www.star.hawaii.edu/cdn/images/star-logo/home-page/starhome-link.svg" alt="star-logo" class="star-logo">
                </a>
            </div>

             <img src="https://star.hawaii.edu/cdn/images/civic-engagement/landing_page/1/USERVE-Logo-white.svg" alt="application-logo" class="application-logo">
        </section>

        <div class="home-header">
            <c:if test="${pageScope.showError}">
                <div class="warning-message">
                    <div class="ui error message">
                        <c:out value="${pageScope.errorMessage}"/>
                    </div>
                </div>
            </c:if>
            <div id="slideshow" class="mobile welcome-image">
                <img id="first-welcome" class="image-content" src="https://www.star.hawaii.edu/cdn/images/civic-engagement/landing_page/Assets/1/userve-bg1.jpg">
            </div>
        </div>

        <!-- STUDENT SIDE HOMEPAGE -->
        <c:if test="${ initParam.applicationType == 'student' }">
            <%@include file="student-home.jsp"%>
        </c:if>
        <c:if test="${ initParam.applicationType == 'advisor' }">
            <%@include file="admin-home.jsp"%>
        </c:if>
            
        <div class="mobile home-footer">
            <span class="footer-left-container">
                <a href="https://www.star.hawaii.edu/help/#/" target="_blank" rel="noopener noreferrer">
                    <img class="footer-icon" src="https://www.star.hawaii.edu/cdn/images/star-logo/StarLogo-withtag.svg"/>
                </a>
            </span>
            <div class="footer-info">
                <p>
                    At STAR, University of Hawai&#699;i, we strive for accurate displays of the Hawaiian language in all our applications. 
                    Unfortunately, at times external vendors and other third-party technology integrations do not support the use of these characters. 
                    Mahalo for your understanding.
                </p>
                <span class="word-group">
                    &copy; 
                    <%= Calendar.getInstance().get(Calendar.YEAR) %> 
                    STAR
                </span>
            </div>
        </div>   
    </body>
</html>




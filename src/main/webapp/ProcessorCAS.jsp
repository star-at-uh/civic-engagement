<%@ page
	import="java.util.Enumeration"
	import="edu.hawaii.star.filter.JWTBuilder"
	import="edu.hawaii.star.cipher.Decryptor"
	import="edu.hawaii.star.User"
	import="javax.servlet.http.Cookie"
%>

<%
    User user = (User) session.getAttribute(User.SESSION_ATTR_NAME);
    if (!user.getAuthorized()) {
        session.setAttribute("errorMessage", user.getAuthenticationErrorMessage());
        response.sendRedirect("index.jsp");
        return;
    }

    session.setMaxInactiveInterval( 3600 );
%>
<%
	response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>
<%  //get rid of session variables.
		Enumeration<String> values = session.getAttributeNames();
		while(values.hasMoreElements()){
        String key = values.nextElement();
			if (!key.equals("user")) session.removeAttribute(key);
		}
	// this is how to delete all session variables except for user
%>

<%
    // We are only checking if the user passes the CAS
	// If they have reached this page, it means they were already authenticated with CAS
	
	// add auth token
	JWTBuilder tokenBuilder = new JWTBuilder(user.getUsername(), Decryptor.decryptMessage(request.getServletContext().getInitParameter("JWT_CIPHER_PATH"), request.getServletContext().getInitParameter("JWT_KEY_PATH")));
	String sessionToken = tokenBuilder.createToken();
	if(sessionToken == null){
		System.out.println("Error: could not create API token");
		session.invalidate();
		response.sendRedirect("/");
	} else {
		Cookie apiCookie = new Cookie("userve-api-token",sessionToken);
		apiCookie.setMaxAge(24*60*60); // expire cookie regardless after 24 hours, the token itself may or may not stil be valid
		response.addCookie(apiCookie);
	}
	
    response.sendRedirect("app/");
%>
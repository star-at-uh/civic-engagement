<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ page import="java.util.*, java.lang.Exception, com.google.gson.Gson, edu.hawaii.star.*"%>
<%
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setDateHeader ("Expires", 0); //prevents caching at the proxy server
%>

<sql:query var="results">
    exec PerformanceCheckingDB.dbo.View_SecurityViewDebugWebPageTbl_proc ?
    <sql:param value="${ sessionScope.user.username }" />
</sql:query>

<html>
  <head>
  	<style type="text/css">
      #statements textarea.my-textarea {
        background-color: #333;
        color: #fff;
        font-family: monospace;
        font-size: 14px;
        line-height: 1;
        margin-bottom: 10px;
        padding: 5px 10px;
        width: 100%;
        max-width: 100%;
        min-height: 200px;
      }
  	</style>
    <link href="https://www.star.hawaii.edu/cdn/ajax/libs/bootswatch/3.3.5/paper/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
        <c:if test="${results.rows[0].IsAuthorized == 1}">
            <h3>SQL Debug Statements</h3>
            <p class="lead">Latest proc execution is on top. <a href="">Refresh page.</a></p>
            <div id="statements">
                <%
                    SessionManager star = (SessionManager) SessionManager.get(request);
                    if (star.getDebugStatements() != null) {
                        int length = star.getDebugStatements().size();
                        for (int i = length - 1; i >= 0; i--) {
                            String statement = star.getDebugStatements().get(i);
                            Date date = star.getDebugStatements().getTime(i);
                            String time = (date != null) ? date.toString() : "";
                            statement = statement.replaceAll("\t", "");
                            out.println("<code>" + time + "</code>");
                            out.println("<textarea class='form-control my-textarea' readonly>" + statement + "</textarea>");
                        }
                    }
                %>
            </div>
        </c:if>
    </div>
  </body>
</html>
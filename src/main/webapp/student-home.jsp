<div class="login-subsection">
    <div id="login-header" class="mobile header-container">
        <img class="step-indicator" src="https://star.hawaii.edu/cdn/images/civic-engagement/landing_page/Assets/desktoppart1.png">
        <h1 class="welcome-header student">
            Your place to serve
        </h1>
        <p class="welcome-sub-header student">
            Your place to learn
        </p>
        <a href="${pageContext.request.contextPath}/app">
            <button class="login-button">
                Enter
            </button>
        </a>
        <%-- id="casEnterLogin" --%>
    </div>
</div>

<div class="home-body">
    <div class="light-gray-backgound-wrapper">
        <div class="mobile tutorial-container">
            <div class="tutorial-text">
                <img class="step-indicator" src="https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/LandingPage/part2.svg">
                <h2 class="title">Find a range of opportunities with ease.</h2>
                <p>
                    From environmental to humanitarian, you can easily find what interests you, and sign up.
                </p>
            </div>
            <div>
                <img class="screenshot-preview horizontal" src="https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/LandingPage/opportunities%20img.svg">
            </div>
        </div>
    </div>
    <div class="gray-backgound-wrapper">
        <div class="mobile tutorial-container">
            <div>
                <img class="screenshot-preview leading" src="https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/LandingPage/collab%20img.svg">
            </div>
            <div class="tutorial-text">
                <img class="step-indicator" src="https://star.hawaii.edu/cdn/images/civic-engagement/12-07/svg/LandingPage/part3.svg">
                <h2 class="title">Collaborate with you peers and work with local organizations.</h2>
                <p>
                    Meet people, make friends and find organizations who are all aiming to better Hawai'i. We are together in this.
                </p>
                <a href="${pageContext.request.contextPath}/app">
                    <button class="login-button light-blue">
                        Enter
                    </button>
                </a>
                <%-- id="casEnterBottomLogin" --%>
            </div>
        </div>
    </div>
</div>
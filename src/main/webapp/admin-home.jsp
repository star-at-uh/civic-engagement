<div class="login-subsection">
    <div id="login-header" class="mobile header-container">
        <h1 class="welcome-header">
            Welcome Faculty/Staff
        </h1>
        <a href="admin-login.jsp">
            <button class="login-button">
                Enter
            </button>
        </a>
    </div>
</div>

<div class="home-body">
    <div class="light-gray-backgound-wrapper">
        <div class="mobile summary-container">
            <div class="summary-text">
                <h2 class="title">Executive Summary:</h2>
                <h2 class="sub-title">USERVE Admin</h2>
                <p>
                    Edit/add groups, manage events and manage student progress and help them in their journey.
                </p>
            </div>
            <div>
                <img class="screenshot-preview" src="https://www.star.hawaii.edu/cdn/images/civic-engagement/landing_page/1/admin/userveadmin-ss.jpg">
            </div>
        </div>
    </div>
</div>
/**
 * Handles google related logins
 */
function googleLogin() {
    const WIN = window.open("http://accounts.google.com/logout", "something", "width=550,height=570");
    fetch('./api/general/new/login', {
        method: 'GET'
    })
    .then(response => {
        response.json()
                .then(function(data) {
                    WIN.close();
                    sessionStorage.setItem("isGoogleAuth", true);
                    window.location.replace(data);
                });
    });
}

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById("googleEnterLogin")?.addEventListener("click", googleLogin);
});
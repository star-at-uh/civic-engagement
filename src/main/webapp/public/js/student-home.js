import '../css/student-home.scss';

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById("casEnterBottomLogin")?.addEventListener("click", triggerCAS);
});

/**
 * Setup slideshow animation after html (excluding css) is loaded
 */
document.addEventListener('DOMContentLoaded', () => {
    // since elements are rendered from the top down, 
    // the 2nd, 3th, etc images are not included in the html so that
    // the browser would immediately load the 1st image
    const SLIDESHOW_CONTAINER = document.getElementById('slideshow');

    let slideshowContent = [
        {src: 'https://www.star.hawaii.edu/cdn/images/civic-engagement/landing_page/Assets/1/userve-bg2.jpg', id: 'second-welcome'},
        {src: 'https://www.star.hawaii.edu/cdn/images/civic-engagement/landing_page/Assets/1/userve-bg3.jpg', id: 'third-welcome'}
    ];

    slideshowContent.forEach((content) => {
        const IMAGE_ELEMENT = document.createElement('img');

        IMAGE_ELEMENT.src = content.src;
        IMAGE_ELEMENT.setAttribute('id', content.id);
        IMAGE_ELEMENT.classList.add('image-content', 'slide-up', 'fade-out');

        SLIDESHOW_CONTAINER.prepend(IMAGE_ELEMENT);
    });

    slideshow(SLIDESHOW_CONTAINER);
});

const TIMER_ID_TRACKER = {};
/**
 * Create the slideshow used by the welcome images
 */
function slideshow(container) {
    const ANIMATION_ELEMENTS = Array.from(container.getElementsByClassName('image-content')).reverse();

    /**
     * add css class that could trigger animation
     */
    const animate = (element, cssClass, waitMS) => setTimeout(() => element.classList.add(cssClass), waitMS);
    /**
     * remove css class, this will also trigger animation due to the transition css rule
     */
    const reverseAnimate = (element, cssClass, waitMS) => setTimeout(() => element.classList.remove(cssClass), waitMS);

    /**
     * Creates the timing between the different transition points, the order
     * in which the element are passed matters
     */
    const scheduleKeyframes = (elements) => {
        const WAIT_TIME = 13000;
        const NEXT_STEP_TIME = 300;
        let currentDuration = 0;

        elements.forEach((element, index) => {
            currentDuration += WAIT_TIME;
            animate(element, 'slide-down', currentDuration);
            animate(element, 'fade-out', currentDuration);

            // reset once animation finishes
            reverseAnimate(element, 'slide-down', currentDuration + WAIT_TIME);
            animate(element, 'slide-up', currentDuration + WAIT_TIME);

            const NEXT_INDEX = (index + 1) % elements.length;
            reverseAnimate(elements[NEXT_INDEX], 'slide-up', currentDuration);
            // other than the first image, the other images needs the fade-out to trigger
            // a little before the slide-up so that there won't be a brief moment of 
            // having a white background
            const REDUCE_WAIT = NEXT_INDEX == 0 ? 0 : 1000;
            reverseAnimate(elements[NEXT_INDEX], 'fade-out', currentDuration - REDUCE_WAIT);

            currentDuration += NEXT_STEP_TIME;
        });

        return currentDuration;
    };

    // animation time table should look like this:
    // |---13s---[first image goes down and fade]
    //           [second image goes down and becomes visible]---13s---[second image goes down and fade]
    //                                                                [third image goes down and becomes visible]---13s---[third image goes down and fade]
    //                                                                                                                    [first image goes down and becomes visible]---| back to start
    const ANIMATION_DURATION = scheduleKeyframes(ANIMATION_ELEMENTS);
    setInterval(() => scheduleKeyframes(ANIMATION_ELEMENTS), ANIMATION_DURATION);

    // don't do transitions during resize to reduce the jank that appears at that moment
    window.addEventListener("resize", () => {
        ANIMATION_ELEMENTS.forEach((element) => {
            element.classList.add("disable-transition-animation");
            clearTimeout(TIMER_ID_TRACKER[element.id]);
            TIMER_ID_TRACKER[element.id] = setTimeout(() => element.classList.remove("disable-transition-animation"), 400);
        });
    });
}
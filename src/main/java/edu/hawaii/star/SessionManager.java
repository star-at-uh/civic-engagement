package edu.hawaii.star;

import java.security.cert.PKIXRevocationChecker.Option;

import javax.annotation.Nullable;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import edu.hawaii.star.model.DebugStatements;

/**
 * Manages the currect user session
 */
public class SessionManager {

    /**
     * Create a instance of the manager
     */
    public static SessionManager get(ServletContext context) {
        return new SessionManager(context, null);
    }

    /**
     * Create a instance of the manager
     */
    public static SessionManager get(HttpServletRequest request) {
        return new SessionManager(request.getSession().getServletContext(), request);
    }

    /**
     * Create a instance of the manager
     */
    public static SessionManager get(ServletContext context, HttpServletRequest request) {
        return new SessionManager(context, request);
    }

    private ServletContext context;
    @SuppressWarnings("unused")
    private HttpServletRequest request;
    private HttpSession session;

    /**
     * Init the manager, linking it to the https request and app
     */
    public SessionManager(ServletContext context, HttpServletRequest request) {
        this.context = context;
        this.request = request;
        if (request != null) {
            session = request.getSession();
        }
    }

    /**
     * Returns the DebugManager instance
     */
    public DebugManager getDebugManager() {
        DebugManager manager = (DebugManager) session.getAttribute("debugManager");
        if (manager == null) {
            manager = new DebugManager(25);
            session.setAttribute("debugManager", manager);
        }
        return manager;
    }

    public User getUser() {
        return (User) session.getAttribute("user");
      }

    /**
     * Records the proc call
     */
    public void addDebugStatement(String debug) {
        DebugStatements<String> debugStatements = getDebugStatements();
        if (debugStatements == null) {
            debugStatements = new DebugStatements<String>(25);
            session.setAttribute("debugStatements", debugStatements);
        }
        debugStatements.add(debug);
    }

    /**
     * Returns the version of the app defined by the context.xml for the app
     */
    public String getVersion() {
        return context.getInitParameter("version");
    }

    /**
     * Recieves the proc calls logs from the current session
     */
    @SuppressWarnings("unchecked")
    public DebugStatements<String> getDebugStatements() {
        return (DebugStatements<String>) session.getAttribute("debugStatements");
    }

    public String getName() {
        return context.getInitParameter("CASApplicationName");
    }

    /**
     * Store the current image base 64 string that is being shown in the 
     * edit organization/event page
     */
    public void setBufferedImageURL(String base64String) {
        session.setAttribute("image64String", base64String);
    }

    /**
     * Get the current image base 64 string that is being shown in the 
     * edit organization/event page
     */
    @Nullable
    public String getBufferedImageURL() {
        return (String) session.getAttribute("image64String");
    }
}

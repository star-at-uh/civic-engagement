package edu.hawaii.star.jersey.api;

public class ServiceHoursParams {

  private int userId;
  
  public int getUserId(){
      return userId;
  }

  private int goalId;
  
  public int getGoalId(){
      return goalId;
  }

  private String goalName;
  
  public String getGoalName(){
      return goalName;
  }

  private String startDate;
  
  public String getStartDate(){
      return startDate;
  }

  private String endDate;
  
  public String getEndDate(){
      return endDate;
  }

  private int goalHours;

  public int getGoalHours(){
        return goalHours;
  }

  private Boolean isAdmin;

  public Boolean getIsAdmin(){
      return isAdmin;
  }
  
}

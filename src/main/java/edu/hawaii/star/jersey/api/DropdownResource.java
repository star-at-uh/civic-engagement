package edu.hawaii.star.jersey.api;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.codec.binary.Base64;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.dao.sqlserver.ResultHandler;

@Path("combobox")
public class DropdownResource {

  private static Gson gson = new GsonBuilder().create();

  @Context
  private HttpServletRequest request;

  @Context
  private ServletContext context;

  @Path("attendanceTypes")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String attendaceTypes() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.ComboBox_EventAttendanceTypes_Event_Proc");
    sessionManager.addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * Fetches all organizations viewable by the current logged in user
   * @return an object containing a list of organizations
   * @throws SQLException
   */
  @Path("all-organizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getAllOrganizations() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationList_Proc");
    proc.add(userPidm);
    proc.add(1); // Is the civic engage interface
    proc.add(context.getInitParameter("applicationType"));

    sessionManager.addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());   
  }

  /**
   * Fetches all organizations the current logged in user is a part of
   * @return a list of organizations the user is an admin in
   * @throws SQLException
   */
  @Path("owned-organizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getOwnOrganizations() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.ComboBox_EventOrganizations_Event_Proc");
    proc.add(userPidm);
    proc.add(1); // Is civic engage interface

    sessionManager.addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());   
  }

  @Path("organization-labels")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String organizationLabels() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.ComboBox_MasterLabels_Organization_Proc");
    sessionManager.addDebugStatement(proc.getSQLStatement());
    
    return gson.toJson(proc.execute().getRows());
  }

  @Path("socialCohort")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String socialCohort() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.ComboBox_EventSocialCohortTypes_Event_Proc");
    sessionManager.addDebugStatement(proc.getSQLStatement());
    
    return gson.toJson(proc.execute().getRows());
  }
}

package edu.hawaii.star.jersey.api.params;

public class ClockTimeParams {
    private int clockedTimeID;
    private int eventID;
    private int studentId;
    
    /**
     * @return the clockedTimeID
     */
    public int getClockedTimeID() {
        return clockedTimeID;
    }

    /**
     * @return the eventID
     */
    public int getEventID() {
        return eventID;
    }

    /**
     * @return the studentId
     */
    public int getStudentId() {
        return studentId;
    }
}

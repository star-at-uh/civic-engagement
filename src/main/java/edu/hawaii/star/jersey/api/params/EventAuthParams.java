package edu.hawaii.star.jersey.api.params;

/**
 * This class holds the params used for authorizing events
 */
public class EventAuthParams {

  private int userId;
  
  /**
   * @return the userId
   */
  public int getUserId(){
      return userId;
  }

  private String eventList;
  
  /**
   * @return the eventList
   */
  public String getEventList(){
      return eventList;
  }

  private String authorizationComment;

  /**
   * @return the authorizationComment
   */
  public String getAuthorizationComment() {
      return authorizationComment;
  }
}

package edu.hawaii.star.jersey.api;

import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;

import edu.hawaii.star.SessionManager;

@Path("serviceHours")
public class ServiceHoursResource {

  private static Gson gson = new GsonBuilder().create();

  @Context
  private HttpServletRequest request;

  @Context
  private ServletContext context;

  /**
   * STUDENT SIDE METHODS
   */

   /**
   * For viewing my individual progress report
   */
  @Path("myProgress")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String myProgress(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[View_UserServiceHoursDetail_User_Proc]");

    proc.add(userPidm); // @StudentUserID
    proc.add(userPidm); // @RequestingUserID

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
  
  /**
   * For viewing all user goals
   */
  @Path("comboboxUserGoals")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String comboboxUserGoals(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[ComboBox_UserGoals_User_Proc]");
    
    proc.add(userPidm); // @UserID
    proc.add(userPidm); // @RequestingUserID

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For setting a new goal
   */
  @Path("setNewGoal")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String setNewGoal(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    ServiceHoursParams params = gson.fromJson(body, ServiceHoursParams.class);
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[Insert_UserGoal_User_Proc]");
    
    proc.add(userPidm); // @UserID                
    proc.add(params.getGoalName()); // @GoalName
    proc.add(params.getStartDate());// @GoalStartDate
    proc.add(params.getEndDate()); // @GoalEndDate
    proc.add(params.getGoalHours()); // @GoalHoursTotal

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
  
  /**
   * For editing an existing goal
   */
  @Path("editGoal")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String editGoal(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    ServiceHoursParams params = gson.fromJson(body, ServiceHoursParams.class);
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[Update_UserGoal_User_Proc]");
    
    proc.add(params.getGoalId()); // @GoalID                
    proc.add(params.getGoalName()); // @GoalName
    proc.add(params.getStartDate());// @GoalStartDate
    proc.add(params.getEndDate()); // @GoalEndDate
    proc.add(params.getGoalHours()); // @GoalHoursTotal
    proc.add(userPidm); // @RequestingUserID

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }


  /**
   * ADMIN SIDE METHODS
   */
  
  /**
   * For viewing a list of all students and their total service hours for a particular event
   */
  @Path("viewServiceHoursRoster")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewServiceHoursRoster(@QueryParam("eventId") int eventId) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_ServiceHoursUsers_Event_Proc");
    proc.add(userPidm);
    proc.add(eventId);

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.multiTableExecute());
  }

  /**
   * For editing an existing goal
   */
  @Path("viewStudentServiceRecord")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewStudentServiceRecord(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    ServiceHoursParams params = gson.fromJson(body, ServiceHoursParams.class);
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[View_UserServiceHoursDetail_User_Proc]");

    proc.add(params.getUserId()); // @StudentUserID
    proc.add(userPidm); // @RequestingUserID

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For viewing single student goals
   */
  @Path("studentGoals")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String studentGoals(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    ServiceHoursParams params = gson.fromJson(body, ServiceHoursParams.class);
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[ComboBox_UserGoals_User_Proc]");
    
    proc.add(params.getUserId()); // @UserID
    proc.add(userPidm); // @RequestingUserID

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
}

package edu.hawaii.star.jersey.api;

public class OrganizationsParams {
    private int userId;
    private int organizationId;
    private String organizationName;
    private String locationAddress1;
    private String locationAddress2;
    private String locationCity;
    private String locationState;
    private String locationZip;
    private String imageUrl;
    private String description;
    private String contactPersonName;
    private String contactPersonEmail;
    private String contactPersonPhone;
    private String itemsToBring;
    private String foodProvided;
    private String searchKeyword;
    private String webPageURL;
    private Boolean notAcceptingVolunteers;
    private String attendanceTypeIds;
    private String organizationLabels;
    private float imageCenter;
    private String adminUser;
    private String adminFirstName;
    private String adminLastName;
    private int authorizationStatus;
    private String authorizationComment;
    private boolean imageChanged;

    public int getUserId() {
        return userId;
    }

    public void setOrganizationId(int organizationId) {
        this.organizationId = organizationId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public String getLocationAddress1() {
        return locationAddress1;
    }

    public String getLocationAddress2() {
        return locationAddress2;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public String getLocationState() {
        return locationState;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public String getContactPersonPhone() {
        return contactPersonPhone;
    }

    public String getItemsToBring() {
        return itemsToBring;
    }

    public String getFoodProvided() {
        return foodProvided;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public String getWebPageURL() {
        return webPageURL;
    }

    public Boolean getNotAcceptingVolunteers() {
        return notAcceptingVolunteers;
    }

    public String getAttendanceTypeIds() {
        return attendanceTypeIds;
    }

    public String getOrganizationLabels() {
        return organizationLabels;
    }

    public float getImageCenter() {
        return imageCenter;
    }

    public String getAdminUser() {
        return adminUser;
    }

    public String getAdminFirstName() {
        return adminFirstName;
    }

    public String getAdminLastName() {
        return adminLastName;
    }

    public int getAuthorizationStatus() {
        return authorizationStatus;
    }

    public String getAuthorizationComment() {
        return authorizationComment;
    }

    public boolean getImageChanged() {
        return imageChanged;
    }
}

package edu.hawaii.star.jersey.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.model.PDFTimesheet;
import edu.hawaii.star.upload.MainFileUploadSecurity;
import edu.hawaii.star.upload.Response;
import edu.hawaii.star.upload.types.EnumFileType;
import edu.hawaii.star.upload.types.EnumStorage;

@Path("media")
public class MediaResource {
    private static Gson gson = new GsonBuilder().create();
    private static Logger logger = LoggerFactory.getLogger(MediaResource.class);

    @Context
    private HttpServletRequest request;

    @Context
    private ServletContext context;

    /**
     * Uploads the timesheet PDF to the backend as a base64 string
     */
    @Path("uploadTimesheet")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public String uploadTimesheet() throws SQLException {
        SessionManager sessionManager = SessionManager.get(request);
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");

        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);

        HashMap<String, Object> result = new HashMap<>();

         try {
            List<FileItem> items = upload.parseRequest(request);
            Response response = new Response();
            response.setFailed("Unable to process file");
            String fileName = "";
            String eventId = "";

            for (FileItem item : items) {
                if (!item.isFormField()) {
                    fileName = item.getName();
        
                    MainFileUploadSecurity uploadManager = new MainFileUploadSecurity(sessionManager.getUser().getUsername(), sessionManager.getName(), null);
                    
                    response = uploadManager.handleFile(item, EnumStorage.BASE64_STRING, "TimesheetPDF", EnumFileType.PDF);
                }
                else {
                    eventId = item.getString();
                }
            }

            if (response.isSuccess()) {
                logger.info("Recieved base64 byte array for {}, by user {}", fileName, sessionManager.getUser().getId());

                @SuppressWarnings("rawtypes")
                SortedMap procResult = PDFTimesheet.uploadTimesheet(sessionManager, response.getPayloadBytes(), eventId, userPidm, fileName);
                
                result.put("status", procResult.get("Status"));
                result.put("message", procResult.get("ErrorMessage"));

            }
            else {
                result.put("status", false);
                result.put("message", response.getMesssage());
            }
        } 
        catch (SQLException | IOException | FileUploadException exception) {
            logger.info("Failed to save PDF Timesheet for user {}", sessionManager.getUser().getId());
            result.put("status", false);
            result.put("message", "Failed to upload PDF");
            exception.printStackTrace();
        }
        
        return gson.toJson(result);
    }

    /**
     * Get all timesheets pdf that the student/admin has uploaded
     */
    /**
     * Get the timesheet's info for that event
     */
    @Path("getAllTimesheet")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getAllTimesheet(@QueryParam("eventId") String eventId) throws SQLException {
        SessionManager manager = SessionManager.get(request);
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");

        return gson.toJson(PDFTimesheet.getPDF(manager, userPidm, eventId, context.getInitParameter("applicationType")));
    }

    /**
     * Get the timesheet PDF's base64 string
     */
    @Path("getPDFData")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getPDFData(@QueryParam("timeSheetKey") int timeSheetKey) throws SQLException {
        SessionManager manager = SessionManager.get(request);
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");

        return gson.toJson(PDFTimesheet.getPDFData(manager, userPidm, timeSheetKey, context.getInitParameter("applicationType")));
    }

    /**
     * Removes a timesheet PDF's base64 string from the DB
     */
    @Path("deletePDFData")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String deletePDFData(String body) throws SQLException {
        SessionManager manager = SessionManager.get(request);
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");

        PDFTimesheet.CommentParams params = gson.fromJson(body, PDFTimesheet.CommentParams.class);

        return gson.toJson(PDFTimesheet.deletePDFData(manager, userPidm, params.timeSheetKey));
    }

    /**
     * Updates the comment associated with a specific PDF
     */
    @Path("updateTimesheetComment")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String updateTimesheetComment(String body) throws SQLException {
        SessionManager manager = SessionManager.get(request);
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");
        
        PDFTimesheet.CommentParams params = gson.fromJson(body, PDFTimesheet.CommentParams.class);

        return gson.toJson(PDFTimesheet.updateTimesheetComment(manager, userPidm, params.timeSheetKey, params.timeSheetComment));
    }
}

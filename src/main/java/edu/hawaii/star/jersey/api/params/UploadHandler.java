package edu.hawaii.star.jersey.api.params;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Nullable;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.upload.MainFileUploadSecurity;
import edu.hawaii.star.upload.Response;
import edu.hawaii.star.upload.types.EnumFileType;
import edu.hawaii.star.upload.types.EnumStorage;

public class UploadHandler {
    private static Logger logger = LoggerFactory.getLogger(UploadHandler.class);

    /**
     * Take the file stream and turn it into a 
     * base64 string
     */
    @Nullable
    public static String parseBase64(SessionManager sessionManager, FileItem item, String uploadCategory) throws SQLException, IOException {
        String fileName = item.getName();
        String contentType = item.getContentType();

        MainFileUploadSecurity uploadManager = new MainFileUploadSecurity(sessionManager.getUser().getUsername(), sessionManager.getName(), null);
        
        Response response = uploadManager.handleFile(item, EnumStorage.BASE64_STRING, uploadCategory, EnumFileType.JPEG, EnumFileType.JPG, EnumFileType.PNG);

        if (response.isSuccess()) {
            logger.info("Recieved base64 byte array for {}, by user {}", fileName, sessionManager.getUser().getUsername());
            
            return "data:" +  contentType + ";base64," + Base64.encodeBase64String(response.getPayloadBytes());
        }
        else {
           return null;
        }
    }
}

package edu.hawaii.star.jersey.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.hawaii.star.dao.sqlserver.Bind;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.model.Status;
import edu.hawaii.star.model.UserInfo;
import edu.hawaii.star.jersey.api.params.UploadHandler;
import edu.hawaii.star.SessionManager;
import edu.hawaii.star.User;

@Path("organizations")
public class OrganizationsResource {

  private static Gson gson = new GsonBuilder().create();

  @Context
  private HttpServletRequest request;

  /**
   * ADMIN-ONLY METHODS
   */

  /**
   * Grabs the currently logged in user's organizations they are a part of
   */
  @Path("getOwnOrganizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getOwnOrganizations() throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserOrganizations_Organization_Proc");
    proc.add(userPidm);
    proc.add(1); // IsCivicEngageInterface
    

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For creating an org
   */
  @POST
  @Path("createOrganization")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public String createOrganization() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    OrganizationsParams params = this.parseOrganizationFormPayload(sessionManager);

    if (params != null) {
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");
        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Create_Organization_Organization_Proc");
    
        proc.add(userPidm);// @CreatingUserID INT,
        proc.add(params.getOrganizationName());// @OrganizationName VARCHAR(100),
        proc.add(params.getLocationAddress1());// @OrganizationAddressLine1 VARCHAR(200) = NULL,
        proc.add(params.getLocationAddress2());// @OrganizationAddressLine2 VARCHAR(200) = NULL,
        proc.add(params.getLocationCity());// @OrganizationAddressCity VARCHAR(200) = NULL,
        proc.add(params.getLocationState());// @OrganizationAddressState VARCHAR(50) = NULL,
        proc.add(params.getLocationZip());// @OrganizationAddressZipCode VARCHAR(50) = NULL,
        proc.add(params.getContactPersonPhone());// @OrganizationPhoneNumber VARCHAR(50) = NULL,
        proc.add(params.getContactPersonName());// @OrganizationContactPersonFullName VARCHAR(350) = NULL,
        proc.add(params.getDescription());// @OrganizationDescription VARCHAR(2000) = NULL,
        proc.add(params.getWebPageURL());// @OrganizationWebpageURL VARCHAR(100) = NULL,
        proc.add(params.getContactPersonEmail());// @OrganizationEmailAddress VARCHAR(100) = NULL,
        proc.add(params.getNotAcceptingVolunteers());// @OrganizationNotAcceptingVolunteers BIT = 0, -- If 1, organization does not allow volunteers to join org
        proc.add(params.getAttendanceTypeIds());// @OrganizationAttendanceTypeIDsToAllow VARCHAR(200) = NULL -- comma-delimited list of AttendanceTypeIDs, pass in if org only allows certain attendancetypes. Otherwise, org allows all attendance types
        proc.add(1); // isCivicEngagement
        proc.add(params.getOrganizationLabels());
    
        sessionManager.addDebugStatement(proc.getSQLStatement());
    
        @SuppressWarnings("rawtypes")
        SortedMap[] result = proc.execute().getRows();
        @SuppressWarnings("rawtypes")
        SortedMap row = result[0];
    
        // if success, add image now
        if(row.get("Status").toString().equalsIgnoreCase("1")) {
            params.setOrganizationId((int) row.get("OrganizationID"));
            this.saveOrganizationImage(userPidm, params);
        }
    
        return gson.toJson(result);
    }
    else {
        return "";
    }
  }

  /**
   * For editing an org
   */
  @POST
  @Path("editOrganization")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public String editOrganization() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    OrganizationsParams params = this.parseOrganizationFormPayload(sessionManager);

    if (params != null) {
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");
        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_OrganizationDetails_Organization_Proc");
    
        proc.add(params.getOrganizationId()); //@OrganizationID INT,
        proc.add(params.getOrganizationName());// @OrganizationName VARCHAR(100),
        proc.add(params.getLocationAddress1());// @OrganizationAddressLine1 VARCHAR(200) = NULL,
        proc.add(params.getLocationAddress2());// @OrganizationAddressLine2 VARCHAR(200) = NULL,
        proc.add(params.getLocationCity());// @OrganizationAddressCity VARCHAR(200) = NULL,
        proc.add(params.getLocationState());// @OrganizationAddressState VARCHAR(50) = NULL,
        proc.add(params.getLocationZip());// @OrganizationAddressZipCode VARCHAR(50) = NULL,
        proc.add(params.getContactPersonPhone());// @OrganizationPhoneNumber VARCHAR(50) = NULL,
        proc.add(params.getContactPersonName());// @OrganizationContactPersonFullName VARCHAR(350) = NULL,
        proc.add(params.getDescription());// @OrganizationDescription VARCHAR(2000) = NULL,
        proc.add(params.getWebPageURL());// @OrganizationWebpageURL VARCHAR(100) = NULL,
        proc.add(params.getContactPersonEmail());// @OrganizationEmailAddress VARCHAR(100) = NULL,
        proc.add(params.getNotAcceptingVolunteers());// @OrganizationNotAcceptingVolunteers BIT = 0, -- If 1, organization does not allow volunteers to join org
        proc.add(params.getAttendanceTypeIds());// @OrganizationAttendanceTypeIDsToAllow VARCHAR(200) = NULL -- comma-delimited list of AttendanceTypeIDs, pass in if org only allows certain attendancetypes. Otherwise, org allows all attendance types
        proc.add(userPidm);// @UpdatingUserID INT
        proc.add(params.getOrganizationLabels());
        
        SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
    
        @SuppressWarnings("rawtypes")
        SortedMap[] result = proc.execute().getRows();
    
        // if success, add image now
        if(result[0].get("Status").toString().equalsIgnoreCase("1")) {
            if (!params.getImageChanged()) {
                params.setImageUrl(sessionManager.getBufferedImageURL());
            }

            this.saveOrganizationImage(userPidm, params);
        }
    
        return gson.toJson(result);
    }
    else {
        return "";
    }
  }

  /**
   * Save the organization image into the SQL server
   */
  private void saveOrganizationImage(Long userPidm, OrganizationsParams params) {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_OrganizationImage_Organization_Proc");
    proc.add(userPidm);
    proc.add(params.getOrganizationId());
    proc.add(params.getImageUrl());
    proc.add(params.getImageCenter());

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    // Catch empty result set for now
    try {
      proc.execute();
    } 
    catch(Exception exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Extract the proc params and image stream from the 
   * post request payload that's sent when a user creates/edit
   * an organization
   */
  @Nullable
  private OrganizationsParams parseOrganizationFormPayload(SessionManager sessionManager) throws SQLException {
    OrganizationsParams params = null; 
    String imageBase64 = "";
    
    DiskFileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);

    try {
        List<FileItem> items = upload.parseRequest(request);

        for (FileItem item : items) {
            if (!item.isFormField()) {
                imageBase64 = UploadHandler.parseBase64(sessionManager, item, "OrganizationImage");
            }
            else if (item.getFieldName().equals("params")) {
                params = gson.fromJson(item.getString(), OrganizationsParams.class);
            }
        }
    } 
    catch (IOException | FileUploadException exception) {
        exception.printStackTrace();
    }

    if (params != null) {
        params.setImageUrl(imageBase64);
    }

    return params;
  }

  /**
   * Deletes the given organization
   * @param body A string containing organizationId param
   * @return the status of the deletion attempt
   */
  @Path("deleteOrganization")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String deleteOrganization(String body) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_Organization_Organization_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(userPidm);
    proc.add(params.getOrganizationId());
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * For viewing upcoming events list
   * @param orgId id of the organization to grab the admin list from
   * @return a list of admins associated with the organization
   */
  @Path("organizationAdmins")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUpcomingEvents(@QueryParam("orgId") int orgId) throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationAdmins_Proc");
    proc.add(orgId == 0 ? null : orgId); //org id; a real org id should never be 0

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
  /**
   * For viewing user info when adding a user before an organization exists
   * @param userId
   * @return user data for the id if exists
   * @throws SQLException
   */
  @Path("userInfo")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUserInfo(@QueryParam("userId") String userId) throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserProfileInfo_User_Proc");
    proc.add(userId);

    Bind<UserInfo> userInfoCallback = Bind.get(UserInfo.class);
    Bind<Status> statusCallback = Bind.get(Status.class);
    SessionManager.get(request)
                  .addDebugStatement(proc.getSQLStatement());

    proc.execute(userInfoCallback, statusCallback);

    Map<String, Object> userInfo = new HashMap<String, Object>();
    userInfo.put("results", userInfoCallback.getModel());
    userInfo.put("status", statusCallback.getModel().isStatus());
    userInfo.put("errorMsg", statusCallback.getModel().getErrorMsg());
    
    return gson.toJson(userInfo);
  }

  /**
   * Insert the current logged in user as an admin into the given organization differs from the other inserts as user could be either login type
   * @param body A string containing organizationId params
   * @return the result of the insertion
   */
  @Path("newSelfAdmin")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String newSelfAdmin(String body) throws SQLException {
    String googleLogin = (String) request.getSession().getAttribute("googleLogin");
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);
    
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, googleLogin != null ? "dbo.Insert_UserNonInstiutionalOrganizationSecurity_Proc" : "dbo.Insert_UserOrganizationSecurity_Proc");
    
    if (googleLogin != null) {
      proc.add(googleLogin);
      proc.add((String) request.getSession().getAttribute("firstName"));
      proc.add((String) request.getSession().getAttribute("lastName"));
      proc.add(params.getOrganizationId());
      proc.add(1);
    }
    else {
      proc.add(((User) request.getSession().getAttribute(User.SESSION_ATTR_NAME)).getUsername());
      proc.add(params.getOrganizationId());
      proc.add(1);
      
    }
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }
  
  /**
   * Insert a new admin into the given organization
   * @param body A string containing adminUser and organizationId params
   * @return the result of the insertion
   */
  @Path("newUHAdmin")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String newUHAdmin(String body) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_UserOrganizationSecurity_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(params.getAdminUser());
	  proc.add(params.getOrganizationId());
    proc.add(1);
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * Insert a new non uh admin into an organization
   * @param body A string containing adminUser, adminFirstName, adminLastName, and organizationId params
   * @return the result of the insertion
   */
  @Path("newNonUHAdmin")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String newNonUHAdmin(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_UserNonInstiutionalOrganizationSecurity_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(params.getAdminUser());
    proc.add(params.getAdminFirstName());
    proc.add(params.getAdminLastName());
	  proc.add(params.getOrganizationId());
    proc.add(1);
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * Remove a UH admin from an organization
   * @param body A string containing adminUser and organizationId params
   * @return the result of the deletion
   */
  @Path("removeUHAdmin")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String removeUHAdmin(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_UserOrganizationSecurity_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(params.getAdminUser()); // Id to be deleted
	  proc.add(params.getOrganizationId());
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * Remove a non-uh admin from an organization
   * @param body A string containing adminUser and organizationId params
   * @return the result of the deletion
   */
  @Path("removeNonUHAdmin")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String removeNonUHAdmin(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_UserNonInstitutionalOrganizationSecurity_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(params.getAdminUser()); // Id to be deleted
	  proc.add(params.getOrganizationId());
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * Changes an organizations active state to active/inactive
   * @param body A string containing isInactive [boolean] and organizationId params
   * @return the result of the toggle attempt
   */
  @Path("toggleActiveState")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String toggleActiveState(String body) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_OrganizationInactive_Organization_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    proc.add(userPidm);
    proc.add(params.getOrganizationId());
    proc.add(params.getAuthorizationStatus()); // -1 Unprocessed, 0 approved, 1 rejected
    proc.add(params.getAuthorizationComment());
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
    
    return gson.toJson(proc.execute().getRows());
  }

  /**
   * Gets the number of unauthorized organizations
   * @return the number of unauthorized organizations
   * @throws SQLException
   */
  @Path("getNumOfUnauthorizedOrganizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getNumOfUnauthorizedOrganizations(String body) throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationAuthorizationCount_Proc");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
    
    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View authorized Organizations
   * @return A list of authorized Organizations
   * @throws SQLException
   */
  @Path("viewAuthorizedOrganizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewAuthorizedOrganizations() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationAuthorization_Proc");
    proc.add(0); // IsAuthorized       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View unauthorized Organizations
   * @return A list of unauthorized Organizations
   * @throws SQLException
   */
  @Path("viewUnauthorizedOrganizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUnauthorizedOrganizations() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationAuthorization_Proc");
    proc.add(-1); // IsUnauthorized       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View rejected Organizations
   * @return A list of rejected Organizations
   * @throws SQLException
   */
  @Path("viewRejectedOrganizations")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewRejectedOrganizations() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllOrganizationAuthorization_Proc");
    proc.add(1); // IsRejected       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
  /**
   * SHARED STUDENT + ADMIN METHODS
   */


  /**
   * For viewing org details
   */
  @Path("viewOrganizationDetails")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewOrganizationDetails(String body) throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[View_OrganizationDetails_Organization_Proc]");
    
    proc.add(params.getOrganizationId());
    proc.add(userPidm);

    sessionManager.addDebugStatement(proc.getSQLStatement());

    @SuppressWarnings("rawtypes")
    SortedMap[] resultSet = proc.execute().getRows();
    sessionManager.setBufferedImageURL((String) resultSet[0].get("OrganizationImageURL"));

    return gson.toJson(resultSet);
  }

  /**
   * For viewing past events
   */
  @Path("viewPastEvents")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewPastEvents(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[View_EventDiscoveryFeed_Events_Proc]");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);
    proc.add(userPidm); // UserID
    proc.add(1); // isPastView
    proc.add(0); // isRegisteredView
    proc.add(0); // isFilteredByFollowedOrg
    proc.add(params.getOrganizationId() == 0 ? null : params.getOrganizationId()); // OrganizationIDFilter
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * For viewing past events
   */
  @Path("viewUpcomingEvents")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUpcomingEvents(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "[dbo].[View_EventDiscoveryFeed_Events_Proc]");
    OrganizationsParams params = gson.fromJson(body, OrganizationsParams.class);
    proc.add(userPidm); // UserID
    proc.add(0); // isPastView
    proc.add(0); // isRegisteredView
    proc.add(0); // isFilteredByFollowedOrg
    proc.add(params.getOrganizationId() == 0 ? null : params.getOrganizationId()); // OrganizationIDFilter; Org Ids will never be 0
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }
}

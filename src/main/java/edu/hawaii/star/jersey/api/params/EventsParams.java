package edu.hawaii.star.jersey.api.params;

public class EventsParams {
    private int userId;
    private int eventId;
    private int organizationId;
    private String eventName;
    private String startDate;
    private String endDate;
    private String locationName;
    private String locationAddress1;
    private String locationAddress2;
    private String locationCity;
    private String locationState;
    private String locationZip;
    private Boolean allowWaitlist;
    private Boolean allowOutsideAttendees;
    private Boolean allowDiscussion;
    private Boolean allowRideshare;
    private Boolean isAttendeeLimit;
    private int attendeeLimit;
    private int attendanceTypeId;
    private int socialCohortTypeId;
    private int categoryId;
    private String eventAccess;
    private String imageUrl;
    private String description;
    private String contactPersonName;
    private String contactPersonEmail;
    private String contactPersonPhone;
    private String itemsToBring;
    private String foodProvided;
    private String searchKeyword;
    private float imageCenter;
    private String outsideAttendeesInstruction;
    private String virtualMeetingLink;
    private String clockInClockOutStamp;
    private String externalSignupLink;
    private boolean receiveEmails;
    private String eventType;
    private boolean imageChanged;

    public int getUserId() {
        return userId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getEventId() {
        return eventId;
    }

    public int getOrganizationId() {
        return organizationId;
    }

    public String getEventName() {
        return eventName;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getLocationAddress1() {
        return locationAddress1;
    }

    public String getLocationAddress2() {
        return locationAddress2;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public String getLocationState() {
        return locationState;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public Boolean getAllowWaitlist() {
        return allowWaitlist;
    }

    public Boolean getAllowOutsideAttendees() {
        return allowOutsideAttendees;
    }

    public Boolean getAllowDiscussion() {
        return allowDiscussion;
    }

    public Boolean getAllowRideshare() {
        return allowRideshare;
    }

    public Boolean getIsAttendeeLimit() {
        return isAttendeeLimit;
    }

    public int getAttendeeLimit() {
        return attendeeLimit;
    }

    public int getAttendanceTypeId() {
        return attendanceTypeId;
    }

    public int getSocialCohortTypeId() {
        return socialCohortTypeId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public String getEventAccess() {
        return eventAccess;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getContactPersonName() {
        return contactPersonName;
    }

    public String getContactPersonEmail() {
        return contactPersonEmail;
    }

    public String getContactPersonPhone() {
        return contactPersonPhone;
    }

    public String getItemsToBring() {
        return itemsToBring;
    }

    public String getFoodProvided() {
        return foodProvided;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public float getImageCenter() {
        return imageCenter;
    }

    public String getOutsideAttendeesInstruction() {
        return outsideAttendeesInstruction;
    }

    public String getVirtualMeetingLink() {
        return virtualMeetingLink;
    }

    public String getClockInClockOutStamp() {
        return clockInClockOutStamp;
    }

    public String getExternalSignupLink() {
        return externalSignupLink;
    }

    public boolean isReceiveEmails() {
        return receiveEmails;
    }

    public String getEventType() {
        return eventType;
    }
    
    public boolean getImageChanged() {
        return imageChanged;
    }
}

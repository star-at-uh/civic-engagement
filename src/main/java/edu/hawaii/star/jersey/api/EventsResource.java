package edu.hawaii.star.jersey.api;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;

import javax.annotation.Nullable;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.SessionManager;

import edu.hawaii.star.jersey.api.params.ClockTimeParams;
import edu.hawaii.star.jersey.api.params.EventAuthParams;
import edu.hawaii.star.jersey.api.params.EventsParams;
import edu.hawaii.star.jersey.api.params.UploadHandler;
@Path("events")
public class EventsResource {

  private static Gson gson = new GsonBuilder().create();

  @Context
  private HttpServletRequest request;

  @Context
  private ServletContext context;

  /**
   * STUDENT SIDE METHODS
   */

  /**
   * For viewing registerd events list
   */
  @Path("viewRegisteredEvents")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewRegisteredEvents(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc.add(userPidm); // @UserID                
    proc.add(0); // @isPastView = false
    proc.add(1); // @isRegisteredView = true

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }
  
  /**
   * For viewing explore events list
   */
  @Path("viewExploreEvents")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewExploreEvents(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc.add(userPidm); // @UserID         
    proc.add(0); // @isPastView = false
    proc.add(0); // @isRegisteredView = true       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For viewing registered view including past for clocking in
   */
  @Path("viewClockInEvents")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String viewClockInEvents(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc.add(userPidm); // @UserID         
    proc.add(0); // @isPastView = false
    proc.add(1); // @isRegisteredView = true
    proc.add(0); // @isFilteredByFollowedOrg BIT = 0, -- if 1, only show events from orgs user is following
    proc.add(null); // @OrganizationIDFilter INT = NULL, -- if not null, Show only events for a given org ID
    proc.add(1); // @IsCivicEngageInterface BIT = 1,
    proc.add(context.getInitParameter("applicationType")); // @ApplicationType varchar(20) = 'student' 

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap registeredEvents[] = proc.execute().getRows();

    NamedProc proc2 = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc2.add(userPidm); // @UserID         
    proc2.add(1); // @isPastView = false
    proc2.add(1); // @isRegisteredView = true       
    proc2.add(0); // @isFilteredByFollowedOrg BIT = 0, -- if 1, only show events from orgs user is following
    proc2.add(null); // @OrganizationIDFilter INT = NULL, -- if not null, Show only events for a given org ID
    proc2.add(1); // @IsCivicEngageInterface BIT = 1,
    proc2.add(context.getInitParameter("applicationType")); // @ApplicationType varchar(20) = 'student'

    SessionManager.get(request).addDebugStatement(proc2.getSQLStatement());

    SortedMap pastEvents[] = proc2.execute().getRows();
    
    SortedMap clockInEvents[] = Arrays.copyOf(registeredEvents, registeredEvents.length + pastEvents.length);
    System.arraycopy(pastEvents, 0, clockInEvents, registeredEvents.length, pastEvents.length);

    return gson.toJson(clockInEvents);
  }

  /**
   * View a user's liked events
   * @return A list of liked events
   * @throws SQLException
   */
  @Path("viewFavoriteEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewFavoriteEvents() throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserEventLike_Proc");
    proc.add(userPidm); // IsAuthorized       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For event registration
   */
  @Path("register")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String register(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_VolunteerUser_Event_Proc");
    
    proc.add(userPidm);
    proc.add(params.getEventId());
    proc.add(params.getDescription());

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  /**
   * For cancelling event registration
   */
  @Path("cancelRegistration")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String cancelRegistration(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Cancel_VolunteerRegistration_Event_Proc");

    proc.add(params.getEventId());
    proc.add(userPidm);

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  /**
   * For button clock in
   */
  @Path("clockButton")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String clockButton(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_ButtonClockInClockOut_Event_Proc");
    
    proc.add(userPidm);
    proc.add(params.getEventId());

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  
  /**
   * For manual clock in
   */
  @Path("clockManual")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String clockManual(String body) throws SQLException {
    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    String appType = context.getInitParameter("applicationType");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_ManualClockInClockOut_Event_Proc");
    proc.add(userPidm);
    proc.add(params.getEventId());
    proc.add(params.getClockInClockOutStamp());
    proc.add(appType.equals("student") ? null : userPidm);
    proc.add(appType);

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  /**
   * Get a list of the student's service hours, showing 
   * the clock-in and clock-out time of thier session
   */
  @Path("getClockedTimes")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getClockedTimes(@QueryParam("studentId") Long studentId, @QueryParam("eventId") int eventId) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    String appType = context.getInitParameter("applicationType");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_ManualClockInClockOut_Event_Proc");
    proc.add(appType.equals("student") ? userPidm : studentId);
    proc.add(eventId);
    proc.add(appType.equals("student") ? null : userPidm);
    proc.add(appType);

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * Remove a student's clock-in and clock-out time
   */
  @Path("discardClockedTimes")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String removeClockedTimes(String payload) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");
    String appType = context.getInitParameter("applicationType");
    ClockTimeParams params = gson.fromJson(payload, ClockTimeParams.class);

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_ManualClockInClockOut_Event_Proc");
    proc.add(params.getClockedTimeID());
    proc.add(appType.equals("student") ? userPidm : params.getStudentId());
    proc.add(params.getEventID());
    proc.add(appType.equals("student") ? null : userPidm);
    proc.add(appType);

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For viewing upcoming events list
   */
  @Path("viewUpcomingEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUpcomingEvents(@QueryParam("orgId") int orgId) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc.add(userPidm); // @UserID       
    proc.add(0); //is past
    proc.add(0); //registered view
    proc.add(0); //orgFilter
    proc.add(orgId == 0 ? null : orgId); //org id; a real org id should never be 0
    proc.add(1); // civic engage
    proc.add(context.getInitParameter("applicationType"));

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For viewing upcoming events list
   */
  @Path("viewPastEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewPastEvents(@QueryParam("orgId") int orgId) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDiscoveryFeed_Events_Proc");
    
    proc.add(userPidm); // @UserID    
    proc.add(1); //is past
    proc.add(0); //registered view
    proc.add(0); //orgFilter
    proc.add(orgId == 0 ? null : orgId); //org id; a real org id should never be 0
    proc.add(1); // civic engage
    proc.add(context.getInitParameter("applicationType")); // App Type

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View authorized events
   * @return A list of authorized or unauthorized events
   * @throws SQLException
   */
  @Path("viewAuthorizedEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewAuthorizedEvents() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllEventAuthorization_Proc");
    proc.add(1); // IsAuthorized       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View unauthorized events
   * @return A list of unauthorized events
   * @throws SQLException
   */
  @Path("viewUnauthorizedEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewUnauthorizedEvents() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllEventAuthorization_Proc");
    proc.add(-1); // IsUnauthorized       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * View rejected events
   * @return A list of rejected events
   * @throws SQLException
   */
  @Path("viewRejectedEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewRejectedEvents() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllEventAuthorization_Proc");
    proc.add(0); // IsRejected       

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * Get the number of unauthorized events
   * @return the number of unauthorized events
   * @throws SQLException
   */
  @Path("viewNumUnauthorizedEvents")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String viewNumUnauthorizedEvents() throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_AllEventAuthorizationCount_Proc");

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());
  }

  /**
   * For authorizing an event
   */
  @Path("authorizeEvent")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String authorizeEvent(String body) throws SQLException {
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_EventIsAutherizedIndicator_proc");
    EventAuthParams params = gson.fromJson(body, EventAuthParams.class);
    proc.add(userPidm); // CreatingUserID                
    proc.add(params.getEventList()); // JSON of event ids, event auth status, and auth comment                 

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }  

  /**
   * For creating an event
   */
  @Path("createEvent")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String createEvent() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    EventsParams params = this.parseEventFormPayload(sessionManager);

    if (params != null) {
        Long userPidm = (Long) request.getSession().getAttribute("userPidm");
        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Create_Event_Event_Proc");
    
        proc.add(userPidm); // @CreatingUserID                
        proc.add(params.getOrganizationId()); // @GroupID                    
        proc.add(params.getEventName()); // @EventName                    
        proc.add(params.getStartDate()); // @EventStartDate                
        proc.add(params.getEndDate()); // @EventEndDate                
        proc.add(params.getLocationName()); // @EventLocationName            
        proc.add(params.getLocationAddress1()); // **@EventLocationAddressL1**    
        proc.add(params.getLocationAddress2()); // **@EventLocationAddressL2**        
        proc.add(params.getLocationCity()); // @EventLocationCity            
        proc.add(params.getLocationState()); // @EventLocationState            
        proc.add(params.getLocationZip()); // @EventLocationZip            
        proc.add(params.getAllowWaitlist()); // @EventAllowWaitlist            
        proc.add(params.getAllowOutsideAttendees()); // @EventAllowOutsideAttendees 
        proc.add(params.getAllowDiscussion()); // @EventAllowDiscussion        
        proc.add(params.getAllowRideshare()); // @EventAllowRideshare        
        proc.add(params.getAttendeeLimit() > 0 ? 1 : 0); // @EventIsAttendeeLimit        
        proc.add(params.getAttendeeLimit()); // @EventAttendeeLimit      
        proc.add(params.getAttendanceTypeId()); //@EventAttendanceTypeID
        proc.add(params.getSocialCohortTypeId());  // @EventSocialCohortTypeID		INT,
        proc.add(params.getCategoryId());  // @EventCategoryID				INT = NULL,
        proc.add(params.getEventAccess()); // @EventAccess  
        proc.add(params.getImageUrl()); // @EventImageURL
        proc.add(params.getDescription()); // @EventDescription
        proc.add(params.getContactPersonName());  // @EventContactPersonName			VARCHAR(200),
        proc.add(params.getContactPersonEmail());  // @EventContactPersonEmailAddress VARCHAR(100),
        proc.add(params.getContactPersonPhone());  // @EventContactPersonPhoneNumber	VARCHAR(50) = NULL,
        proc.add(params.isReceiveEmails()); // @EventContactReceiveCopyOfAtendeeEmailNotifications	BIT = 0,
        proc.add(params.getItemsToBring());  // @EventItemsToBring				VARCHAR(1000) = NULL,
        proc.add(params.getFoodProvided());  // @EventFoodProvided				VARCHAR(1000) = NULL
        proc.add(params.getOutsideAttendeesInstruction()); // @EventOutsideAttendeesInstructionMessage VARCHAR(1000) = NULL,
        proc.add(params.getVirtualMeetingLink()); // @EventVirtualMeetingLink        VARCHAR(200) = NULL
        proc.add(params.getExternalSignupLink()); // VARCHAR(MAX) = NULL 
        proc.add(params.getEventType()); // INT
    
        SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
    
        @SuppressWarnings("rawtypes")
        SortedMap[] result = proc.execute().getRows();
        @SuppressWarnings("rawtypes")
        SortedMap row = result[0];
    
        // if success, add image
        if(row.size() > 2){
            params.setEventId((int) row.get("EventID"));
            this.saveEventImage(userPidm, params);
        }
          
        return gson.toJson(result);
    }
    else {
        return "";
    }
  }

  /**
   * For editing an event's details
   */
  @POST
  @Path("editEvent")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public String editEvent() throws SQLException {
    SessionManager sessionManager = SessionManager.get(request);
    EventsParams params = this.parseEventFormPayload(sessionManager);

    if (params != null) {
        if (!params.getImageChanged()) {
            params.setImageUrl(sessionManager.getBufferedImageURL());
        }

        Long userPidm = (Long) request.getSession().getAttribute("userPidm");
        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_EventDetails_Event_Proc");
    
        proc.add(params.getEventId());// @EventID						INT,
        proc.add(userPidm);// @UpdatingUserID					INT,
        proc.add(params.getOrganizationId());// @OrganizationID					INT,
        proc.add(params.getEventName());// @EventName						VARCHAR(100),
        proc.add(params.getStartDate()); // @EventStartDate                
        proc.add(params.getEndDate()); // @EventEndDate                
        proc.add(params.getLocationName()); // @EventLocationName            
        proc.add(params.getLocationAddress1()); // **@EventLocationAddressL1**    
        proc.add(params.getLocationAddress2()); // **@EventLocationAddressL2**        
        proc.add(params.getLocationCity()); // @EventLocationCity            
        proc.add(params.getLocationState()); // @EventLocationState            
        proc.add(params.getLocationZip()); // @EventLocationZip            
        proc.add(params.getAllowWaitlist()); // @EventAllowWaitlist            
        proc.add(params.getAllowOutsideAttendees()); // @EventAllowOutsideAttendees 
        proc.add(params.getAllowDiscussion()); // @EventAllowDiscussion        
        proc.add(params.getAllowRideshare()); // @EventAllowRideshare        
        proc.add(params.getAttendeeLimit() > 0 ? 1 : 0); // @EventIsAttendeeLimit        
        proc.add(params.getAttendeeLimit()); // @EventAttendeeLimit      
        proc.add(params.getAttendanceTypeId()); //@EventAttendanceTypeID
        proc.add(params.getSocialCohortTypeId());  // @EventSocialCohortTypeID		INT,
        proc.add(params.getCategoryId());  // @EventCategoryID				INT = NULL,
        proc.add(params.getEventAccess()); // @EventAccess
        proc.add(params.getImageUrl()); // @EventImageURL
        proc.add(params.getDescription()); // @EventDescription
        proc.add(params.getContactPersonName());  // @EventContactPersonName			VARCHAR(200),
        proc.add(params.getContactPersonEmail());  // @EventContactPersonEmailAddress VARCHAR(100),
        proc.add(params.getContactPersonPhone());  // @EventContactPersonPhoneNumber	VARCHAR(50) = NULL,
        proc.add(params.isReceiveEmails()); // @EventContactReceiveCopyOfAtendeeEmailNotifications	BIT = 0,
        proc.add(params.getItemsToBring());  // @EventItemsToBring				VARCHAR(1000) = NULL,
        proc.add(params.getFoodProvided());  // @EventFoodProvided				VARCHAR(1000) = NULL
        proc.add(params.getOutsideAttendeesInstruction()); // @EventOutsideAttendeesInstructionMessage VARCHAR(1000) = NULL,
        proc.add(params.getVirtualMeetingLink()); // @EventVirtualMeetingLink        VARCHAR(200) = NULL
        proc.add(params.getExternalSignupLink()); // VARCHAR(MAX) = NULL 
        proc.add(params.getEventType()); // INT
    
        SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
    
        @SuppressWarnings("rawtypes")
        SortedMap[] result = proc.execute().getRows();
    
        if(params.getImageUrl() != null) {
          this.saveEventImage(userPidm, params);
        }
    
        return gson.toJson(result);
    }
    else {
        return "";
    }
  }

  /**
   * Save the organization image into the SQL server
   */
  private void saveEventImage(Long userPidm, EventsParams params) {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_EventImage_Event_Proc");
    proc.add(userPidm);
    proc.add(params.getEventId());
    proc.add(params.getImageUrl());
    proc.add(params.getImageCenter());

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    // Catch empty result set for now
    try {
      proc.execute();
    } 
    catch(Exception exception) {
      exception.printStackTrace();
    }
  }

  /**
   * Extract the proc params and image stream from the 
   * post request payload that's sent when a user creates/edit
   * an event
   */
  @Nullable
  private EventsParams parseEventFormPayload(SessionManager sessionManager) throws SQLException {
    EventsParams params = null; 
    String imageBase64 = "";
    
    DiskFileItemFactory factory = new DiskFileItemFactory();
    ServletFileUpload upload = new ServletFileUpload(factory);

    try {
        List<FileItem> items = upload.parseRequest(request);

        for (FileItem item : items) {
            if (!item.isFormField()) {
                imageBase64 = UploadHandler.parseBase64(sessionManager, item, "EventImage");
            }
            else if (item.getFieldName().equals("params")) {
                params = gson.fromJson(item.getString(), EventsParams.class);
            }
        }
    } 
    catch (IOException | FileUploadException exception) {
        exception.printStackTrace();
    }

    if (params != null) {
        params.setImageUrl(imageBase64);
    }

    return params;
  }

  /**
   * Attempts to call a proc to duplicate the given event with the give datetime range
   */
  @Path("duplicateEvent")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String duplicateEvent(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Create_Event_Duplicate_Proc");
    EventsParams params = gson.fromJson(body, EventsParams.class);
    proc.add(userPidm); // @CreatingUserID                
    proc.add(params.getEventId()); // @GroupID                    
    proc.add(params.getStartDate()); // @EventStartDate                
    proc.add(params.getEndDate()); // @EventEndDate                

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * Attempts to delete a specific event based on a given event id
   * @param body a string that contains a EventId param
   * @return an object containing the status of the deletion attempt
   * @throws SQLException
   */
  @Path("deleteEvent")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String deleteEvent(String body) throws SQLException {

    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_Event_Event_Proc");
    EventsParams params = gson.fromJson(body, EventsParams.class);
    proc.add(params.getEventId()); // @GroupID                    
    proc.add(userPidm); // @CreatingUserID                

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    SortedMap[] result = proc.execute().getRows();

    return gson.toJson(result);
  }

  /**
   * SHARED METHODS
   */

  /**
   * For getting event details
   */
  @Path("eventDetails")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String eventDetails(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    SessionManager sessionManager = SessionManager.get(request);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventDetails_Event_Proc");
    
    proc.add(params.getEventId());
    if (userPidm != null) {
      proc.add(context.getInitParameter("applicationType").equals("advisor") && params.getUserId() != 0 ? params.getUserId() : userPidm);
    }
    else {
      proc.add(null);
    }
    proc.add(context.getInitParameter("applicationType"));

    sessionManager.addDebugStatement(proc.getSQLStatement());

    @SuppressWarnings("rawtypes")
    SortedMap[] resultSet = proc.execute().getRows();
    sessionManager.setBufferedImageURL((String) resultSet[0].get("EventImageURL"));

    return gson.toJson(resultSet);
  }
  
  /**
   * Grabs possible event types
   */
  @Path("getEventTypes")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getEventTypes(String body) throws SQLException {
    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.ComboBox_EventType_Event_Proc");
    
    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  /**
   * For getting event attendees
   */
  @Path("eventAttendees")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String eventAttendees(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventVolunteers_Event_Proc");
    
    proc.add(params.getEventId());

    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    HashMap jsonResponse = new HashMap();
    SortedMap[] members = proc.execute().getRows();
    int membersCount = 0;

    // Attendees who still need to be approved are not counted
    for (int i = 0; i < members.length; i++) {
      if ((boolean) members[i].get("EventIsSelectiveAdmissions") == true) {
        if ((boolean) members[i].get("IsAcceptedForSelectAdmissions") == true) {
          membersCount++;
        }
      }
      else {
        membersCount++;
      }
    }

    jsonResponse.put("count", membersCount);

    if(context.getInitParameter("applicationType").equalsIgnoreCase("advisor")){
      jsonResponse.put("members", members);
    }

    return gson.toJson(jsonResponse);

  }


  /**
   * For searching events
   */
  @Path("search")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String search(String body) throws SQLException {
    SessionManager session = SessionManager.get(request);
    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventSearch_Event_Proc");
    proc.add(userPidm);
    proc.add(params.getSearchKeyword());
    proc.add(context.getInitParameter("applicationType"));

    session.addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }


  /**
   * For liking events (Insert_UserEventLike_Proc)
   */
  @Path("like")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String like(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_UserEventLike_Proc");
    
    proc.add(userPidm);
    proc.add(params.getEventId());


    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }

  /**
  * for disliking events (Delete_UserEventLike_Proc)
  */
  @Path("dislike")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String dislike(String body) throws SQLException {

    EventsParams params = gson.fromJson(body, EventsParams.class);
    Long userPidm = (Long) request.getSession().getAttribute("userPidm");

    NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_UserEventLike_Proc");
    
    proc.add(userPidm);
    proc.add(params.getEventId());


    SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

    return gson.toJson(proc.execute().getRows());

  }
}

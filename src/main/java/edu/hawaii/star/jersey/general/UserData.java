package edu.hawaii.star.jersey.general;

import java.sql.SQLException;

import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.dao.sqlserver.Bind;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.model.SQLModel;

/**
 * Holds the row returned by dbo.SummerApp_GetStudentCampusAndPidm_Proc
 */
public class UserData extends SQLModel {
    private static final long serialVersionUID = -8876671987101818838L;

    /**
     * Recieves info needed for the user that was not given by CAS
     */
    public static UserData getAdditionalUserData(HttpServletRequest request, String username) throws SQLException {
        NamedProc proc = NamedProc.get(DB.STUDENT_SUPPORT_HUB, "dbo.View_UsernameVerification_Users_Proc");
        proc.add(username);

        Bind<UserData> result = Bind.get(UserData.class);
        SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
        proc.execute(result);

        return result.getModel();
    }

    /**
     * Use Long wrapper to allow for nulls
     */
    @Column(name = "UserID")
    private Long pidm;

    /**
     * @param pidm the pidm to set
     */
    public void setPidm(Long pidm) {
        this.pidm = pidm;
    }

    /**
     * @return the pidm
     */
    public Long getPidm() {
        return pidm;
    }
}
package edu.hawaii.star.jersey.general;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.User;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.model.SQLModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.SortedMap;

/**
 * Handles logging api errors
 */
@Path("error")
public class ErrorLogResource extends SQLModel {
    private static final long serialVersionUID = -5548883652298950961L;

    private Gson gson = new GsonBuilder().create();

    @Context
    HttpServletRequest request;

    @Context
    ServletContext context;

    /**
     * params class 
     */
    private class ErrorParams {

        private String jsonResponse;
        private String locationUrl;
        
        public String getJsonResponse(){
            return jsonResponse;
        }
        
        public String getLocationUrl(){
            return locationUrl;
        }
    }

    /**
     * sends front-end ajax errors to db
     */
    @POST
    @Path("log")
    @Produces(MediaType.APPLICATION_JSON)
    public String addUserSessionInfo(String body) throws SQLException {
    
        ErrorParams params = gson.fromJson(body, ErrorParams.class);

        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.CaptureHttpError");
        proc.add(context.getInitParameter("applicationType")); // appType (student or admin)
        proc.add(request.getSession().getAttribute("userPidm")); // current logged in user id
        proc.add(params.getLocationUrl()); // window.location.href of users browser
        proc.add(params.getJsonResponse()); // error message json

        SessionManager.get(request).addDebugStatement(proc.getSQLStatement());
        
        return gson.toJson(proc.execute().getRows());        
    }
}
package edu.hawaii.star.jersey.general;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.User;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;
import edu.hawaii.star.model.SQLModel;
import edu.hawaii.star.cipher.Decryptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.Arrays;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

/**
 * Handles communication to the sql server
 */
@Path("general")
public class GeneralResource extends SQLModel {
    private static final long serialVersionUID = -5548883652298950961L;

    private Gson gson = new GsonBuilder().create();

    @Context
    HttpServletRequest request;

    @Context
    ServletContext context;

    private static final String CLIENT_ID = "21354085062-gtt396qpjpen67orkrh6ajaicn7ssp78.apps.googleusercontent.com";
    private static final List<String> SCOPES = Arrays.asList("https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile");

    /**
     * Request a url from Google that will allow a user to authenticate through Google's login service
     * @param body (unused param)
     * @return A url that leads the an authorization page for the user to login through
     * @throws IOException
     */
    @GET
    @Path("new/login")
    @Produces(MediaType.APPLICATION_JSON)
    public String newAuth(String body) throws IOException {
         String uri = String.format("%s://%s", request.getScheme(), request.getServerName());
         int port = request.getServerPort();
         if (port != 443 && port != 80) {
             uri += ":" + port;
         }

         switch (context.getInitParameter("version")) {
            case "live":
                uri = uri + "/userve-admin";
                break;
            case "localhost":
                uri = uri + "/civic";
                break;
            default:
                uri = uri + "/userve";
        }
        
        uri = uri + "/api/general/redirect";

        String authorizationUrl = new GoogleAuthorizationCodeRequestUrl(CLIENT_ID, uri, SCOPES)
                                        .setAccessType("offline")
                                        .setApprovalPrompt("force")
                                        .build();

         return gson.toJson(authorizationUrl);
    }

    /**
     * Receive the response from Google about the user's login and grab the requested information via the token sent by Google
     * @param code A token that serves as the identifier for a user's login
     * @return A url to navigate the user back to after authenticating with Google
     * @throws IOException
     * @throws URISyntaxException
     * @throws SQLException
     */
    @GET
    @Path("redirect")
    @Produces(MediaType.APPLICATION_JSON)
    public Response parseAndRedirect(@QueryParam("code") String code) throws IOException, URISyntaxException, SQLException {
        SessionManager session = SessionManager.get(request);
        String uri = String.format("%s://%s", request.getScheme(), request.getServerName());
        int port = request.getServerPort();

        if (port != 443 && port != 80) {
            uri += ":" + port;
        }

        switch (context.getInitParameter("version")) {
            case "live":
                uri = uri + "/userve-admin";
                break;
            case "localhost":
                uri = uri + "/civic";
                break;
            default:
                uri = uri + "/userve";
        }

        String redirectURL = uri + "/ProcessorNonCAS.jsp";
        uri = uri + "/api/general/redirect";

        // [0] is the login secret
        String[] secrets = Decryptor.decryptMessage(context.getInitParameter("GLOGIN_CIPHER_PATH"), context.getInitParameter("GLOGIN_KEYSET_PATH")).split(",");

        // Request accessToken, refreshToken, and idToken from Google
        GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(new NetHttpTransport(),
                                                    JacksonFactory.getDefaultInstance(), "https://www.googleapis.com/oauth2/v4/token", CLIENT_ID,
                                                    secrets[0], code, uri).execute();

        request.getSession().setAttribute("googleLogin", tokenResponse.parseIdToken().getPayload().getEmail());
        request.getSession().setAttribute("lastName", tokenResponse.parseIdToken().getPayload().get("family_name"));
        request.getSession().setAttribute("firstName", tokenResponse.parseIdToken().getPayload().get("given_name"));

        User nonUHUser = new User();
        nonUHUser.setUsername(tokenResponse.parseIdToken().getPayload().getEmail());

        request.getSession().setAttribute("user", nonUHUser);

        URI targetURIForRedirection = new URI(redirectURL);
        return Response.temporaryRedirect(targetURIForRedirection).build();
    }

    /**
     * Generate error message about expired session to return to the caller
     */
    private String noUser() {
        HashMap<String, Integer> message = new HashMap<>();

        message.put("error", 401);

        return gson.toJson(message);
    }

    /**
     * add User Info to session such as Pidm and Campus
     */
    @GET
    @Path("addUserSessionInfo")
    @Produces(MediaType.APPLICATION_JSON)
    public String addUserSessionInfo() throws SQLException {
        HashMap<String, Object> jsonResponse = new HashMap<String, Object>();
        SortedMap[] results = null;
        String googleLogin = (String) request.getSession().getAttribute("googleLogin");
        User user = (User) request.getSession().getAttribute(User.SESSION_ATTR_NAME);
        if (user == null && googleLogin == null) {
            if (context.getInitParameter("applicationType").equals("student")) {
                jsonResponse.put("valid", true);
                jsonResponse.put("isLogged", false);
                return gson.toJson(jsonResponse);
            }
            else {
                return noUser();
            }
        }
        else if (googleLogin != null) {
            NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserNonInsitutionalSecurity_Proc");
            proc.add(googleLogin); // @email
            
            SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

            results = proc.execute().getRows();
            
            if (results[0].get("UserID") != null) {
                // userPidm is a Long for UH users, but UserID comes back as an int which cannot directly be cast to a Long
                request.getSession().setAttribute("userPidm", ((Integer) results[0].get("UserID")).longValue());
                jsonResponse.put("pidm", ((Integer) results[0].get("UserID")).longValue());
            }
            else {
                // This is a fresh user coming in. They won't have a user id so we make them one.
                NamedProc proc2 = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_UserNonInstiutional_SubProc");
                proc2.add(googleLogin);
                proc2.add((String) request.getSession().getAttribute("firstName"));
                proc2.add((String) request.getSession().getAttribute("lastName"));

                SessionManager.get(request).addDebugStatement(proc2.getSQLStatement());

                long userId = ((Integer) proc2.execute().getRows()[0].get("UserID")).longValue();

                request.getSession().setAttribute("userPidm", userId);
                jsonResponse.put("pidm", userId);
            }
            user.setAuthorized(true);
            request.getSession().setAttribute("user", user);

            jsonResponse.put("valid", true);
            jsonResponse.put("username", googleLogin);
            jsonResponse.put("applicationType", context.getInitParameter("applicationType"));
        }
        else {
            Long pidm = (Long) request.getSession().getAttribute("userPidm");
            if (pidm == null) {
                UserData data = UserData.getAdditionalUserData(request, user.getUsername());

                if (data == null) { // if for some reason their user data doesn't exist in the SSH User table, check non institutional people for Userve
                    NamedProc nonInstProc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserNonInsitutionalSecurity_Proc");
                    nonInstProc.add(user.getEmail()); // @email
                    
                    SessionManager.get(request).addDebugStatement(nonInstProc.getSQLStatement());
        
                    results = nonInstProc.execute().getRows();

                    if (results[0].get("UserID") != null) {
                        pidm = ((Integer) results[0].get("UserID")).longValue();
                        request.getSession().setAttribute("userPidm", pidm);
                    }
                }
                else if (data.getPidm() != 0) {
                    pidm = data.getPidm();
                    request.getSession().setAttribute("userPidm", pidm);
                }
                // if pidm is still null, we failed to find the user in cas and the database
                if (pidm == null) {
                    jsonResponse.put("valid", false);
                    return gson.toJson(jsonResponse);
                }
            }

            // audit login
            NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_UserSecurity_Proc");  
            proc.add(pidm); // @Logincampus VARCHAR(20) = 'unknown'
            
            SessionManager.get(request).addDebugStatement(proc.getSQLStatement());

            results = proc.execute().getRows();

            jsonResponse.put("valid", true);
            jsonResponse.put("pidm", pidm);
            jsonResponse.put("username", user.getUsername());
            jsonResponse.put("fullname", user.getFullName());
            jsonResponse.put("displayISATWarning", user.isISATWarning());
            jsonResponse.put("ISATDaysLeft", user.getISATDaysLeft());
        }

        // Log user logins
        if (request.getSession().getAttribute("loginWasLogged") == null) {
            NamedProc loginAuditProc= NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_SecurityLogonAudit_Proc");
            if (googleLogin != null) {
                loginAuditProc.add((String) request.getSession().getAttribute("firstName"));
                loginAuditProc.add((String) request.getSession().getAttribute("lastName"));
            }
            else {
                loginAuditProc.add(user.getFirstName());
                loginAuditProc.add(user.getLastName());
            }
            loginAuditProc.add(jsonResponse.get("username"));
            loginAuditProc.add(googleLogin != null ? "non-uh" : user.getAffiliation());
            loginAuditProc.add(request.getRemoteAddr());
            loginAuditProc.add(request.getHeader("User-Agent"));
            loginAuditProc.add(context.getInitParameter("applicationType"));
            loginAuditProc.add(null);
            loginAuditProc.add(googleLogin != null ? "N/A" : user.getCampus());
            loginAuditProc.execute();
        }
        
        jsonResponse.put("siteAdmin", results[0].get("SiteAdmin"));
        jsonResponse.put("organizationAdmin", results[0].get("OrganizationAdmin"));
        jsonResponse.put("applicationType", context.getInitParameter("applicationType"));
        jsonResponse.put("isLogged", true);

        request.getSession().setAttribute("siteAdmin", results[0].get("SiteAdmin"));
        request.getSession().setAttribute("organizationAdmin", results[0].get("OrganizationAdmin"));

        return gson.toJson(jsonResponse);
    }

    /**
     * Returns the user's email
     */
    @GET
    @Path("email")
    @Produces(MediaType.APPLICATION_JSON)
    public String getUserEmail() throws SQLException {
        String googleLogin = (String) request.getSession().getAttribute("googleLogin");
        User user = (User) request.getSession().getAttribute(User.SESSION_ATTR_NAME);
        HashMap<String, Object> jsonResponse = new HashMap<String, Object>();
        
        if (googleLogin == null) {
            jsonResponse.put("email", user.getEmail());
        }
        else {
            jsonResponse.put("email", googleLogin);
        }
        
        return gson.toJson(jsonResponse);
    }

    /**
     * Logs the user out
     */
    @GET
    @Path("logout")
    @Produces(MediaType.APPLICATION_JSON)
    public void logout() throws SQLException {
        request.getSession().invalidate();
    }

    /**
     * @return the current server time
     */
    @GET
    @Path("current/time")
    @Produces(MediaType.APPLICATION_JSON)
    public String getServerTime() {
        return gson.toJson(LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMM dd, uuuu hh:mm:ss a")));
    }

    /**
     * Holds the params passed in a http post request
     */
    public static class Params {
        public String comment;
        public String semester;
        public String classCrn;
        public String classSemester;
        public String classCampus;
        public String selection0;
        public String selection1;
        public String selection2;
        public String selection3;
        public String selection4;
        // repeasent a 0/1 boolean, this is done so the debug page will show the correct value for the proc call
        public int isHighSchoolGrad; 
    }

}
package edu.hawaii.star;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages the logs of sql calls made by this app
 */
public class DebugManager {

    private Map<String, List<DebugStatement>> statements;
    private int maxSize;

    /**
     * Start up the manager
     */
    public DebugManager(int maxSize) {
        this.statements = new HashMap<String, List<DebugStatement>>();
        this.maxSize = maxSize;
    }

    /**
     * Create a log of the proc call
     * 
     * @param key
     * @param statement
     */
    public void add(String key, String statement) {
        List<DebugStatement> list = statements.get(key);
        if (list == null) {
            list = new ArrayList<DebugStatement>();
            statements.put(key, list);
        }

        if (list.size() >= maxSize) {
            list.remove(list.size() - 2);
        }
        list.add(0, new DebugStatement(statement));
    }

    /**
     * Get the record of a proc call
     * 
     * @param key
     * @return
     */
    public List<DebugStatement> get(String key) {
        return statements.get(key);
    }

    /**
     * Helper class
     */
    class DebugStatement {

        private long time;
        private String statement;

        /**
         * Creates a record of a proc call
         */
        public DebugStatement(String statement) {
            this.time = System.currentTimeMillis();
            this.statement = statement;
        }

        /**
         * Return the time that the proc was called
         */
        public long getTime() {
            return time;
        }

        /**
         * Return the proc call used
         */
        public String getStatement() {
            return statement;
        }
    }
}

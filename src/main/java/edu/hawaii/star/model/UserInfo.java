package edu.hawaii.star.model;
import javax.persistence.Column;

/**
 * Java class used a binding class to grab and store user information returned by the SQL
 * from calling a stored procedure
 */
public class UserInfo extends SQLModel {
    @Column(name="pidm")
    private int pidm;
    @Column(name="EmailAddress")
    private String emailAddress;
    @Column(name="LastName")
    private String lastName;
    @Column(name="FirstName")
    private String firstName;
    @Column(name="Username")
    private String username;
    @Column(name="BannerId")
    private String bannerId;

    /**
     * Returns the pidm
     * @return an integer repesenting a pidm
     */
    public int getPidm() {
        return pidm;
    }

    /**
     * Sets the pidm to the given value
     * @param pidm an integer
     */
    public void setPidm(int pidm) {
        this.pidm = pidm;
    }

    /**
     * Returns the email address
     * @return a string representing an email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the email address
     * @param emailAddress a string representing an email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Returns a last name
     * @return a string representing a last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets a last name
     * @param lastName a string representing a last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns a first name
     * @return a string representing a first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets a first name
     * @param firstName  a string representing a first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns a username
     * @return a string representing a username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets a username
     * @param username a string representing a username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns a bannerId
     * @return a string representing a bannerId
     */
    public String getBannerId() {
        return bannerId;
    }

    /**
     * Sets a bannerid
     * @param bannerId a string representing a bannerid
     */
    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }
}
package edu.hawaii.star.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Logs SQL calls made by this app
 */
public class DebugStatements<T> extends ArrayList<T> {

    private int maxCapacity;
    private List<Long> timeStamps;

    /**
     * SEtup storage for the logs
     * 
     * @param maxCapacity
     */
    public DebugStatements(int maxCapacity) {
        this.maxCapacity = maxCapacity;
        timeStamps = new ArrayList<Long>();
    }

    /**
     * Record a proc call
     */
    @Override
    public boolean add(T t) {
        synchronized (this) {
            if (this.size() >= maxCapacity && this.size() > 0) {
                this.remove(0);
            }
            if (timeStamps.size() >= maxCapacity && timeStamps.size() > 0) {
                timeStamps.remove(0);
            }

            timeStamps.add(System.currentTimeMillis());
            return super.add(t);
        }
    }

    /**
     * Gets the record of a proc call
     * 
     * @param index
     * @return
     */
    public Date getTime(int index) {
        if (timeStamps == null || timeStamps.size() <= index) {
            return null;
        }
        Long time = timeStamps.get(index);
        return new Date(time);
    }
}

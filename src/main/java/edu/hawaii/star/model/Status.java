package edu.hawaii.star.model;

import javax.persistence.Column;

/**
 * Java class used as the binding class to grab the status and error message from SQL after
 * calling a stored procedure
 */
public class Status extends SQLModel {
    @Column(name="Status")
    private boolean status;
    @Column(name="ErrorMsg")
    private String errorMsg;

    /**
     * Returns the status
     * @return a boolean indicating the completion status of the call
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * Sets the status
     * @param status a boolean indicating what the status should be set to
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Returns the errorMsg 
     * @return
     */
    public String getErrorMsg() {
        return errorMsg;
    }

    /**
     * Sets the error msg
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
package edu.hawaii.star.model;

import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.SortedMap;

import javax.persistence.Column;

import org.apache.commons.codec.binary.Base64;

import edu.hawaii.star.SessionManager;
import edu.hawaii.star.dao.sqlserver.Bind;
import edu.hawaii.star.dao.sqlserver.DB;
import edu.hawaii.star.dao.sqlserver.NamedProc;

public class PDFTimesheet {
    /**
     * Get the timesheet's information
     */
    public static List<ViewPDF> getPDF(SessionManager manager, Long userPidm, String eventId, String appType) throws SQLException {
        Bind<PDFTimesheet.ViewPDF> bind = Bind.get(PDFTimesheet.ViewPDF.class);
        Long studentLookupId;
        String eventLookupId;

        if (appType.equals("student")) {
            studentLookupId = userPidm;
            eventLookupId = null;
        }
        else {
            studentLookupId = null;
            eventLookupId = eventId;
        }

        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.View_EventUploadedTimeSheet_proc");
        proc.add(studentLookupId);
        proc.add(eventLookupId);
        proc.add(appType);

        manager.addDebugStatement(proc.getSQLStatement());
        proc.execute(bind);

        return bind.getModels();
    }

    /**
     * Upload the timesheet PDF base64 String
     */
    @SuppressWarnings("rawtypes")
    public static SortedMap uploadTimesheet(SessionManager manager, byte[] base64PDF, String eventId, Long userPidm, String fileName) throws SQLException {
        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Insert_EventUploadedTimeSheet_proc");
        proc.add(userPidm);
        proc.add(base64PDF);
        proc.add(userPidm);
        proc.add(eventId);
        proc.add("student");
        proc.add(fileName);

        manager.addDebugStatement(proc.getSQLStatement());

        return proc.execute().getRows()[0];
    }

    /**
     * Fetches the PDF string containing the contents of the file associated with the timesheet key given
     * @param manager the session object of the current user
     * @param userPidm the id of the user accessing the data
     * @param timeSheetKey the id of the timesheet
     * @param appType what application type is the current context (ie student or advisor)
     * @return an object containing the pdf's data string
     * @throws SQLException
     */
    public static List<ViewPDF> getPDFData(SessionManager manager, Long userPidm, int timeSheetKey, String appType) throws SQLException {
        Bind<PDFTimesheet.ViewPDF> bind = Bind.get(PDFTimesheet.ViewPDF.class);

        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Download_EventTimeSheetPDFFile_Proc");
        proc.add(userPidm);
        proc.add(timeSheetKey);
        proc.add(appType);

        manager.addDebugStatement(proc.getSQLStatement());
        proc.execute(bind);

        return bind.getModels();
    }

    /**
     * Attempts to delete the PDF data associated with a timesheet entry
     * @param manager the session object of the current user
     * @param userPidm the id of the user attempting the deletion
     * @param timeSheetKey the id of the timesheet
     * @return an object containing the status of the update
     * @throws SQLException
     */
    public static SortedMap[] deletePDFData(SessionManager manager, Long userPidm, String timeSheetKey) throws SQLException {
        Bind<PDFTimesheet.ViewPDF> bind = Bind.get(PDFTimesheet.ViewPDF.class);

        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Delete_EventUploadedTimeSheet_Proc");
        proc.add(timeSheetKey);
        proc.add(userPidm);

        manager.addDebugStatement(proc.getSQLStatement());

        return proc.execute().getRows();
    }

    /**
     * Updates the comment associated with a specific uploaded PDF
     * @param manager the session object of the current user
     * @param userPidm the id of the user accessing the data
     * @param timeSheetKey the id of the timesheet
     * @param timeSheetComment the comment to be attached to the PDF
     * @return an object containing the status of the update
     * @throws SQLException
     */
    public static SortedMap[] updateTimesheetComment(SessionManager manager, Long userPidm, String timeSheetKey, String timeSheetComment) throws SQLException {
        Bind<PDFTimesheet.ViewPDF> bind = Bind.get(PDFTimesheet.ViewPDF.class);

        NamedProc proc = NamedProc.get(DB.CIVIC_ENGAGE, "dbo.Update_TimeSheetComment_Proc");
        proc.add(timeSheetKey);
        proc.add(timeSheetComment);
        proc.add(userPidm);

        manager.addDebugStatement(proc.getSQLStatement());

        return proc.execute().getRows();
    }

    /**
     * Params to get the base 64 encoded pdf
     */
    public static class ViewParams {
        public String key;
    }

    /**
     * Params for the post request used to update a PDF's comment
     */
    public static class CommentParams {
        public String timeSheetKey;
        public String timeSheetComment;
    }

    /**
     * Model used to bind the rows from the view proc
     */
    public static class ViewPDF extends SQLModel {
        @Column(name = "TimeSheetPDF")
        private transient byte[] pdfBytes;

        @Column(name = "OrganizationName")
        private String organization;
        
        @Column(name = "EventName")
        private String eventName;
        
        @Column(name = "TimeSheetUploadDate")
        private String uploadDate;

        @Column(name = "TimeSheetPDFName")
        private String fileName;

        @Column(name = "StudentID")
        private String studentId;

        @Column(name = "FirstName")
        private String firstName;

        @Column(name = "LastName")
        private String lastName;

        @Column(name = "EventID")
        private int eventId;

        @Column(name = "EventClockInClockOutUploadTimeSheetKey")
        private int timeSheetKey;

        @Column(name = "Status")
        private int status;

        @Column(name = "ErrorMsg")
        private String errorMsg;

        @Column(name = "TimeSheetComment")
        private String timeSheetComment;

        @Column(name = "TimeSheetDeleted")
        private boolean timeSheetDeleted;

        @SuppressWarnings("unused")
        private String pdfBase64;

        /**
         * Bind rows to annotated variable. During binding, create a base 64 string from the byte array of the ralated pdf column
         */
        @Override
        public void bindObject(ResultSet set) throws IllegalAccessException, SQLException, InvocationTargetException {
            super.bindObject(set);

            this.pdfBase64 = Base64.encodeBase64String(this.pdfBytes);
        }

        /**
         * Set the byte array repesenting a pdf
         * @param pdfBytes
         */
        public void setPdfBytes(byte[] pdfBytes) {
            this.pdfBytes = pdfBytes;
        }

        /**
         * Provide the byte array repesenting a pdf
         */
        public byte[] getPdfBytes() {
            return pdfBytes;
        }

        /**
         * @param eventName the eventName to set
         */
        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        /**
         * @param organization the organization to set
         */
        public void setOrganization(String organization) {
            this.organization = organization;
        }

        /**
         * @param uploadDate the uploadDate to set
         */
        public void setUploadDate(String uploadDate) {
            this.uploadDate = uploadDate;
        }
        
        /**
         * Assigns the received file name to the fileName variable
         * @param fileName the fileName to set
         */
        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        /**
         * @return the fileName
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * @return the studentId
         */
        public String getStudentId() {
            return studentId;
        }

        /**
         * Assigns the student id of the particular PDF to the studentId variable
         * @param studentId the studentId to set
         */
        public void setStudentId(String studentId) {
            this.studentId = studentId;
        }

        /**
         * @return the firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * Assigns the received first name to the firstName variable
         * @param firstName the firstName to set
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return the lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * Assigns the received last name to the lastName variable
         * @param lastName the lastName to set
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return the eventId
         */
        public int getEventId() {
            return eventId;
        }

        /**
         * @param eventId the eventId to set
         */
        public void setEventId(int eventId) {
            this.eventId = eventId;
        }

        /**
         * @return the timeSheetKey
         */
        public int getTimeSheetKey() {
            return timeSheetKey;
        }

        /**
         * @param timeSheetKey the timeSheetKey to set
         */
        public void setTimeSheetKey(int timeSheetKey) {
            this.timeSheetKey = timeSheetKey;
        }

        /**
         * @return the errorMsg
         */
        public String getErrorMsg() {
            return errorMsg;
        }

        /**
         * @param errorMsg the errorMsg to set
         */
        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        /**
         * @return the status
         */
        public int getStatus() {
            return status;
        }

        /**
         * @param status the status to set
         */
        public void setStatus(int status) {
            this.status = status;
        }

        /**
         * @return the timeSheetComment
         */
        public String getTimeSheetComment() {
            return timeSheetComment;
        }

        /**
         * @param timeSheetComment the timeSheetComment to set
         */
        public void setTimeSheetComment(String timeSheetComment) {
            this.timeSheetComment = timeSheetComment;
        }

        /**
         * @param timeSheetDeleted the timeSheetDeleted to set
         */
        public void setTimeSheetDeleted(boolean timeSheetDeleted) {
            this.timeSheetDeleted = timeSheetDeleted;
        }

        /**
         * @return the timeSheetDeleted
         */
        public boolean isTimeSheetDeleted() {
            return timeSheetDeleted;
        }
    }
}
